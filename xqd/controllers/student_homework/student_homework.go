package student_homework

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"xqd/models/student_homework"
)

//根据id获得象棋作业
func GetStudentHomeworkById(u *gin.Context) {
	homework_id := u.Query("homework_id")
	student_id := u.Query("student_id")
	ss := student_homework.GetStudentHomeworkById(homework_id,student_id)
	u.JSON(http.StatusOK, ss)
}

//象棋作业信息,每日两道作业
func GetStudentHomeworkX(u *gin.Context) {
	id := u.Query("id")
	ss := student_homework.GetStudentHomeworkX(id)
	u.JSON(http.StatusOK, ss)
}

//象棋作业信息分页
func GetStudentHomeworkBySearch(u *gin.Context) {
	id := u.Query("id")
	clientPage,_ := strconv.Atoi(u.Query("clientPage"))
	everyPage,_ := strconv.Atoi(u.Query("everyPage"))
	ss := student_homework.GetStudentHomeworkBySearch(id, clientPage,everyPage)
	u.JSON(http.StatusOK, ss)
}

//象棋作业信息删除
func DeleteStudentHomeworkById(u *gin.Context) {
	id := u.Param("id")
	ss := student_homework.DeleteStudentHomeworkByid(id)
	u.JSON(http.StatusOK, ss)
}

//象棋作业信息修改
func UpdateStudentHomework(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := student_homework.UpdateStudentHomework(values)
	u.JSON(http.StatusOK, ss)
}

//新增象棋作业信息
func CreateStudentHomework(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := student_homework.CreateStudentHomework(values)
	u.JSON(http.StatusOK, ss)
}