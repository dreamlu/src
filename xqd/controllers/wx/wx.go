package wx

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/medivhzhan/weapp"
	"github.com/medivhzhan/weapp/code"
	"github.com/medivhzhan/weapp/payment"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
	"xqd/models/student"
	"xqd/util/db"
	"xqd/util/lib"
)

var AppID = "wx69791d1332166afa"
var Secret = "4d4f75e718c3e230e726698bbc95b0de"
var PaySecret = ""
var MchID = ""

//小程序access_token
type AccessToken struct {
	AccessToken string `json:"access_token"`
}

//小程序登录
func WxLogin(u *gin.Context) {

	code := u.PostForm("code")
	var getinfo lib.GetInfoN

	var flag = 0 	//新用户
	var join = 0 	//默认未加班、机构

	//var info interface{}
	res, err := weapp.Login(AppID, Secret, code)
	if err != nil {
		u.JSON(http.StatusOK, lib.MapDataError{lib.CodeWx, err.Error()})
		return
	}

	//fmt.Printf("返回结果: %#v", res)

	var user student.Student
	db.DB.Where("openid = ?", res.OpenID).First(&user)
	//新用户新增操作
	if user.Openid == "" && res.OpenID != "" {
		sql := "insert `student`(openid) value(?)"
		db.DB.Exec(sql,res.OpenID)
		//同时创建对应和默认老师的好友关系
	} else {//老用户查询
		if user.Phone != ""{
			flag = 1
		}
	}
	//查询对应用户信息
	db.DB.Where("openid = ?", res.OpenID).First(&user)

	switch {
	case user.AgencyID != 0 && user.ClassroomId != 0:
		join = 1
	}

	//创建默认班级
	switch {
	case flag == 0://新用户
	//创建对应老师好友列表
		student.UpdateStudentS(strconv.FormatInt(user.ID, 10), strconv.FormatInt(1, 10), strconv.FormatInt(1, 10))
	}

	getinfo.Data = map[string]interface{}{"client_id": user.ID, "openid": res.OpenID, "flag":flag, "join":join}
	getinfo.Msg = lib.MsgSuccess
	getinfo.Status = lib.CodeSuccess

	u.JSON(http.StatusOK, getinfo)
}

//解密
func WxDecrypt(u *gin.Context) {
	var getinfo lib.GetInfoN

	rawData := u.PostForm("rawData")
	//fmt.Println(rawData)
	encryptedData := u.PostForm("encryptedData")
	signature := u.PostForm("signature")
	iv := u.PostForm("iv")
	session_key := u.PostForm("session_key")

	//var info interface{}
	// 解密用户信息
	//
	// @rawData 不包括敏感信息的原始数据字符串, 用于计算签名。
	// @encryptedData 包括敏感数据在内的完整用户信息的加密数据
	// @signature 使用 sha1( rawData + session_key ) 得到字符串, 用于校验用户信息
	// @iv 加密算法的初始向量
	// @ssk 微信 session_key
	userinfo, err := weapp.DecryptUserInfo(rawData, encryptedData, signature, iv, session_key)
	if err != nil {
		u.JSON(http.StatusOK, lib.MapDataError{lib.CodeEcrypt, err.Error()})
		return
	}
	//phone , err := weapp.DecryptPhoneNumber(session_key, encryptedData, iv)

	getinfo.Data = userinfo
	getinfo.Msg = lib.MsgSuccess
	getinfo.Status = lib.CodeSuccess

	u.JSON(http.StatusOK, getinfo)
}

//提现
func WxWithDraw(u *gin.Context) {

	var getinfo lib.GetInfoN
	//生成时间戳
	nanos := time.Now().UnixNano()
	tradeNo := strconv.FormatInt(nanos, 10)

	// 新建退款订单
	form := payment.Transferer{
		// 必填 ...
		AppID:  "APPID",
		MchID:  MchID,
		Amount: 100, //"总金额(分)",
		//OutRefundNo: "商户退款单号",
		OutTradeNo: tradeNo, //"商户订单号", // or TransactionID: "微信订单号",
		ToUser:     "ozjfE5O5hFU0cQBW4eJeaWhvIjTc",
		Desc:       "转账描述", // 若商户传入, 会在下发给用户的退款消息中体现退款原因

		/*// 选填 ...
		IP: "发起转账端 IP 地址", // 若商户传入, 会在下发给用户的退款消息中体现退款原因
		CheckName: "校验用户姓名选项 true/false",
		RealName: "收款用户真实姓名", // 如果 CheckName 设置为 true 则必填用户真实姓名
		Device:   "发起转账设备信息",*/
	}

	// 需要证书
	res, err := form.Transfer(PaySecret, "conf/cert/apiclient_cert.pem", "conf/cert/apiclient_key.pem")
	if err != nil {
		u.JSON(http.StatusOK, lib.GetMapDataError(lib.CodeWxWithDraw, err.Error()))
		return
	}

	//fmt.Printf("返回结果: %#v", res)
	getinfo.Data = res
	getinfo.Msg = lib.MsgSuccess
	getinfo.Status = lib.CodeSuccess
	u.JSON(http.StatusOK, getinfo)
}

//支付,范围对应支付的多个(5)参数
func WxPay(u *gin.Context) {

	var getinfo lib.GetInfoN

	openid := u.PostForm("openid")
	price, _ := strconv.Atoi(u.PostForm("price"))

	//生成时间戳
	nanos := time.Now().UnixNano()
	tradeNo := strconv.FormatInt(nanos, 10)
	// 新建支付订单
	form := payment.Order{
		// 必填
		AppID:      AppID,
		MchID:      MchID,
		Body:       "墅懒日租",
		NotifyURL:  "通知地址",
		OpenID:     openid,
		OutTradeNo: tradeNo, //"商户订单号",
		TotalFee:   price,

		// 选填 ...
		/*IP:        "发起支付终端IP",
		NoCredit:  "是否允许使用信用卡",
		StartedAt: "交易起始时间",
		ExpiredAt: "交易结束时间",
		Tag:       "订单优惠标记",
		Detail:    "商品详情",
		Attach:    "附加数据",*/
	}

	res, err := form.Unify(PaySecret)
	if err != nil {
		u.JSON(http.StatusOK, lib.GetMapDataError(lib.CodeWxPay, err.Error()))
		return
	}

	//fmt.Printf("返回结果: %#v", res)

	// 获取小程序前点调用支付接口所需参数
	params, err := payment.GetParams(res.AppID, PaySecret, res.NonceStr, res.PrePayID)
	if err != nil {
		u.JSON(http.StatusOK, lib.GetMapDataError(lib.CodeWxPay, err.Error()))
		return
	}

	getinfo.Data = params
	getinfo.Msg = lib.MsgSuccess
	getinfo.Status = lib.CodeSuccess
	u.JSON(http.StatusOK, getinfo)
}

//获得access_token
func GetAccessToken(u *gin.Context) {
	var getinfo lib.GetInfoN

	te_uri := "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + AppID + "&secret=" + Secret
	res, _ := http.Get(te_uri)
	body, _ := ioutil.ReadAll(res.Body)
	//fmt.Printf("返回结果: %#v", res)
	json.Unmarshal(body, &getinfo.Data)
	getinfo.Msg = lib.MsgSuccess
	getinfo.Status = lib.CodeSuccess
	u.JSON(http.StatusOK, getinfo)
}

//二维码,业务量多的情况
func GetQRCode(u *gin.Context) {
	//var getinfo lib.GetInfoN

	scene := u.Query("scene")
	page := u.Query("page")

	te_uri := "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + AppID + "&secret=" + Secret
	res, _ := http.Get(te_uri)
	body, _ := ioutil.ReadAll(res.Body)
	//fmt.Printf("返回结果: %#v", res)
	var at AccessToken
	json.Unmarshal(body, &at)

	coder := code.QRCoder{
		Scene:     scene, // 参数数据
		Page:      page,  // 识别二维码后进入小程序的页面链接
		Width:     430,   // 图片宽度
		IsHyaline: false, // 是否需要透明底色
		AutoColor: true,  // 自动配置线条颜色, 如果颜色依然是黑色, 则说明不建议配置主色调
		LineColor: code.Color{ //  AutoColor 为 false 时生效, 使用 rgb 设置颜色 十进制表示
			R: "50",
			G: "50",
			B: "50",
		},
	}

	// token: 微信 access_token
	resu, err := coder.UnlimitedAppCode(at.AccessToken)
	defer resu.Body.Close()
	if err != nil {
		u.JSON(http.StatusOK, lib.GetMapDataError(lib.CodeWx, err.Error()))
		return
	}

	bodyu, _ := ioutil.ReadAll(resu.Body)
	u.Writer.Header().Add("Content-Type", "image/png")
	u.Writer.Write(bodyu)
}
