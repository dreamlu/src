package student_testx

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/student_testx"
)

//根据id获得测试
func GetStudentTest(u *gin.Context) {
	id := u.Query("id")
	ss := student_test.GetStudentTest(id)
	u.JSON(http.StatusOK, ss)
}

//新增测试信息
func CreateStudentTest(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := student_test.CreateStudentTest(values)
	u.JSON(http.StatusOK, ss)
}