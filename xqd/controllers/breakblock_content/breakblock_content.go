package breakblock_content

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/breakblock_content"
)

//根据id获得闯关棋盘内容
func GetBreakblockContentById(u *gin.Context) {
	id := u.Query("id")
	ss := breakblock_content.GetBreakblockContentById(id)
	u.JSON(http.StatusOK, ss)
}

//闯关棋盘内容信息分页
func GetBreakblockContentBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := breakblock_content.GetBreakblockContentBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//闯关棋盘内容信息删除
func DeleteBreakblockContentById(u *gin.Context) {
	id := u.Param("id")
	ss := breakblock_content.DeleteBreakblockContentByid(id)
	u.JSON(http.StatusOK, ss)
}

//闯关棋盘内容信息修改
func UpdateBreakblockContent(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := breakblock_content.UpdateBreakblockContent(values)
	u.JSON(http.StatusOK, ss)
}

//新增闯关棋盘内容信息
func CreateBreakblockContent(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := breakblock_content.CreateBreakblockContent(values)
	u.JSON(http.StatusOK, ss)
}