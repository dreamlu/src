package agency

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/agency"
)

//获得机构下拉框
func GetAgency(u *gin.Context) {
	ss := agency.GetAgency()
	u.JSON(http.StatusOK, ss)
}
