package classroom

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/classroom"
)

//根据id获得班级
func GetClassroomById(u *gin.Context) {
	id := u.Query("id")
	ss := classroom.GetClassroomById(id)
	u.JSON(http.StatusOK, ss)
}

//班级信息分页
func GetClassroomBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := classroom.GetClassroomBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//班级信息删除
func DeleteClassroomById(u *gin.Context) {
	id := u.Param("id")
	ss := classroom.DeleteClassroomByid(id)
	u.JSON(http.StatusOK, ss)
}

//班级信息修改
func UpdateClassroom(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := classroom.UpdateClassroom(values)
	u.JSON(http.StatusOK, ss)
}

//新增班级信息
func CreateClassroom(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := classroom.CreateClassroom(values)
	u.JSON(http.StatusOK, ss)
}