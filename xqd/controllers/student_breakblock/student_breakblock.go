package student_breakblock

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"xqd/models/student_breakblock"
)

//根据id获得闯关
func GetGiftByBTId(u *gin.Context) {
	breakblock_id := u.Query("breakblock_id")
	student_id := u.Query("student_id")
	ss := student_breakblock.GetGiftByBTId(breakblock_id,student_id)
	u.JSON(http.StatusOK, ss)
}

//根据id获得闯关
func GetStudentBreakblockById(u *gin.Context) {
	breakblock_id := u.Query("breakblock_id")
	student_id := u.Query("student_id")
	ss := student_breakblock.GetStudentBreakblockById(breakblock_id,student_id)
	u.JSON(http.StatusOK, ss)
}

//闯关信息分页
func GetStudentBreakblockX(u *gin.Context) {
	id := u.Query("id")
	ss := student_breakblock.GetStudentBreakblockX(id)
	u.JSON(http.StatusOK, ss)
}

//闯关信息分页
func GetStudentBreakblockBySearch(u *gin.Context) {
	id := u.Query("id")
	clientPage,_ := strconv.Atoi(u.Query("clientPage"))
	everyPage,_ := strconv.Atoi(u.Query("everyPage"))
	ss := student_breakblock.GetStudentBreakblockBySearch(id, clientPage,everyPage)
	u.JSON(http.StatusOK, ss)
}

//闯关信息删除
func DeleteStudentBreakblockById(u *gin.Context) {
	id := u.Param("id")
	ss := student_breakblock.DeleteStudentBreakblockByid(id)
	u.JSON(http.StatusOK, ss)
}

//闯关信息修改
func UpdateStudentBreakblock(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := student_breakblock.UpdateStudentBreakblock(values)
	u.JSON(http.StatusOK, ss)
}

//新增闯关信息
func CreateStudentBreakblock(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := student_breakblock.CreateStudentBreakblock(values)
	u.JSON(http.StatusOK, ss)
}