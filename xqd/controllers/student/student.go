package student

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/student"
)

//获得老师好友--学生列表,根据老师id,招生提醒--(领取礼物)
func GetStudentGift(u *gin.Context) {
	teacher_id := u.Query("teacher_id")
	classroom_id := u.Query("classroom_id")
	ss := student.GetStudentGift(teacher_id, classroom_id)
	u.JSON(http.StatusOK, ss)
}

//根据学生id,我的等级
func GetCard(u *gin.Context) {
	id := u.Query("id")
	flag := u.Query("flag")
	ss := student.GetCard(id, flag)
	u.JSON(http.StatusOK, ss)
}

//根据学生id,我的奖品
func GetGift(u *gin.Context) {
	id := u.Query("id")
	ss := student.GetGift(id)
	u.JSON(http.StatusOK, ss)
}
//新增学生领取礼物信息
func CreateGift(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := student.CreateGift(values)
	u.JSON(http.StatusOK, ss)
}

//获得老师好友--学生列表,根据老师id
func GetStudentsX(u *gin.Context) {
	teacher_id := u.Query("teacher_id")
	classroom_id := u.Query("classroom_id")
	ss := student.GetStudentsX(teacher_id, classroom_id)
	u.JSON(http.StatusOK, ss)
}

//根据id获得个人排名等
func GetStudentRankById(u *gin.Context) {
	id := u.Query("id")
	flag := u.Query("flag")
	ss := student.GetStudentRankById(id, flag)
	u.JSON(http.StatusOK, ss)
}

//根据id获得学生,小程序
func GetStudentByIdX(u *gin.Context) {
	id := u.Query("id")
	ss := student.GetStudentByIdX(id)
	u.JSON(http.StatusOK, ss)
}

//根据id获得学生
func GetStudentById(u *gin.Context) {
	id := u.Query("id")
	ss := student.GetStudentById(id)
	u.JSON(http.StatusOK, ss)
}

//学生信息分页
func GetStudentBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := student.GetStudentBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//学生信息删除
func DeleteStudentById(u *gin.Context) {
	id := u.Param("id")
	ss := student.DeleteStudentByid(id)
	u.JSON(http.StatusOK, ss)
}

//学生信息修改
func UpdateStudent(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := student.UpdateStudent(values)
	u.JSON(http.StatusOK, ss)
}

//学生批量转机构班级
func UpdateStudentS(u *gin.Context) {
	ids := u.PostForm("ids")
	agency_id := u.PostForm("agency_id")
	classroom_id := u.PostForm("classroom_id")
	ss := student.UpdateStudentS(ids, agency_id, classroom_id)
	u.JSON(http.StatusOK, ss)
}
