package student_breakblock_content

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/student_breakblock_content"
)

//创建学生闯关棋盘内容记录
func CreateStuentBreakblockContent(u *gin.Context){
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := student_breakblock_content.CreateStuentBreakblockContent(values)
	u.JSON(http.StatusOK, ss)
}
