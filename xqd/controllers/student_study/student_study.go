package student_study

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/student_study"
)

//获得学生对应作业的学习数据
func GetStudyData(u *gin.Context){
	student_id := u.Query("student_id")
	homework_id := u.Query("homework_id")
	ss := student_study.GetStudyData(student_id,homework_id)
	u.JSON(http.StatusOK, ss)
}

//创建学生学习记录
func CreateStuentStudy(u *gin.Context){
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := student_study.CreateStuentStudy(values)
	u.JSON(http.StatusOK, ss)
}
