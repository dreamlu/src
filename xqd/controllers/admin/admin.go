package admin

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"xqd/controllers"
	"xqd/util"
	"xqd/util/db"
	"xqd/util/lib"
)

//管理员
type Admin struct {
	ID       int64  `json:"id"`
	Account  string `json:"account"`  //账号
	Password string `json:"password"` //密码
}

//根据id获得admin
func GetById(u *gin.Context) {
	id := u.Query("id")
	ss := GetAdminById(id)
	u.JSON(http.StatusOK, ss)
}

//admin信息修改
func Update(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := UpdateAdmin(values)
	u.JSON(http.StatusOK, ss)
}

//获得,根据id
func GetAdminById(id string) interface{} {

	db.DB.AutoMigrate(&Admin{})
	var ad = Admin{}
	return db.GetDataById(&ad, id)
}

//修改
func UpdateAdmin(args map[string][]string) interface{} {

	if _,ok := args["password"];ok{
		args["password"][0] = util.AesEn(args["password"][0])
	}

	return db.UpdateData("admin", args)
}

//登录
func Login(u *gin.Context) {
	var info interface{}
	var user controllers.Account
	var sql string

	account := u.PostForm("account")
	password := u.PostForm("password")

	sql = "SELECT id,password FROM `admin` WHERE account = ?"
	dba := db.DB.Raw(sql, account).Scan(&user)
	num := dba.RowsAffected
	if dba.Error == nil && num > 0 {
		password = util.AesEn(password)
		if user.Password == password {
			//字符为16的倍数
			strID := strconv.Itoa(user.ID)
			key := "-Iloveyouchinese"
			user_id, err := util.Encrypt([]byte(strID + string([]byte(key)[:len(key)-len(strID)])))
			if err != nil {
				fmt.Println("cookie加密错误Encrypt: ", err)
				return
			}
			u.SetCookie("uid", string(user_id), 24*3600, "/", "*", false, true)
			info = map[string]interface{}{"status": lib.CodeSuccess, "msg": "请求成功", "uid": strconv.Itoa(user.ID)}
		} else {
			info = lib.MapCountErr
		}
	} else {
		info = lib.MapNoCount
	}

	u.JSON(http.StatusOK, info)
}
