package breakblock

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/breakblock"
)

//根据id获得闯关
func GetBreakblockById(u *gin.Context) {
	id := u.Query("id")
	ss := breakblock.GetBreakblockById(id)
	u.JSON(http.StatusOK, ss)
}

//闯关信息分页
func GetBreakblockBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := breakblock.GetBreakblockBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//闯关信息删除
func DeleteBreakblockById(u *gin.Context) {
	id := u.Param("id")
	ss := breakblock.DeleteBreakblockByid(id)
	u.JSON(http.StatusOK, ss)
}

//闯关信息修改
func UpdateBreakblock(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := breakblock.UpdateBreakblock(values)
	u.JSON(http.StatusOK, ss)
}

//新增闯关信息
func CreateBreakblock(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := breakblock.CreateBreakblock(values)
	u.JSON(http.StatusOK, ss)
}