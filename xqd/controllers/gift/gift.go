package gift

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/gift"
)

//根据id获得礼品
func GetGiftById(u *gin.Context) {
	id := u.Query("id")
	ss := gift.GetGiftById(id)
	u.JSON(http.StatusOK, ss)
}

//礼品信息分页
func GetGiftBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := gift.GetGiftBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//礼品信息删除
func DeleteGiftById(u *gin.Context) {
	id := u.Param("id")
	ss := gift.DeleteGiftByid(id)
	u.JSON(http.StatusOK, ss)
}

//礼品信息修改
func UpdateGift(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := gift.UpdateGift(values)
	u.JSON(http.StatusOK, ss)
}

//新增礼品信息
func CreateGift(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := gift.CreateGift(values)
	u.JSON(http.StatusOK, ss)
}