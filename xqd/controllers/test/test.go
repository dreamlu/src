package test

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/test"
)

//根据id获得测试
func GetTestById(u *gin.Context) {
	id := u.Query("id")
	ss := test.GetTestById(id)
	u.JSON(http.StatusOK, ss)
}

//测试信息分页
func GetTestBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := test.GetTestBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//测试信息删除
func DeleteTestById(u *gin.Context) {
	id := u.Param("id")
	ss := test.DeleteTestByid(id)
	u.JSON(http.StatusOK, ss)
}

//测试信息修改
func UpdateTest(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := test.UpdateTest(values)
	u.JSON(http.StatusOK, ss)
}

//新增测试信息
func CreateTest(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := test.CreateTest(values)
	u.JSON(http.StatusOK, ss)
}