package homework

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/homework"
)

//作业下拉框
func GetHomeworkXL(u *gin.Context) {
	student_id := u.Query("student_id")
	ss := homework.GetHomeworkXL(student_id)
	u.JSON(http.StatusOK, ss)
}

//根据id获得象棋作业
func GetHomeworkById(u *gin.Context) {
	id := u.Query("id")
	ss := homework.GetHomeworkById(id)
	u.JSON(http.StatusOK, ss)
}

//象棋作业信息分页
func GetHomeworkBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := homework.GetHomeworkBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//象棋作业信息删除
func DeleteHomeworkById(u *gin.Context) {
	id := u.Param("id")
	ss := homework.DeleteHomeworkByid(id)
	u.JSON(http.StatusOK, ss)
}

//象棋作业信息修改
func UpdateHomework(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := homework.UpdateHomework(values)
	u.JSON(http.StatusOK, ss)
}

//新增象棋作业信息
func CreateHomework(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := homework.CreateHomework(values)
	u.JSON(http.StatusOK, ss)
}