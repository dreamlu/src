package suggest

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/suggest"
)

var p suggest.Suggest

//根据id
func GetById(u *gin.Context) {
	id := u.Query("id")
	ss := p.GetById(id)
	u.JSON(http.StatusOK, ss)
}

//分页
func GetBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := p.GetBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//删除
func Delete(u *gin.Context) {
	id := u.Param("id")
	ss := p.Delete(id)
	u.JSON(http.StatusOK, ss)
}

//修改
func Update(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form

	ss := p.Update(values)
	u.JSON(http.StatusOK, ss)
}

//新增
func Create(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form

	ss := p.Create(values)
	u.JSON(http.StatusOK, ss)
}
