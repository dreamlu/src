package store

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/store"
)

//根据id获得门店
func GetStoreById(u *gin.Context) {
	id := u.Query("id")
	ss := store.GetStoreById(id)
	u.JSON(http.StatusOK, ss)
}

//门店信息分页
func GetStoreBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := store.GetStoreBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//门店信息删除
func DeleteStoreById(u *gin.Context) {
	id := u.Param("id")
	ss := store.DeleteStoreByid(id)
	u.JSON(http.StatusOK, ss)
}

//门店信息修改
func UpdateStore(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := store.UpdateStore(values)
	u.JSON(http.StatusOK, ss)
}

//新增门店信息
func CreateStore(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := store.CreateStore(values)
	u.JSON(http.StatusOK, ss)
}