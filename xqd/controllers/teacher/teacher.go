package teacher

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/teacher"
)

//根据校长id获得统计分析
func GetAnalysisData(u *gin.Context) {
	id := u.Query("id")
	flag := u.Query("flag")
	classroom_id := u.Query("classroom_id")
	ss := teacher.GetAnalysisData(id,flag,classroom_id)
	u.JSON(http.StatusOK, ss)
}

//根据校长id获得学生作业数据
func GetHomeWorkData(u *gin.Context) {
	id := u.Query("id")
	classroom_id := u.Query("classroom_id")
	ss := teacher.GetHomeWorkData(id,classroom_id)
	u.JSON(http.StatusOK, ss)
}

//根据校长id获得一些数据
func GetSomeData(u *gin.Context) {
	id := u.Query("id")
	ss := teacher.GetSomeData(id)
	u.JSON(http.StatusOK, ss)
}

//根据id获得校长好友列表
func GetTeachersX(u *gin.Context) {
	id := u.Query("id")
	ss := teacher.GetTeachersX(id)
	u.JSON(http.StatusOK, ss)
}

//根据id获得老师/校长
func GetTeacherById(u *gin.Context) {
	id := u.Query("id")
	ss := teacher.GetTeacherById(id)
	u.JSON(http.StatusOK, ss)
}

//老师/校长信息分页
func GetTeacherBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := teacher.GetTeacherBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//老师/校长信息删除
func DeleteTeacherById(u *gin.Context) {
	id := u.Param("id")
	ss := teacher.DeleteTeacherByid(id)
	u.JSON(http.StatusOK, ss)
}

//老师/校长信息修改
func UpdateTeacher(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := teacher.UpdateTeacher(values)
	u.JSON(http.StatusOK, ss)
}

//新增老师/校长信息
func CreateTeacher(u *gin.Context) {
	//var form teacher.Teacher
	//if err := u.ShouldBind(&form); err != nil {
	//	u.JSON(http.StatusOK, lib.GetMapDataError(lib.CodeValidator,err.Error()))
	//	return
	//}
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := teacher.CreateTeacher(values)
	u.JSON(http.StatusOK, ss)
}