package test_content

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/test_content"
)

//根据id获得测试内容
func GetTestContentById(u *gin.Context) {
	id := u.Query("id")
	ss := test_content.GetTestContentById(id)
	u.JSON(http.StatusOK, ss)
}

//测试内容信息分页
func GetTestContentBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := test_content.GetTestContentBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//测试内容信息删除
func DeleteTestContentById(u *gin.Context) {
	id := u.Param("id")
	ss := test_content.DeleteTestContentByid(id)
	u.JSON(http.StatusOK, ss)
}

//测试内容信息修改
func UpdateTestContent(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := test_content.UpdateTestContent(values)
	u.JSON(http.StatusOK, ss)
}

//新增测试内容信息
func CreateTestContent(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := test_content.CreateTestContent(values)
	u.JSON(http.StatusOK, ss)
}