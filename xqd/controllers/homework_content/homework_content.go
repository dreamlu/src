package homework_content

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"xqd/models/homework_content"
)

//根据id获得作业内容
func GetHomeworkContentById(u *gin.Context) {
	id := u.Query("id")
	ss := homework_content.GetHomeworkContentById(id)
	u.JSON(http.StatusOK, ss)
}

//作业内容信息分页
func GetHomeworkContentBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := homework_content.GetHomeworkContentBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//作业内容信息删除
func DeleteHomeworkContentById(u *gin.Context) {
	id := u.Param("id")
	ss := homework_content.DeleteHomeworkContentByid(id)
	u.JSON(http.StatusOK, ss)
}

//作业内容信息修改
func UpdateHomeworkContent(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := homework_content.UpdateHomeworkContent(values)
	u.JSON(http.StatusOK, ss)
}

//新增作业内容信息
func CreateHomeworkContent(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := homework_content.CreateHomeworkContent(values)
	u.JSON(http.StatusOK, ss)
}