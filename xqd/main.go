package main

import (
	"github.com/gin-gonic/gin"
	"xqd/conf"
	"xqd/routers"
	_ "xqd/util/db"
)

func main() {
	gin.SetMode(gin.DebugMode)
	router := routers.SetRouter()
	// Listen and Server in 0.0.0.0:8080
	router.Run(":" + conf.GetConfigValue("http_port"))
}
