package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"xqd/conf"
)

var (
	DB *gorm.DB
)

func init() {
	var err error
	//数据库,全局初始化一次
	DB, err = gorm.Open("mysql", conf.GetConfigValue("db.user")+":"+conf.GetConfigValue("db.password")+"@/"+conf.GetConfigValue("db.name")+"?charset=utf8&parseTime=True&loc=Local")
	//defer DB.Close()
	if err != nil {
		fmt.Println(err)
	}
	//全局禁用表名复数
	DB.SingularTable(true)
	//sql打印
	DB.LogMode(true)

	//文件open
	//nowTime := time.Now().Format("2006-01-02")
	//filename := "log/"+nowTime+"-sql.log"
	//var f *os.File
	//_, err = os.Stat(filename)
	//if err != nil {
	//	if os.IsNotExist(err) {//不存在，创建
	//		f,_ = os.Create(filename)
	//	}
	//} else {
	//	f,_ = os.Open(filename)
	//}
	//DB.SetLogger(SetLogger(f))//打印错误信息和对应的sql

	//连接池
	//最大闲置连接与打开连接
	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	DB.DB().SetMaxIdleConns(20)

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	DB.DB().SetMaxOpenConns(200)
}
