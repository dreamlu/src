package routers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
	"xqd/controllers"
	"xqd/controllers/admin"
	"xqd/controllers/agency"
	"xqd/controllers/basic"
	"xqd/controllers/breakblock"
	"xqd/controllers/breakblock_content"
	"xqd/controllers/chat"
	"xqd/controllers/classroom"
	"xqd/controllers/gift"
	"xqd/controllers/homework"
	"xqd/controllers/homework_content"
	"xqd/controllers/store"
	"xqd/controllers/student"
	"xqd/controllers/student_breakblock"
	"xqd/controllers/student_breakblock_content"
	"xqd/controllers/student_homework"
	"xqd/controllers/student_study"
	"xqd/controllers/student_testx"
	"xqd/controllers/suggest"
	"xqd/controllers/teacher"
	"xqd/controllers/test"
	"xqd/controllers/test_content"
	"xqd/controllers/wx"
	"xqd/util/file"
	"xqd/util/lib"
	"xqd/util/str"
)

func SetRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	router := gin.New()
	str.MaxUploadMemory = router.MaxMultipartMemory
	//router.Use(CorsMiddleware())

	router.Use(CheckLogin()) //简单登录验证

	// load the casbin model and policy from files, database is also supported.
	//权限中间件
	//e := casbin.NewEnforcer("conf/authz_model.conf", "conf/authz_policy.csv")
	//router.Use(controllers.NewAuthorizer(e))

	//静态目录
	router.Static("api/v1/static", "static")

	// Ping test
	router.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
	//组的路由,version
	v1 := router.Group("/api/v1")
	{
		v := v1
		//网站基本信息
		v.GET("/basic/getbasic", basic.GetBasicInfo)
		//文件上传
		v.POST("/file/upload", file.UpoadFile)
		//验证码
		/*captchas := v.Group("/captcha")
		{
			captchas.GET("/getkey", captcha.GetKey)
			captchas.GET("/showimage", captcha.ShowImage)
			//captchas.GET("/verify", captcha.Verify)
		}*/
		//用户登录
		v.POST("/login/login", controllers.Login)
		//管理员
		admins := v.Group("/admin")
		{
			admins.POST("/login", admin.Login)
			admins.GET("/getbyid", admin.GetById)
			admins.PUT("/update", admin.Update)
		}
		//小程序
		wxs := v.Group("/wx")
		{
			wxs.POST("/wxlogin", wx.WxLogin)
			wxs.POST("/wxdecrypt", wx.WxDecrypt)
			wxs.POST("/wxwithdraw", wx.WxWithDraw)
			wxs.POST("/wxpay", wx.WxPay)
			wxs.GET("/getaccesstoken", wx.GetAccessToken)
			wxs.GET("/getqrcode", wx.GetQRCode)
		}
		//学生
		students := v.Group("/student")
		{
			students.GET("/getbysearch", student.GetStudentBySearch)
			students.GET("/getbyid", student.GetStudentById)
			students.DELETE("/delete/:id", student.DeleteStudentById)
			students.PUT("/update", student.UpdateStudent)
			students.PUT("/updates", student.UpdateStudentS)
			//小程序
			students.GET("/getbyidx", student.GetStudentByIdX)
			students.GET("/getrankbyid", student.GetStudentRankById)
			students.GET("/gets", student.GetStudentsX)
			students.GET("/getgift", student.GetGift)
			students.POST("/creategift", student.CreateGift)
			students.GET("/getcard", student.GetCard)
			students.GET("/getsg", student.GetStudentGift)
		}
		//老师校长
		teachers := v.Group("/teacher")
		{
			teachers.GET("/getbysearch", teacher.GetTeacherBySearch)
			teachers.GET("/getbyid", teacher.GetTeacherById)
			teachers.DELETE("/delete/:id", teacher.DeleteTeacherById)
			teachers.POST("/create", teacher.CreateTeacher)
			teachers.PUT("/update", teacher.UpdateTeacher)
			teachers.GET("/gets", teacher.GetTeachersX)
			teachers.GET("/getsomedata", teacher.GetSomeData)
			teachers.GET("/getsthomework", teacher.GetHomeWorkData)
			teachers.GET("/getasdata", teacher.GetAnalysisData)
		}
		//班级
		classrooms := v.Group("/classroom")
		{
			classrooms.GET("/getbysearch", classroom.GetClassroomBySearch)
			classrooms.GET("/getbyid", classroom.GetClassroomById)
			classrooms.DELETE("/delete/:id", classroom.DeleteClassroomById)
			classrooms.POST("/create", classroom.CreateClassroom)
			classrooms.PUT("/update", classroom.UpdateClassroom)
		}
		//门店
		stores := v.Group("/store")
		{
			stores.GET("/getbysearch", store.GetStoreBySearch)
			stores.GET("/getbyid", store.GetStoreById)
			stores.DELETE("/delete/:id", store.DeleteStoreById)
			stores.POST("/create", store.CreateStore)
			stores.PUT("/update", store.UpdateStore)
		}
		//礼物
		gifts := v.Group("/gift")
		{
			gifts.GET("/getbysearch", gift.GetGiftBySearch)
			gifts.GET("/getbyid", gift.GetGiftById)
			gifts.DELETE("/delete/:id", gift.DeleteGiftById)
			gifts.POST("/create", gift.CreateGift)
			gifts.PUT("/update", gift.UpdateGift)
		}
		//闯关
		breakblocks := v.Group("/breakblock")
		{
			breakblocks.GET("/getbysearch", breakblock.GetBreakblockBySearch)
			breakblocks.GET("/getbyid", breakblock.GetBreakblockById)
			breakblocks.DELETE("/delete/:id", breakblock.DeleteBreakblockById)
			breakblocks.POST("/create", breakblock.CreateBreakblock)
			breakblocks.PUT("/update", breakblock.UpdateBreakblock)
		}
		//闯关题目
		bkcontents := v.Group("/bkcontent")
		{
			bkcontents.GET("/getbysearch", breakblock_content.GetBreakblockContentBySearch)
			bkcontents.GET("/getbyid", breakblock_content.GetBreakblockContentById)
			bkcontents.DELETE("/delete/:id", breakblock_content.DeleteBreakblockContentById)
			bkcontents.POST("/create", breakblock_content.CreateBreakblockContent)
			bkcontents.PUT("/update", breakblock_content.UpdateBreakblockContent)
		}
		//测试
		tests := v.Group("/test")
		{
			tests.GET("/getbysearch", test.GetTestBySearch)
			tests.GET("/getbyid", test.GetTestById)
			tests.DELETE("/delete/:id", test.DeleteTestById)
			tests.POST("/create", test.CreateTest)
			tests.PUT("/update", test.UpdateTest)
		}
		//测试题目
		ts := v.Group("/tcontent")
		{
			ts.GET("/getbysearch", test_content.GetTestContentBySearch)
			ts.GET("/getbyid", test_content.GetTestContentById)
			ts.DELETE("/delete/:id", test_content.DeleteTestContentById)
			ts.POST("/create", test_content.CreateTestContent)
			ts.PUT("/update", test_content.UpdateTestContent)
		}
		//作业
		homeworks := v.Group("/homework")
		{
			homeworks.GET("/getbysearch", homework.GetHomeworkBySearch)
			homeworks.GET("/getbyid", homework.GetHomeworkById)
			homeworks.DELETE("/delete/:id", homework.DeleteHomeworkById)
			homeworks.POST("/create", homework.CreateHomework)
			homeworks.PUT("/update", homework.UpdateHomework)
			homeworks.GET("/getxl", homework.GetHomeworkXL)
		}
		//作业题目
		hks := v.Group("/hkcontent")
		{
			hks.GET("/getbysearch", homework_content.GetHomeworkContentBySearch)
			hks.GET("/getbyid", homework_content.GetHomeworkContentById)
			hks.DELETE("/delete/:id", homework_content.DeleteHomeworkContentById)
			hks.POST("/create", homework_content.CreateHomeworkContent)
			hks.PUT("/update", homework_content.UpdateHomeworkContent)
		}
		//学生作业
		sts := v.Group("/sthomework")
		{
			sts.GET("/getx", student_homework.GetStudentHomeworkX)
			sts.GET("/getbysearch", student_homework.GetStudentHomeworkBySearch)
			sts.GET("/getbyid", student_homework.GetStudentHomeworkById)
			//sts.DELETE("/delete/:id", student_homework.DeleteStudentHomeworkById)
			sts.POST("/create", student_homework.CreateStudentHomework)
			//sts.PUT("/update", student_homework.UpdateStudentHomework)
		}
		//学生作业数据统计
		stys := v.Group("/study")
		{
			stys.GET("/getstudydata", student_study.GetStudyData)
			stys.POST("/create", student_study.CreateStuentStudy)
		}
		//学生闯关
		stbs := v.Group("/stbreakblock")
		{
			stbs.GET("/getx", student_breakblock.GetStudentBreakblockX)
			stbs.GET("/getbysearch", student_breakblock.GetStudentBreakblockBySearch)
			stbs.GET("/getbyid", student_breakblock.GetStudentBreakblockById)
			//stbs.DELETE("/delete/:id", student_breakblock.DeleteStudentBreakblockById)
			stbs.POST("/create", student_breakblock.CreateStudentBreakblock)
			//stbs.PUT("/update", student_breakblock.UpdateStudentBreakblock)
			stbs.GET("/getbytbid",student_breakblock.GetGiftByBTId)
			stbs.POST("/createcontent",student_breakblock_content.CreateStuentBreakblockContent)
		}
		//学生测试
		stests := v.Group("/stest")
		{
			stests.GET("/get", student_testx.GetStudentTest)
			stests.POST("/create", student_testx.CreateStudentTest)
		}
		//机构
		v.GET("/agency/get",agency.GetAgency)

		//群聊
		chats := v.Group("/chat")
		{
			chats.GET("/",chat.Chat)
			chats.GET("/ws",chat.ChatWS)
			chats.GET("/getglmsg",chat.GetGroupLastMsg)
			chats.GET("/getallmsg",chat.GetAllGroupMsg)
			chats.PUT("/readglmsg",chat.ReadGroupLastMsg)
			chats.POST("/massmsg",chat.MassMessage)
		}
		//意见反馈
		suggests := v.Group("/suggest")
		{
			suggests.GET("/search", suggest.GetBySearch)
			suggests.GET("/id", suggest.GetById)
			suggests.DELETE("/delete/:id", suggest.Delete)
			suggests.POST("/create", suggest.Create)
			suggests.PUT("/update", suggest.Update)
		}
	}
	//不存在路由
	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{
			"status": 404,
			"msg":    "接口不存在->('.')/请求方法错误",
		})
	})

	return router
}

/*登录失效验证*/
func CheckLogin() gin.HandlerFunc {
	return func(c *gin.Context) {
		//小程序端
		if c.Request.Header.Get("Authorization") == "wechat" {
			return
		}
		path := c.Request.URL.String()
		if !strings.Contains(path, "login") && !strings.Contains(path, "/static") {
			_, err := c.Cookie("uid")
			if err != nil {
				c.Abort()
				c.JSON(http.StatusOK, lib.MapNoToken)
			}
		}

		/*cookie,err := c.Request.Cookie("uid")
		if err != nil{
			fmt.Println("cookie-->uid不存在")
		}
		ss, _ := url.QueryUnescape(cookie.Value)
		// 解密
		uid, err := util.Decrypt([]byte(ss))
		if err != nil {
			fmt.Println("cookie解密失败: ", err)
			c.Abort()
			c.JSON(http.StatusOK, lib.MapNoToken)
		}*/
	}
}