package store

import "xqd/util/db"

/*门店模型*/
type Store struct {
	ID        int64  `json:"id"`
	TeacherId int64  `json:"teacher_id"`	//校长id
	Address   string `json:"address"`		//地址
	Contact   string `json:"contact"`		//联系人
	Phone     string `json:"phone"`			//电话
}

//获得门店,根据id
func GetStoreById(id string) interface{} {

	db.DB.AutoMigrate(&Store{})
	var store = Store{}
	return db.GetDataById(&store, id)
}

//获得门店,分页/查询
func GetStoreBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Store{})
	var stores = []*Store{}
	return db.GetDataBySearch(Store{}, &stores, "store", args) //匿名Store{}
}

//删除门店,根据id
func DeleteStoreByid(id string) interface{} {

	return db.DeleteDataByName("store", "id", id)
}

//修改门店
func UpdateStore(args map[string][]string) interface{} {

	return db.UpdateData("store", args)
}

//创建门店
func CreateStore(args map[string][]string) interface{} {

	return db.CreateData("store", args)
}
