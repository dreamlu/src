package teacher

import (
	"fmt"
	"strconv"
	"strings"
	"xqd/conf"
	"xqd/models/agency"
	"xqd/models/chat"
	"xqd/util"
	"xqd/util/db"
	"xqd/util/lib"
)

/*老师/校长模型*/
type Teacher struct {
	ID       int64  `json:"id"`
	Username string `json:"username"`  //用户名
	Headimg  string `json:"headimg"`   //头像
	Phone    string `json:"phone"`     //电话
	Account  string `json:"account"`   //账户
	Password string `json:"password"`  //密码
	ParentId int64  `json:"parent_id"` //上级id
}

/*老师/校长*/
type TeacherInfo struct {
	Teacher
	AgencyID int64  `json:"agency_id"` //
	Agency   string `json:"agency"`    //机构
	GroupID  string  `json:"group_id"`  //群聊id
}

/*校长好友列表*/
type TeacherX struct {
	ID       int64  `json:"id"`
	Username string `json:"username"` //用户名
	Headimg  string `json:"headimg"`  //头像
	GroupId  string  `json:"group_id"`
	IsMsg     int64  `json:"is_msg"` 	//是否有新消息
}

//一些数据
type SomeData struct {
	StudentNum int64 `json:"student_num"` //学生数量
	ClassNum   int64 `json:"class_num"`   //班级数量
	TeacherNum int64 `json:"teacher_num"` //老师数量
}

/*老师端--作业数据*/
type HomeWorkData struct {
	Name         string        `json:"name"`          //作业名
	CompleteNum  int64         `json:"complete_num"`  //完成人数
}

/*老师端--统计分析*/
type AnalysisData struct {
	Username   string `json:"username"`   //姓名
	Headimg    string `json:"headimg"`    //头像
	Chessvalue int64  `json:"chessvalue"` //棋力值
	TestNum    int64  `json:"test_num"`   //测试数
	BreakNum   int64  `json:"break_num"`  //闯关数
}

//根据老师id(统计分析),和排序，0棋力值，1测试，2闯关
func GetAnalysisData(id, flag, classroom_id string) interface{} {

	sql := `select 
		username,
		headimg,
		chessvalue,
		(select count(*) from student_test where student_id=a.id) as test_num,
		(select count(*) from student_breakblock where student_id=a.id) as break_num
		from student a
		inner join classroom b on a.classroom_id=b.id
		where b.teacher_id=? and a.classroom_id=` + classroom_id + ` `

	switch flag {
	case "0":
		sql += "order by chessvalue desc"
	case "1":
		sql += "order by test_num desc"
	case "2":
		sql += "order by break_num desc"
	}
	var as []AnalysisData
	return db.GetDataBySql(&as, sql, id)
}

//获得作业数据
func GetHomeWorkData(id, classroom_id string) interface{} {
	sql := `select 
	d.` + `name` + `,
	count(distinct a.id) as complete_num
	from student_homework a
	inner join student b
	on a.student_id=b.id
	inner join classroom c
	on b.classroom_id=c.id
	inner join homework d
	on d.id=a.homework_id
	where c.teacher_id=? and c.id=` + classroom_id + `
	group by d.id,d.` + `name` + `,completetime`
	var hd []HomeWorkData
	return db.GetDataBySql(&hd, sql, id)
}

//根据校长id获得学生数量、班级数量、老师数量
func GetSomeData(id string) interface{} {
	sql := `select
		(select count(*) from student aa inner join agency bb on aa.agency_id=bb.id where bb.teacher_id=a.parent_id) as student_num,
		(select count(*) from classroom where parent_id=a.parent_id) as class_num,
		count(*) as teacher_num
		from teacher a
		where a.parent_id=?
		group by student_num,class_num`
	var sd SomeData
	return db.GetDataBySql(&sd, sql, id)
}

//获得校长好友--老师列表
func GetTeachersX(id string) interface{} {

	var teachers []TeacherX
	var flag = "0"
	sql := `select group_id,uid as id,username,headimg,
	(select count(*) from group_last_msg where uid=`+id+` and group_id=a.group_id and flag=0 and is_read=0) as is_msg
	from group_users a
	inner join teacher b
	on a.uid=b.id
	# 筛选出自己之外的好友
	where group_id in (select group_id from group_users where uid=` + id + ` and flag=` + flag + `) and uid !=` + id + ` and flag=` + flag

	return db.GetDataBySql(&teachers, sql)
}

//获得老师/校长,根据id
func GetTeacherById(id string) interface{} {

	db.DB.AutoMigrate(&Teacher{})
	var teacher = TeacherInfo{}
	sql := fmt.Sprintf("select %s,(select group_id from group_users where uid=a.id and flag=0 limit 0,1) as group_id from `teacher` a where id = ?", db.GetSqlColumnsSql(Teacher{}))
	info := db.GetDataBySql(&teacher, sql, id)
	var ay agency.Agency
	switch teacher.ParentId {
	case 0: //校长
		db.DB.Where(" teacher_id = ?", teacher.ID).First(&ay)
	default:
		db.DB.Where(" teacher_id = ?", teacher.ParentId).First(&ay)
	}
	teacher.AgencyID = ay.ID
	teacher.Agency = ay.Name
	return info
}

//获得老师/校长,分页/查询,后台用
func GetTeacherBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Teacher{})
	var teachers = []*TeacherInfo{}
	//info := db.GetDataBySearch(Teacher{}, &teachers, "teacher", args) //匿名Teacher{}
	//页码,每页数量
	every := ""
	table1 := "teacher"
	table2 := "agency"
	parent_id := "0"

	//页码,每页数量
	clientPageStr := conf.GetConfigValue("clientPage") //默认第1页
	everyPageStr := conf.GetConfigValue("everyPage")   //默认10页

	//尝试将select* 变为对应的字段名
	sql := fmt.Sprintf("select %s,name as agency from "+table1+" "+
		"inner join "+table2+" "+
		"on "+table2+"."+table1+"_id="+table1+".id where 1=1 and ", db.GetColumnsSql(Teacher{}, table1))

	sqlnolimit := "select count(" + table1 + ".id) as sum_page from " + table1 + " " +
		"inner join " + table2 + " " +
		"on " + table2 + "." + table1 + "_id=" + table1 + ".id where 1=1 and "
	for k, v := range args {
		if k == "clientPage" {
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" {
			everyPageStr = v[0]
			continue
		}
		if k == "every" {
			every = v[0]
			continue
		}
		if k == "agency" {
			v[0] = strings.Replace(v[0], "'", "\\'", -1) //转义
			sql += table2 + ".name= '" + v[0] + "' and "
			sqlnolimit += table2 + ".name = '" + v[0] + "' and "
			continue
		}
		if v[0] == "" { //条件值为空,舍弃
			continue
		}
		if k == "parent_id" {
			parent_id = v[0]
			switch parent_id {
			case "0": //校长
			default: //老师
				sql = strings.Replace(sql, "agency.teacher_id=teacher.id", "agency.teacher_id=teacher.parent_id", -1)
				sqlnolimit = strings.Replace(sqlnolimit, "agency.teacher_id=teacher.id", "agency.teacher_id=teacher.parent_id", -1)
			}
		}

		v[0] = strings.Replace(v[0], "'", "\\'", -1) //转义
		sql += table1 + "." + k + " = '" + v[0] + "' and "
		sqlnolimit += table1 + "." + k + " = '" + v[0] + "' and "
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	sql = string([]byte(sql)[:len(sql)-4])                      //去and
	sqlnolimit = string([]byte(sqlnolimit)[:len(sqlnolimit)-4]) //去and
	if every == "" {
		sql += "order by " + table1 + ".id desc limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr
	}

	info := db.GetDataBySqlSearch(&teachers, sql, sqlnolimit, clientPage, everyPage)
	return info
}

//删除老师/校长,根据id
func DeleteTeacherByid(id string) interface{} {

	switch id {
	case "1","2":
		return lib.GetMapDataError(lib.CodeText, "禁止删除基本机构及老师")
	}

	//查询下面是否存在班级
	var ca agency.Classroom
	db.DB.Raw("select id from `classroom` where teacher_id=?",id).Scan(&ca)
	if ca.ID != 0 {
		return lib.GetMapDataError(lib.CodeText, "其下存在班级,禁止删除")
	}
	//直接尝试删除校长下面对应的机构(先查询是否是校长再删除,费资源,直接进行删除操作)
	db.DeleteDataByName("agency", "teacher_id", id)

	return db.DeleteDataByName("teacher", "id", id)
}

//修改老师/校长
func UpdateTeacher(args map[string][]string) interface{} {

	if _, ok := args["password"]; ok {
		args["password"][0] = util.AesEn(args["password"][0])
	}
	pid, _ := strconv.ParseInt(args["parent_id"][0], 10, 64)
	id, _ := strconv.ParseInt(args["id"][0], 10, 64)
	//当为校长时修改机构
	if pid == 0 {
		if _, ok := args["agency"]; ok {
			db.DB.Exec("update `agency` set name=? where teacher_id=?", args["agency"][0], id)
			delete(args, "agency") //删除机构
		}
	}
	info := db.UpdateData("teacher", args)

	return info
}

//创建老师/校长
func CreateTeacher(args map[string][]string) interface{} {
	var teacher Teacher
	var info interface{}

	args["password"][0] = util.AesEn(args["password"][0]) //加密


	pid, _ := strconv.ParseInt(args["parent_id"][0], 10, 64)

	var agencyName string
	if _, ok := args["agency"]; ok {
		agencyName = args["agency"][0]
		delete(args, "agency")
	} else {
		if pid == 0 {
			return lib.GetMapDataError(lib.CodeText, "机构不能为空")
		}
	}

	sql := db.GetInsertSql("teacher", args)
	dba := db.DB.Exec(sql)

	if dba.Error != nil {
		info = lib.GetSqlError(dba.Error.Error())
	} else {
		info = lib.MapCreate
		//创建成功后
		//当为校长时创建机构
		db.DB.Raw("select max(id) as id from `teacher`").Scan(&teacher)
		if pid == 0 {

			var ay = agency.Agency{
				Name:      agencyName,
				TeacherID: teacher.ID,
			}
			db.DB.Create(&ay)
		} else { //当为老师时,创建与校长的群聊(自动创建)
			chat.DistributeGroup("", strconv.FormatInt(teacher.ID, 10)+","+strconv.FormatInt(pid, 10))
		}
	}
	db.DB.Raw("select max(id) as id from `teacher`").Scan(&teacher)
	return info
}
