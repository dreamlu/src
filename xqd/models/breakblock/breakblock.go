package breakblock

import "xqd/util/db"

/*闯关模板*/
type Breakblock struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`   //闯关名
	Score  int    `json:"score"`  //棋力值
	Reward int64  `json:"reward"` //奖励
}

//获得闯关,根据id
func GetBreakblockById(id string) interface{} {

	db.DB.AutoMigrate(&Breakblock{})
	var breakblock = Breakblock{}
	return db.GetDataById(&breakblock, id)
}

//获得闯关,分页/查询
func GetBreakblockBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Breakblock{})
	var breakblocks = []*Breakblock{}
	return db.GetDataBySearch(Breakblock{}, &breakblocks, "breakblock", args) //匿名Breakblock{}
}

//删除闯关,根据id
func DeleteBreakblockByid(id string) interface{} {

	return db.DeleteDataByName("breakblock", "id", id)
}

//修改闯关
func UpdateBreakblock(args map[string][]string) interface{} {

	return db.UpdateData("breakblock", args)
}

//创建闯关
func CreateBreakblock(args map[string][]string) interface{} {

	return db.CreateData("breakblock", args)
}
