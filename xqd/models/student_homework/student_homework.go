package student_homework

import (
	"strconv"
	"xqd/models/homework_content"
	"xqd/util"
	"xqd/util/db"
	"xqd/util/lib"
)

/*象棋作业模型*/
type StudentHomework struct {
	ID         int64 `json:"id"`
	StudentId  int64 `json:"student_id"`  //学生id
	HomeworkId int64 `json:"homework_id"` //作业id
	//Content      string        `json:"content"`
	Myscore      int64         `json:"myscore"`      //学生成绩
	Completetime util.JsonTime `json:"completetime"` //完成时间
}

/*学生作业返回模型*/
type StudentHKS struct {
	CompleteNum int64       `json:"complete_num"` //已完成作业数,计算压力给服务端...
	StudentHK   []StudentHK `json:"student_hk"`
}

/*学生作业返回模型*/
type StudentHK struct {
	ID           int64         `json:"id"`     //作业id
	Name         string        `json:"name"`   //作业名
	Score        int64         `json:"score"`  //分数
	Reward       int64         `json:"reward"` //奖励
	Card1        string        `json:"card1"`  //两种卡片
	Card2        string        `json:"card2"`
	Iscomplete   int64         `json:"iscomplete"`   //是否完成
	Completetime util.JsonDate `json:"completetime"` //完成时间
}

///*已完成或新的棋盘内容*/
//type StudentHKContent struct {
//	Name    string `json:"name"`    //内容标题
//	Voice   string `json:"voice"`   //音频
//	Content string `json:"content"` //已完成内容
//}

//获得学生完成的象棋作业,根据id
func GetStudentHomeworkById(homework_id, student_id string) interface{} {

	sql := `select a.id,name,voice,c.content,a.homework_id
		from ` + `homework_content` + ` a
		inner join student_homework b
		on a.homework_id=b.homework_id
		inner join student_study c
		on a.id=c.homework_content_id and c.student_id=b.student_id
		where a.homework_id=` + homework_id + ` and b.student_id = ` + student_id
	var student_homework []homework_content.HomeworkContent

	return db.GetDataBySql(&student_homework, sql)
}

//获得象棋作业
func GetStudentHomeworkX(id string) interface{} {

	sql := `# 已完成的数据,不包含当天日期
	select a.*,b.completetime,(select count(*) from student_homework where homework_id=a.id and student_id=b.student_id) as iscomplete
		from homework a
		left join student_homework b
		on a.id=b.homework_id
		and student_id= `+id+`
		where b.completetime is not null and datediff(b.completetime,now()) != 0
	
	UNION
	
	# 当天日期不得超过两题,limit限制
	(select a.*,b.completetime,(select count(*) from student_homework where homework_id=a.id and student_id=b.student_id) as iscomplete
		from homework a
		left join student_homework b
		on a.id=b.homework_id
		and student_id= `+id+`
		where b.completetime is null or datediff(b.completetime,now()) = 0
		limit 0,2)
		order by id desc`

	var student_homeworks StudentHKS
	db.DB.Raw("select count(*) as complete_num from student_homework where student_id=?", id).Scan(&student_homeworks)

	var info interface{}
	var getinfo lib.GetInfoN

	dba := db.DB.Raw(sql).Scan(&student_homeworks.StudentHK)
	//有数据是返回相应信息
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else {

		//统计页码等状态
		getinfo.Status = lib.CodeSuccess
		getinfo.Msg = lib.MsgSuccess
		getinfo.Data = student_homeworks //数据
		info = getinfo
	}
	return info
}

//获得象棋作业,分页/查询
func GetStudentHomeworkBySearch(id string, clientPage, everyPage int) interface{} {

	sql := `select a.*,b.completetime,(select count(*) from student_homework where homework_id=a.id and student_id=` + id + `) as iscomplete
		from homework a
		left join student_homework b
		on a.id=b.homework_id
		and student_id= ` + id + `
		limit ` + strconv.Itoa((clientPage-1)*everyPage) + `,` + strconv.Itoa(everyPage)

	sqlnolimit := `select count(*) as sum_page from homework a`

	var student_homeworks StudentHKS
	db.DB.Raw("select count(*) as complete_num from student_homework where student_id=?", id).Scan(&student_homeworks)

	var info interface{}
	var getinfo lib.GetInfo

	dba := db.DB.Raw(sqlnolimit).Scan(&getinfo.Pager)
	num := getinfo.Pager.SumPage
	//有数据是返回相应信息
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapNoResult
	} else {
		//DB.Debug().Find(&dest)
		dba = db.DB.Raw(sql).Scan(&student_homeworks.StudentHK)
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
			return info
		}
		//统计页码等状态
		getinfo.Status = lib.CodeSuccess
		getinfo.Msg = lib.MsgSuccess
		getinfo.Data = student_homeworks //数据
		//getinfo.Pager.SumPage = num
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	}
	return info
}

//删除象棋作业,根据id
func DeleteStudentHomeworkByid(id string) interface{} {

	return db.DeleteDataByName("student_homework", "id", id)
}

//修改象棋作业
func UpdateStudentHomework(args map[string][]string) interface{} {

	return db.UpdateData("student_homework", args)
}

//创建象棋作业
func CreateStudentHomework(args map[string][]string) interface{} {

	return db.CreateData("student_homework", args)
}
