package student_study

import (
	"fmt"
	"xqd/util/db"
	"xqd/util/lib"
)

/*学习数据模型*/
type StuentStudy struct {
	ID                int64  `json:"id"`
	StudentID         int64  `json:"student_id"`          //学生id
	Second            int64  `json:"second"`              //完成该题目时间
	HomeworkContentID int64  `json:"homework_content_id"` //具体内容id
	Content           string `json:"content"`             //学生完成的具体棋盘内容
}

//返回学习数据
type StStudyData struct {
	Second     []Data
	FastSecond int64 `json:"fast_second"`
	SlowSecond int64 `json:"slow_second"`
}

type Data struct {
	Second int `json:"second"`
}

//返回学习数据,根据学生id和作业id
func GetStudyData(student_id, homework_id string) interface{} {

	sql1 := "select `second` "
	sql2 := " select min(`second`) as fast_second, max(`second`) as slow_second "
	sql := fmt.Sprintf(` 
	from student_study a
	inner join homework_content b
	on a.homework_content_id=b.id

	inner join homework c
	on b.homework_id=c.id
	where homework_id=%s and student_id=%s`, homework_id, student_id)

	sql1 += sql
	sql2 += sql

	//var data []Data
	//db.DB.Raw(sql1).Scan(&data)
	var stdata StStudyData
	info := db.GetDataBySql(&stdata, sql2)
	db.DB.Raw(sql1).Scan(&stdata.Second)
	return info
}

//学习记录创建
func CreateStuentStudy(args map[string][]string) interface{} {

	var info interface{}

	sql := db.GetInsertSql("student_study", args)
	dba := db.DB.Exec(sql)
	if dba.Error != nil {
		info = lib.GetSqlError(dba.Error.Error())
		//创建失败,尝试更新
		dba = db.DB.Exec("update `student_study` set second=?,content=? where student_id=? and homework_content_id=?",
			args["second"],args["content"],args["student_id"],args["homework_content_id"])
		if dba.Error == nil{
			info = lib.MapCreate
		}

	} else {
		info = lib.MapCreate
	}
	return info
}
