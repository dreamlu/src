package homework

import "xqd/util/db"

/*象棋作业模型*/
type Homework struct {
	ID     int64  `json:"id"`
	Name   string `json:"name"`   //作业名
	Score  int64  `json:"score"`  //棋力值
	Reward int64  `json:"reward"` //奖励
	Card1  string `json:"card1"`  //卡片1
	Card2  string `json:"card2"`  //卡牌2
}

/*作业下拉框*/
type HomeworkXL struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

//获得象棋作业,根据id
func GetHomeworkXL(student_id string) interface{} {

	var homework []HomeworkXL
	sql := `# 已完成的数据,不包含当天日期
	select a.id,a.name
		from homework a
		left join student_homework b
		on a.id=b.homework_id
		and student_id= `+student_id+`
		where b.completetime is not null and datediff(b.completetime,now()) != 0
	
	UNION
	
	# 当天日期不得超过两题,limit限制
	(select a.id,a.name
		from homework a
		left join student_homework b
		on a.id=b.homework_id
		and student_id= `+student_id+`
		where b.completetime is null or datediff(b.completetime,now()) = 0
		limit 0,2)`
	return db.GetDataBySql(&homework, sql)
}

//获得象棋作业,根据id
func GetHomeworkById(id string) interface{} {

	db.DB.AutoMigrate(&Homework{})
	var homework = Homework{}
	return db.GetDataById(&homework, id)
}

//获得象棋作业,分页/查询
func GetHomeworkBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Homework{})
	var homeworks = []*Homework{}
	return db.GetDataBySearch(Homework{}, &homeworks, "homework", args) //匿名Homework{}
}

//删除象棋作业,根据id
func DeleteHomeworkByid(id string) interface{} {

	info := db.DeleteDataByName("homework", "id", id)
	db.DeleteDataByName("homework_content", "homework_id", id)
	db.DeleteDataByName("student_homework", "homework_id", id)
	return info
}

//修改象棋作业
func UpdateHomework(args map[string][]string) interface{} {

	return db.UpdateData("homework", args)
}

//创建象棋作业
func CreateHomework(args map[string][]string) interface{} {

	return db.CreateData("homework", args)
}
