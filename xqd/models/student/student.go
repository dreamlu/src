package student

import (
	"strconv"
	"strings"
	"xqd/conf"
	"xqd/models/chat"
	"xqd/models/classroom"
	"xqd/util"
	"xqd/util/db"
	"xqd/util/lib"
)

/*学生模型*/
type Student struct {
	ID          int64         `json:"id"`
	Openid      string        `json:"openid"`       //小程序唯一标识
	Nickname    string        `json:"nickname"`     //昵称
	Username    string        `json:"username"`     //用户名
	Headimg     string        `json:"headimg"`      //头像
	Borndate    util.JsonDate `json:"borndate"`     //出生日期
	Phone       string        `json:"phone"`        //电话
	City        string        `json:"city"`         //城市
	Address     string        `json:"address"`      //地址
	AgencyID    int64         `json:"agency_id"`    //所属机构
	Isrecruit   int64         `json:"isrecruit"`    //是否招生
	Chessvalue  int64         `json:"chessvalue"`   //棋力值
	ClassroomId int64         `json:"classroom_id"` //班级id
	Division    string        `json:"division"`     //段位
}

/*小程序个人信息*/
type StudentX struct {
	StudentBank
	BkNum         int64  `json:"bk_num"`         //闯关总数
	BkedNum       int64  `json:"bked_num"`       //已闯关数
	HkNum         int64  `json:"hk_num"`         //作业总数
	HkedNum       int64  `json:"hked_num"`       //已完成作业数
	IsHk          int64  `json:"is_hk"`          //是否有新的作业
	IsGift        int64  `json:"is_gift"`        //是否有我的奖品
	GroupID       string `json:"group_id"`       //群聊id
	AgencyName    string `json:"agency_name"`    //机构名称
	ClassroomName string `json:"classroom_name"` //班级名称
	TeacherName   string `json:"teacher_name"`   //老师名称
}

/*全市排名信息*/
type StudentRank struct {
	Nickname   string `json:"nickname"`
	Headimg    string `json:"headimg"`    //头像
	Chessvalue int64  `json:"chessvalue"` //棋力值
	Rank       int64  `json:"rank"`       //排名
	//CountryRank int64  `json:"country_rank"` //全国排名
	//CityRank    int64  `json:"city_rank"`    //市排名
}

/*排名信息*/
type StudentRankPerson struct {
	Chessvalue  int64          `json:"chessvalue"`   //棋力值
	CountryRank int64          `json:"country_rank"` //全国排名
	CityRank    int64          `json:"city_rank"`    //市排名
	City        string         `json:"city"`         //城市
	CityRanks   []*StudentRank `json:"city_ranks"`   //全市排名
}

/*学生后台信息*/
type StudentBank struct {
	ID         int64         `json:"id"`
	Username   string        `json:"username"`    //用户名
	Borndate   util.JsonDate `json:"borndate"`    //出生日期
	Phone      string        `json:"phone"`       //电话
	Address    string        `json:"address"`     //地址
	AgencyID   int64         `json:"agency_id"`   //所属机构
	AgencyName string        `json:"agency_name"` //机构格名称
	Isrecruit  int64         `json:"isrecruit"`   //是否招生
	Chessvalue int64         `json:"chessvalue"`  //棋力值
	Testnum    int64         `json:"testnum"`     //测试数
	Subschool  string        `json:"subschool"`   //所在学校
	Subcroom   string        `json:"subcroom"`    //所在班级
	Subteacher string        `json:"subteacher"`  //代班老师
}

/*学生小程序*/
type StudentBankS struct {
	StudentBank
	BkNum   int64 `json:"bk_num"`   //闯关总数
	BkedNum int64 `json:"bked_num"` //已闯关数
	HkNum   int64 `json:"hk_num"`   //作业总数
	HkedNum int64 `json:"hked_num"` //已完成作业数
}

/*校长好友列表*/
type StudentSX struct {
	ID        int64  `json:"id"`
	Username  string `json:"username"`  //用户名
	Headimg   string `json:"headimg"`   //头像
	Isrecruit int64  `json:"isrecruit"` //是否招生
	GroupId   string `json:"group_id"`
	IsMsg     int64  `json:"is_msg"` //是否有新消息
}

/*我的奖品*/
type MyGift struct {
	ID             int64  `json:"id"`              //礼物id
	GiftName       string `json:"gift_name"`       //礼物名
	BreakblockID   int64  `json:"breakblock_id"`   //闯关id
	BreakblockName string `json:"breakblock_name"` //闯关名
	IsGet          int64  `json:"is_get"`          //是否领取
}

/*我的卡片,我的等级*/
type MyCard struct {
	CardNum int64  `json:"card_num"` //对应卡片总数
	Card    []Card `json:"card"`
}

/*卡片*/
type Card struct {
	Card string `json:"card"`
}

/*招生提醒(领取礼物)--老师列表*/
type StudentSSX struct {
	ID            int64  `json:"id"`
	Username      string `json:"username"`       //用户名
	Headimg       string `json:"headimg"`        //头像
	BreakblockNum int64  `json:"breakblock_num"` //闯关数
	GroupId       string `json:"group_id"`
}

//获得老师好友--学生列表,根据老师id,招生提醒(领取礼物)
func GetStudentGift(teacher_id, classroom_id string) interface{} {

	var sts []StudentSSX
	var flag = "1"
	sql := `select group_id,uid as id,username,headimg,
	(select count(*) from student_breakblock where student_id=b.id) as breakblock_num
	from group_users a
	inner join student b
	on a.uid=b.id
	# 筛选出老师之外的学生好友
	where group_id in (select group_id from group_users where uid=` + teacher_id + ` and flag=0) and uid !=` + teacher_id + ` and flag=` + flag + `
	and b.classroom_id=?`

	return db.GetDataBySql(&sts, sql, classroom_id)
}

//根据id获得学生所有的卡片
func GetCard(id, flag string) interface{} {
	sql := `select
	card1 as card
	from homework a
	inner join student_homework b
	on a.id=b.homework_id
	where card1 != '' and student_id=` + id + ` order by a.id`

	sqlnum := "select count(*) as card_num from homework where card1 != ''"

	switch flag {
	case "0": //勋章卡片
	case "1": //杀法卡片
		sql = strings.Replace(sql, "card1", "card2", -1)
		sqlnum = strings.Replace(sqlnum, "card1", "card2", -1)
	}

	var mc MyCard

	db.DB.Raw(sql).Scan(&mc.Card)

	return db.GetDataBySql(&mc, sqlnum)
}

//根据学生id,我的奖品,0表示未领取礼物
func GetGift(id string) interface{} {

	sql := `select
		a.id,
		e.id as breakblock_id,
		a.` + `name` + ` as gift_name,
		e.` + `name` + ` as breakblock_name,
		(select count(*) from student_gift where gift_id=a.id and student_id=d.id and breakblock_id=e.id) as is_get

		from gift a
		inner join agency c on a.teacher_id=c.teacher_id
		inner join student d on c.id=d.agency_id
		inner join breakblock e on a.breakblock_id=e.id
		inner join student_breakblock f on e.id=f.breakblock_id
		where d.id=? and d.id=f.student_id`
	var mygift []MyGift
	return db.GetDataBySql(&mygift, sql, id)
}

//创建学生礼物
func CreateGift(args map[string][]string) interface{} {
	return db.CreateData("student_gift", args)
}

//获得老师好友--学生列表,根据老师id
func GetStudentsX(teacher_id, classroom_id string) interface{} {

	var sts []StudentSX
	var flag = "1"
	sql := `select group_id, a.uid as id,b.username,b.headimg,isrecruit,
	# 统计针对老师的消息状态
	(select count(*) from group_last_msg where uid=` + teacher_id + ` and group_id=a.group_id and flag=0 and is_read=0) as is_msg
	from group_users a
	inner join student b
	on a.uid=b.id
	# 筛选出老师之外的学生好友
	where group_id in (select group_id from group_users where uid=` + teacher_id + ` and flag=0) and uid !=` + teacher_id + ` and flag=` + flag + `
	and b.classroom_id=?`

	return db.GetDataBySql(&sts, sql, classroom_id)
}

//获得学生棋力值排名等信息,根据id
func GetStudentRankById(id, flag string) interface{} {

	var st Student
	db.DB.First(&st, id)

	if st.City == "" {
		return lib.GetMapDataError(lib.CodeText, "暂无城市信息")
	}

	sql := `select 
		chessvalue,
		(select rank from (select 
		id,
		@rownum := @rownum + 1 as rank
		from student a,(SELECT @rownum := 0) r
		order by chessvalue desc) a 
		where id=` + id + `) as country_rank,
		(select rank from (select 
		id,
		@rownum2 := @rownum2 + 1 as rank
		from student aa,(SELECT @rownum2 := 0) r
		where city='` + st.City + `'
		order by chessvalue desc) t
		where id=a.id) as city_rank,
		city
		from student a
		where a.id = ? and city = '` + st.City + `' limit 100`

	//个人排名等信息
	var student StudentRankPerson
	info := db.GetDataBySql(&student, sql, id)

	//对应城市中的排名数据信息
	sql = `select 
		nickname,
		headimg,
		chessvalue,
		@rownum := @rownum + 1 as rank
		from student a,(SELECT @rownum := 0) r
		order by chessvalue desc`

	switch flag {
	case "0": //全国
	case "1": //全市
		sql = strings.Replace(sql, "order", " where city = '"+st.City+"' order", -1)

	}
	db.GetDataBySql(&student.CityRanks, sql)

	return info
}

//获得学生段位等小程序端信息,根据id
func GetStudentByIdX(id string) interface{} {

	var student StudentX

	//info := db.GetLeftDoubleTableDataById(StudentBank{}, &student, id, "student", "agency")
	sql2 := `select 
		a.id,a.username,borndate,a.phone,address,b.id as agency_id,b.name as agency_name,isrecruit,chessvalue,subschool,subcroom,subteacher,
		c.name as agency_name,d.name as classroom_name,e.username as teacher_name,
		(select count(*) from student_test where student_id=a.id) as testnum,
		(select count(*) from breakblock) as bk_num,
		(select count(*) from student_breakblock where student_id=a.id) as bked_num,
		(select count(*) from homework) as hk_num,
		(select count(*) from student_homework where student_id=a.id) as hked_num,
		(select group_id from group_users where uid=a.id and flag=1 limit 0,1) as group_id,
		(select sum(iscomplete)!=2 from 
		(select (select count(*) from student_homework where homework_id=a.id and student_id=b.student_id) as iscomplete
		from homework a
		left join student_homework b
		on a.id=b.homework_id
		and student_id=` + id + `
		where b.completetime is null or datediff(b.completetime,now()) = 0
		limit 0,2) a) as is_hk
		from student a
		inner join agency b
		on a.agency_id=b.id
		left join agency c
		on a.agency_id=c.id
		left join classroom d
		on a.classroom_id=d.id
		left join teacher e
		on d.teacher_id=e.id
		where a.id=?`
	info := db.GetDataBySql(&student, sql2, id)

	////0没作业，1有作业
	//if student.HkedNum != student.HkNum {
	//	student.IsHk = 1
	//}

	sql := `select
		a.id,
		a.` + `name` + ` as gift_name,
		e.` + `name` + ` as breakblock_name,
		(select count(*) from student_gift where gift_id=a.id and student_id=d.id and breakblock_id=e.id) as is_get

		from gift a
		inner join agency c on a.teacher_id=c.teacher_id
		inner join student d on c.id=d.agency_id
		inner join breakblock e on a.breakblock_id=e.id
		inner join student_breakblock f on e.id=f.breakblock_id
		where d.id=? and d.id=f.student_id`
	var mygift []MyGift
	db.GetDataBySql(&mygift, sql, id)
	for _, v := range mygift {
		if v.IsGet == 0 {
			student.IsGift = 1 //存在可领礼物
			break
		}
	}
	return info
}

//获得学生,根据id
func GetStudentById(id string) interface{} {

	var student StudentBankS

	//info := db.GetLeftDoubleTableDataById(StudentBank{}, &student, id, "student", "agency")
	sql2 := `select 
		a.id,username,borndate,phone,address,b.id as agency_id,b.name as agency_name,isrecruit,chessvalue,
		(select count(*) from student_test where student_id=a.id) as testnum,
		(select count(*) from breakblock) as bk_num,
		(select count(*) from student_breakblock where student_id=a.id) as bked_num,
		(select count(*) from homework) as hk_num,
		(select count(*) from student_homework where student_id=a.id) as hked_num
		from student a
		inner join agency b
		on a.agency_id=b.id
		where a.id=?`
	info := db.GetDataBySql(&student, sql2, id)
	return info
}

//获得学生,分页/查询
func GetStudentBySearch(args map[string][]string) interface{} {
	//页码,每页数量
	clientPageStr := conf.GetConfigValue("clientPage") //默认第1页
	everyPageStr := conf.GetConfigValue("everyPage")   //默认10页
	every := ""

	//尝试将select* 变为对应的字段名
	sql := `select 
		a.id,username,borndate,phone,address,b.id as agency_id,b.name as agency_name,isrecruit,chessvalue,subschool,subcroom,subteacher,
		(select count(*) from student_test where student_id=a.id) as testnum,
		(select count(*) from breakblock) as bk_num,
		(select count(*) from student_breakblock where student_id=a.id) as bked_num,
		(select count(*) from homework) as hk_num,
		(select count(*) from student_homework where student_id=a.id) as hked_num
		from student a
		left join agency b
		on a.agency_id=b.id
		where 1=1 and `

	sqlnolimit := `select 
		count(*) as sum_page
		from student a
		left join agency b
		on a.agency_id=b.id
		where 1=1 and `
	for k, v := range args {
		if k == "clientPage" {
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" {
			everyPageStr = v[0]
			continue
		}
		if k == "every" {
			every = v[0]
			continue
		}
		if k == "agency" {
			sql += "b.name = '" + v[0] + "' and "
			sqlnolimit += "b.name = '" + v[0] + "' and "
			continue
		}
		if k == "complete" && v[0] != "" {
			sql += "a.phone != '' and "
			sqlnolimit += "a.phone != '' and "
			continue
		}
		if v[0] == "" { //条件值为空,舍弃
			continue
		}

		v[0] = strings.Replace(v[0], "'", "\\'", -1) //转义
		sql += k + " = '" + v[0] + "' and "
		sqlnolimit += k + " = '" + v[0] + "' and "
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	sql = string([]byte(sql)[:len(sql)-4])                      //去and
	sqlnolimit = string([]byte(sqlnolimit)[:len(sqlnolimit)-4]) //去and
	if every == "" {
		sql += "order by a.id desc limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr
	}
	var students []StudentBankS
	return db.GetDataBySqlSearch(&students, sql, sqlnolimit, clientPage, everyPage)
}

//删除学生,根据id
func DeleteStudentByid(id string) interface{} {

	return db.DeleteDataByName("student", "id", id)
}

//修改学生
func UpdateStudent(args map[string][]string) interface{} {

	if _, ok := args["chessvalue"]; ok { //棋力值单独计算
		var st Student
		db.DB.First(&st, args["id"][0])
		if st.Chessvalue != 0 {
			cv, _ := strconv.ParseInt(args["chessvalue"][0], 10, 64)
			args["chessvalue"][0] = strconv.FormatInt(cv+st.Chessvalue, 10)
		}
	}
	return db.UpdateData("student", args)
}

//学生批量转机构
func UpdateStudentS(ids, agency_id, classroom_id string) interface{} {

	idsStr := strings.Split(ids, ",")

	//更新对应机构班级
	sql := "update `student` set agency_id = ?,classroom_id=" + classroom_id + " where id in("
	//删除之前默认的学生好友列表
	sql2 := "delete from `group_users` where flag=1 and uid in("

	for _, v := range idsStr {
		sql += v + ","
		sql2 += v + ","
	}
	sql = string([]byte(sql)[:len(sql)-1]) + ")"
	sql2 = string([]byte(sql2)[:len(sql2)-1]) + ")"

	info := db.UpdateDataBySql(sql, agency_id) //更新机构班级
	db.DB.Exec(sql2)                           //删除学生之前的数据

	//同时进行好友群组创建
	for _, v := range idsStr { //可能group_id会重复
		//先查询班级对应老师id
		var cla classroom.Classroom
		db.DB.First(&cla, classroom_id)
		//创建老师对应的好友列表
		chat.DistributeGroup(v, strconv.FormatInt(cla.TeacherID, 10))
	}

	return info
}
