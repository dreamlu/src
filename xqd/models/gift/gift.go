package gift

import (
	"xqd/util/db"
)

/*礼品模型*/
type Gift struct {
	ID           int64  `json:"id"`
	TeacherId    int64  `json:"teacher_id"`    //校长id
	BreakblockId int64  `json:"breakblock_id"` //闯关id
	Name         string `json:"name"`          //名称
	Imageurl     string `json:"imageurl"`      //封面
	StoreId      int64  `json:"store_id"`      //门店id
}

/*礼品返回详情*/
type GiftInfo struct {
	Name     string `json:"name"`     //名称
	Imageurl string `json:"imageurl"` //封面
	Address  string `json:"address"`  //地址
	Contact  string `json:"contact"`  //联系人
	Phone    string `json:"phone"`    //电话
}

//礼品详情,包含闯关名称
type GiftBank struct {
	ID             int64  `json:"id"`
	TeacherId      int64  `json:"teacher_id"`      //校长id
	BreakblockId   int64  `json:"breakblock_id"`   //闯关id
	BreakblockName string `json:"breakblock_name"` //闯关名称
	Name           string `json:"name"`            //名称
	Imageurl       string `json:"imageurl"`        //封面
	StoreId        int64  `json:"store_id"`        //门店id
}

//获得礼品,根据id
func GetGiftById(id string) interface{} {

	db.DB.AutoMigrate(&Gift{})
	var gift = Gift{}
	return db.GetDataById(&gift, id)
}

//获得礼品,分页/查询
func GetGiftBySearch(args map[string][]string) interface{} {
	var gifts  []GiftBank
	return db.GetDoubleTableDataBySearch(GiftBank{},&gifts,"gift","breakblock",args)
}

//删除礼品,根据id
func DeleteGiftByid(id string) interface{} {

	return db.DeleteDataByName("gift", "id", id)
}

//修改礼品
func UpdateGift(args map[string][]string) interface{} {

	return db.UpdateData("gift", args)
}

//创建礼品
func CreateGift(args map[string][]string) interface{} {

	return db.CreateData("gift", args)
}
