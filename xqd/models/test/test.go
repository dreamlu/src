package test

import "xqd/util/db"

/*测试模板*/
type Test struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`  //测试名
	Score int    `json:"score"` //棋力值
}

//获得测试,根据id
func GetTestById(id string) interface{} {

	db.DB.AutoMigrate(&Test{})
	var test = Test{}
	return db.GetDataById(&test, id)
}

//获得测试,分页/查询
func GetTestBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Test{})
	var tests = []*Test{}
	return db.GetDataBySearch(Test{}, &tests, "test", args) //匿名Test{}
}

//删除测试,根据id
func DeleteTestByid(id string) interface{} {

	return db.DeleteDataByName("test", "id", id)
}

//修改测试
func UpdateTest(args map[string][]string) interface{} {

	return db.UpdateData("test", args)
}

//创建测试
func CreateTest(args map[string][]string) interface{} {

	return db.CreateData("test", args)
}
