package suggest

import (
	"xqd/util/db"
)

/*suggest model*/
type Suggest struct {
	ID       uint   `json:"id" gorm:"primary_key"`
	Phone    string `json:"phone"`
	Email    string `json:"email"`
	Content  string `json:"content"`  //内容
	Category string `json:"category"` //类别
}

// get suggest, by id
func (c *Suggest) GetById(id string) interface{} {

	db.DB.AutoMigrate(&Suggest{})
	var suggest = Suggest{}
	return db.GetDataById(&suggest, id)
}

// get suggest, limit and search
// clientPage 1, everyPage 10 default
func (c *Suggest) GetBySearch(args map[string][]string) interface{} {
	//db.DB.AutoMigrate(&Suggest{})
	//var suggests = []*Suggest{}
	var suggests []*Suggest
	return db.GetDataBySearch(Suggest{}, &suggests, "suggest", args) //匿名Suggest{}
}

// delete suggest, by id
func (c *Suggest) Delete(id string) interface{} {

	return db.DeleteDataByName("suggest", "id", id)
}

// update suggest
func (c *Suggest) Update(args map[string][]string) interface{} {

	return db.UpdateData("suggest", args)
}

// create suggest
func (c *Suggest) Create(args map[string][]string) interface{} {

	//args["createtime"] = append(args["createtime"], time.Now().Format("2006-01-02 15:04:05"))
	return db.CreateData("suggest", args)
}
