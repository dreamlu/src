package classroom

import (
	"xqd/util/db"
	"xqd/util/lib"
	"xqd/util/str"
)

/*班级模型*/
type Classroom struct {
	ID        int64  `json:"id"`
	Name      string `json:"name"`       //班级名
	TeacherID int64  `json:"teacher_id"` //班级分销比例
	ParentID  int64  `json:"parent_id"`  //校长
}

//班级详情
type ClassroomInfo struct {
	ID              int    `json:"id"`
	Name            string `json:"name"`             //班级名
	TeacherID       int64  `json:"teacher_id"`       //班级分销比例
	TeacherUsername string `json:"teacher_username"` //老师名
}

//获得班级,根据id
func GetClassroomById(id string) interface{} {

	db.DB.AutoMigrate(&ClassroomInfo{})
	var classroom = ClassroomInfo{}
	return db.GetDoubleTableDataById(ClassroomInfo{}, &classroom, id, "classroom", "teacher")
}

//获得班级,分页/查询
func GetClassroomBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&ClassroomInfo{})
	var classrooms = []*ClassroomInfo{}
	return db.GetDoubleTableDataBySearch(ClassroomInfo{}, &classrooms, "classroom", "teacher", args) //匿名Classroom{}
}

//删除班级,根据id
func DeleteClassroomByid(id string) interface{} {

	switch id {
	case "1":
		return lib.GetMapDataError(lib.CodeText, "禁止删除基本班级")
	}

	//查询下面是否存在班级
	var value str.Value
	db.DB.Raw("select id as value from `student` where classroom_id=?",id).Scan(&value)
	if value.Value != "" {
		return lib.GetMapDataError(lib.CodeText, "其下存在学生,禁止删除")
	}
	return db.DeleteDataByName("classroom", "id", id)
}

//修改班级
func UpdateClassroom(args map[string][]string) interface{} {

	switch args["id"][0] {
	case "1":
		return lib.GetMapDataError(lib.CodeText, "禁止修改基本班级")
	}
	return db.UpdateData("classroom", args)
}

//创建班级
func CreateClassroom(args map[string][]string) interface{} {

	return db.CreateData("classroom", args)
}
