package breakblock_content

import "xqd/util/db"

/*闯关棋盘内容管理*/
type BreakblockContent struct {
	ID           int64  `json:"id"`
	Name         string `json:"name"`          //名称
	Voice        string `json:"voice"`         //语音
	Content      string `json:"content"`       //棋盘内容
	BreakblockId int64  `json:"breakblock_id"` //闯关id
}

//获得闯关棋盘内容,根据id
func GetBreakblockContentById(id string) interface{} {

	db.DB.AutoMigrate(&BreakblockContent{})
	var breakblock_content = BreakblockContent{}
	return db.GetDataById(&breakblock_content, id)
}

//获得闯关棋盘内容,分页/查询
func GetBreakblockContentBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&BreakblockContent{})
	var breakblock_contents = []*BreakblockContent{}
	return db.GetDataBySearch(BreakblockContent{}, &breakblock_contents, "breakblock_content", args) //匿名BreakblockContent{}
}

//删除闯关棋盘内容,根据id
func DeleteBreakblockContentByid(id string) interface{} {

	return db.DeleteDataByName("breakblock_content", "id", id)
}

//修改闯关棋盘内容
func UpdateBreakblockContent(args map[string][]string) interface{} {

	return db.UpdateData("breakblock_content", args)
}

//创建闯关棋盘内容
func CreateBreakblockContent(args map[string][]string) interface{} {

	return db.CreateData("breakblock_content", args)
}
