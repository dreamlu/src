package test_content

import "xqd/util/db"

/*测试内容管理*/
type TestContent struct {
	ID      int64  `json:"id"`
	Name    string `json:"name"`    //名称
	Voice   string `json:"voice"`   //语音
	Content string `json:"content"` //内容
	TestId  int64  `json:"test_id"` //测试id
}

//获得测试内容,根据id
func GetTestContentById(id string) interface{} {

	db.DB.AutoMigrate(&TestContent{})
	var test_content = TestContent{}
	return db.GetDataById(&test_content, id)
}

//获得测试内容,分页/查询
func GetTestContentBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&TestContent{})
	var test_contents = []*TestContent{}
	return db.GetDataBySearch(TestContent{}, &test_contents, "test_content", args) //匿名TestContent{}
}

//删除测试内容,根据id
func DeleteTestContentByid(id string) interface{} {

	return db.DeleteDataByName("test_content", "id", id)
}

//修改测试内容
func UpdateTestContent(args map[string][]string) interface{} {

	return db.UpdateData("test_content", args)
}

//创建测试内容
func CreateTestContent(args map[string][]string) interface{} {

	return db.CreateData("test_content", args)
}
