package student_breakblock

import (
	"fmt"
	"strconv"
	"xqd/models/breakblock_content"
	"xqd/models/gift"
	"xqd/util"
	"xqd/util/db"
	"xqd/util/lib"
)

/*学生闯关模板*/
type StudentBreakblock struct {
	ID           int64         `json:"id"`
	StudentId    int64         `json:"student_id"`
	BreakblockId int64         `json:"breakblock_id"` //学生闯关id
	Star         int64         `json:"star"`          //星级
	Completetime util.JsonDate `json:"completetime"`  //完成时间
}

/*学生闯关返回模型*/
type StudentBKS struct {
	CompleteNum int64       `json:"complete_num"` //已闯关数,计算压力给服务端...
	StudentBK   []StudentBK `json:"student_bk"`
}

/*学生闯关返回模型*/
type StudentBK struct {
	ID           int64         `json:"id"`           //作业id
	Name         string        `json:"name"`         //作业名
	Score        int64         `json:"score"`        //分数
	Reward       int64         `json:"reward"`       //奖励
	Star         int64         `json:"star"`         //星级
	Iscomplete   int64         `json:"iscomplete"`   //是否完成
	IsGift       int64         `json:"is_gift"`      //是否有礼物
	Completetime util.JsonDate `json:"completetime"` //完成时间
}

/*已完成棋盘内容*/
type StudentBKContent struct {
	Name            string `json:"name"`             //内容标题
	Voice           string `json:"voice"`            //音频
	CompleteContent string `json:"complete_content"` //已完成内容
}

//获得礼物详情,根据闯关id和学生id
func GetGiftByBTId(breakblock_id, student_id string) interface{} {

	var giftinfo gift.GiftInfo
	sql := fmt.Sprintf("select a.name,imageurl,b.address,contact,b.phone from `gift` a inner join store b on a.store_id=b.id "+
		"inner join agency c on a.teacher_id=c.teacher_id "+
		"inner join student d on c.id=d.agency_id "+
		"where breakblock_id = %s and d.id=%s", breakblock_id, student_id)

	return db.GetDataBySql(&giftinfo, sql)
}

//获得学生闯关,根据闯关id和学生id
func GetStudentBreakblockById(breakblock_id, student_id string) interface{} {

	sql := `select ` + `name` + `,voice,c.content as complete_content 
		from ` + `breakblock_content` + ` a
		inner join student_breakblock b
		on a.breakblock_id=b.breakblock_id
		inner join student_breakblock_content c
		on a.id=c.breakblock_content_id and c.student_id=b.student_id
		where a.breakblock_id=` + breakblock_id + ` and b.student_id = ` + student_id
	var student_breakblock []breakblock_content.BreakblockContent

	return db.GetDataBySql(&student_breakblock, sql)
}

//获得象棋闯关
func GetStudentBreakblockX(id string) interface{} {

	sql := `# 已完成的数据,不包含当天日期
	select a.*,b.completetime,(select count(*) from student_breakblock where breakblock_id=a.id and student_id=b.student_id) as iscomplete,
		(select count(*) from student_breakblock aa inner join gift bb on aa.breakblock_id=bb.breakblock_id inner join agency cc on cc.teacher_id=bb.teacher_id inner join student dd on dd.agency_id=cc.id
		where aa.breakblock_id=a.id and student_id=` + id + ` and dd.id=` + id + `) as is_gift
		from breakblock a
		left join student_breakblock b
		on a.id=b.breakblock_id
		and student_id= `+id+`
		where b.completetime is not null and datediff(b.completetime,now()) != 0
	
	UNION
	
	# 当天日期不得超过5题,limit限制
	(select a.*,b.completetime,(select count(*) from student_breakblock where breakblock_id=a.id and student_id=b.student_id) as iscomplete,
		(select count(*) from student_breakblock aa inner join gift bb on aa.breakblock_id=bb.breakblock_id inner join agency cc on cc.teacher_id=bb.teacher_id inner join student dd on dd.agency_id=cc.id
		where aa.breakblock_id=a.id and student_id=` + id + ` and dd.id=` + id + `) as is_gift
		from breakblock a
		left join student_breakblock b
		on a.id=b.breakblock_id
		and student_id= `+id+`
		where b.completetime is null or datediff(b.completetime,now()) = 0
		limit 0,5)
		# order by id desc`

	var sb StudentBKS
	db.DB.Raw("select count(*) as complete_num from student_breakblock where student_id=?", id).Scan(&sb)

	var info interface{}
	var getinfo lib.GetInfoN

	dba := db.DB.Raw(sql).Scan(&sb.StudentBK)
	//有数据是返回相应信息
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else {

		//统计页码等状态
		getinfo.Status = lib.CodeSuccess
		getinfo.Msg = lib.MsgSuccess
		getinfo.Data = sb //数据
		info = getinfo
	}
	return info
}

//获得学生闯关,分页/查询
func GetStudentBreakblockBySearch(id string, clientPage, everyPage int) interface{} {
	sql := `select a.*,
		(select count(*) from student_breakblock where breakblock_id=a.id and student_id=` + id + `) as iscomplete,
		(select count(*) from student_breakblock aa inner join gift bb on aa.breakblock_id=bb.breakblock_id inner join agency cc on cc.teacher_id=bb.teacher_id inner join student dd on dd.agency_id=cc.id
		where aa.breakblock_id=a.id and student_id=` + id + ` and dd.id=` + id + `) as is_gift,
		star,
		completetime
		from breakblock a
		left join student_breakblock b
		on a.id=b.breakblock_id
		and student_id= ` + id + `
		limit ` + strconv.Itoa((clientPage-1)*everyPage) + `,` + strconv.Itoa(everyPage)

	sqlnolimit := `select count(*) as sum_page
		from breakblock a`

	var stbk StudentBKS
	db.DB.Raw("select count(*) as complete_num from student_breakblock where student_id=?", id).Scan(&stbk)

	var info interface{}
	var getinfo lib.GetInfo

	dba := db.DB.Raw(sqlnolimit).Scan(&getinfo.Pager)
	num := getinfo.Pager.SumPage
	//有数据是返回相应信息
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapNoResult
	} else {
		//DB.Debug().Find(&dest)
		dba = db.DB.Raw(sql).Scan(&stbk.StudentBK)
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
			return info
		}

		//统计页码等状态
		getinfo.Status = lib.CodeSuccess
		getinfo.Msg = lib.MsgSuccess
		getinfo.Data = stbk //数据
		//getinfo.Pager.SumPage = num
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	}
	return info
}

//删除学生闯关,根据id
func DeleteStudentBreakblockByid(id string) interface{} {

	return db.DeleteDataByName("student_breakblock", "id", id)
}

//修改学生闯关
func UpdateStudentBreakblock(args map[string][]string) interface{} {

	return db.UpdateData("student_breakblock", args)
}

//创建学生闯关
func CreateStudentBreakblock(args map[string][]string) interface{} {

	return db.CreateData("student_breakblock", args)
}
