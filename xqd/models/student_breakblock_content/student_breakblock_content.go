package student_breakblock_content

import (
	"xqd/util/db"
	"xqd/util/lib"
)

/*学生闯关完成模型*/
type StuentBreakblockContent struct {
	ID                  int64  `json:"id"`
	StudentID           int64  `json:"student_id"`            //学生id
	BreakblockContentID int64  `json:"breakblock_content_id"` //具体内容id
	Content             string `json:"content"`               //学生完成的具体棋盘内容
}

//闯关记录创建
func CreateStuentBreakblockContent(args map[string][]string) interface{} {
	var info interface{}

	sql := db.GetInsertSql("student_breakblock_content", args)
	dba := db.DB.Exec(sql)
	if dba.Error != nil {
		info = lib.GetSqlError(dba.Error.Error())
		//创建失败,尝试更新
		dba = db.DB.Exec("update `student_breakblock_content` set content=? where student_id=? and breakblock_content_id=?",
			args["content"],args["student_id"],args["breakblock_content_id"])
		if dba.Error == nil{
			info = lib.MapCreate
		}

	} else {
		info = lib.MapCreate
	}
	return info
}
