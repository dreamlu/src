package agency

import (
	"xqd/util/db"
)

/*机构模型*/
type Agency struct {
	ID        int64  `json:"id"`
	Name      string `json:"name"`
	TeacherID int64  `json:"teacher_id"`	//校长id
}

/*机构班级返回模型*/
type AgencyInfo struct {
	ID        int64  `json:"id"`
	Name      string `json:"name"`
	TeacherID int64  `json:"-"`	//不返回
	Classroom	[]Classroom
}

/*班级下拉框模型*/
type Classroom struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`       //班级名
}

//获得机构下拉框
func GetAgency() interface{} {

	var ay []AgencyInfo
	sql := "select a.id,a.name,a.teacher_id from `agency` a inner join `teacher` b on a.teacher_id=b.id"
	info := db.GetDataBySql(&ay,sql)
	//后台用,优化？不存在的～～
	//查询机构对应的校长
	for k,v := range ay{
		db.DB.Raw("select id,name from `classroom` where parent_id=?",v.TeacherID).Scan(&ay[k].Classroom)
	}
	return info
}