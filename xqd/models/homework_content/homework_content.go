package homework_content

import "xqd/util/db"

/*作业内容管理*/
type HomeworkContent struct {
	ID         int64  `json:"id"`
	Name       string `json:"name"`        //名称
	Voice      string `json:"voice"`       //语音
	Content    string `json:"content"`     //内容
	HomeworkId int64  `json:"homework_id"` //作业id
}

//获得作业内容,根据id
func GetHomeworkContentById(id string) interface{} {

	db.DB.AutoMigrate(&HomeworkContent{})
	var homework_content = HomeworkContent{}
	return db.GetDataById(&homework_content, id)
}

//获得作业内容,分页/查询
func GetHomeworkContentBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&HomeworkContent{})
	var homework_contents = []*HomeworkContent{}
	return db.GetDataBySearch(HomeworkContent{}, &homework_contents, "homework_content", args) //匿名HomeworkContent{}
}

//删除作业内容,根据id
func DeleteHomeworkContentByid(id string) interface{} {

	return db.DeleteDataByName("homework_content", "id", id)
}

//修改作业内容
func UpdateHomeworkContent(args map[string][]string) interface{} {

	return db.UpdateData("homework_content", args)
}

//创建作业内容
func CreateHomeworkContent(args map[string][]string) interface{} {

	return db.CreateData("homework_content", args)
}
