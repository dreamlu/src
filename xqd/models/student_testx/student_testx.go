package student_test

import (
	"xqd/util/db"
	"xqd/util/lib"
)

/*学生测试模板*/
type StudentTest struct {
	ID        int    `json:"id"`
	StudentID string `json:"student_id"` //学生测试名
	TestID    int    `json:"test_id"`    //棋力值
	Content   string `json:"content"`    //测试数据棋盘内容
}

/*返回的测试内容*/
type TestContent struct {
	ID      int64  `json:"id"`
	Name    string `json:"name"`    //名称
	Voice   string `json:"voice"`   //语音
	Content string `json:"content"` //内容
	Score	int64 `json:"-"`
}

//测试分数，随机测试
type TestX struct {
	lib.GetInfoN
	Score int64 `json:"score"`
}

//var flag = 0//防止没数据时无限递归
// 随机获得一个获得学生测试
func GetStudentTest(id string) interface{} {

	var testcontent []TestContent
	sql := `select a.id,b.name,b.voice,b.content,a.score
		from
		(#随机获得一个测试题
		select
		id,name,score
		from test
		#条件：完成作业数==测试数
		#根据测试数找到对应最大id
		#id < 测试数最大id
		where id <= (select id from test a where (select count(*) from test where id<=a.id)=(
		select count(distinct a.id) from homework a
		inner join student_homework b
		on a.id=b.homework_id
		where student_id=?))
		order by rand()
		limit 0,1) a
		inner join test_content b
		on a.id=b.test_id`

	var info interface{}
	var getinfo lib.GetInfoN

	dba := db.DB.Raw(sql, id).Scan(&testcontent)
	num := dba.RowsAffected

	//有数据是返回相应信息
	if dba.Error != nil {
		info = lib.GetSqlError(dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		////没有数据，假随机，删除做的测试记录
		//db.DB.Exec("delete from `student_test` where student_id=?",id)
		//if flag == 1 {
		//	return nil
		//}
		//flag ++
		//GetStudentTest(id)

		info = lib.MapNoResult
	} else {
		//统计页码等状态
		getinfo.Status = lib.CodeSuccess
		getinfo.Msg = lib.MsgSuccess
		getinfo.Data = testcontent //数据

		var stest TestX
		stest.GetInfoN = getinfo
		//查询分数
		stest.Score = testcontent[0].Score
		info = stest
	}
	return info
}

//创建学生测试
func CreateStudentTest(args map[string][]string) interface{} {

	return db.CreateData("student_test", args)
}
