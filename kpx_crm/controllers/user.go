package controllers

import (
	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"kpx_crm/models"
	"kpx_crm/util/file"
	"kpx_crm/util/lib"
	"net/http"
)

//顾问id和姓名
func GetUser(u *gin.Context){
	ss := models.GetUser()
	u.JSON(http.StatusOK, ss)
}

//用户批量导入
func ExportUser(u *gin.Context){
	ss := models.ExportUser()
	u.JSON(http.StatusOK, ss)
}

//用户批量导入
func ImportUser(u *gin.Context){
	filename := file.GetUpoadFile(u)
	ss := models.ImportUser(filename)
	u.JSON(http.StatusOK, ss)
}

//根据id获得用户对应下级用户树
func GetChildrenById(u *gin.Context) {
	id := u.Query("id")
	ss := models.GetChildrenById(id)
	u.JSON(http.StatusOK, ss)
}

//根据id获得用户获取
func GetUserById(u *gin.Context) {
	id := u.Query("id")
	ss := models.GetUserById(id)
	u.JSON(http.StatusOK, ss)
}

//用户信息分页
func GetUserBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := models.GetUserBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//用户信息删除
func DeleteUserById(u *gin.Context) {
	id := u.Param("id")
	ss := models.DeleteUserById(id)
	u.JSON(http.StatusOK, ss)
}

//用户信息修改
func UpdateUser(u *gin.Context) {
	phone := u.PostForm("phone")
	valid := validation.Validation{}
	valid.Mobile(phone, "phone")

	if valid.HasErrors() && phone != "" {
		u.JSON(http.StatusOK, lib.MapPhone)
	}else {
		u.Request.ParseForm()
		values := u.Request.Form //在使用之前需要调用ParseForm方法
		ss := models.UpdateUser(values)
		u.JSON(http.StatusOK, ss)
	}
}

//新增用户信息
func CreateUser(u *gin.Context) {

	number := u.PostForm("number")
	if string([]byte(number)[:1]) == "0" {
		u.JSON(http.StatusOK, lib.GetMapDataError(lib.CodeText, "员工编号为数字类型"))
		return
	}

	phone := u.PostForm("phone")
	valid := validation.Validation{}
	valid.Mobile(phone, "phone")

	if valid.HasErrors() && phone != "" {
		u.JSON(http.StatusOK, lib.MapPhone)
	}else {
		u.Request.ParseForm()
		values := u.Request.Form //在使用之前需要调用ParseForm方法
		ss := models.CreateUser(values)
		u.JSON(http.StatusOK, ss)
	}
}
