package bascore

import (
	"github.com/gin-gonic/gin"
	"kpx_crm/models/bascore"
	"kpx_crm/util/lib"
	"net/http"
	"strconv"
)

func GetBascoreBySearch(u *gin.Context) {
	user_id := u.Query("user_id")
	date := u.Query("date")
	clientPage,_ := strconv.Atoi(u.Query("clientPage"))
	everyPage,_ := strconv.Atoi(u.Query("everyPage"))

	flag := u.Query("flag")
	var ss interface{}
	switch flag {
	case "0":
		ss = bascore.GetBascoreBySearchV2(user_id, date , clientPage, everyPage)	//客户量表
	case "1":
		ss = bascore.GetStageBySearchV2(user_id, date , clientPage, everyPage) //客户阶段比
	case "2":
		ss = bascore.GetStageFailBySearchV2(user_id, date , clientPage, everyPage)//客户失效阶段比
	case "3":
		ss = bascore.GetStageAdBySearchV2(user_id, date , clientPage, everyPage) //进阶转化率
	case "4":
		ss = bascore.GetStageJDBySearchV2(user_id, date , clientPage, everyPage)
	default:
		ss = lib.MapNoArgs
	}

	u.JSON(http.StatusOK, ss)
}