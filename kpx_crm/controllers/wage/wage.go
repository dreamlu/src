package wage

import (
	"github.com/gin-gonic/gin"
	"kpx_crm/models/wage"
	"kpx_crm/util/lib"
	"net/http"
	"strconv"
)

func GetWageBySearch(u *gin.Context) {
	user_id := u.Query("user_id")
	date := u.Query("date")
	clientPage,_ := strconv.Atoi(u.Query("clientPage"))
	everyPage,_ := strconv.Atoi(u.Query("everyPage"))

	flag := u.Query("flag")
	var ss interface{}
	switch flag {
	case "0":	//工资绩效
		ss = wage.GetWageBySearch(user_id,date,clientPage,everyPage)
	case "1"://年终奖
		ss = wage.GetWageBySearch2(user_id,date,clientPage,everyPage)
	case "2": //学期奖励
		ss = wage.GetWageBySearch3(user_id,date,clientPage,everyPage)
	default:
		ss = lib.MapNoArgs
	}

	u.JSON(http.StatusOK, ss)
}