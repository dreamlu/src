package assets

import (
	"github.com/gin-gonic/gin"
	"kpx_crm/models/assets"
	"net/http"
)

//根据id获得成本获取
func GetCostById(u *gin.Context) {
	id := u.Query("id")
	ss := assets.GetCostById(id)
	u.JSON(http.StatusOK, ss)
}

//成本信息分页
func GetCostBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := assets.GetCostBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//成本信息删除
func DeleteCostById(u *gin.Context) {
	id := u.Param("id")
	ss := assets.DeleteCostById(id)
	u.JSON(http.StatusOK, ss)
}

//成本信息修改
func UpdateCost(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := assets.UpdateCost(values)
	u.JSON(http.StatusOK, ss)
}

//新增成本信息
func CreateCost(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := assets.CreateCost(values)
	u.JSON(http.StatusOK, ss)
}
