package assets

import (
	"github.com/gin-gonic/gin"
	"kpx_crm/models/assets"
	"net/http"
)

//根据id获得成本获取
func GetTrancostById(u *gin.Context) {
	id := u.Query("id")
	ss := assets.GetTrancostById(id)
	u.JSON(http.StatusOK, ss)
}

//成本信息分页
func GetTrancostBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := assets.GetTrancostBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//成本信息删除
func DeleteTrancostById(u *gin.Context) {
	id := u.Param("id")
	ss := assets.DeleteTrancostById(id)
	u.JSON(http.StatusOK, ss)
}

//成本信息修改
func UpdateTrancost(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := assets.UpdateTrancost(values)
	u.JSON(http.StatusOK, ss)
}

//新增成本信息
func CreateTrancost(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := assets.CreateTrancost(values)
	u.JSON(http.StatusOK, ss)
}
