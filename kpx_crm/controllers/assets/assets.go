package assets

import (
	"github.com/gin-gonic/gin"
	"kpx_crm/models/assets"
	"net/http"
	"strconv"
)

//用户信息分页(未分发的客户数据)
func GetAssetsBySearch(u *gin.Context) {
	user_id := u.Query("user_id")
	date := u.Query("date")
	clientPage,_ := strconv.Atoi(u.Query("clientPage"))
	everyPage,_ := strconv.Atoi(u.Query("everyPage"))
	ss := assets.GetAssetsBySearchV2(user_id, date , clientPage, everyPage)
	u.JSON(http.StatusOK, ss)
}