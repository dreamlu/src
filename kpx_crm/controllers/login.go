package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"kpx_crm/controllers/captcha"
	"kpx_crm/util"
	"kpx_crm/util/db"
	"kpx_crm/util/lib"
	"net/http"
	"strconv"
)

type User struct {
	Id           int    `json:"id"`
	Number       int    `json:"number"`
	Username     string `json:"username"`     //用户真实名称
	Role_id      int    `json:"role_id"`      //角色id
	Userpassword string `json:"userpassword"` //密码
}

//登录
func Login(u *gin.Context) {
	var info interface{}
	r := u.Request
	//w := u.Writer

	key := r.FormValue("key")
	code := r.FormValue("code") // 验证汉子编码问题

	suc, _ := captcha.Ccaptcha.Verify(key, code)
	if false == suc{
		info = lib.MapCaptcha
	}else {
		number := u.PostForm("number")
		userpassword := u.PostForm("userpassword")
		var user User
		dba := db.DB.Raw("SELECT id,username,role_id,userpassword FROM `user` WHERE number = ?", number).Scan(&user)
		num := dba.RowsAffected
		if dba.Error == nil && num > 0 {
			userpassword = util.AesEn(userpassword)
			if user.Userpassword == userpassword {
				//name	string	cookie_key
				//value	string	cookie_val
				//maxAge	int	生存期（秒）
				//path	string	有效域
				//domain	string	有效域名
				//secure	bool	是否安全传输 是则只走https
				//httpOnly	bool	是否仅网络使用 是则js无法获取
				// encript
				//字符为16的倍数
				role_id, err := util.Encrypt([]byte(strconv.Itoa(user.Role_id)+"lloveyouchinese"))
				if err != nil {
					fmt.Println("Encrypt: ", err)
					return
				}
				//u.SetCookie("uid", strconv.Itoa(user.Id), 1800, "/", "*", false, true)
				u.SetCookie("role_id", string(role_id), 24*3600, "/", "*", false, true)
				//this.SetSecureCookie(beego.AppConfig.String("secertkey"), "uid", strconv.Itoa(v.Id))
				info = map[string]interface{}{"status": "200", "msg": "请求成功", "uid": strconv.Itoa(user.Id), "username": user.Username, "role_id": strconv.Itoa(user.Role_id)}
			} else {
				info = lib.MapCountErr
			}
		} else {
			info = lib.MapNoCount
		}
	}

	u.JSON(http.StatusOK, info)
}
