package client

import (
	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"kpx_crm/models/client"
	"kpx_crm/util/file"
	"kpx_crm/util/lib"
	"net/http"
	"strconv"
)

//用户批量导入
func ExportClient(u *gin.Context){
	ss := client.ExportClient()
	u.JSON(http.StatusOK, ss)
}

//用户批量导入
func ImportClient(u *gin.Context){
	filename := file.GetUpoadFile(u)
	u.Request.ParseForm()
	values := u.Request.Form
	ss := client.ImportClient(filename,values)
	u.JSON(http.StatusOK, ss)
}

//客户分发
func DistributionClient(u *gin.Context) {
	user_id := u.PostForm("user_id")
	client_ids := u.PostForm("client_ids")
	ss := client.DistributionClient(user_id,client_ids)
	u.JSON(http.StatusOK, ss)
}

//根据id获得用户获取
func GetClientById(u *gin.Context) {
	id := u.Query("id")
	ss := client.GetClientById(id)
	u.JSON(http.StatusOK, ss)
}

//用户信息分页(未分发的客户数据)
func GetClientBySearchV2(u *gin.Context) {
	user_id := u.Query("user_id")
	username := u.Query("username")
	clientPage,_ := strconv.Atoi(u.Query("clientPage"))
	everyPage,_ := strconv.Atoi(u.Query("everyPage"))
	kind := u.Query("kind") //客户种类
	ss := client.GetClientBySearchV3(kind,user_id,username,clientPage,everyPage)
	u.JSON(http.StatusOK, ss)
}

//用户信息分页(未分发的客户数据)
func GetClientBySearch(u *gin.Context) {
	clientPage,_ := strconv.Atoi(u.Query("clientPage"))
	everyPage,_ := strconv.Atoi(u.Query("everyPage"))
	key := u.Query("key")
	flag := u.Query("flag")
	ss := client.GetClientBySearch(clientPage,everyPage,key,flag)
	u.JSON(http.StatusOK, ss)
}

//用户信息删除
func DeleteClientById(u *gin.Context) {
	id := u.Param("id")
	ss := client.DeleteClientById(id)
	u.JSON(http.StatusOK, ss)
}

//用户信息修改
func UpdateClient(u *gin.Context) {
	phone := u.PostForm("phone")
	valid := validation.Validation{}
	valid.Mobile(phone, "phone")

	if valid.HasErrors() && phone != "" {
		u.JSON(http.StatusOK, lib.MapPhone)
	}else {
		u.Request.ParseForm()
		values := u.Request.Form //在使用之前需要调用ParseForm方法
		ss := client.UpdateClient(values)
		u.JSON(http.StatusOK, ss)
	}
}

//新增用户信息
func CreateClient(u *gin.Context) {
	//验证手机号
	phone := u.PostForm("phone")
	valid := validation.Validation{}
	valid.Mobile(phone, "phone")

	if valid.HasErrors() && phone != ""{
		u.JSON(http.StatusOK, lib.MapPhone)
	}else {
		u.Request.ParseForm()
		values := u.Request.Form //在使用之前需要调用ParseForm方法
		ss := client.CreateClient(values)
		u.JSON(http.StatusOK, ss)
	}
}

//同步网站或小程序客户数据
func SynchronizeClientData(u *gin.Context) {
	ss := client.SynchronizeClientData()
	u.JSON(http.StatusOK, ss)
}
