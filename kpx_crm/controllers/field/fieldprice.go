package field

import (
	"github.com/gin-gonic/gin"
	"kpx_crm/models/field"
	"net/http"
)

//根据id获得意向、面询、对接价格获取
func GetFieldpriceById(u *gin.Context) {
	id := u.Query("id")
	ss := field.GetFieldpriceById(id)
	u.JSON(http.StatusOK, ss)
}

//意向、面询、对接价格信息分页
func GetFieldprice(u *gin.Context) {
	ss := field.GetFieldprice()
	u.JSON(http.StatusOK, ss)
}

//意向、面询、对接价格信息修改
func UpdateFieldprice(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := field.UpdateFieldprice(values)
	u.JSON(http.StatusOK, ss)
}