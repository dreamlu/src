package field

import (
	"github.com/gin-gonic/gin"
	"kpx_crm/models/field"
	"net/http"
)

//根据id获得字段设置获取
func GetFieldByFlag(u *gin.Context) {
	flag := u.Query("flag")
	ss := field.GetFieldByFlag(flag)
	u.JSON(http.StatusOK, ss)
}

//字段设置信息删除
func DeleteFieldById(u *gin.Context) {
	id := u.Param("id")
	ss := field.DeleteFieldById(id)
	u.JSON(http.StatusOK, ss)
}

//字段设置信息修改
func UpdateField(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := field.UpdateField(values)
	u.JSON(http.StatusOK, ss)
}

//新增字段设置信息
func CreateField(u *gin.Context) {
	//验证手机号
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := field.CreateField(values)
	u.JSON(http.StatusOK, ss)
}
