package permission

import (
	"github.com/gin-gonic/gin"
	"kpx_crm/models/permission"
	"net/http"
)

//根据角色id获得上级所有用户
func GetParentUsers(u *gin.Context) {
	id := u.Query("id")
	ss := permission.GetParentUsers(id)
	u.JSON(http.StatusOK, ss)
}

//获得所有角色
func GetRoles(u *gin.Context) {
	ss := permission.GetRoles()
	u.JSON(http.StatusOK, ss)
}
