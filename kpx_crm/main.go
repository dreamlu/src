package main

import (
	"github.com/gin-gonic/gin"
	"io"
	"kpx_crm/conf"
	"kpx_crm/routers"
	_ "kpx_crm/util/db"
	"os"
	"time"
)

func main() {
	gin.SetMode(gin.DebugMode)
	//路由日志
	f,_ := os.Create("log/"+time.Now().Format("2006-01-02")+"-gin.log")
	/*if err != nil{
		f, _ = os.Create("log/gin.log")
	}*/
	gin.DefaultWriter = io.MultiWriter(f)
	router := routers.SetRouter()
	// Listen and Server in 0.0.0.0:8080
	router.Run(":" + conf.GetConfigValue("http_port"))
}
