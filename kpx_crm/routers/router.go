package routers

import (
	"fmt"
	"github.com/casbin/casbin"
	"github.com/gin-gonic/gin"
	"kpx_crm/controllers"
	"kpx_crm/controllers/assets"
	"kpx_crm/controllers/bascore"
	"kpx_crm/controllers/basic"
	"kpx_crm/controllers/captcha"
	"kpx_crm/controllers/client"
	"kpx_crm/controllers/field"
	"kpx_crm/controllers/permission"
	"kpx_crm/controllers/wage"
	"kpx_crm/util/file"
	"kpx_crm/util/lib"
	"kpx_crm/util/str"
	"net/http"
	"strings"
)

func SetRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	router := gin.Default()
	str.MaxUploadMemory = router.MaxMultipartMemory
	//router.Use(CorsMiddleware())

	// load the casbin model and policy from files, database is also supported.
	//权限中间件
	e := casbin.NewEnforcer("conf/authz_model.conf", "conf/authz_policy.csv")
	router.Use(CheckLogin())
	router.Use(controllers.NewAuthorizer(e))

	//静态目录
	router.Static("api/v1/static", "static")

	// Ping test
	router.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
	//组的路由,version
	v1 := router.Group("/api/v1")
	{
		v := v1
		//网站基本信息
		v.GET("/basic/getbasic",basic.GetBasicInfo)
		//文件上传
		v.POST("/file/upload", file.UpoadFile)
		//验证码
		captchas := v.Group("/captcha")
		{
			captchas.GET("/getkey",captcha.GetKey)
			captchas.GET("/showimage",captcha.ShowImage)
			captchas.GET("/verify",captcha.Verify)
		}
		//用户
		user := v.Group("/user")
		{
			user.GET("/getbysearch", controllers.GetUserBySearch)
			user.GET("/getbyid", controllers.GetUserById)
			user.DELETE("/deletebyid/:id", controllers.DeleteUserById)
			user.POST("/create", controllers.CreateUser)
			user.PATCH("/update", controllers.UpdateUser)
			user.GET("/getchildren",controllers.GetChildrenById)
			user.POST("/login",controllers.Login)
			user.POST("/import",controllers.ImportUser)
			user.GET("/export",controllers.ExportUser)
			user.GET("/get",controllers.GetUser)
		}
		//同步小程序/网站数据
		v.GET("/sych", client.SynchronizeClientData)
		//客户管理
		clients := v.Group("/client")
		{
			clients.GET("/getbysearchv2", client.GetClientBySearchV2)
			clients.GET("/getbysearch", client.GetClientBySearch)
			clients.GET("/getbyid", client.GetClientById)
			clients.DELETE("deletebyid/:id", client.DeleteClientById)
			clients.POST("/create", client.CreateClient)
			clients.PATCH("/update", client.UpdateClient)
			clients.POST("/distribute",client.DistributionClient)
			clients.POST("/import",client.ImportClient)
			clients.GET("/export",client.ExportClient)
		}
		//字段设置
		fields := v.Group("/field")
		{
			fields.GET("/getbyflag", field.GetFieldByFlag)
			fields.DELETE("/deletebyid/:id", field.DeleteFieldById)
			fields.POST("/create", field.CreateField)
			fields.PATCH("/update", field.UpdateField)
		}
		fieldprices := v.Group("/fieldprice")
		{
			fieldprices.GET("/get",field.GetFieldprice)
			fieldprices.GET("/getbyid", field.GetFieldpriceById)
			fieldprices.PATCH("/update", field.UpdateFieldprice)
		}
		//用户角色(职务)
		roles := v.Group("/role")
		{
			roles.GET("/getroles", permission.GetRoles)
			roles.GET("/getparent", permission.GetParentUsers)
		}
		//资产合计
		assetss := v.Group("/assets")
		{
			assetss.GET("/getbysearch", assets.GetAssetsBySearch)
		}
		//成本设置
		costs := v.Group("/cost")
		{
			costs.GET("/getbysearch", assets.GetCostBySearch)
			costs.GET("/getbyid", assets.GetCostById)
			costs.DELETE("/deletebyid/:id", assets.DeleteCostById)
			costs.POST("/create", assets.CreateCost)
			costs.PATCH("/update", assets.UpdateCost)
		}
		//交通成本设置
		trancosts := v.Group("/trancost")
		{
			trancosts.GET("/getbysearch", assets.GetTrancostBySearch)
			trancosts.GET("/getbyid", assets.GetTrancostById)
			trancosts.DELETE("/deletebyid/:id", assets.DeleteTrancostById)
			trancosts.POST("/create", assets.CreateTrancost)
			trancosts.PATCH("/update", assets.UpdateTrancost)
		}
		//平衡计分统计
		bascores := v.Group("/bascore")
		{
			bascores.GET("/getbysearch", bascore.GetBascoreBySearch)
		}
		//资产合计统计
		wages := v.Group("/wage")
		{
			wages.GET("/getbysearch",wage.GetWageBySearch)
		}
	}
	//不存在路由
	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{
			"status": 404,
			"msg":    "接口不存在->('.')/请求方法错误",
		})
	})
	return router
}

/*登录失效验证*/
func CheckLogin() gin.HandlerFunc{
	return func(c *gin.Context) {
		path := c.Request.URL.String()
		if !strings.Contains(path,"login") && !strings.Contains(path,"/captcha/getkey") && !strings.Contains(path,"/captcha/showimage") {
			_,err := c.Cookie("role_id")
			if err != nil{
				c.Abort()
				c.JSON(http.StatusOK,lib.MapNoToken)
			}
		}
	}
}

/*跨域解决方案*/
func CorsMiddleware() gin.HandlerFunc {

	return func(c *gin.Context) {
		method := c.Request.Method

		origin := c.Request.Header.Get("Origin")
		var headerKeys []string
		for k, _ := range c.Request.Header {
			headerKeys = append(headerKeys, k)
		}
		headerStr := strings.Join(headerKeys, ", ")
		if headerStr != "" {
			headerStr = fmt.Sprintf("access-control-allow-origin, access-control-allow-headers, %s", headerStr)
		} else {
			headerStr = "access-control-allow-origin, access-control-allow-headers"
		}
		if origin != "" {
			//下面的都是乱添加的-_-~
			// c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
			c.Header("Access-Control-Allow-Origin", "http://fe.com")
			c.Header("Access-Control-Allow-Headers", headerStr)
			c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
			// c.Header("Access-Control-Allow-Headers", "Authorization, Content-Length, X-CSRF-Token, Accept, Origin, Host, Connection, Accept-Encoding, Accept-Language,DNT, X-CustomHeader, Keep-Alive, User-Agent, X-Requested-With, If-Modified-Since, Cache-Control, Content-Type, Pragma")
			c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
			// c.Header("Access-Control-Max-Age", "172800")
			c.Header("Access-Control-Allow-Credentials", "true")
			c.Set("content-type", "application/json")
		}

		//放行所有OPTIONS方法
		if method == "OPTIONS" {
			c.JSON(http.StatusOK, "Options Request!")
		}

		c.Next()
	}
}
