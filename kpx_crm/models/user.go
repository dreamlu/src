package models

import (
	"github.com/tealeg/xlsx"
	"kpx_crm/models/client"
	"kpx_crm/util"
	"kpx_crm/util/db"
	"kpx_crm/util/lib"
	"reflect"
	"strconv"
	"time"
)

/*用户,员工*/
type User struct {
	ID           uint          `json:"id" gorm:"primary_key"`
	Username     string        `json:"username"`     //真实姓名
	Number       int           `json:"number"`       //员工编号
	Userpassword string        `json:"userpassword"` //密码
	Phone        string        `json:"phone"`        //电话
	Parent_id    int           `json:"parent_id"`    //上级领导id
	Role_id      int           `json:"role_id"`      //角色职位
	Createtime   util.JsonTime `json:"createtime"`   //创建时间
}

/*用户,员工,包含上级领导姓名*/
type UserLd struct {
	//User
	ID           int           `json:"id" gorm:"primary_key"`
	Username     string        `json:"username"`     //真实姓名
	Number       int           `json:"number"`       //员工编号
	Userpassword string        `json:"userpassword"` //密码
	Phone        string        `json:"phone"`        //电话
	Parent_id    int           `json:"parent_id"`    //上级领导id
	Role_id      int           `json:"role_id"`      //角色职位
	Createtime   util.JsonTime `json:"createtime"`   //创建时间
	Rolename     string        `json:"rolename"`     //角色,职位
	Leadername   string        `json:"leadername"`
}

/*用户上下级*/
type UserMenu struct {
	Id int `json:"id"`
	//Parent_id int //父级菜单
	Role_id  int        `json:"-"`
	Rolename string     `json:"rolename"`
	Username string     `json:"username"` //菜单
	Children []UserMenu `json:"children"` //下一级菜单
}

/*用户id,姓名*/
type UserN struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
}

type Value struct {
	Value string
}

//获得用户id和姓名,顾问角色
func GetUser() interface{} {
	var info interface{}
	var getinfo lib.GetInfoN
	var users []UserN
	var dba = db.DB.Raw("select id,username from `user` where role_id=5").Scan(&users)
	num := dba.RowsAffected
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		return info
	} else if num == 0 && dba.Error == nil {
		info = lib.MapNoResult
	} else {
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = users //数据

		info = getinfo
	}
	return info
}

//用户批量导出
func ExportUser() interface{} {
	var info interface{}
	var users []*UserLd

	//用select *我开心
	dba := db.DB.Raw("select *from `user`").Scan(&users)
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		return info
	}
	for _, v := range users {
		db.DB.Raw("select username leadername from `user` where id=?", v.Parent_id).Scan(&v)
		db.DB.Raw("select rolename from `role` where id=?", v.Role_id).Scan(&v)
	}
	//导出用户数据
	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell
	var err error

	file = xlsx.NewFile()
	sheet, _ = file.AddSheet("Sheet1")

	for _, v := range users {
		row = sheet.AddRow()
		ref := reflect.ValueOf(v).Elem()
		col := reflect.TypeOf(v).Elem()
		//去id
		for i := 1; i < ref.NumField(); i++ {
			column := col.Field(i).Name
			var value string
			t := ref.Field(i)
			switch t.Type().String() {
			case "int":
				value = strconv.FormatInt(t.Int(), 10)
			case "string":
				value = t.String()
			}

			switch column {
			case "Userpassword":
				continue
			case "Parent_id":
				continue
			case "Role_id":
				continue
			case "Createtime":
				continue
			}

			cell = row.AddCell()
			cell.Value = value
		}
	}

	filepath := "static/file/用户数据.xlsx"
	err = file.Save(filepath)
	if err != nil {
		info = lib.MapDataError{224, err.Error()}
	} else {
		info = map[string]interface{}{"status": lib.CodeSuccess, "msg": lib.MsgSuccess, "filepath": filepath}
	}
	return info
}

//用户批量导入
func ImportUser(filename string) interface{} {
	var info interface{}
	xlFile, _ := xlsx.OpenFile(filename)

	sql := "insert `user`("

	//sql拼接
	for _, sheet := range xlFile.Sheets {
		//去掉第一行记录
		for i := 1; i < len(sheet.Rows); i++ {
			row := sheet.Rows[i]
			if i == 1 {
				for _, cell := range row.Cells {
					text := cell.String()
					sql += text + ","
				}
				c := []byte(sql)
				sql = string(c[:len(c)-1]) + ") value(" //去掉点
				continue
			}

			for k, cell := range row.Cells {
				text := cell.String()

				switch k {
				//上级领导,替换成id
				case 4:
					var value Value
					db.DB.Raw("select id as value from `user` where number=?", cell.String()).Scan(&value)
					text = value.Value
				//角色,职位,替换成id
				case 5:
					var value Value
					db.DB.Raw("select id as value from `role` where rolename=?", cell.String()).Scan(&value)
					text = value.Value
				}

				sql += "'" + text + "',"
			}
			c := []byte(sql)
			sql = string(c[:len(c)-1]) + "),(" //去掉点
		}
	}
	c := []byte(sql)
	sql = string(c[:len(c)-2])
	//批量插入
	dba := db.DB.Exec(sql)

	num := dba.RowsAffected
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapExistOrNo
	} else {
		info = lib.MapCreate
	}
	return info
}

//用户下级递归查询
func GetChildrenMenuByParentId(parent_id int) []UserMenu {
	var menus []UserMenu
	dba := db.DB.Raw("select id,role_id,username from `user` where parent_id=?", parent_id).Scan(&menus)
	num := dba.RowsAffected
	if num > 0 {
		for k, v := range menus {
			//角色id
			db.DB.Raw("select rolename from `role` where id=?", v.Role_id).Scan(&menus[k])
			menusv := GetChildrenMenuByParentId(v.Id)
			for _, v2 := range menusv {
				menus[k].Children = append(menus[k].Children, v2)
			}
		}
	}
	return menus
}

func GetChildrenById(id string) interface{} {
	var info interface{}
	var UserMenu []UserMenu
	var getinfo lib.GetInfoN
	dba := db.DB.Raw("select id,role_id,username from `user` where id=?", id).Scan(&UserMenu)
	num := dba.RowsAffected
	if num > 0 && dba.Error == nil {
		for k, v := range UserMenu {
			db.DB.Raw("select rolename from `role` where id=?", v.Role_id).Scan(&UserMenu[k])
			menusv := GetChildrenMenuByParentId(v.Id)
			for _, v2 := range menusv {
				UserMenu[k].Children = append(UserMenu[k].Children, v2)
			}
		}
		//统计页码等状态
		getinfo.Status = lib.CodeSuccess
		getinfo.Msg = lib.MsgSuccess
		getinfo.Data = UserMenu //数据
		info = getinfo
	} else if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapNoResult
	}
	return info
}

//获得用户,根据id
func GetUserById(id string) interface{} {

	db.DB.AutoMigrate(&User{})
	var user = User{}
	return db.GetDataById(&user, id)
}

//获得用户,分页/查询
func GetUserBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&User{})
	var users = []*UserLd{}
	info := db.GetDataBySearch(User{}, &users, "user", args)
	for _, v := range users {
		db.DB.Raw("select username leadername from `user` where id=?", v.Parent_id).Scan(&v)
		db.DB.Raw("select rolename from `role` where id=?", v.Role_id).Scan(&v)
	}
	return info //匿名User{}
}

//删除用户,根据id
func DeleteUserById(id string) interface{} {

	switch id {
	case "1":
		return lib.GetMapDataError(lib.CodeText, "根管理员,禁止删除")
	}

	var info interface{}
	//判断用户是否有下级,无,则为顾问
	var user User
	dba := db.DB.Where("parent_id = ?",id).First(&user)
	num := dba.RowsAffected
	if num == 0 && dba.Error != nil {
		//判断顾问是否拥有客户,有,则禁止删除
		var userclient client.Userclient
		dba = db.DB.Where("user_id = ?",id).First(&userclient)
		num = dba.RowsAffected
		if num == 0 && dba.Error != nil {
			//查询数据为0且sql操作正确,进行删除操作
			info = db.DeleteDataByName("user", "id", id)
			//同时删除该用户的成本数据
			db.DeleteDataByName("trancost", "user_id", id)
		} else {
			var mapError lib.MapDataError
			mapError.Status = lib.CodeNoDelete
			mapError.Msg = "该顾问下拥有客户,禁止删除" //自定义error
			info = mapError
		}


	} else {
		var mapError lib.MapDataError
		mapError.Status = lib.CodeNoDelete
		mapError.Msg = "该用户拥有下级员工,禁止删除" //自定义error
		info = mapError
	}

	return info
}

//修改用户
func UpdateUser(args map[string][]string) interface{} {

	return db.UpdateData("user", args)
}

//创建用户
func CreateUser(args map[string][]string) interface{} {
	//后台添加创建时间
	createtime := time.Now().Format("2006-01-02 15:04:05")
	args["createtime"] = append(args["createtime"], createtime)

	return db.CreateData("user", args)
}
