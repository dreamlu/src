package assets

import (
	"kpx_crm/util/db"
)

/*交通成本设置*/
type Trancost struct {
	ID       int64   `json:"id"`
	Date     string  `json:"date"`
	Trancost float64 `json:"trancost"` //交通成本
	User_id  int64   `json:"user_id"`
	Username string  `json:"username"`
}

//获得成本,根据id
func GetTrancostById(id string) interface{} {

	db.DB.AutoMigrate(&Trancost{})
	var trancost = Trancost{}
	info := db.GetDataById(&trancost, id)
	db.DB.Raw("select username from `user` where id=?",trancost.User_id).Scan(&trancost)
	return info
}

//获得成本,分页/查询
func GetTrancostBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Trancost{})
	var trancost = []*Trancost{}
	info := db.GetDataBySearch(Trancost{}, &trancost, "trancost", args)
	for _,v := range trancost{
		db.DB.Raw("select username from `user` where id=?",v.User_id).Scan(&v)
	}
	return info //匿名Trancost{}
}

//删除成本,根据id
func DeleteTrancostById(id string) interface{} {

	return db.DeleteDataByName("trancost", "id", id)
}

//修改成本
func UpdateTrancost(args map[string][]string) interface{} {

	return db.UpdateData("trancost", args)
}

//创建成本
func CreateTrancost(args map[string][]string) interface{} {

	return db.CreateData("trancost", args)
}
