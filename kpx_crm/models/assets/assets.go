package assets

import (
	"kpx_crm/models/client"
	"kpx_crm/util/db"
	"kpx_crm/util/lib"
	"strings"
)

/*资产合计*/
type Assets struct {
	Year        string  `json:"year"`        //`json:"年份"`
	Month       string  `json:"month"`       //`json:"月份"`
	Sumclients  float64 `json:"sumclients"`  //`json:"总客户数"`
	Totalprice  float64 `json:"totalprice"`  //`json:"资产总价"`
	Intentnum   float64 `json:"intentnum"`   //`json:"意向客户数"`
	Intentprice float64 `json:"intentprice"` //`json:"意向客户总价"`
	Inqnum      float64 `json:"inqnum"`      //`json:"面询客户数"`
	Inqprice    float64 `json:"inqprice"`    //`json:"面询客户总价"`
	Docknum     float64 `json:"docknum"`     //`json:"对接客户数"`
	Dockprice   float64 `json:"dockprice"`   //`json:"对接客户总价"`
	Rentcost    float64 `json:"rentcost"`    //房租成本
	Trancost    float64 `json:"trancost"`    //交通成本
	Officecost  float64 `json:"officecost"`  //办公成本
	Hycost      float64 `json:"hycost"`      //水电成本
	Othercost   float64 `json:"othercost"`   //其他成本
	//Depnum      float64 `json:"depnum"`      //`json:"定金客户数"`
	//Signnum     float64 `json:"signnum"`     //`json:"签约客户数"`
	//Failnum     float64 `json:"failnum"`     //`json:"失效客户数"`
}

//顾问模型,1级
type Client1 struct {
	Username
	Assets
}

//经理模型,2级
type Client2 struct {
	Username
	Assets
}

//总监模型,3级
type Client3 struct {
	Username
	Assets
}

//总经理模型,4级
type Client4 struct {
	Username
	Assets
}

type Username struct {
	Username1 string `json:"username1"`
	Username2 string `json:"username2"`
	Username3 string `json:"username3"`
	Username4 string `json:"username4"`
}

//获得资产合计,分页/查询
func GetAssetsBySearchV2(user_id, date string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var value client.Value
	//查询固定角色id
	db.DB.Raw("select role_id as value from `user` where id=?", user_id).Scan(&value)
	switch value.Value {
	case "5": //顾问
		var client1 []Client1
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=(select parent_id from user where id=c.parent_id))) as username1,
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username2,
		(select username from user where id=(c.parent_id)) as username3,
		c.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		sum(intentprice)+sum(inqprice)+sum(dockprice) as totalprice,
		count(case when clientkind=0 then 1 else null end) as intentnum,
		sum(intentprice) as intentprice,
		count(case when clientkind=1 then 1 else null end) as inqnum,
		sum(inqprice) as inqprice,
		count(case when clientkind=2 then 1 else null end) as docknum,
		sum(dockprice) as dockprice,
		round((select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as rentcost,
		round((select trancost from trancost where date=date_format(updatetime,'%Y-%m') and user_id=b.user_id),2) as trancost,
		round((select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as officecost,
		round((select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as hycost,
		round((select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as othercost
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month #,c.username,rentcost,trancost,officecost,hycost,othercost 
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month ) a`

		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql,user_id,(clientPage-1)*everyPage,everyPage).Scan(&client1)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client1 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "4": //经理
		var client2 []Client2
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username1,
		(select username from user where id=(c.parent_id)) as username2,
		c.username username3,
		d.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		sum(intentprice)+sum(inqprice)+sum(dockprice) as totalprice,
		count(case when clientkind=0 then 1 else null end) as intentnum,
		sum(intentprice) as intentprice,
		count(case when clientkind=1 then 1 else null end) as inqnum,
		sum(inqprice) as inqprice,
		count(case when clientkind=2 then 1 else null end) as docknum,
		sum(dockprice) as dockprice,
		round((select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as rentcost,
		round((select trancost from trancost where date=date_format(updatetime,'%Y-%m') and user_id=b.user_id),2) as trancost,
		round((select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as officecost,
		round((select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as hycost,
		round((select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as othercost
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month #,c.username,d.username,rentcost,trancost,officecost,hycost,othercost 
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql,user_id,(clientPage-1)*everyPage,everyPage).Scan(&client2)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client2 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "3": //总监
		var client3 []Client3
		sql := `SELECT
		(select username from user where id=(c.parent_id)) as username1,
		c.username username2,
		d.username username3,
		e.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		sum(intentprice)+sum(inqprice)+sum(dockprice) as totalprice,
		count(case when clientkind=0 then 1 else null end) as intentnum,
		sum(intentprice) as intentprice,
		count(case when clientkind=1 then 1 else null end) as inqnum,
		sum(inqprice) as inqprice,
		count(case when clientkind=2 then 1 else null end) as docknum,
		sum(dockprice) as dockprice,
		round((select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as rentcost,
		round((select trancost from trancost where date=date_format(updatetime,'%Y-%m') and user_id=b.user_id),2) as trancost,
		round((select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as officecost,
		round((select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as hycost,
		round((select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as othercost
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month #,c.username,d.username,e.username,rentcost,trancost,officecost,hycost,othercost 
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql,user_id,(clientPage-1)*everyPage,everyPage).Scan(&client3)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client3 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "2": //总经理
		var client4 []Client4
		sql := `SELECT
		c.username username1,
		d.username username2,
		e.username username3,
		f.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		sum(intentprice)+sum(inqprice)+sum(dockprice) as totalprice,
		count(case when clientkind=0 then 1 else null end) as intentnum,
		sum(intentprice) as intentprice,
		count(case when clientkind=1 then 1 else null end) as inqnum,
		sum(inqprice) as inqprice,
		count(case when clientkind=2 then 1 else null end) as docknum,
		sum(dockprice) as dockprice,
		round((select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as rentcost,
		round((select trancost from trancost where date=date_format(updatetime,'%Y-%m') and user_id=b.user_id),2) as trancost,
		round((select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as officecost,
		round((select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as hycost,
		round((select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime),2) as othercost
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id 
		and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month #,c.username,d.username,e.username,f.username,rentcost,trancost,officecost,hycost,othercost 
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql,user_id,(clientPage-1)*everyPage,everyPage).Scan(&client4)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client4 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	default:
		info = map[string]interface{}{"status": 222, "msg": "用户对应角色不存在"}
	}

	return info
}
