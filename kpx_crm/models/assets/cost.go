package assets

import (
	"fmt"
	"kpx_crm/util/db"
	"time"
)

/*成本设置*/
type Cost struct {
	ID         int64   `json:"id"`
	Date       string  `json:"date"`
	Rentcost   float64 `json:"rentcost"`
	Trancost   float64 `json:"trancost"`
	Officecost float64 `json:"officecost"`
	Hycost     float64 `json:"hycost"`
	Othercost  float64 `json:"othercost"`
}

/*时间格式化*/
type JsonTime time.Time

//实现它的json序列化方法
func (this JsonTime) MarshalJSON() ([]byte, error) {
	var stamp = fmt.Sprintf("\"%s\"", time.Time(this).Format("2006-01"))
	return []byte(stamp), nil
}

//获得成本,根据id
func GetCostById(id string) interface{} {

	db.DB.AutoMigrate(&Cost{})
	var cost = Cost{}
	return db.GetDataById(&cost, id)
}

//获得成本,分页/查询
func GetCostBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Cost{})
	var costs = []*Cost{}
	info := db.GetDataBySearch(Cost{}, &costs, "cost", args)
	return info //匿名Cost{}
}

//删除成本,根据id
func DeleteCostById(id string) interface{} {

	info := db.DeleteDataByName("cost", "id", id)
	return info
}

//修改成本
func UpdateCost(args map[string][]string) interface{} {

	return db.UpdateData("cost", args)
}

//创建成本
func CreateCost(args map[string][]string) interface{} {

	return db.CreateData("cost", args)
}
