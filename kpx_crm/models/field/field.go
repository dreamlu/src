package field

import (
	"kpx_crm/util/db"
)

/*字段模型*/
type Field struct {
	ID    int    `json:"id"`
	Field string `json:"field"` //字段值
	//字段标示,对接留学机构(0),对接作品集机构(1),对接语言机构(2),国家(3)
	//成本统计,房租成本
	Flag        int     `json:"-"`
	Intentprice float64 `json:"intentprice"` //意向价格
	Inqprice    float64 `json:"inqprice"`    //面询价格
	Dockprice   float64 `json:"dockprice"`   //对接机构
}

//获得字段,根据id
func GetFieldByFlag(flag string) interface{} {

	db.DB.AutoMigrate(&Field{})
	var field []Field
	return db.GetDataByName(&field, "flag", flag)
}

//删除字段,根据id
func DeleteFieldById(id string) interface{} {

	return db.DeleteDataByName("field", "id", id)
}

//修改字段
func UpdateField(args map[string][]string) interface{} {

	return db.UpdateData("field", args)
}

//创建字段
func CreateField(args map[string][]string) interface{} {
	//后台添加创建时间
	return db.CreateData("field", args)
}
