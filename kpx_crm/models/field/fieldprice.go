package field

import (
	"kpx_crm/util/db"
	"kpx_crm/util/lib"
)

/*意向、面询、对接,价格设置*/
type Fieldprice struct {
	ID          int     `json:"id"`
	Intentprice float64 `json:"intentprice"` //意向价格
	Inqprice    float64 `json:"inqprice"`     //面询价格
	Dockprice   float64 `json:"dockprice"`   //对接价格
}

//获得意向、面询、对接价格,根据id
func GetFieldpriceById(id string) interface{} {

	db.DB.AutoMigrate(&Fieldprice{})
	var fieldprice = Fieldprice{}
	return db.GetDataById(&fieldprice, id)
}

//获得意向、面询、对接价格
func GetFieldprice() interface{} {
	var info interface{}
	var getinfo lib.GetInfoN
	var fieldprice []*Fieldprice

	dba := db.DB.Raw("select id,intentprice,inqprice,dockprice from `fieldprice`").Scan(&fieldprice)
	num := dba.RowsAffected
	//有数据是返回相应信息
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapNoResult
	} else {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = fieldprice //数据

		info = getinfo
	}
	return info
}

//修改意向、面询、对接价格
func UpdateFieldprice(args map[string][]string) interface{} {

	return db.UpdateData("fieldprice", args)
}
