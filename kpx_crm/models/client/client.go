package client

import (
	"fmt"
	"github.com/tealeg/xlsx"
	"kpx_crm/models/field"
	"kpx_crm/util"
	"kpx_crm/util/db"
	"kpx_crm/util/lib"
	"reflect"
	"strconv"
	"strings"
	"time"
)

/*客户模型表*/
type Client struct {
	ID              int           `json:"id"`              //
	Username        string        `json:"username"`        //客户姓名
	Nickname        string        `json:"nickname"`        //昵称
	Phone           string        `json:"phone"`           //手机号
	School          string        `json:"school"`          //院校
	Grade           string        `json:"grade"`           //年级
	Profession      string        `json:"profession"`      //专业
	Applycountry    string        `json:"applycountry"`    //申请国家
	Applyprofession string        `json:"applyprofession"` //申请专业
	Dsa             string        `json:"dsa"`             //对接留学机构
	Dca             string        `json:"dca"`             //对接作品集机构
	Dla             string        `json:"dla"`             //对接语言机构
	Gpa             float64       `json:"gpa"`             //绩点
	Ie              float64       `json:"ie"`              //雅思分数
	To              float64       `json:"to"`              //托福分数
	Smalling        float64       `json:"smalling"`        //小语种
	Gdmer           string        `json:"gdmer"`           //出国决策人
	Intentprice     float64       `json:"intentprice"`     //意向价格
	Inqprice        float64       `json:"inqprice"`        //面询价格
	Dockprice       float64       `json:"dockprice"`       //对接价格
	Deprice         float64       `json:"deprice"`         //定金价格
	Signprice       float64       `json:"signprice"`       //签约价格
	Applytime       util.JsonTime `json:"applytime"`       //申请时间
	Joindate        string        `json:"joindate"`        //加入时间
	Userorigin      string        `json:"userorigin"`      //客户来源
	//Openumber       int `json:"openumber"`
	//Staytime        int `json:"staytime"`
	//Sharenumber     int `json:"sharenumber"`
	Clientkind int64         `json:"clientkind"` //客户种类,意向/面询..客户,默认0
	Clientfail int64         `json:"clientfail"` //客户失效阶段
	Docktime   string        `json:"docktime"`   //对接时间
	Updatetime util.JsonTime `json:"updatetime"` //更新时间
	Revenue    float64       `json:"revenue"`    //营收
	Dataorigin string        `json:"dataorigin"` //数据来源
}

/*网站、小程序客户数据模型*/
type User struct {
	Nickname   string `json:"nickname"`   //昵称
	Username   string `json:"username"`   //客户姓名
	School     string `json:"school"`     //院校
	Grade      string `json:"grade"`      //年级
	Phone      string `json:"phone"`      //手机号
	Joindate   string `json:"joindate"`   //加入日期
	Userorigin string `json:"userorigin"` //客户来源
}

/*客户分发*/
type Userclient struct {
	ID        int `json:"id"`
	User_id   int `json:"user_id"`
	Client_id int `json:"client_id"`
}

//顾问模型,1级
type Client1 struct {
	Username
	Client
}

//经理模型,2级
type Client2 struct {
	Username
	Client
}

//总监模型,3级
type Client3 struct {
	Username
	Client
}

//总经理模型,4级
type Client4 struct {
	Username
	Client
}

type Username struct {
	Username1 string `json:"username1"`
	Username2 string `json:"username2"`
	Username3 string `json:"username3"`
	Username4 string `json:"username4"`
}

type Value struct {
	Value string
}

//客户批量导出
func ExportClient() interface{} {
	var info interface{}
	var clients []*Client

	//用select *我开心
	dba := db.DB.Raw("select *from `client`").Scan(&clients)
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		return info
	}
	//导出客户数据
	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell
	var err error

	file = xlsx.NewFile()
	sheet, _ = file.AddSheet("Sheet1")

	for _, v := range clients {
		row = sheet.AddRow()
		ref := reflect.ValueOf(v).Elem()
		col := reflect.TypeOf(v).Elem()
		//去id
		for i := 1; i < ref.NumField(); i++ {
			column := col.Field(i).Name
			var value string
			t := ref.Field(i)
			switch t.Type().String() {
			case "int":
				value = strconv.FormatInt(t.Int(), 10)
			case "string":
				value = t.String()
			}

			switch column {
			case "Userpassword":
				continue
			case "Parent_id":
				continue
			case "Role_id":
				continue
			case "Createtime":
				continue
			}
			cell = row.AddCell()
			cell.Value = value
		}
	}

	filepath := "static/file/客户数据.xlsx"
	err = file.Save(filepath)
	if err != nil {
		info = lib.MapDataError{224, err.Error()}
	} else {
		info = map[string]interface{}{"status": lib.CodeSuccess, "msg": lib.MsgSuccess, "filepath": filepath}
	}
	return info
}

//客户批量导入
func ImportClient(filename string, args map[string][]string) interface{} {
	var info interface{}
	xlFile, _ := xlsx.OpenFile(filename)

	sql := "insert `client`("

	//sql拼接
	for _, sheet := range xlFile.Sheets {
		//去掉第一行记录
		for i := 1; i < len(sheet.Rows); i++ {
			row := sheet.Rows[i]
			if i == 1 {
				for _, cell := range row.Cells {
					text := cell.String()
					sql += "`" + text + "`,"
				}
				c := []byte(sql)
				sql = string(c[:len(c)-1]) + ") value(" //去掉点
				continue
			}

			for _, cell := range row.Cells {
				text := cell.String()
				if text == "" {
					text = "0"
				}
				sql += "'" + text + "',"
			}
			c := []byte(sql)
			sql = string(c[:len(c)-1]) + "),(" //去掉点
		}
	}
	c := []byte(sql)
	sql = string(c[:len(c)-2])
	//批量插入
	dba := db.DB.Exec(sql)

	num := dba.RowsAffected
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapExistOrNo
	} else {
		info = lib.MapCreate
	}
	return info
}

//客户分发
func DistributionClient(user_id, client_ids string) interface{} {

	var info interface{}
	var num int64 //返回影响的行数

	id := strings.Split(client_ids, "&")
	sql := "insert `userclient`(user_id,client_id) values"
	for _, v := range id {
		sql += "(" + user_id + "," + v + "),"
	}
	sql = string([]byte(sql)[:len(sql)-1])

	dba := db.DB.Exec(sql)
	num = dba.RowsAffected

	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}

//获得客户,根据id
func GetClientById(id string) interface{} {

	db.DB.AutoMigrate(&Client{})
	var client = Client{}
	return db.GetDataById(&client, id)
}

//获得客户,分页/查询,(顾问的客户数据)
func GetClientBySearchV3(kind, user_id, username string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var value Value
	//查询固定角色id
	db.DB.Raw("select role_id as value from `user` where id=?", user_id).Scan(&value)
	switch value.Value {
	case "5": //顾问
		var client1 []Client1
		sql := fmt.Sprintf(`select 
			(select username from user where id=(select parent_id from user where id=(select parent_id from user where id=c.parent_id))) as username1,
			(select username from user where id=(select parent_id from user where id=c.parent_id)) as username2,
			(select username from user where id=(c.parent_id)) as username3,
			c.username username4
			,%s from client a 
			inner join userclient b 
			inner join user c 
			on a.id=b.client_id and b.user_id=c.id and c.id=%s where a.clientkind=%s
			order by a.id desc limit %d,%d`, db.GetColumnsSql(Client{}, "a"), user_id, kind, (clientPage-1)*everyPage, everyPage) //fmt.Sprintf("select c.username username1,d.username username2, a.%s from `client` a inner join `userclient` b inner join `user` c on b.user_id=c.id and a.id=b.client_id and b.user_id=%s order by id desc limit %d,%d", db.GetSqlColumnsSql(Client{}), user_id, (clientPage-1)*everyPage, everyPage)
		sqlnolimit := `select count(*) as sum_page from client a 
			inner join userclient b 
			inner join user c
			on a.id=b.client_id and b.user_id=c.id and c.id=? where a.clientkind=?`
		if username != "" {
			sql = strings.Replace(sql, "where", "where a.username='"+username+"' and ", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "where", "where a.username='"+username+"' and ", -1)
		}

		dba := db.DB.Raw(sqlnolimit, user_id, kind).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql).Scan(&client1)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = client1 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "4": //经理
		var client2 []Client2
		sql := fmt.Sprintf(`select 
			(select username from user where id=(select parent_id from user where id=c.parent_id)) as username1,
			(select username from user where id=(c.parent_id)) as username2,
			c.username username3,
			d.username username4,
			%s from client a 
			inner join userclient b 
			inner join user c 
			inner join user d
			on a.id=b.client_id and b.user_id=d.id and c.id=%s
			and c.id=d.parent_id where a.clientkind=%s 
			order by a.id desc limit %d,%d`, db.GetColumnsSql(Client{}, "a"), user_id, kind, (clientPage-1)*everyPage, everyPage) //fmt.Sprintf("select c.username username1,d.username username2, a.%s from `client` a inner join `userclient` b inner join `user` c on b.user_id=c.id and a.id=b.client_id and b.user_id=%s order by id desc limit %d,%d", db.GetSqlColumnsSql(Client{}), user_id, (clientPage-1)*everyPage, everyPage)
		sqlnolimit := `select count(*) as sum_page from client a 
			inner join userclient b 
			inner join user c 
			inner join user d
			on a.id=b.client_id and b.user_id=d.id and c.id=?
			and c.id=d.parent_id  where a.clientkind=?`
		if username != "" {
			sql = strings.Replace(sql, "where", "where a.username='"+username+"' and ", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "where", "where a.username='"+username+"' and ", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id, kind).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql).Scan(&client2)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}

			//统计页码等状态
			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = client2 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "3": //总监
		var client3 []Client3
		sql := fmt.Sprintf(`select 
			(select username from user where id=(c.parent_id)) as username1,
			c.username username2,
			d.username username3,
			e.username username4,
			%s from client a 
			inner join userclient b 
			inner join user c 
			inner join user d
			inner join user e
			on a.id=b.client_id and b.user_id=e.id and c.id=%s
			and c.id=d.parent_id and d.id=e.parent_id where a.clientkind=%s
			order by a.id desc limit %d,%d`, db.GetColumnsSql(Client{}, "a"), user_id, kind, (clientPage-1)*everyPage, everyPage) //fmt.Sprintf("select c.username username1,d.username username2, a.%s from `client` a inner join `userclient` b inner join `user` c on b.user_id=c.id and a.id=b.client_id and b.user_id=%s order by id desc limit %d,%d", db.GetSqlColumnsSql(Client{}), user_id, (clientPage-1)*everyPage, everyPage)
		sqlnolimit := `select count(*) as sum_page from client a 
			inner join userclient b 
			inner join user c 
			inner join user d
			inner join user e
			on a.id=b.client_id and b.user_id=e.id and c.id=?
			and c.id=d.parent_id and d.id=e.parent_id where a.clientkind=?`
		if username != "" {
			sql = strings.Replace(sql, "where", "where a.username='"+username+"' and ", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "where", "where a.username='"+username+"' and ", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id, kind).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql).Scan(&client3)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}

			//统计页码等状态
			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = client3 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "2": //总经理
		var client4 []Client4
		sql := fmt.Sprintf(`select c.username username1,d.username username2,e.username username3,f.username username4,%s from client a 
			inner join userclient b 
			inner join user c 
			inner join user d
			inner join user e
			inner join user f
			on a.id=b.client_id and b.user_id=f.id and c.id=%s
			and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id where a.clientkind=%s
			order by a.id desc limit %d,%d`, db.GetColumnsSql(Client{}, "a"), user_id, kind, (clientPage-1)*everyPage, everyPage) //fmt.Sprintf("select c.username username1,d.username username2, a.%s from `client` a inner join `userclient` b inner join `user` c on b.user_id=c.id and a.id=b.client_id and b.user_id=%s order by id desc limit %d,%d", db.GetSqlColumnsSql(Client{}), user_id, (clientPage-1)*everyPage, everyPage)
		sqlnolimit := `select count(*) as sum_page from client a 
			inner join userclient b 
			inner join user c 
			inner join user d
			inner join user e
			inner join user f
			on a.id=b.client_id and b.user_id=f.id and c.id=?
			and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id where a.clientkind=?`
		if username != "" {
			sql = strings.Replace(sql, "where", "where a.username='"+username+"' and ", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "where", "where a.username='"+username+"' and ", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id, kind).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql).Scan(&client4)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client4 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "1":
		info = map[string]interface{}{"status": 222, "msg": "根管理员！"}
	default:
		info = map[string]interface{}{"status": 222, "msg": "客户对应角色不存在"}
	}

	return info
}

/*根据model中表模型的json标签获取表字段,增加select where 条件 关键字查询*/
//尝试单条件搜索全部,多条件搜索关系为且
func GetSearchSqlByKey(model interface{}, keys []string) (sql string) {
	typ := reflect.TypeOf(model)
	//var buffer bytes.Buffer

	for _, v := range keys {
		//客户种类单独查询,index数字
		if strings.Contains(v, "clientkind") {
			sql += "(clientkind='" + string([]byte(v)[len(v)-1:len(v)]) + "') and "
			continue
		}
		sql += "("

		for i := 0; i < typ.NumField(); i++ {
			tag := typ.Field(i).Tag.Get("json")
			//buffer.WriteString("`"+tag + "`,")
			sql += "`" + tag + "` like binary '%" + v + "%' or "
		}
		sql = string([]byte(sql)[:len(sql)-3]) //去掉or
		//多条件and关系
		sql += ") and "
	}

	sql = string([]byte(sql)[:len(sql)-4]) //去掉and
	return sql
}

//获得客户,分页/查询,(未分发(flag="")、已分发(flag="1")的客户数据)
func GetClientBySearch(clientPage, everyPage int, key, flag string) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var clients []*Client

	//尝试将select* 变为对应的字段名
	sql := fmt.Sprintf("select a.%s from `%s` a where a.id not in(select client_id from `userclient`) and ", db.GetSqlColumnsSql(Client{}), "client")
	sqlnolimit := fmt.Sprintf("select count(a.id) as sum_page from `%s` a where a.id not in(select client_id from `userclient`) and ", "client")
	//flag
	switch flag {
	case "":
		//未分发
	case "1":
		//已分发
		sql = strings.Replace(sql, "not in", "in", -1)
		sqlnolimit = strings.Replace(sqlnolimit, "not in", "in", -1)
	}

	//进行关键字搜索
	key = strings.Trim(key, " ")
	keys := strings.Split(key, " ") //空格分割
	sql += GetSearchSqlByKey(Client{}, keys)
	sqlnolimit += GetSearchSqlByKey(Client{}, keys)

	//c := []byte(sql)
	//sql = string(c[:len(c)-4]) //去and
	//sqlnolimit = string([]byte(sqlnolimit)[:len(sqlnolimit)-4])
	sql += "order by id desc limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + strconv.Itoa(everyPage)

	dba := db.DB.Raw(sqlnolimit).Scan(&getinfo.Pager)
	num := getinfo.Pager.SumPage
	//有数据是返回相应信息
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapNoResult
	} else {
		//DB.Debug().Find(&dest)
		dba = db.DB.Raw(sql).Scan(&clients)
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
			return info
		}

		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = clients //数据
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	}
	return info
}

//客户删除
func DeleteClientById(id string) interface{} {

	//先删除对应逻辑外健数据
	db.DeleteDataByName("userclient", "client_id", id)
	info := db.DeleteDataByName("client", "id", id)
	return info
}

//修改客户
func UpdateClient(args map[string][]string) interface{} {

	info := db.UpdateData("client", args)
	//用户更新后,判断填充 意向价格、面询价格、对接价格
	var id string
	for k, v := range args {
		if k == "id" {
			id = v[0]
			continue
		}
	}
	var client Client
	sql := fmt.Sprintf("select %s from `client` where id=%s ", db.GetSqlColumnsSql(Client{}), id)
	dba := db.DB.Raw(sql).Scan(&client)
	//根据查出来的客户进行判断
	//判断条件:国家,作品集,对接语言机构
	if dba.RowsAffected > 0 && (client.Applycountry != "" || client.Dca != "" || client.Dla != "") {
		//查出最新的价格值
		//对接作品集的三种价格
		var fieldprice1 field.Fieldprice
		db.DB.Raw("select id,intentprice,inqprice,dockprice from `field` where flag=1 and field=?", client.Dca).Scan(&fieldprice1)
		//对接语言机构的三种价格
		var fieldprice2 field.Fieldprice
		db.DB.Raw("select id,intentprice,inqprice,dockprice from `field` where flag=2 and field=?", client.Dla).Scan(&fieldprice2)
		//国家的三种价格
		var fieldprice3 field.Fieldprice
		//当为美国/加拿大等国家时
		if strings.Contains("美国/加拿大", client.Applycountry) {
			db.DB.Raw("select id,intentprice,inqprice,dockprice from `field` where flag=3 and field like '%" + client.Applycountry + "%'").Scan(&fieldprice3)
		} else {
			db.DB.Raw("select id,intentprice,inqprice,dockprice from `field` where flag=3 and field = '其他国家'").Scan(&fieldprice3)
		}

		var intentprice float64 //意向价格
		var inqprice float64    //面询价格
		var dockprice float64   //对接价格
		intentprice = JudgeBigFour(fieldprice1.Intentprice, fieldprice2.Intentprice, fieldprice3.Intentprice)
		inqprice = JudgeBigFour(fieldprice1.Inqprice, fieldprice2.Inqprice, fieldprice3.Inqprice)
		dockprice = JudgeBigFour(fieldprice1.Dockprice, fieldprice2.Dockprice, fieldprice3.Dockprice)

		//更新意向价格、面询价格、对接价格
		dba.Exec("update `client` set intentprice=?,inqprice=?,dockprice=? where id=?", intentprice, inqprice, dockprice, client.ID)
	}

	return info
}

//可变数量最大数判断
func JudgeBigFour(args ...float64) (max float64) {

	max = 0
	for _, v := range args {
		if max < v {
			max = v
		}
	}
	return max
}

//创建客户
func CreateClient(args map[string][]string) interface{} {

	//后台添加创建时间
	createtime := time.Now().Format("2006-01-02 15:04:05")
	args["joindate"] = append(args["joindate"], createtime)
	info := db.CreateData("client", args)

	return info
}

//数据同步
func SynchronizeClientData() interface{} {
	var info interface{}
	//var getinfo lib.GetInfoN
	var errs []string
	var num int64
	sql := "select * from kpx.user where phone != ''"
	sql2 := "select * from kpx2.user where phone != ''"

	var users []*User
	db.DB.Raw(sql).Scan(&users)
	for _, v := range users {
		//sql3 := "insert `client`(nickname,username,school,grade,phone,joindate,userorigin) value(?,?,?,?,?,?,?)"
		//db.DB.Raw(sql3,v.Nickname,v.Username,v.School,v.Grade, v.Phone,v.Joindate,v.Userorigin)
		//创建一条记录
		client := Client{
			Nickname:   v.Nickname,
			Username:   v.Username,
			School:     v.School,
			Grade:      v.Grade,
			Phone:      v.Phone,
			Joindate:   v.Joindate,
			Userorigin: v.Userorigin,
			Dataorigin: "小程序",
			Docktime:   "2018-08-14 11:47:53", //time.Time("2018-08-14 11:47:53").Format("2006-01-02 15:04:05"),
		}

		dba := db.DB.Create(&client)
		num += dba.RowsAffected
		if dba.Error != nil {
			errs = append(errs, dba.Error.Error())
		}
	}
	db.DB.Raw(sql2).Scan(&users)
	for _, v := range users {
		client := Client{
			Nickname:   v.Nickname,
			Username:   v.Username,
			School:     v.School,
			Grade:      v.Grade,
			Phone:      v.Phone,
			Joindate:   v.Joindate,
			Userorigin: v.Userorigin,
			Dataorigin: "网站",
			Docktime:   "2018-08-14 11:47:53",
		}
		dba := db.DB.Create(&client)
		num += dba.RowsAffected
		if dba.Error != nil {
			errs = append(errs, dba.Error.Error())
		}
	}
	//同步成功与否,有问题,暂时都默认成功吧
	//if num > 0 {
	info = map[string]interface{}{"status": 200, "msg": "同步结束"}
	//} else {
	//	getinfo.Status = 222
	//	getinfo.Msg = "数据同步错误"
	//	getinfo.Data = errs
	//	info = getinfo
	//}

	return info
}
