package wage

import (
	"kpx_crm/models/client"
	"kpx_crm/util/db"
	"kpx_crm/util/lib"
	"strings"
)

/*工资绩效,鼓励金、业绩奖金、团队价值*/
type Wage struct {
	Year          string  `json:"year"`          //年份
	Month         string  `json:"month"`         //月份
	Totalprice    float64 `json:"totalprice"`    //价值创造总额
	Encouragement float64 `json:"encouragement"` //鼓励金
	Perbonus      float64 `json:"perbonus"`      //业绩奖金
	Teamvalue     float64 `json:"teamvalue"`     //团队价值
}

/*工资绩效,年终奖,学期奖励*/
type WageAward struct {
	Year       string  `json:"year"`       //年份
	Semester   string  `json:"semester"`   //学期
	Totalprice float64 `json:"totalprice"` //价值创造总额
	Awards     float64 `json:"awards"`     //奖金
}

//顾问模型,1级
type Client1 struct {
	Username
	Wage
}

//经理模型,2级
type Client2 struct {
	Username
	Wage
}

//总监模型,3级
type Client3 struct {
	Username
	Wage
}

//总经理模型,4级
type Client4 struct {
	Username
	Wage
}

//顾问模型,1级
type Client11 struct {
	Username
	WageAward
}

//经理模型,2级
type Client22 struct {
	Username
	WageAward
}

//总监模型,3级
type Client33 struct {
	Username
	WageAward
}

//总经理模型,4级
type Client44 struct {
	Username
	WageAward
}

type Username struct {
	Username1 string `json:"username1"`
	Username2 string `json:"username2"`
	Username3 string `json:"username3"`
	Username4 string `json:"username4"`
}


//学期奖励
func GetWageBySearch3(user_id, date string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var value client.Value
	//查询固定角色id
	db.DB.Raw("select role_id as value from `user` where id=?", user_id).Scan(&value)
	switch value.Value {
	case "5": //顾问
		var client1 []Client11
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=(select parent_id from user where id=c.parent_id))) as username1,
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username2,
		(select username from user where id=(c.parent_id)) as username3,
		c.username username4,
		year(updatetime) year,
		case
		when updatetime between concat(year(updatetime)-1,'-09') and concat(year(updatetime),'-02') then '第一学期'
		else '第二学期'
		end as semester,
		
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		) as totalprice,
		
		(((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)-
		(select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id)*0.05)*0.3- 
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2
		as awards
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,semester #,c.username,totalprice,awards
		order by year ,semester limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		year(updatetime) year,
		case
		when updatetime between concat(year(updatetime)-1,'-09') and concat(year(updatetime),'-02') then '第一学期'
		else '第二学期'
		end as semester
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,semester ) a`
		if date == "" {
			sql = strings.Replace(sql, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
		} else {
			sql = strings.Replace(sql, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id, (clientPage-1)*everyPage, everyPage).Scan(&client1)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			cl := client1
			for k, v := range cl {
				if v.Totalprice < 0 {
					cl[k].Totalprice = 0
				}
				if v.Awards < 0 {
					cl[k].Awards = 0
				}
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client1 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "4": //经理
		var client2 []Client22
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username1,
		(select username from user where id=(c.parent_id)) as username2,
		c.username username3,
		d.username username4,
		year(updatetime) year,
		case
		when updatetime between concat(year(updatetime)-1,'-09') and concat(year(updatetime),'-02') then '第一学期'
		else '第二学期'
		end as semester,
		
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		) as totalprice,
		
		(((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)-
		(select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id)*0.05)*0.3- 
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2
		as awards
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,semester #,c.username,d.username,totalprice,awards
		order by year ,semester limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		year(updatetime) year,
		case
		when updatetime between concat(year(updatetime)-1,'-09') and concat(year(updatetime),'-02') then '第一学期'
		else '第二学期'
		end as semester
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,semester ) a`
		if date == "" {
			sql = strings.Replace(sql, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
		} else {
			sql = strings.Replace(sql, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id, (clientPage-1)*everyPage, everyPage).Scan(&client2)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			cl := client2
			for k, v := range cl {
				if v.Totalprice < 0 {
					cl[k].Totalprice = 0
				}
				if v.Awards < 0 {
					cl[k].Awards = 0
				}
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client2 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "3": //总监
		var client3 []Client33
		sql := `SELECT
		(select username from user where id=(c.parent_id)) as username1,
		c.username username2,
		d.username username3,
		e.username username4,
		year(updatetime) year,
		case
		when updatetime between concat(year(updatetime)-1,'-09') and concat(year(updatetime),'-02') then '第一学期'
		else '第二学期'
		end as semester,
		
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		) as totalprice,
		
		(((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)-
		(select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id)*0.05)*0.3- 
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2
		as awards
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,semester #,c.username,d.username,e.username,totalprice,awards
		order by year ,semester limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		year(updatetime) year,
		case
		when updatetime between concat(year(updatetime)-1,'-09') and concat(year(updatetime),'-02') then '第一学期'
		else '第二学期'
		end as semester
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,semester ) a`
		if date == "" {
			sql = strings.Replace(sql, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
		} else {
			sql = strings.Replace(sql, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id, (clientPage-1)*everyPage, everyPage).Scan(&client3)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			cl := client3
			for k, v := range cl {
				if v.Totalprice < 0 {
					cl[k].Totalprice = 0
				}
				if v.Awards < 0 {
					cl[k].Awards = 0
				}
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client3 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "2": //总经理
		var client4 []Client44
		sql := `SELECT
		c.username username1,
		d.username username2,
		e.username username3,
		f.username username4,
		year(updatetime) year,
		case
		when updatetime between concat(year(updatetime)-1,'-09') and concat(year(updatetime),'-02') then '第一学期'
		else '第二学期'
		end as semester,
		
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		) as totalprice,
		
		(((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)-
		(select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id)*0.05)*0.3- 
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2
		as awards
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id 
		and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,semester #,c.username,d.username,e.username,f.username,totalprice,awards
		order by year ,semester limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		year(updatetime) year,
		case
		when updatetime between concat(year(updatetime)-1,'-09') and concat(year(updatetime),'-02') then '第一学期'
		else '第二学期'
		end as semester
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,semester ) a`
		if date == "" {
			sql = strings.Replace(sql, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
		} else {
			sql = strings.Replace(sql, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id, (clientPage-1)*everyPage, everyPage).Scan(&client4)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			cl := client4
			for k, v := range cl {
				if v.Totalprice < 0 {
					cl[k].Totalprice = 0
				}
				if v.Awards < 0 {
					cl[k].Awards = 0
				}
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client4 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	default:
		info = map[string]interface{}{"status": 222, "msg": "用户对应角色不存在"}
	}

	return info
}

//年终奖
func GetWageBySearch2(user_id, date string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var value client.Value
	//查询固定角色id
	db.DB.Raw("select role_id as value from `user` where id=?", user_id).Scan(&value)
	switch value.Value {
	case "5": //顾问
		var client1 []Client11
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=(select parent_id from user where id=c.parent_id))) as username1,
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username2,
		(select username from user where id=(c.parent_id)) as username3,
		c.username username4,
		date_format(updatetime,'%Y') year,
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		) as totalprice,
		
		(((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)-
		(select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id)*0.05)*0.4- 
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2
		as awards
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year #,c.username,totalprice,awards
		order by year limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year ) a`
		if date == "" {
			sql = strings.Replace(sql, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
		} else {
			sql = strings.Replace(sql, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id, (clientPage-1)*everyPage, everyPage).Scan(&client1)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			cl := client1
			for k, v := range cl {
				if v.Totalprice < 0 {
					cl[k].Totalprice = 0
				}
				if v.Awards < 0 {
					cl[k].Awards = 0
				}
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client1 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "4": //经理
		var client2 []Client22
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username1,
		(select username from user where id=(c.parent_id)) as username2,
		c.username username3,
		d.username username4,
		date_format(updatetime,'%Y') year,
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		) as totalprice,
		
		(((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)-
		(select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id)*0.05)*0.4- 
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2
		as awards
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year #,c.username,d.username,totalprice,awards
		order by year limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year ) a`
		if date == "" {
			sql = strings.Replace(sql, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
		} else {
			sql = strings.Replace(sql, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id, (clientPage-1)*everyPage, everyPage).Scan(&client2)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			cl := client2
			for k, v := range cl {
				if v.Totalprice < 0 {
					cl[k].Totalprice = 0
				}
				if v.Awards < 0 {
					cl[k].Awards = 0
				}
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client2 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "3": //总监
		var client3 []Client33
		sql := `SELECT
		(select username from user where id=(c.parent_id)) as username1,
		c.username username2,
		d.username username3,
		e.username username4,
		date_format(updatetime,'%Y') year,
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		) as totalprice,
		
		(((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)-
		(select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id)*0.05)*0.4- 
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2
		as awards
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year #,c.username,d.username,e.username,totalprice,awards
		order by year limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year ) a`
		if date == "" {
			sql = strings.Replace(sql, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
		} else {
			sql = strings.Replace(sql, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id, (clientPage-1)*everyPage, everyPage).Scan(&client3)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			cl := client3
			for k, v := range cl {
				if v.Totalprice < 0 {
					cl[k].Totalprice = 0
				}
				if v.Awards < 0 {
					cl[k].Awards = 0
				}
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client3 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "2": //总经理
		var client4 []Client44
		sql := `SELECT
		c.username username1,
		d.username username2,
		e.username username3,
		f.username username4,
		date_format(updatetime,'%Y') year,
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		) as totalprice,
		
		(((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)-
		(select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id)*0.05)*0.4- 
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2
		as awards
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id 
		and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year #,c.username,d.username,e.username,f.username,totalprice,awards
		order by year limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year ) a`
		if date == "" {
			sql = strings.Replace(sql, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
		} else {
			sql = strings.Replace(sql, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id, (clientPage-1)*everyPage, everyPage).Scan(&client4)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			cl := client4
			for k, v := range cl {
				if v.Totalprice < 0 {
					cl[k].Totalprice = 0
				}
				if v.Awards < 0 {
					cl[k].Awards = 0
				}
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client4 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	default:
		info = map[string]interface{}{"status": 222, "msg": "用户对应角色不存在"}
	}

	return info
}

//获得工资绩效,分页/查询
func GetWageBySearch(user_id, date string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var value client.Value
	//查询固定角色id
	db.DB.Raw("select role_id as value from `user` where id=?", user_id).Scan(&value)
	switch value.Value {
	case "5": //顾问
		var client1 []Client1
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=(select parent_id from user where id=c.parent_id))) as username1,
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username2,
		(select username from user where id=(c.parent_id)) as username3,
		c.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		) as totalprice,
		
		sum(a.revenue)*0.05 as encouragement,
		
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2 as perbonus,
		
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)-
		(select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id)*0.05- 
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2
		as teamvalue
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month #,c.username,totalprice,perbonus,teamvalue
		order by year ,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month ) a`
		if date == "" {
			sql = strings.Replace(sql, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
		} else {
			sql = strings.Replace(sql, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id, (clientPage-1)*everyPage, everyPage).Scan(&client1)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			cl := client1
			for k, v := range cl {
				if v.Totalprice < 0 {
					cl[k].Totalprice = 0
				}
				if v.Perbonus < 0 {
					cl[k].Perbonus = 0
				}
				if v.Teamvalue < 0 {
					cl[k].Teamvalue = 0
				}
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client1 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "4": //经理
		var client2 []Client2
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username1,
		(select username from user where id=(c.parent_id)) as username2,
		c.username username3,
		d.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		) as totalprice,
		
		sum(a.revenue)*0.05 as encouragement,
		
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2 as perbonus,
		
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)-
		(select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id)*0.05- 
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2
		as teamvalue
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month #,c.username,d.username,totalprice,perbonus,teamvalue
		order by year ,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month ) a`
		if date == "" {
			sql = strings.Replace(sql, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
		} else {
			sql = strings.Replace(sql, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id, (clientPage-1)*everyPage, everyPage).Scan(&client2)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			cl := client2
			for k, v := range cl {
				if v.Totalprice < 0 {
					cl[k].Totalprice = 0
				}
				if v.Perbonus < 0 {
					cl[k].Perbonus = 0
				}
				if v.Teamvalue < 0 {
					cl[k].Teamvalue = 0
				}
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client2 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "3": //总监
		var client3 []Client3
		sql := `SELECT
		(select username from user where id=(c.parent_id)) as username1,
		c.username username2,
		d.username username3,
		e.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		) as totalprice,
		
		sum(a.revenue)*0.05 as encouragement,
		
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2 as perbonus,
		
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)-
		(select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id)*0.05- 
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2
		as teamvalue
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month #,c.username,d.username,e.username,totalprice,perbonus,teamvalue
		order by year ,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month ) a`
		if date == "" {
			sql = strings.Replace(sql, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
		} else {
			sql = strings.Replace(sql, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id, (clientPage-1)*everyPage, everyPage).Scan(&client3)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			cl := client3
			for k, v := range cl {
				if v.Totalprice < 0 {
					cl[k].Totalprice = 0
				}
				if v.Perbonus < 0 {
					cl[k].Perbonus = 0
				}
				if v.Teamvalue < 0 {
					cl[k].Teamvalue = 0
				}
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client3 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "2": //总经理
		var client4 []Client4
		sql := `SELECT
		c.username username1,
		d.username username2,
		e.username username3,
		f.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		) as totalprice,
		
		sum(a.revenue)*0.05 as encouragement,
		
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2 as perbonus,
		
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)-
		(select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id)*0.05- 
		((select sum(revenue) from client aa where aa.updatetime=a.updatetime and aa.id=a.id) - 
		(select rentcost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) - 
		(select officecost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select hycost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime) -
		(select othercost from cost where date=date_format(updatetime,'%Y-%m'))/(select count(*) from user where role_id=5 and createtime<updatetime)
		)*0.2
		as teamvalue
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id 
		and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month #,c.username,d.username,e.username,f.username,totalprice,perbonus,teamvalue
		order by year ,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id and c.id=d.parent_id and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month ) a`
		if date == "" {
			sql = strings.Replace(sql, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "and date_format(updatetime,'%Y-%m') like '%?%'", "", -1)
		} else {
			sql = strings.Replace(sql, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
			sqlnolimit = strings.Replace(sqlnolimit, "date_format(updatetime,'%Y-%m') like '%?%'", "date_format(updatetime,'%Y-%m') like '%"+date+"%'", -1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id, (clientPage-1)*everyPage, everyPage).Scan(&client4)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			cl := client4
			for k, v := range cl {
				if v.Totalprice < 0 {
					cl[k].Totalprice = 0
				}
				if v.Perbonus < 0 {
					cl[k].Perbonus = 0
				}
				if v.Teamvalue < 0 {
					cl[k].Teamvalue = 0
				}
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client4 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	default:
		info = map[string]interface{}{"status": 222, "msg": "用户对应角色不存在"}
	}

	return info
}
