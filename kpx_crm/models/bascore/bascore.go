package bascore

import (
	"kpx_crm/models/client"
	"kpx_crm/util/db"
	"kpx_crm/util/lib"
	"strings"
)

/*平衡计分,客户量表模型*/
type Bascore struct {
	Year          string `json:"year"`          //`json:"年份"`
	Month         string `json:"month"`         //`json:"月份"`
	Sumclients    string `json:"sumclients"`    //`json:"总客户数"`
	Failclient    float64 `json:"failclient"`    //失效客户总量
	Intentnum     float64 `json:"intentnum"`     //`json:"意向客户数"`
	Intentfailnum float64 `json:"intentfailnum"` //意向客户是小数
	Inqnum        float64 `json:"inqnum"`        //`json:"面询客户数"`
	Inqfailnum    float64 `json:"inqfailnum"`    //面询客户失效数
	Docknum       float64 `json:"docknum"`       //`json:"对接客户数"`
	Dockfailnum   float64 `json:"dockfailnum"`   //对接失效客户数
	Depnum        float64 `json:"depnum"`        //`json:"定金客户数"`
	Depfailnum    float64 `json:"depfailnum"`    //定金客户失效数
	Signnum       float64 `json:"signnum"`       //`json:"签约客户数"`
	Signfailnum   float64 `json:"signfailnum"`   //签约失效客户数
}

/*平衡计分,客户阶段比模型*/
type Stage struct {
	Year       string  `json:"year"`       //`json:"年份"`
	Month      string  `json:"month"`      //`json:"月份"`
	Intentpro  float64 `json:"intentpro"`  //微信比例
	Inqpro     float64 `json:"inqpro"`     //面询比例
	Dockpro    float64 `json:"dockpro"`    //对接比例
	Depro      float64 `json:"depro"`      //定金比例
}

//顾问模型,1级
type Client1 struct {
	Username
	Bascore
}

//经理模型,2级
type Client2 struct {
	Username
	Bascore
}

//总监模型,3级
type Client3 struct {
	Username
	Bascore
}

//总经理模型,4级
type Client4 struct {
	Username
	Bascore
}

//顾问模型,1级
type Client11 struct {
	Username
	Stage
}

//经理模型,2级
type Client22 struct {
	Username
	Stage
}

//总监模型,3级
type Client33 struct {
	Username
	Stage
}

//总经理模型,4级
type Client44 struct {
	Username
	Stage
}

type Username struct {
	Username1 string `json:"username1"`
	Username2 string `json:"username2"`
	Username3 string `json:"username3"`
	Username4 string `json:"username4"`
}

//获得客户量表,分页/查询
func GetBascoreBySearchV2(user_id, date string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var value client.Value
	//查询固定角色id
	db.DB.Raw("select role_id as value from `user` where id=?", user_id).Scan(&value)
	switch value.Value {
	case "5": //顾问
		var client1 []Client1
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=(select parent_id from user where id=c.parent_id))) as username1,
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username2,
		(select username from user where id=(c.parent_id)) as username3,
		c.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=5 then 1 else null end) failclient,
	
		count(case when clientkind=0 then 1 when clientkind=5 and clientfail=1 then 1 else null end) as intentnum,
		count(case when clientfail=1 then 1 else null end) as intentfailnum,
	
		count(case when clientkind=1 then 1 when clientkind=5 and clientfail=2 then 1 else null end) as inqnum,
		count(case when clientfail=2 then 1 else null end) as inqfailnum,
	
		count(case when clientkind=2 then 1 when clientkind=5 and clientfail=3 then 1 else null end) as docknum,
		count(case when clientfail=3 then 1 else null end) as dockfailnum,
	
		count(case when clientkind=3 then 1 when clientkind=5 and clientfail=4 then 1 else null end) as depnum,
		count(case when clientfail=4 then 1 else null end) as depfailnum,
	
		count(case when clientkind=4 then 1 when clientkind=5 and clientfail=5 then 1 else null end) as signnum,
		count(case when clientfail=5 then 1 else null end) as signfailnum
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,c.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,c.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client1)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client1 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "4": //经理
		var client2 []Client2
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username1,
		(select username from user where id=(c.parent_id)) as username2,
		c.username username3,
		d.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=5 then 1 else null end) failclient,
	
		count(case when clientkind=0 then 1 when clientkind=5 and clientfail=1 then 1 else null end) as intentnum,
		count(case when clientfail=1 then 1 else null end) as intentfailnum,
	
		count(case when clientkind=1 then 1 when clientkind=5 and clientfail=2 then 1 else null end) as inqnum,
		count(case when clientfail=2 then 1 else null end) as inqfailnum,
	
		count(case when clientkind=2 then 1 when clientkind=5 and clientfail=3 then 1 else null end) as docknum,
		count(case when clientfail=3 then 1 else null end) as dockfailnum,
	
		count(case when clientkind=3 then 1 when clientkind=5 and clientfail=4 then 1 else null end) as depnum,
		count(case when clientfail=4 then 1 else null end) as depfailnum,
	
		count(case when clientkind=4 then 1 when clientkind=5 and clientfail=5 then 1 else null end) as signnum,
		count(case when clientfail=5 then 1 else null end) as signfailnum
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,d.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,d.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client2)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client2 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "3": //总监
		var client3 []Client3
		sql := `SELECT
		(select username from user where id=(c.parent_id)) as username1,
		c.username username2,
		d.username username3,
		e.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=0 then 1 when clientkind=5 and clientfail=1 then 1 else null end) as intentnum,
		count(case when clientfail=1 then 1 else null end) as intentfailnum,
	
		count(case when clientkind=1 then 1 when clientkind=5 and clientfail=2 then 1 else null end) as inqnum,
		count(case when clientfail=2 then 1 else null end) as inqfailnum,
	
		count(case when clientkind=2 then 1 when clientkind=5 and clientfail=3 then 1 else null end) as docknum,
		count(case when clientfail=3 then 1 else null end) as dockfailnum,
	
		count(case when clientkind=3 then 1 when clientkind=5 and clientfail=4 then 1 else null end) as depnum,
		count(case when clientfail=4 then 1 else null end) as depfailnum,
	
		count(case when clientkind=4 then 1 when clientkind=5 and clientfail=5 then 1 else null end) as signnum,
		count(case when clientfail=5 then 1 else null end) as signfailnum
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month ,e.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,e.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client3)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client3 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "2": //总经理
		var client4 []Client4
		sql := `SELECT
		c.username username1,
		d.username username2,
		e.username username3,
		f.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=0 then 1 when clientkind=5 and clientfail=1 then 1 else null end) as intentnum,
		count(case when clientfail=1 then 1 else null end) as intentfailnum,
	
		count(case when clientkind=1 then 1 when clientkind=5 and clientfail=2 then 1 else null end) as inqnum,
		count(case when clientfail=2 then 1 else null end) as inqfailnum,
	
		count(case when clientkind=2 then 1 when clientkind=5 and clientfail=3 then 1 else null end) as docknum,
		count(case when clientfail=3 then 1 else null end) as dockfailnum,
	
		count(case when clientkind=3 then 1 when clientkind=5 and clientfail=4 then 1 else null end) as depnum,
		count(case when clientfail=4 then 1 else null end) as depfailnum,
	
		count(case when clientkind=4 then 1 when clientkind=5 and clientfail=5 then 1 else null end) as signnum,
		count(case when clientfail=5 then 1 else null end) as signfailnum
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id 
		and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,f.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,f.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client4)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client4 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	default:
		info = map[string]interface{}{"status": 222, "msg": "用户对应角色不存在"}
	}

	return info
}

//获得客户阶段比,分页/查询
func GetStageBySearchV2(user_id, date string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var value client.Value
	//查询固定角色id
	db.DB.Raw("select role_id as value from `user` where id=?", user_id).Scan(&value)
	switch value.Value {
	case "5": //顾问
		var client1 []Client11
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=(select parent_id from user where id=c.parent_id))) as username1,
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username2,
		(select username from user where id=(c.parent_id)) as username3,
		c.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=0 then 1 else null end)/count(a.id) as intentpro,
		count(case when clientkind=1 then 1 else null end)/count(a.id) as inqpro,
		count(case when clientkind=2 then 1 else null end)/count(a.id) as dockpro,
		count(case when clientkind=3 then 1 else null end)/count(a.id) as depro

		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month ,c.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,c.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client1)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client1 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "4": //经理
		var client2 []Client22
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username1,
		(select username from user where id=(c.parent_id)) as username2,
		c.username username3,
		d.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
	
		count(case when clientkind=0 then 1 else null end)/count(a.id) as intentpro,
		count(case when clientkind=1 then 1 else null end)/count(a.id) as inqpro,
		count(case when clientkind=2 then 1 else null end)/count(a.id) as dockpro,
		count(case when clientkind=3 then 1 else null end)/count(a.id) as depro

		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month ,d.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,d.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client2)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client2 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "3": //总监
		var client3 []Client33
		sql := `SELECT
		(select username from user where id=(c.parent_id)) as username1,
		c.username username2,
		d.username username3,
		e.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=0 then 1 else null end)/count(a.id) as intentpro,
		count(case when clientkind=1 then 1 else null end)/count(a.id) as inqpro,
		count(case when clientkind=2 then 1 else null end)/count(a.id) as dockpro,
		count(case when clientkind=3 then 1 else null end)/count(a.id) as depro

		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,e.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,e.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client3)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client3 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "2": //总经理
		var client4 []Client44
		sql := `SELECT
		c.username username1,
		d.username username2,
		e.username username3,
		f.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=0 then 1 else null end)/count(a.id) as intentpro,
		count(case when clientkind=1 then 1 else null end)/count(a.id) as inqpro,
		count(case when clientkind=2 then 1 else null end)/count(a.id) as dockpro,
		count(case when clientkind=3 then 1 else null end)/count(a.id) as depro

		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id 
		and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,f.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,f.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client4)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client4 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	default:
		info = map[string]interface{}{"status": 222, "msg": "用户对应角色不存在"}
	}

	return info
}

//获得客户失效阶段比,分页/查询
func GetStageFailBySearchV2(user_id, date string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var value client.Value
	//查询固定角色id
	db.DB.Raw("select role_id as value from `user` where id=?", user_id).Scan(&value)
	switch value.Value {
	case "5": //顾问
		var client1 []Client11
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=(select parent_id from user where id=c.parent_id))) as username1,
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username2,
		(select username from user where id=(c.parent_id)) as username3,
		c.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientfail=1 then 1 else null end)/count(a.id) as intentpro,
		count(case when clientfail=2 then 1 else null end)/count(a.id) as inqpro,
		count(case when clientfail=3 then 1 else null end)/count(a.id) as dockpro,
		count(case when clientfail=4 then 1 else null end)/count(a.id) as depro

		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,c.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,c.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client1)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client1 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "4": //经理
		var client2 []Client22
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username1,
		(select username from user where id=(c.parent_id)) as username2,
		c.username username3,
		d.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
	
		count(case when clientfail=1 then 1 else null end)/count(a.id) as intentpro,
		count(case when clientfail=2 then 1 else null end)/count(a.id) as inqpro,
		count(case when clientfail=3 then 1 else null end)/count(a.id) as dockpro,
		count(case when clientfail=4 then 1 else null end)/count(a.id) as depro

		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,d.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,d.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client2)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client2 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "3": //总监
		var client3 []Client33
		sql := `SELECT
		(select username from user where id=(c.parent_id)) as username1,
		c.username username2,
		d.username username3,
		e.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientfail=1 then 1 else null end)/count(a.id) as intentpro,
		count(case when clientfail=2 then 1 else null end)/count(a.id) as inqpro,
		count(case when clientfail=3 then 1 else null end)/count(a.id) as dockpro,
		count(case when clientfail=4 then 1 else null end)/count(a.id) as depro

		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,e.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,e.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client3)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client3 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "2": //总经理
		var client4 []Client44
		sql := `SELECT
		c.username username1,
		d.username username2,
		e.username username3,
		f.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientfail=1 then 1 else null end)/count(a.id) as intentpro,
		count(case when clientfail=2 then 1 else null end)/count(a.id) as inqpro,
		count(case when clientfail=3 then 1 else null end)/count(a.id) as dockpro,
		count(case when clientfail=4 then 1 else null end)/count(a.id) as depro

		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id 
		and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,f.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,f.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client4)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client4 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	default:
		info = map[string]interface{}{"status": 222, "msg": "用户对应角色不存在"}
	}

	return info
}

//获得客户进阶转化率,分页/查询
func GetStageAdBySearchV2(user_id, date string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var value client.Value
	//查询固定角色id
	db.DB.Raw("select role_id as value from `user` where id=?", user_id).Scan(&value)
	switch value.Value {
	case "5": //顾问
		var client1 []Client11
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=(select parent_id from user where id=c.parent_id))) as username1,
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username2,
		(select username from user where id=(c.parent_id)) as username3,
		c.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end)/count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5 or 1) then 1 else null end) as intentpro,
		count(case when clientkind=(2 or 3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end)/count(case when clientkind=(2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end) as inqpro,
		count(case when clientkind=(3 or 4) or clientfail=(4 or 5) then 1 else null end)/count(case when clientkind=(3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end) as dockpro,
		count(case when clientkind=(4) or clientfail=(5) then 1 else null end)/count(case when clientkind=(4) or clientfail=(4 or 5) then 1 else null end) as depro

		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,c.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,c.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client1)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client1 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "4": //经理
		var client2 []Client22
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username1,
		(select username from user where id=(c.parent_id)) as username2,
		c.username username3,
		d.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
	
		count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end)/count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5 or 1) then 1 else null end) as intentpro,
		count(case when clientkind=(2 or 3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end)/count(case when clientkind=(2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end) as inqpro,
		count(case when clientkind=(3 or 4) or clientfail=(4 or 5) then 1 else null end)/count(case when clientkind=(3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end) as dockpro,
		count(case when clientkind=(4) or clientfail=(5) then 1 else null end)/count(case when clientkind=(4) or clientfail=(4 or 5) then 1 else null end) as depro

		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,d.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,d.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client2)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client2 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "3": //总监
		var client3 []Client33
		sql := `SELECT
		(select username from user where id=(c.parent_id)) as username1,
		c.username username2,
		d.username username3,
		e.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end)/count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5 or 1) then 1 else null end) as intentpro,
		count(case when clientkind=(2 or 3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end)/count(case when clientkind=(2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end) as inqpro,
		count(case when clientkind=(3 or 4) or clientfail=(4 or 5) then 1 else null end)/count(case when clientkind=(3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end) as dockpro,
		count(case when clientkind=(4) or clientfail=(5) then 1 else null end)/count(case when clientkind=(4) or clientfail=(4 or 5) then 1 else null end) as depro

		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,e.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,e.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client3)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client3 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "2": //总经理
		var client4 []Client44
		sql := `SELECT
		c.username username1,
		d.username username2,
		e.username username3,
		f.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end)/count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5 or 1) then 1 else null end) as intentpro,
		count(case when clientkind=(2 or 3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end)/count(case when clientkind=(2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end) as inqpro,
		count(case when clientkind=(3 or 4) or clientfail=(4 or 5) then 1 else null end)/count(case when clientkind=(3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end) as dockpro,
		count(case when clientkind=(4) or clientfail=(5) then 1 else null end)/count(case when clientkind=(4) or clientfail=(4 or 5) then 1 else null end) as depro

		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id 
		and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,f.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,f.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client4)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client4 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	default:
		info = map[string]interface{}{"status": 222, "msg": "用户对应角色不存在"}
	}

	return info
}

//获得客户进阶签约率,分页/查询
func GetStageJDBySearchV2(user_id, date string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var value client.Value
	//查询固定角色id
	db.DB.Raw("select role_id as value from `user` where id=?", user_id).Scan(&value)
	switch value.Value {
	case "5": //顾问
		var client1 []Client11
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=(select parent_id from user where id=c.parent_id))) as username1,
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username2,
		(select username from user where id=(c.parent_id)) as username3,
		c.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end)/count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5 or 1) then 1 else null end) as intentpro,
		count(case when clientkind=(2 or 3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end)/count(case when clientkind=(2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end) as inqpro,
		count(case when clientkind=(3 or 4) or clientfail=(4 or 5) then 1 else null end)/count(case when clientkind=(3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end) as dockpro,
		count(case when clientkind=(4) or clientfail=(5) then 1 else null end)/count(case when clientkind=(4) or clientfail=(4 or 5) then 1 else null end) as depro

		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,c.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		on a.id=b.client_id and b.user_id=c.id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,c.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client1)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client1 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "4": //经理
		var client2 []Client22
		sql := `SELECT
		(select username from user where id=(select parent_id from user where id=c.parent_id)) as username1,
		(select username from user where id=(c.parent_id)) as username2,
		c.username username3,
		d.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
	
		count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end)/count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5 or 1) then 1 else null end) as intentpro,
		count(case when clientkind=(2 or 3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end)/count(case when clientkind=(2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end) as inqpro,
		count(case when clientkind=(3 or 4) or clientfail=(4 or 5) then 1 else null end)/count(case when clientkind=(3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end) as dockpro,
		count(case when clientkind=(4) or clientfail=(5) then 1 else null end)/count(case when clientkind=(4) or clientfail=(4 or 5) then 1 else null end) as depro

		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,d.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		on a.id=b.client_id and b.user_id=d.id and c.id=d.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,d.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client2)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client2 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "3": //总监
		var client3 []Client33
		sql := `SELECT
		(select username from user where id=(c.parent_id)) as username1,
		c.username username2,
		d.username username3,
		e.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end)/count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5 or 1) then 1 else null end) as intentpro,
		count(case when clientkind=(2 or 3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end)/count(case when clientkind=(2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end) as inqpro,
		count(case when clientkind=(3 or 4) or clientfail=(4 or 5) then 1 else null end)/count(case when clientkind=(3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end) as dockpro,
		count(case when clientkind=(4) or clientfail=(5) then 1 else null end)/count(case when clientkind=(4) or clientfail=(4 or 5) then 1 else null end) as depro

		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,e.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		on a.id=b.client_id and b.user_id=e.id and c.id=d.parent_id and d.id=e.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,e.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client3)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client3 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	case "2": //总经理
		var client4 []Client44
		sql := `SELECT
		c.username username1,
		d.username username2,
		e.username username3,
		f.username username4,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month,
		count(a.id) sumclients,
		count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end)/count(case when clientkind=(1 or 2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5 or 1) then 1 else null end) as intentpro,
		count(case when clientkind=(2 or 3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end)/count(case when clientkind=(2 or 3 or 4) or clientfail=(2 or 3 or 4 or 5) then 1 else null end) as inqpro,
		count(case when clientkind=(3 or 4) or clientfail=(4 or 5) then 1 else null end)/count(case when clientkind=(3 or 4) or clientfail=(3 or 4 or 5) then 1 else null end) as dockpro,
		count(case when clientkind=(4) or clientfail=(5) then 1 else null end)/count(case when clientkind=(4) or clientfail=(4 or 5) then 1 else null end) as depro

		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id 
		and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,f.username
		order by year,month limit ?,?`

		sqlnolimit := `select count(*) as sum_page from (
		SELECT
		c.username username1,
		date_format(updatetime,'%Y') year,
		date_format(updatetime,'%m') month
		FROM client a
		inner join userclient b
		inner join user c
		inner join user d
		inner join user e
		inner join user f
		on a.id=b.client_id and b.user_id=f.id and c.id=d.parent_id and d.id=e.parent_id and e.id=f.parent_id
		where c.id=? and date_format(updatetime,'%Y-%m') like '%?%'
		group by year,month,f.username ) a`
		if date == ""{
			sql = strings.Replace(sql,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"and date_format(updatetime,'%Y-%m') like '%?%'","",-1)
		}else {
			sql = strings.Replace(sql,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
			sqlnolimit = strings.Replace(sqlnolimit,"date_format(updatetime,'%Y-%m') like '%?%'","date_format(updatetime,'%Y-%m') like '%"+date+"%'",-1)
		}
		dba := db.DB.Raw(sqlnolimit, user_id).Scan(&getinfo.Pager)
		num := getinfo.Pager.SumPage
		//有数据是返回相应信息
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		} else if num == 0 && dba.Error == nil {
			info = lib.MapNoResult
		} else {
			//DB.Debug().Find(&dest)
			dba = db.DB.Raw(sql, user_id,(clientPage-1)*everyPage,everyPage).Scan(&client4)
			if dba.Error != nil {
				info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
				return info
			}
			//统计页码等状态
			getinfo.Status = lib.CodeSuccess
			getinfo.Msg = lib.MsgSuccess
			getinfo.Data = client4 //数据
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	default:
		info = map[string]interface{}{"status": 222, "msg": "用户对应角色不存在"}
	}

	return info
}
