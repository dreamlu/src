package permission

import "kpx_crm/util/db"

/*角色模型*/
type Role struct {
	ID        int    `json:"id"`
	Parent_id int    `json:"-"`
	Rolename  string `json:"rolename"`
}

/*角色上级领导*/
type ParentUser struct {
	User_id  int    `json:"user_id"`
	Username string `json:"username"`
}

//根据角色id获得上一级所有用户
func GetParentUsers(id string) interface{} {

	sql := "select c.id as user_id,c.username from `role` a inner join `role` b inner join `user` c on a.id=b.parent_id and a.id=c.role_id where b.id=?"

	db.DB.AutoMigrate(&ParentUser{})
	var ParentUser []ParentUser
	return db.GetDataBySql(&ParentUser, sql, id)
}

//用户角色(职位)
func GetRoles() interface{} {

	db.DB.AutoMigrate(&Role{})
	var role []Role
	return db.GetDataByName(&role, "1", "1")
}
