package examination

import (
	"github.com/astaxie/beego"
	"kpx/models/examination"
)

type StudyRecordController struct {
	beego.Controller
}

//根据用户id获得学习记录获取
// @router /studyrecord/getstudyrecordbyuser [post]
func (u *StudyRecordController) GetStudyRecordUser() {
	user_id, _ := u.GetInt("user_id") //页码
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := examination.GetStudyRecordUser(user_id,clientPage,everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//学习记录信息分页
// @router /studyrecord/getstudyrecordbypage [get]
func (u *StudyRecordController) GetStudyRecordByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := examination.GetStudyRecordByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增学习记录信息
// @router /studyrecord/createstudyrecord [post]
func (u *StudyRecordController) CreateStudyRecord() {
	values := u.Ctx.Request.Form
	ss := examination.CreateStudyRecord(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
