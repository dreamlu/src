package examination

import (
	"github.com/astaxie/beego"
	"kpx/models/examination"
)

type ChoiceQuestionController struct {
	beego.Controller
}

//获得选择题,根据测试(试卷)id
// @router /choicequestion/getchoicequestionbyexaminationid [get]
func (u *ChoiceQuestionController) GetChoicequestionByExaminationId() {
	examination_id, _ := u.GetInt("examination_id") //每页数量
	ss := examination.GetChoicequestionByExaminationId(examination_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得选择题获取
// @router /choicequestion/getchoicequestionbyid [get]
func (u *ChoiceQuestionController) GetChoiceQuestionById() {
	id, _ := u.GetInt("id") //每页数量
	ss := examination.GetChoicequestionById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//选择题信息分页
// @router /choicequestion/getchoicequestionbypage [get]
func (u *ChoiceQuestionController) GetChoiceQuestionByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := examination.GetChoicequestionByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//选择题信息删除
// @router /choicequestion/deletechoicequestionbyid/:id [delete]
func (u *ChoiceQuestionController) DeleteChoiceQuestionById() {
	id, _ := u.GetInt(":id")
	ss := examination.DeleteChoicequestionByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//选择题信息修改
// @router /choicequestion/updatechoicequestion [post]
func (u *ChoiceQuestionController) UpdateChoiceQuestion() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := examination.UpdateChoicequestion(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增选择题信息
// @router /choicequestion/createchoicequestion [post]
func (u *ChoiceQuestionController) CreateChoiceQuestion() {
	values := u.Ctx.Request.Form
	ss := examination.CreateChoicequestion(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
