package examination

import (
	"github.com/astaxie/beego"
	"kpx/models/examination"
	)

type ExaminationController struct {
	beego.Controller
}

//根据搜索条件获得试卷
// @router /document/getexaminationbysearch [post]
func (u *ExaminationController) GetExaminationBySearch() {
	values := u.Ctx.Request.Form
	ss := examination.GetExaminationBySearch(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//获得试卷,根据类别,分页
// @router /examination/getexaminationbycategory [get]
func (u *ExaminationController) GetExaminationByCategory() {
	category := u.GetString("category")
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := examination.GetExaminationByCategory(category, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得试卷获取
// @router /examination/getexaminationbyid [get]
func (u *ExaminationController) GetExaminationById() {
	id, _ := u.GetInt("id")
	ss := examination.GetExaminationById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//试卷信息分页
// @router /examination/getexaminationbypage [get]
func (u *ExaminationController) GetExaminationByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := examination.GetExaminationByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//试卷信息删除
// @router /examination/deleteexaminationbyid/:id [delete]
func (u *ExaminationController) DeleteExaminationById() {
	id, _ := u.GetInt(":id")
	ss := examination.DeleteExaminationByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//试卷信息修改
// @router /examination/updateexamination [post]
func (u *ExaminationController) UpdateExamination() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := examination.UpdateExamination(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增试卷信息
// @router /examination/createexamination [post]
func (u *ExaminationController) CreateExamination() {
	values := u.Ctx.Request.Form
	ss := examination.CreateExamination(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
