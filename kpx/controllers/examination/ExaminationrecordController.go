package examination

import (
	"github.com/astaxie/beego"
	"kpx/models/examination"
)

type ExaminationrecordController struct {
	beego.Controller
}

//根据用户id试卷试题记录信息分页,服务器端
// @router /examinationrecord/getexaminationrecordbyuseridpc [get]
func (u *ExaminationrecordController) GetExaminationrecordByUserIdPC() {
	user_id, _ := u.GetInt("user_id")
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := examination.GetExaminationrecordByUserIdPC(user_id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据用户id和测试记录id获得对应测试选择题
// @router /examinationrecord/geterrorexam [get]
func (u *ExaminationrecordController) GetErrorExam() {
	user_id, _ := u.GetInt("user_id")
	id, _ := u.GetInt("id") //页码
	ss := examination.GetErrorExam(user_id, id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据用户id试卷试题记录信息分页,小程序端
// @router /examinationrecord/getexaminationrecordbyuseridx [get]
func (u *ExaminationrecordController) GetExaminationrecordByUserIdx() {
	user_id, _ := u.GetInt("user_id")
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	category := u.GetString("category")
	ss := examination.GetExaminationrecordByUserIdx(user_id, clientPage, everyPage,category)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据用户id试卷试题记录信息分页
// @router /examinationrecord/getexaminationrecordbyuserid [get]
func (u *ExaminationrecordController) GetExaminationrecordByUserId() {
	user_id, _ := u.GetInt("user_id")
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	category := u.GetString("category")
	ss := examination.GetExaminationrecordByUserId(user_id, clientPage, everyPage,category)
	u.Data["json"] = ss
	u.ServeJSON()
}

//试卷试题记录信息删除
// @router /examinationrecord/deleteexaminationrecordbyid/:id [delete]
func (u *ExaminationrecordController) DeleteExaminationrecordById() {
	id, _ := u.GetInt(":id")
	ss := examination.DeleteExaminationrecordByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//试卷试题记录信息修改
// @router /examinationrecord/updateexaminationrecord [post]
func (u *ExaminationrecordController) UpdateExaminationrecord() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := examination.UpdateExaminationrecord(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增试卷试题记录信息
// @router 	/examinationrecord/createexaminationrecord [post]
func (u *ExaminationrecordController) CreateExaminationrecord() {
	values := u.Ctx.Request.Form
	examination_id := u.GetString("examination_id")
	//user_id := u.GetString("user_id")
	//examinationdate := u.GetString("examinationdate")
	answers := u.GetString("answers")
	//fmt.Println(answers)
	ss := examination.CreateExaminationrecord(values, answers,examination_id)
	u.Data["json"] = ss
	u.ServeJSON()
}
