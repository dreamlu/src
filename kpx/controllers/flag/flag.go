package flag

import (
	"github.com/astaxie/beego"
	"kpx/models/flag"
)

type FlagController struct {
	beego.Controller
}

//根据id获得flag获取
// @router /flag/getflagbyid [get]
func (u *FlagController) GetFlagById() {
	id, _ := u.GetInt("id")
	ss := flag.GetFlagById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//flag信息分页
// @router /flag/getflagbypage [get]
func (u *FlagController) GetFlagByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := flag.GetFlagByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//flag信息删除
// @router /flag/deleteflagbyid/:id [delete]
func (u *FlagController) DeleteFlagById() {
	id, _ := u.GetInt(":id")
	ss := flag.DeleteFlagByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//flag信息修改
// @router /flag/updateflag [post]
func (u *FlagController) UpdateFlag() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := flag.UpdateFlag(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增flag信息
// @router /flag/createflag [post]
func (u *FlagController) CreateFlag() {
	values := u.Ctx.Request.Form
	ss := flag.CreateFlag(values)
	u.Data["json"] = ss
	u.ServeJSON()
}