package message

import (
	"github.com/astaxie/beego"
	"kpx/models/message"
)

type SignnameController struct {
	beego.Controller
}

//根据id获得签名获取
// @router /signname/getbyid [get]
func (u *SignnameController) GetSignnameById() {
	id, _ := u.GetInt("id")
	ss := message.GetSignnameById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//签名信息分页
// @router /signname/getbypage [get]
func (u *SignnameController) GetSignnameByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := message.GetSignnameByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//签名信息删除
// @router /signname/deletebyid/:id [delete]
func (u *SignnameController) DeleteSignnameById() {
	id, _ := u.GetInt(":id")
	ss := message.DeleteSignnameByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//签名信息修改
// @router /signname/update [patch]
func (u *SignnameController) UpdateSignname() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := message.UpdateSignname(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增签名信息
// @router /signname/create [post]
func (u *SignnameController) CreateSignname() {
	values := u.Ctx.Request.Form
	ss := message.CreateSignname(values)
	u.Data["json"] = ss
	u.ServeJSON()
}