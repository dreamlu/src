package message

import (
	"github.com/Dreamlu/aliyun-sms-go-sdk/dysms"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/google/uuid"
	"github.com/tealeg/xlsx"
	"kpx/models/message"
	"strings"
	"time"
)

type MessageController struct {
	beego.Controller
}

//发送明细结构体
type SmsSendDetailDTOs struct {
	PhoneNum    string `json:"phone_num"`    //手机号
	SendStatus  int    `json:"send_status"`  //发送状态 1：等待回执，2：发送失败，3：发送成功
	SendDate    string `json:"send_date"`    //发送日期
	ReceiveDate string `json:"receive_date"` //接收日期
}

var (
	gatewayUrl      = "http://dysmsapi.aliyuncs.com/"
	accessKeyId     = "LTAIixfNGYqOaaUA"
	accessKeySecret = "vwkazxBbbCLq2NglgcgN4UjS7pYN9t"
	//signName     = "考培侠"
	//templateCode = "SMS_146700012"
	templateParam = "{}"
)

//获得上传的文件路径,不对外暴露(小写)
func (u *MessageController) filePath() string {
	//获得文件及保存到文件夹
	f, h, _ := u.GetFile("file") //获取上传的文件
	//上传文件重命名
	filenameSplit := strings.Split(h.Filename, ".")
	//防止文件名中多个“.”,获得文件后缀
	filename := "." + filenameSplit[len(filenameSplit)-1]
	filename = time.Now().Format("20060102150405") + filename //时间戳"2006-01-02 15:04:05"是参考格式,具体数字可变(经测试)
	path := beego.AppConfig.String("filepath") + filename     //文件目录
	defer f.Close()                                           //关闭上传的文件，不然的话会出现临时文件不能清除的情况
	u.SaveToFile("file", path)
	return path
}

//短信发送
// @router /message/send [post]
func (u *MessageController) SmsSend() {
	var info interface{}
	//var sds []message.Sendstatus
	//var sd message.Sendstatus

	path := u.filePath()

	signName := u.GetString("signname")    //签名
	templateCode := u.GetString("smscode") //模板code
	phoneNumbers := ""

	sql := "insert sendstatus(phone) value"

	xlFile, _ := xlsx.OpenFile(path)

	for _, sheet := range xlFile.Sheets {
		for _, row := range sheet.Rows {
			for _, cell := range row.Cells {
				if cell.String() == ""{
					continue
				}
				phoneNumbers += cell.String() + ","
				//sd.Phone = cell.String()
				//sds = append(sds, sd)
				sql += "("+cell.String() + "),"
			}
		}
	}
	sql = string([]byte(sql)[:len(sql)-1])

	phoneNumbers = string([]byte(phoneNumbers)[:len(phoneNumbers)-1])

	dysms.HTTPDebugEnable = true
	dysms.SetACLClient(accessKeyId, accessKeySecret) // dysms.New(ACCESSID, ACCESSKEY)
	// 短信发送
	respSendSms, err := dysms.SendSms(uuid.New().String(), phoneNumbers, signName, templateCode, templateParam).DoActionWithException()
	if err != nil {
		//fmt.Println("短信发送失败", err, respSendSms.Error())
		info = map[string]interface{}{"status": 500, "msg": respSendSms.Error()}
		//os.Exit(0)
	} else {
		info = map[string]interface{}{"status": 200, "msg": "发送完成"}
		//存数据库
		o := orm.NewOrm()
		o.Raw(sql).Exec()
	}

	u.Data["json"] = info
	u.ServeJSON()
}

//查询短信发送状态
// @router /message/find [get]
func (u *MessageController) FindSendstatus() {
	dysms.HTTPDebugEnable = true
	dysms.SetACLClient(accessKeyId, accessKeySecret) // dysms.New(ACCESSID, ACCESSKEY)

	sendDate := u.GetString("sendDate")//格式20181020

	//var sd SmsSendDetailDTOs
	var sds []*message.Sendstatus
	o := orm.NewOrm()
	//查询未同步状态的即可
	o.Raw("select *from sendstatus where status=1").QueryRows(&sds)
	//fmt.Printf("Returned Rows Num: %s, %s", num, err)
	for _, v := range sds {
		// 查询短信,一条数据即可
		rq, err := dysms.QuerySendDetails("", v.Phone, "1", "1", sendDate).DoActionWithException()
		if err == nil {
			so := rq.SmsSendDetailDTOs.SmsSendDetailDTO
			if len(so) > 0{
				o.Raw("update sendstatus set status=?,senddate=? where id=?",so[0].SendStatus,so[0].SendDate,v.Id).Exec()
			}
		}
	}

	u.Data["json"] = map[string]interface{}{"status": 200, "msg": "同步查询完成"}
	u.ServeJSON()
}
