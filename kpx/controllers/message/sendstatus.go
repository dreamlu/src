package message

import (
	"github.com/astaxie/beego"
	"kpx/models/message"
)

type SendstatusController struct {
	beego.Controller
}

//发送状态信息分页
// @router /sendstatus/getbypage [get]
func (u *SendstatusController) GetSendstatusByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := message.GetSendstatusByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}