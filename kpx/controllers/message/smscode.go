package message

import (
	"github.com/astaxie/beego"
	"kpx/models/message"
)

type SmscodeController struct {
	beego.Controller
}

//根据id获得模板code获取
// @router /smscode/getbyid [get]
func (u *SmscodeController) GetSmscodeById() {
	id, _ := u.GetInt("id")
	ss := message.GetSmscodeById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//模板code信息分页
// @router /smscode/getbypage [get]
func (u *SmscodeController) GetSmscodeByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := message.GetSmscodeByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//模板code信息删除
// @router /smscode/deletebyid/:id [delete]
func (u *SmscodeController) DeleteSmscodeById() {
	id, _ := u.GetInt(":id")
	ss := message.DeleteSmscodeByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//模板code信息修改
// @router /smscode/update [patch]
func (u *SmscodeController) UpdateSmscode() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := message.UpdateSmscode(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增模板code信息
// @router /smscode/create [post]
func (u *SmscodeController) CreateSmscode() {
	values := u.Ctx.Request.Form
	ss := message.CreateSmscode(values)
	u.Data["json"] = ss
	u.ServeJSON()
}