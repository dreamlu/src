package wx

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"github.com/Dreamlu/wechatpay"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/clbanning/mxj"
	"github.com/skip2/go-qrcode"
	"io/ioutil"
	"kpx/lib"
	"kpx/models/flag"
	"log"
	"strconv"
	"strings"
	"time"
)

type WxController struct {
	beego.Controller
}

var (
	wechat_cert    = "cert/apiclient_cert.pem"          //证书cert
	wechat_key     = "cert/apiclient_key.pem"           //证书key
	wechat_app_id  = "wx61d6f8124a4222b3"               //呵~ 这里是小程序的appid             //"wx2b9c8cd08c79022b"                   //应用的appid
	wechat_mch_id  = "1520812131"                       //商户号
	wechat_api_key = "yP5MIIebIPvtaxd6M3E0Zu7yokybYc4Y" //商户支付密钥
)

type XMLStruct struct {
	ReturnCode string `json:"return_code"`
	ReturnMsg  string `json:"return_msg"`
}

//var wechat_client *wechatpay.WechatPay

var wechat_client = wechatpay.New(wechat_app_id, wechat_mch_id, wechat_api_key, []byte(wechat_cert), []byte(wechat_key))

//微信扫码支付
// @router /wx/vipay [get]
func (u *WxController) WxPay() {
	//用户在线id
	uid, ok := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"), "uid")
	if !ok {
		u.Ctx.WriteString("{\"status\": \"203\", \"msg\": \"请求非法\"}")
		return
	}
	fmt.Println("扫码支付：用户id---->" + uid)
	//查询vip价格
	o := orm.NewOrm()
	var flag *flag.Flag
	o.Raw("select *from `flag` where id=?", 1).QueryRow(&flag)
	if flag.Value == "" {
		u.Data["json"] = lib.GetMapDataError(271, "价格不能设置为空")
		u.ServeJSON()
		return
	}
	price, _ := strconv.ParseFloat(flag.Value, 64)

	var pay_data wechatpay.UnitOrder
	pay_data.NotifyUrl = "https://kaopeixia.com/webapi/wx/vipayinfo" //?uid=" + uid //不想抽取!
	pay_data.TradeType = "NATIVE"
	pay_data.Body = "考培侠"
	//pay_data.SpbillCreateIp = "47.94.8.18"
	pay_data.TotalFee = int(price * 100)
	pay_data.OutTradeNo = strconv.FormatInt(time.Now().Unix(), 10) + "-" + uid
	result, err := wechat_client.Pay(pay_data)
	if err != nil {
		log.Println(err)
		u.Data["json"] = err
		u.ServeJSON()
		return
	}
	fmt.Println(result.CodeUrl)
	Qrcode(u, result.CodeUrl)
}

//二维码生成
func Qrcode(u *WxController, url string) {
	w := u.Ctx.ResponseWriter

	var err error
	defer func() {
		if err != nil {
			w.WriteHeader(500)
			return
		}
	}()
	png, err := qrcode.Encode(url, qrcode.Medium, 256)
	if err != nil {
		return
	}
	w.Header().Set("Content-Type", "image/png")
	w.Header().Set("Content-Length", fmt.Sprintf("%d", len(png)))
	w.Write(png)
}

//微信扫码回调地址
// @router /wx/vipayinfo [post]
func (u *WxController) WechatWebNotifyUrl() {
	log.Println("wechat notify start")
	body, err := ioutil.ReadAll(u.Ctx.Request.Body)
	if err != nil {
		log.Println("read http body failed！error msg:" + err.Error())
	}
	log.Println("wechat pay notify body :" + string(body))

	var wx_notify_req wechatpay.PayNotifyResult

	err = xml.Unmarshal(body, &wx_notify_req)
	if err != nil {
		log.Println("read http body xml failed! err :" + err.Error())
	}
	mv, err := mxj.NewMapXml(body)
	if err != nil {
		log.Println("body to map err :", err.Error())
	}

	//进行签名校验
	if wechat_client.VerifySign(mv["xml"].(map[string]interface{}), mv["xml"].(map[string]interface{})["sign"].(string)) {
		_, err := json.Marshal(wx_notify_req)
		fmt.Println("record:", wx_notify_req)
		if err != nil {
			log.Println(err, "wechat pay marshal err :"+err.Error())
		}
		//支付成功,vip状态改变
		if wx_notify_req.ResultCode == "SUCCESS" {
			fmt.Println("商户订单号：", wx_notify_req.OutTradeNo)
			fmt.Println("支付价(分)：", wx_notify_req.TotalFee)
			fmt.Println("支付价(元)：", wx_notify_req.TotalFee*1.0/100)
			otd := strings.Split(wx_notify_req.OutTradeNo, "-")
			o := orm.NewOrm()
			o.Raw("update `user` set is_vip=1,vipprice=? where id=? ", float64(wx_notify_req.TotalFee)/100, otd[1]).Exec()
		}
		u.Data["xml"] =
			&XMLStruct{
				ReturnCode: "SUCCESS",
				ReturnMsg:  "OK",
			}
		u.ServeXML()
	} else {
		u.Data["xml"] =
			&XMLStruct{
				ReturnCode: "FAIL",
				ReturnMsg:  "failed to verify sign, please retry!",
			}
		u.ServeXML()
	}
	return
}
