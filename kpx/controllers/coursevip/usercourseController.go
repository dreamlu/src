package coursevip

import (
	"github.com/astaxie/beego"
	"kpx/models/coursevip"
)

type UserCourseController struct {
	beego.Controller
}

//获得用户选中的栏目(听说读写),根据用户id和课程id
// @router /coursevip/getcourselistbycourseid [get]
func (u *UserCourseController) GetCouselistByCourseId() {
	user_id, _ := u.GetInt("user_id")
	course_id, _ := u.GetInt("course_id")
	ss := coursevip.GetCouselistByCourseId(user_id,course_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据user_id获得用户课程获取
// @router /coursevip/getusercoursebyuserid [get]
func (u *UserCourseController) GetUserCourseByUserId() {
	user_id, _ := u.GetInt("user_id")
	category := u.GetString("category")
	clientPage,_ := u.GetInt("clientPage")
	everyPage,_ := u.GetInt("everyPage")
	ss := coursevip.GetUserCourseByUserId(user_id,category,clientPage,everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//用户课程信息删除
// @router /coursevip/deleteusercoursebyid/:id [delete]
func (u *UserCourseController) DeleteUserCourseById() {
	id, _ := u.GetInt(":id")
	ss := coursevip.DeleteUserCourseByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//用户课程信息修改
// @router /coursevip/updateusercourse [post]
func (u *UserCourseController) UpdateUserCourse() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := coursevip.UpdateUserCourse(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增用户课程信息
// @router /coursevip/createusercourse [post]
func (u *UserCourseController) CreateUserCourse() {
	values := u.Ctx.Request.Form
	ss := coursevip.CreateUserCourse(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
