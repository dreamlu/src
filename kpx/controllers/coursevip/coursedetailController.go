package coursevip

import (
	"github.com/astaxie/beego"
	"kpx/models/coursevip"
)

type CoursedetailController struct {
	beego.Controller
}

//根据小栏目条件获得听说读写对应视频
// @router /coursevip/getcoursedetailbycourselistidp [get]
func (u *CoursedetailController) GetCoursedetailByCourselistIdP() {
	courselist_id := u.GetString("courselist_id")
	ss := coursevip.GetCoursedetailByCourselistIdP(courselist_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据小栏目条件获得听说读写对应视频,小程序
// @router /coursevip/getcoursedetailbycourselistid [get]
func (u *CoursedetailController) GetCoursedetailByCourselistId() {
	user_id := u.GetString("user_id")
	courselist_id := u.GetString("courselist_id")
	ss := coursevip.GetCoursedetailByCourselistId(user_id, courselist_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得听说读写对应视频获取
// @router /coursevip/getcoursedetailbyid [get]
func (u *CoursedetailController) GetCoursedetailById() {
	id, _ := u.GetInt("id")
	ss := coursevip.GetCoursedetailById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//听说读写对应视频信息分页
// @router /coursevip/getcoursedetailbypage [get]
func (u *CoursedetailController) GetCoursedetailByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := coursevip.GetCoursedetailByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//听说读写对应视频信息删除
// @router /coursevip/deletecoursedetailbyid/:id [delete]
func (u *CoursedetailController) DeleteCoursedetailById() {
	id, _ := u.GetInt(":id")
	ss := coursevip.DeleteCoursedetailByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//听说读写对应视频信息修改
// @router /coursevip/updatecoursedetail [post]
func (u *CoursedetailController) UpdateCoursedetail() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := coursevip.UpdateCoursedetail(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增听说读写对应视频信息
// @router /coursevip/createcoursedetail [post]
func (u *CoursedetailController) CreateCoursedetail() {
	values := u.Ctx.Request.Form
	ss := coursevip.CreateCoursedetail(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
