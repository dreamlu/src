package coursevip

import (
	"github.com/astaxie/beego"
	"kpx/models/coursevip"
)

type CourselistController struct {
	beego.Controller
}

//小栏目小程序端,根据课程id
// @router /coursevip/getcourselistbycourseidx [get]
func (u *CourselistController) GetCourselistByCourseIdX() {
	course_id := u.GetString("course_id")
	ss := coursevip.GetCourselistByCourseIdX(course_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//小栏目下拉框,根据课程id
// @router /coursevip/getcourselistbycourseid [get]
func (u *CourselistController) GetCourselistByCourseId() {
	course_id := u.GetString("course_id")
	ss := coursevip.GetCourselistByCourseId(course_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得小栏目获取
// @router /coursevip/getcourselistbyid [get]
func (u *CourselistController) GetCourselistById() {
	id, _ := u.GetInt("id")
	ss := coursevip.GetCourselistById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//小栏目信息分页
// @router /coursevip/getcourselistbypage [get]
func (u *CourselistController) GetCourselistByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := coursevip.GetCourselistByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//小栏目信息删除
// @router /coursevip/deletecourselistbyid/:id [delete]
func (u *CourselistController) DeleteCourselistById() {
	id, _ := u.GetInt(":id")
	ss := coursevip.DeleteCourselistByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//小栏目信息修改
// @router /coursevip/updatecourselist [post]
func (u *CourselistController) UpdateCourselist() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := coursevip.UpdateCourselist(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增小栏目信息
// @router /coursevip/createcourselist [post]
func (u *CourselistController) CreateCourselist() {
	values := u.Ctx.Request.Form
	ss := coursevip.CreateCourselist(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
