package coursevip

import (
	"github.com/astaxie/beego"
	"kpx/models/coursevip"
)

type CourseController struct {
	beego.Controller
}

//根据搜索条件获得课程
// @router /coursevip/getcoursebysearchv3 [get]
func (u *CourseController) GetCourseBySearchV3() {
	category := u.GetString("category")
	clientPage,_ := u.GetInt("clientPage")
	everyPage,_ := u.GetInt("everyPage")
	ss := coursevip.GetCourseBySearchV3(category,clientPage,everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据搜索条件获得课程
// @router /coursevip/getcoursebysearchv2 [get]
func (u *CourseController) GetCourseBySearchV2() {
	category := u.GetString("category")
	clientPage,_ := u.GetInt("clientPage")
	everyPage,_ := u.GetInt("everyPage")
	ss := coursevip.GetCourseBySearchV2(category,clientPage,everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据搜索条件获得课程
// @router /coursevip/getcoursebysearch [post]
func (u *CourseController) GetCourseBySearch() {
	values := u.Ctx.Request.Form
	ss := coursevip.GetCourseBySearch(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得课程获取
// @router /coursevip/getcoursebyid [get]
func (u *CourseController) GetCourseById() {
	id, _ := u.GetInt("id")
	ss := coursevip.GetCourseById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//课程信息分页
// @router /coursevip/getcoursebypage [get]
func (u *CourseController) GetCourseByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := coursevip.GetCourseByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//课程信息删除
// @router /coursevip/deletecoursebyid/:id [delete]
func (u *CourseController) DeleteCourseById() {
	id, _ := u.GetInt(":id")
	ss := coursevip.DeleteCourseByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//课程信息修改
// @router /coursevip/updatecourse [post]
func (u *CourseController) UpdateCourse() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := coursevip.UpdateCourse(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增课程信息
// @router /coursevip/createcourse [post]
func (u *CourseController) CreateCourse() {
	values := u.Ctx.Request.Form
	ss := coursevip.CreateCourse(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
