package coursevip

import (
	"github.com/astaxie/beego"
	"kpx/models/coursevip"
)

type CommentController struct {
	beego.Controller
}

//根据搜索条件获得评论
// @router /coursevip/getcommentbycoursedetailid [get]
func (u *CommentController) GetCommentByCoursedetailId() {
	coursedetail_id, _ := u.GetInt("coursedetail_id")
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := coursevip.GetCommentByCoursedetailId(coursedetail_id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得评论获取
// @router /coursevip/getcommentbyid [get]
func (u *CommentController) GetCommentById() {
	id, _ := u.GetInt("id")
	ss := coursevip.GetCommentById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//评论信息分页
// @router /coursevip/getcommentbypage [get]
func (u *CommentController) GetCommentByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := coursevip.GetCommentByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//评论信息删除
// @router /coursevip/deletecommentbyid/:id [delete]
func (u *CommentController) DeleteCommentById() {
	id, _ := u.GetInt(":id")
	ss := coursevip.DeleteCommentByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//评论信息修改
// @router /coursevip/updatecomment [post]
func (u *CommentController) UpdateComment() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := coursevip.UpdateComment(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增评论信息
// @router /coursevip/createcomment [post]
func (u *CommentController) CreateComment() {
	values := u.Ctx.Request.Form
	ss := coursevip.CreateComment(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
