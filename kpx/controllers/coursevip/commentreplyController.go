package coursevip

import (
	"github.com/astaxie/beego"
	"kpx/models/coursevip"
)

type CommentReplyController struct {
	beego.Controller
}

//回复信息分页
// @router /coursevip/getcommentreplybypage [get]
func (u *CommentReplyController) GetCommentReplyByPage() {
	comment_id, _ := u.GetInt("comment_id") //页码
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := coursevip.GetCommentReplyByPage(comment_id,clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//回复信息删除
// @router /coursevip/deletecommentreplybyid/:id [delete]
func (u *CommentReplyController) DeleteCommentReplyById() {
	id, _ := u.GetInt(":id")
	ss := coursevip.DeleteCommentReplyByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增回复信息
// @router /coursevip/createcommentreply [post]
func (u *CommentReplyController) CreateCommentReply() {
	values := u.Ctx.Request.Form
	ss := coursevip.CreateCommentReply(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
