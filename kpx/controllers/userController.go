package controllers

import (
	"github.com/astaxie/beego"
	"kpx/models"
	"kpx/util"
)

type UserController struct {
	beego.Controller
}

//根据openid获得用户id
// @router /user/getuseridbyopenid [post]
func (u *UserController) GetUserIdByOpenId() {
	openid := u.GetString("openid")
	ss := models.GetUserIdByOpenId(openid)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据搜索条件获得用户
// @router /user/getuserbysearch [post]
func (u *UserController) GetUserBySearch() {
	values := u.Ctx.Request.Form
	ss := models.GetUserBySearch(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得用户状态获取
// @router /user/getusercheckbyid [get]
func (u *UserController) GetUserCheckById() {
	id, _ := u.GetInt("id")
	ss := models.GetUserCheckById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得用户获取
// @router /user/getuserbyid [get]
func (u *UserController) GetUserById() {
	id, _ := u.GetInt("id")
	ss := models.GetUserById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//用户信息分页
// @router /user/getuserbypage [get]
func (u *UserController) GetUserByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := models.GetUserByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//用户信息删除
// @router /user/deleteuserbyid/:id [delete]
func (u *UserController) DeleteUserById() {
	id, _ := u.GetInt(":id")
	ss := models.DeleteUserByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//用户信息修改
// @router /user/updateuser [post]
func (u *UserController) UpdateUser() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := models.UpdateUser(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增用户信息
// @router /user/createuser [post]
func (u *UserController) CreateUser() {
	values := u.Ctx.Request.Form
	ss := models.CreateUser(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//修改用户账号密码
// @router /user/updateaccount [post]
func (u *UserController) UpdateAccount() {
	uid, _ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"), "uid")
	nickname := u.GetString("nickname")
	userpassword := u.GetString("userpassword")
	userpassword =  util.AesEn(userpassword)
	ss := models.UpdateAccount(uid,nickname,userpassword)
	u.Data["json"] = ss
	u.ServeJSON()
}
