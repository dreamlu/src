package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/KenmyZhang/aliyun-communicate"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"io/ioutil"
	"kpx/lib"
	"kpx/util"
	"math/rand"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"
)

/*哎,laji代码*/
type LoginController struct {
	beego.Controller
}

type User struct {
	Id           int
	Openid       string //OpenId不行,只能首字母大写,beego...
	Session_key  string //sessionkey
	Username     string //用户真实名称
	Userpassword string //密码
	Nickname     string //昵称
	Headimg      string //头像
}

type UserN struct {
	AccessToken  string `json:"access_token"`
	ExpiresIn    int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	Openid       string `json:"openid"`
	Scope        string `json:"scope"`
}

type UserInfo struct {
	Id         int      `json:"id"`
	Openid     string   `json:"openid"`
	Nickname   string   `json:"nickname"`
	Sex        int      `json:"sex"`
	Province   string   `json:"province"`
	City       string   `json:"city"`
	Country    string   `json:"country"`
	Headimgurl string   `json:"headimgurl"`
	Privilege  []string `json:"privilege"`
	Unionid    string   `json:"unionid"`
}

/*账号密码等0*/
type UserAcount struct {
	Id           int    `json:"id"`
	Userpassword string `json:"userpassword"`
	IsVip        string    `json:"is_vip"`
}

//用户扫码登录
// @router /openid/getwebtoken [get]
func (this *LoginController) GetWebToken() {

	code := this.GetString("code")

	appid := "wx2b9c8cd08c79022b"                //this.GetString("appid")
	secret := "23ce8d416477b572a80edea4ca789bb6" //this.GetString("secret")
	fmt.Println("临时登录凭证：" + code)

	//获取openid接口
	te_uri := "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid + "&secret=" + secret + "&code=" + code + "&grant_type=authorization_code"
	res, _ := http.Get(te_uri)
	body, _ := ioutil.ReadAll(res.Body)
	fmt.Println("网页微信登录openid等数据:" + string(body))
	defer res.Body.Close()

	//json数据的获取与返回,access_token和openid的获取
	token := UserN{}
	json.Unmarshal(body, &token) //反序列化

	te_uri = "https://api.weixin.qq.com/sns/userinfo?access_token=" + token.AccessToken + "&openid=" + token.Openid
	res, _ = http.Get(te_uri)
	body, _ = ioutil.ReadAll(res.Body)
	fmt.Println("网页微信登录unionid等数据:" + string(body))
	//defer res.Body.Close()

	//json数据的获取与返回,unionid的获取
	userInfo := UserInfo{}
	json.Unmarshal(body, &userInfo) //反序列化

	//新用户注册(无论新老用户尝试一次openid注册)
	var flag = 0 //默认老用户
	o := orm.NewOrm()
	rest, err := o.Raw("insert `user`(unionid,openid,joindate,headimg,nickname) value(?,?,?,?,?)", userInfo.Unionid, userInfo.Openid, time.Now().Format("2006-01-02 15:04:05"), userInfo.Headimgurl, userInfo.Nickname).Exec()
	var num, id int64
	if err == nil {
		num, _ = rest.RowsAffected()
		id, _ = rest.LastInsertId()
		if num > 0 {
			//flag = 1
			//this.SetSecureCookie(beego.AppConfig.String("secertkey"), "uid", strconv.FormatInt(id, 10))
			//默认密码
			o.Raw("insert `useraccount`(user_id,userpassword) value(?,?)", id, util.AesEn("123456")).Exec()
		}
	}
	//无论新老用户查找对应数据
	//根据unionid查询用户id
	var maps []orm.Params
	num, _ = o.Raw("select id,phone,is_vip from `user` where unionid=?", userInfo.Unionid).Values(&maps)
	if num > 0 {
		//手机号为空,新用户
		fmt.Println("手机号数据:" + maps[0]["phone"].(string))
		if maps[0]["phone"].(string) == "" {
			flag = 1
		}
		this.SetSecureCookie(beego.AppConfig.String("secertkey"), "v", maps[0]["is_vip"].(string))
		this.SetSecureCookie(beego.AppConfig.String("secertkey"), "uid", maps[0]["id"].(string))
		this.Data["json"] = map[string]interface{}{"status": 200, "msg": "请求成功", "uid": maps[0]["id"].(string), "flag": flag, "is_vip": maps[0]["id"].(string)}
	} else {
		this.Data["json"] = lib.MapNoAuth
	}
	//this.Redirect("/login.html?access_token="+userInfo.AccessToken+"&openid="+userInfo.Openid, 200)
	this.ServeJSON()
}

// @router /openid/getopenid [get]
func (this *LoginController) Getopenid() {

	code := this.GetString("code")
	appid := this.GetString("appid")
	secret := this.GetString("secret")
	userorigin := this.GetString("userorigin")
	fmt.Println("临时登录凭证：" + code)

	//获取openid接口
	te_uri := "https://api.weixin.qq.com/sns/jscode2session?appid=" + appid + "&secret=" + secret + "&js_code=" + code + "&grant_type=authorization_code"
	res, _ := http.Get(te_uri)
	body, _ := ioutil.ReadAll(res.Body)
	fmt.Println("用户openid等相关数据:" + string(body))
	defer res.Body.Close()

	//json数据的获取与返回
	userInfo := User{}
	json.Unmarshal(body, &userInfo) //反序列化

	//新用户注册(无论新老用户尝试一次openid注册)
	o := orm.NewOrm()
	o.Raw("insert `user`(openid,userorigin,joindate) value(?,?,?)", userInfo.Openid, userorigin, time.Now().Format("2006-01-02 15:04:05")).Exec()

	//反射遍历结构体，赋值给json
	//要求是可导出的字段大写
	k := reflect.ValueOf(userInfo)
	v := reflect.TypeOf(userInfo)
	//返回的json集合
	jsonMap := make(map[string]string)
	for i := 0; i < v.NumField(); i++ {
		typeX := v.Field(i).Type
		switch typeX.String() { //多选语句switch
		case "string":
			jsonMap[v.Field(i).Name] = k.Field(i).Interface().(string)
			//case "int":
			//	this.Data["json"] = map[string]int{v.Field(i).Name: k.Field(i).Interface().(int)}
		}
	}
	this.Data["json"] = jsonMap
	//只返回了最后一个json
	fmt.Println(this.Data["json"])
	this.ServeJSON()
}

//登录
// @router /login/login [post]
func (this *LoginController) Login() {
	var info interface{}
	username := this.GetString("nickname")
	userpassword := this.GetString("userpassword")
	o := orm.NewOrm()
	o.Using("default")
	var user []*User
	num, err := o.Raw("SELECT a.id as id,userpassword FROM `user` a inner join `useraccount` b on a.id=b.user_id WHERE nickname = ?", username).QueryRows(&user)
	if err == nil && num > 0 {
		userpassword = util.AesEn(userpassword)
		for _, v := range user {
			if v.Userpassword == userpassword {
				this.SetSecureCookie(beego.AppConfig.String("secertkey"), "uid", strconv.Itoa(v.Id))
				info = map[string]string{"status": "200", "msg": "请求成功", "uid": strconv.Itoa(v.Id)}
			} else {
				info = lib.MapNoAuth
			}
		}
	} else {
		info = lib.MapNoAuth
	}
	this.Data["json"] = info
	this.ServeJSON()
}

//普通用户注册
// @router /user/register [post]
func (this *LoginController) UserRegister() {
	var info interface{}
	//uid := this.GetString("uid")
	//phone := this.GetString("phone")
	var phone string
	//验证码
	code := this.GetString("code")
	userpassword := this.GetString("userpassword")
	userpassword = util.AesEn(userpassword)
	//用户id
	uid, _ := this.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"), "uid")
	//用户phone和code验证码
	phocode, _ := this.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"), "phocode")

	pco := strings.Split(phocode, "&")
	if len(pco) > 0 {
		phone = pco[0]
		pcode := pco[1]
		//短信验证码判断
		if pcode != code {
			info = map[string]interface{}{"status": 500, "msg": "验证码不正确"}
		} else {
			//手机验证正确后进行以下操作
			o := orm.NewOrm()
			o.Using("default")
			//修改手机号前先检测该手机号是否已经存在(手机端直接手机注册)
			var user UserInfo
			var user2 UserInfo
			o.Raw("select * from `user` where phone=?", phone).QueryRow(&user2)
			//用户存在,合并操作,且是通过微信扫码后注册的
			if uid != "" && user2.Openid != "" {
				//删除刚新增的微信扫码用户
				//并进行相应的合并绑定操作
				o.Begin()
				o.Raw("select * from `user` where id=?", uid).QueryRow(&user)
				o.Raw("delete from `user` where unionid=?", user.Unionid).Exec()
				o.Raw("update `user` set unionid=?,openid=?,headimg=?,nickname=? where phone=?", user.Unionid, user.Openid, user.Headimgurl, user.Nickname, phone).Exec()

				//更新密码
				o.Raw("update `useraccount` set userpassword=? where user_id=?", userpassword, user2.Id).Exec()
				o.Commit()
				info = map[string]string{"status": "200", "msg": "账号密码创建成功"}
				//用户直接手机号注册,但是已存在
			} else if uid == "" && user2.Openid != "" {
				info = map[string]interface{}{"status": 210, "msg": "手机账号已存在"}
				this.Data["json"] = info
				this.ServeJSON()
				return
			} else { //用户不存在,注册新用户
				//修改手机号,前提是先扫码注册
				res, err := o.Raw("update `user` set phone=? where id=?", phone, uid).Exec()
				var num int64
				if err == nil {
					num, _ = res.RowsAffected()
				}

				if err == nil && num > 0 {
					//插入密码
					//userpassword = util.AesEn(userpassword)
					res, err = o.Raw("update `useraccount` set userpassword=? where user_id=?", userpassword, uid).Exec()
					num = 0
					if err == nil {
						num, _ = res.RowsAffected()
					}
					if num > 0 {
						info = map[string]string{"status": "200", "msg": "密码创建成功"}
					} else {
						info = map[string]string{"status": "210", "msg": "密码创建失败"}
					}
				} else {
					//手机号直接注册
					o := orm.NewOrm()
					rest, err := o.Raw("insert `user`(unionid,openid,phone,joindate) value(?,?,?,?)", phone, phone, phone, time.Now().Format("2006-01-02 15:04:05")).Exec()
					var num, id int64
					if err == nil {
						num, _ = rest.RowsAffected()
						id, _ = rest.LastInsertId()
						if num > 0 {
							//this.SetSecureCookie(beego.AppConfig.String("secertkey"), "uid", strconv.FormatInt(id, 10))
							//插入密码
							o.Raw("insert `useraccount`(user_id,userpassword) value(?,?)", id, userpassword).Exec()
							info = map[string]interface{}{"status": 200, "msg": "账号创建成功"}
						} else {
							info = map[string]interface{}{"status": 210, "msg": "密码创建失败"}
						}
					} else {
						info = map[string]interface{}{"status": 210, "msg": "账号创建失败"}
					}
				}
			}
		}
	} else {
		info = map[string]interface{}{"status": 210, "msg": "验证码失效"}
	}

	this.Data["json"] = info
	this.ServeJSON()
}

// 网页用户登陆
// @router /user/login [post]
func (this *LoginController) UserLogin() {
	var info interface{}
	phone := this.GetString("phone")
	userpassword := this.GetString("userpassword")
	o := orm.NewOrm()
	o.Using("default")
	var user []*UserAcount
	num, err := o.Raw("SELECT a.id as id,userpassword,is_vip FROM `user` a inner join `useraccount` b on a.id=b.user_id WHERE phone = ?", phone).QueryRows(&user)
	if err == nil && num > 0 {
		userpassword = util.AesEn(userpassword)
		for _, v := range user {
			if v.Userpassword == userpassword {
				this.SetSecureCookie(beego.AppConfig.String("secertkey"), "v", v.IsVip)
				this.SetSecureCookie(beego.AppConfig.String("secertkey"), "uid", strconv.Itoa(v.Id))
				info = map[string]string{"status": "200", "msg": "请求成功", "uid": strconv.Itoa(v.Id),"is_vip": v.IsVip}
			} else {
				info = lib.MapNoAuth
			}
		}
	} else {
		info = lib.MapNoAuth
	}
	this.Data["json"] = info
	this.ServeJSON()
}

var (
	gatewayUrl      = "http://dysmsapi.aliyuncs.com/"
	accessKeyId     = "LTAIixfNGYqOaaUA"
	accessKeySecret = "vwkazxBbbCLq2NglgcgN4UjS7pYN9t"
	//phoneNumbers    = "15869190407"
	signName     = "考培侠"
	templateCode = "SMS_146700012"
	//templateParam   = "{\"code\":\"2018\"}"
)

//获取短信验证码
// @router /user/smscode [post]
func (this *LoginController) UserSmsCode() {
	var info interface{}

	phoneNumbers := this.GetString("phone")
	var user2 UserInfo
	o := orm.NewOrm()
	o.Raw("select * from `user` where phone=?", phoneNumbers).QueryRow(&user2)
	//用户存在,合并操作,且是通过微信扫码后注册的
	/*if user2.Openid == "" {
		this.Data["json"] = map[string]interface{}{"status":210,"msg":"手机账号不存在"}
		this.ServeJSON()
		return
	}*/
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	vcode := fmt.Sprintf("%06v", rnd.Int31n(1000000))
	templateParam := "{\"code\":\"" + vcode + "\"}"

	this.SetSecureCookie(beego.AppConfig.String("secertkey"), "phocode", phoneNumbers+"&"+vcode)

	smsClient := aliyunsmsclient.New(gatewayUrl)
	result, err := smsClient.Execute(accessKeyId, accessKeySecret, phoneNumbers, signName, templateCode, templateParam)
	fmt.Println("Got raw response from server:", string(result.RawResponse))
	if err != nil {
		panic("Failed to send Message: " + err.Error())
	}

	//resultJson, err := json.Marshal(result)
	if err != nil {
		panic(err)
	}
	if result.IsSuccessful() {
		//fmt.Println("A SMS is sent successfully:", resultJson)
		info = map[string]interface{}{"status": 200, "msg": "验证码发送成功"}

	} else {
		//fmt.Println("Failed to send a SMS:", resultJson)
		info = map[string]interface{}{"status": 500, "msg": "验证码发送失败"}
	}
	this.Data["json"] = info
	this.ServeJSON()
}

//根据短信验证码修改密码
// @router /user/forgetpasswd [post]
func (this *LoginController) ForgetPasswd() {
	var info interface{}

	//验证码
	code := this.GetString("code")
	userpassword := this.GetString("userpassword")
	userpassword = util.AesEn(userpassword)
	//用户id
	//uid, _ := this.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"), "uid")
	//用户phone和code验证码
	phocode, _ := this.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"), "phocode")

	pco := strings.Split(phocode, "&")
	if len(pco) > 0 {
		phone := pco[0]
		pcode := pco[1]
		//短信验证码判断
		if pcode != code {
			info = map[string]interface{}{"status": 500, "msg": "验证码不正确"}
		} else {
			o := orm.NewOrm()
			var maps []orm.Params
			num, _ := o.Raw("select id from `user` where phone=?", phone).Values(&maps)
			if num > 0 {
				o.Raw("update `useraccount` set userpassword=? where user_id=?", userpassword, maps[0]["id"].(string)).Exec()
				info = map[string]interface{}{"status": 200, "msg": "修改成功"}
			} else {
				info = map[string]interface{}{"status": 210, "msg": "用户不存在"}
			}
		}
	} else {
		info = map[string]interface{}{"status": 500, "msg": "请注册或登录"}
	}
	this.Data["json"] = info
	this.ServeJSON()
}
