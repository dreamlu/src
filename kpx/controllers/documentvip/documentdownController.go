package documentvip

import (
	"github.com/astaxie/beego"
	"kpx/models/documentvip"
)

type DocumentDownController struct {
	beego.Controller
}

//根据用户id获得文档下载记录获取
// @router /documentvip/getdocumentdownbyuserid [post]
func (u *DocumentDownController) GetDocumentDownByUserId() {
	values := u.Ctx.Request.Form
	ss := documentvip.GetDocumentDownByUserId(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//文档下载记录信息分页
// @router /documentvip/getdocumentdownbypage [get]
func (u *DocumentDownController) GetDocumentDownByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := documentvip.GetDocumentDownByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//文档下载记录信息修改
// @router /documentvip/updatedocumentdown [post]
func (u *DocumentDownController) UpdateDocumentDown() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := documentvip.UpdateDocumentDown(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增文档下载记录信息
// @router /documentvip/createdocumentdown [post]
func (u *DocumentDownController) CreateDocumentDown() {
	values := u.Ctx.Request.Form
	ss := documentvip.CreateDocumentDown(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
