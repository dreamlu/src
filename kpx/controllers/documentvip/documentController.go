package documentvip

import (
	"github.com/astaxie/beego"
	"kpx/models/documentvip"
)

type DocumentController struct {
	beego.Controller
}

//根据搜索条件获得文档
// @router /documentvip/getdocumentbysearch [post]
func (u *DocumentController) GetDocumentBySearch() {
	values := u.Ctx.Request.Form
	ss := documentvip.GetDocumentBySearch(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得文档获取
// @router /documentvip/getdocumentbyid [get]
func (u *DocumentController) GetDocumentById() {
	id, _ := u.GetInt("id") //每页数量
	ss := documentvip.GetDocumentById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//文档信息分页
// @router /documentvip/getdocumentbypage [get]
func (u *DocumentController) GetDocumentByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := documentvip.GetDocumentByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//文档信息删除
// @router /documentvip/deletedocumentbyid/:id [delete]
func (u *DocumentController) DeleteDocumentById() {
	id, _ := u.GetInt(":id")
	ss := documentvip.DeleteDocumentByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//文档信息修改
// @router /documentvip/updatedocument [post]
func (u *DocumentController) UpdateDocument() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := documentvip.UpdateDocument(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增文档信息
// @router /documentvip/createdocument [post]
func (u *DocumentController) CreateDocument() {
	values := u.Ctx.Request.Form
	ss := documentvip.CreateDocument(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
