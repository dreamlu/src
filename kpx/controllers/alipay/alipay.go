package alipay

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/smartwalle/alipay"
	"kpx/lib"
	"kpx/models/flag"
	"log"
	"strconv"
	"strings"
	"time"
)

type AliController struct {
	beego.Controller
}

var appId = "2018121162519388"
//支付宝公钥
var aliPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArhIbEXQ4wiWq9ES3pFZeCh0AJ0OHMkzoaz0Ya+C5WJgb/U781gAdMrF855aJooheuF+mvWAQ0+XAmQyj+wr3oRcoXUDGlE7EhAAOn2kUbBVBve4dJrXpZLPqSt8iyZzpzD8Jpfv0SR5EGQY+FTaPKx6w5saLDyl0LJPteAJqSUGoSntZH2p8Ts62KHlaOKzp7HEgfGwUU7LwoyylGFVZsYzjthHOFoyYVnlWBM0FGifrpm9rWhhtc2s/Tnj/XEVBcgS5EBkqQT1+97hQZl99VxD5427qnGCQXZOYFbWV2HWZEslNYgw0QXZQ2dAEBhcbie9lZCglZGvsfwYuy7ZovQIDAQAB"
//应用公钥
//var aliPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvUIZrG+Dme6iuqwHcCxeXd73j142RGrwjPBPGoifNIciDM1BkOKTJhwim3V6yLIXHzZVGpaoVNPfgjG0Wy8UP6SpulDcFXtGgE3MGZF1HxiM7bu+EOUFPXJYW4DZU4I23wd7i+vuiTVcL9uny6V1+eeAG/l/beZokW4ygHgvAOKj6GgLyUXfuFYsf80/N30qZiML9P22x/dUqlayRcDc665C4nEJmQlXqn4lwi4E7z0Hrt1cWj1fF3Iodo30fBB5KRF+TT3Vgf8/qIpymYojNRv2e/veCn/eeE/g0MTZuId1jJ1uf3SkE6m6YpKfkxmz/t0mcPYKTEtkvkfCWuPhQwIDAQAB"
var privateKey = "MIIEowIBAAKCAQEAvUIZrG+Dme6iuqwHcCxeXd73j142RGrwjPBPGoifNIciDM1BkOKTJhwim3V6yLIXHzZVGpaoVNPfgjG0Wy8UP6SpulDcFXtGgE3MGZF1HxiM7bu+EOUFPXJYW4DZU4I23wd7i+vuiTVcL9uny6V1+eeAG/l/beZokW4ygHgvAOKj6GgLyUXfuFYsf80/N30qZiML9P22x/dUqlayRcDc665C4nEJmQlXqn4lwi4E7z0Hrt1cWj1fF3Iodo30fBB5KRF+TT3Vgf8/qIpymYojNRv2e/veCn/eeE/g0MTZuId1jJ1uf3SkE6m6YpKfkxmz/t0mcPYKTEtkvkfCWuPhQwIDAQABAoIBAG/NK5N08N4vXcw+scovO7PicoYsFozGS8JGd8Rp7vq91XyQiGoJnvnFZyBE/zquEgCrPQ0O8PPjkca8jBnNrv2FzYo2GD1MbLKAwtBb4D91ZlNONBKQ6E6LTO0JKdkuNDH+nppizrAZaJMRBIfF//KYMlLUSdXnZeB7o7PGy6WOONDTsTz+Qzbyh2qerrECPxTI7SZGXrUSejT672hUUxRC5qxJyfgVm1SBuJQnIXkMocrFjst2TNs9RGEDQIzdze3FttyN9extQX1bBgtTkrRKhRz2u31758k0tyDwyiXPkR+RyV3zLbgyEsBCSoixu6tp+HyTeQ82jKWLpADRMkkCgYEA6ee4R+kJ+P9wmQtohvOsaefrfiMFu+SzHfwmDhXFXDkKgUrSC6Sg5MGfItbGo3xAWCow/eBWDEw7VA9CLxk69eJelByW5atyqOO4ANyQX86OueokTdWGMGIB+5uQrlThWYUdkcScwhD+HTZDEPHf+j8kV2RGJDXTmRzoYdW2e2UCgYEAzyK7CEOAxK4bZGE8kSyelTbqScpNKwKTN4zcyWFs4GwIR1qPEv8M2c+6+R7qohWcwO0UyDcfxYW8kzrG52Sak96aEwiArJmzUYauhC89qNHNwgnMjqbOOl50qxtSw7xMAlALgqueLayGa19q/KJZC9WmrZ0QI27syIqu9eYCI4cCgYBJyWFmver5c4tPnDuzJrdjCVhOiDnM1g5zDRHsK44A07Wup62vIpXm0/WhheNPu2iI53ZhAB3k7z9f+xhDx0/ENU7kpkgr13PqNzvaebnJn12C5IvjSdJ0/NXnS2HOvNQB624yh7VzuuyOxolNlR+0z6plLyq5TYmcqXPdOQ/ByQKBgBUGNW21TFCqS2V46SjjGoWSuV0cB5EiBcdRqOdX2DE/RefYLUWEVc2V5Ch9Ftu0zeBXMBDSZ4so8+cNxDqgX4A6y/C6oDTktOrFfNk14lBMW1IipHymObB6/eSQNWpgpmZlsYVl3fxM0qf7W2ShJeCpou1kT8sI60e/q9qRqg3pAoGBAN8JRArxYBwiImM60j6xH+l6rU2gKvqDC9K/7MaqLKOF6L120yEFWIwI30BUlFzixEphlyYi474U1xqextr7eZfn2eK+dA/R3XDC4q8T4YItgjnvlesEot3QJnCn9sm1HluIneI5ufP7KpwR5AtGLBUZ4LRtCBQbhezoLNRZI5Wg"
var client = alipay.New(appId, aliPublicKey, privateKey, true)

//支付宝扫码支付
// @router /ali/vipay [get]
func (u *AliController) Alipay() {

	//用户在线id
	uid, ok := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"), "uid")
	if !ok {
		u.Ctx.WriteString("{\"status\": \"203\", \"msg\": \"请求非法\"}")
		return
	}
	fmt.Println("扫码支付：用户id---->" + uid)

	//查询vip价格
	o := orm.NewOrm()
	var flag *flag.Flag
	o.Raw("select *from `flag` where id=?", 1).QueryRow(&flag)
	if flag.Value == "" {
		u.Data["json"] = lib.GetMapDataError(271, "价格不能设置为空")
		u.ServeJSON()
		return
	}
	money := flag.Value
	//生成时间戳
	//nanos := time.Now().UnixNano()
	//tradeNo := strconv.FormatInt(nanos, 10)

	var p = alipay.AliPayTradePagePay{}
	p.NotifyURL = "https://kaopeixia.com/webapi/ali/vipayinfo"
	//p.ReturnURL = conf.GetConfigValue("api")+"/alipay/payres?order_id="+url.QueryEscape(string(res))
	p.Subject = "考培侠"
	p.OutTradeNo = strconv.FormatInt(time.Now().Unix(), 10)+"-"+uid//tradeNo //"传递一个唯一单号"
	p.TotalAmount = money
	p.ProductCode = "FAST_INSTANT_TRADE_PAY"

	var url, err = client.TradePagePay(p)
	if err != nil {
		log.Println(err)
		return
	}

	var payURL = url.String()
	log.Println(payURL)
	var getinfo lib.GetInfoN
	getinfo.Msg = "请求成功"
	getinfo.Status = 200
	getinfo.Data = payURL
	u.Data["json"] = getinfo
	u.ServeJSON()
}

// 支付宝扫码回调支付
// @router /ali/vipayinfo [post]
func (u *AliController) AlipayNotifyUrl() {

	//u.Ctx.Request.ParseForm()
	//{
	//	ok, err := client.VerifySign(u.Ctx.Request.Form)
	//	fmt.Println(u.Ctx.Request.Form)
	//	fmt.Println(ok, err)
	//}

	var not, err = client.GetTradeNotification(u.Ctx.Request)
	if err != nil {
		fmt.Println("异步通知",err.Error())
		return
	}

	if not.TradeStatus == "TRADE_SUCCESS"{ //交易成功
		fmt.Println("商户订单号：",not.OutTradeNo)
		fmt.Println("支付价格：",not.TotalAmount)
		otd := strings.Split(not.OutTradeNo,"-")
		o := orm.NewOrm()
		o.Raw("update `user` set is_vip=1,vipprice=? where id=? ",not.TotalAmount,otd[1]).Exec()
	}
	u.Data["json"] = "over"
	u.ServeJSON()
}