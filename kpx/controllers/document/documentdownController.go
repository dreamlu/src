package document

import (
	"github.com/astaxie/beego"
	"kpx/models/document"
)

type DocumentDownController struct {
	beego.Controller
}

//根据用户id获得文档下载记录获取
// @router /documentdown/getdocumentdownbyuserid [post]
func (u *DocumentDownController) GetDocumentDownByUserId() {
	values := u.Ctx.Request.Form
	ss := document.GetDocumentDownByUserId(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//文档下载记录信息分页
// @router /documentdown/getdocumentdownbypage [get]
func (u *DocumentDownController) GetDocumentDownByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := document.GetDocumentDownByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//文档下载记录信息修改
// @router /documentdown/updatedocumentdown [post]
func (u *DocumentDownController) UpdateDocumentDown() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := document.UpdateDocumentDown(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增文档下载记录信息
// @router /documentdown/createdocumentdown [post]
func (u *DocumentDownController) CreateDocumentDown() {
	values := u.Ctx.Request.Form
	ss := document.CreateDocumentDown(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
