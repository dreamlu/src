package document

import (
	"github.com/astaxie/beego"
	"kpx/models/document"
)

type DocumentController struct {
	beego.Controller
}

//根据搜索条件获得文档
// @router /document/getdocumentbysearch [post]
func (u *DocumentController) GetDocumentBySearch() {
	values := u.Ctx.Request.Form
	ss := document.GetDocumentBySearch(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得文档获取
// @router /document/getdocumentbyid [get]
func (u *DocumentController) GetDocumentById() {
	id, _ := u.GetInt("id") //每页数量
	ss := document.GetDocumentById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//文档信息分页
// @router /document/getdocumentbypage [get]
func (u *DocumentController) GetDocumentByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := document.GetDocumentByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//文档信息删除
// @router /document/deletedocumentbyid/:id [delete]
func (u *DocumentController) DeleteDocumentById() {
	id, _ := u.GetInt(":id")
	ss := document.DeleteDocumentByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//文档信息修改
// @router /document/updatedocument [post]
func (u *DocumentController) UpdateDocument() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := document.UpdateDocument(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增文档信息
// @router /document/createdocument [post]
func (u *DocumentController) CreateDocument() {
	values := u.Ctx.Request.Form
	ss := document.CreateDocument(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
