package video

import (
	"github.com/astaxie/beego"
	"kpx/models/video"
)

type UserVideoPrivilegeController struct {
	beego.Controller
}

//根据搜索条件获得用户视频权限
// @router /userVP/search [get]
func (u *UserVideoPrivilegeController) GetUserVideoPrivilegeBySearch() {
	_ = u.Ctx.Request.ParseForm()
	values := u.Ctx.Request.Form
	ss := video.GetUserVideoPrivilegeBySearch(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得用户视频权限获取
// @router /userVP/id [get]
func (u *UserVideoPrivilegeController) GetUserVideoPrivilegeById() {
	id, _ := u.GetInt("id") //每页数量
	ss := video.GetUserVideoPrivilegeById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//用户视频权限信息删除
// @router /userVP/delete/:id [delete]
func (u *UserVideoPrivilegeController) DeleteUserVideoPrivilegeById() {
	id, _ := u.GetInt(":id")
	ss := video.DeleteUserVideoPrivilegeByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//用户视频权限信息修改
// @router /userVP/update [post]
func (u *UserVideoPrivilegeController) UpdateUserVideoPrivilege() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := video.UpdateUserVideoPrivilege(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增用户视频权限信息
// @router /userVP/create [post]
func (u *UserVideoPrivilegeController) CreateUserVideoPrivilege() {
	values := u.Ctx.Request.Form
	ss := video.CreateUserVideoPrivilege(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
