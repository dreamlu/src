package carousel

import (
	"github.com/astaxie/beego"
	"kpx/models/carousel"
)

type CarouselController struct {
	beego.Controller
}

//根据id获得轮播图获取
// @router /carousel/getcarouselbyid [get]
func (u *CarouselController) GetCarouselById() {
	id, _ := u.GetInt("id")
	ss := carousel.GetCarouselById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//轮播图信息分页
// @router /carousel/getcarouselbypage [get]
func (u *CarouselController) GetCarouselByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := carousel.GetCarouselByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//轮播图信息删除
// @router /carousel/deletecarouselbyid/:id [delete]
func (u *CarouselController) DeleteCarouselById() {
	id, _ := u.GetInt(":id")
	ss := carousel.DeleteCarouselByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//轮播图信息修改
// @router /carousel/updatecarousel [post]
func (u *CarouselController) UpdateCarousel() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := carousel.UpdateCarousel(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增轮播图信息
// @router /carousel/createcarousel [post]
func (u *CarouselController) CreateCarousel() {
	values := u.Ctx.Request.Form
	ss := carousel.CreateCarousel(values)
	u.Data["json"] = ss
	u.ServeJSON()
}