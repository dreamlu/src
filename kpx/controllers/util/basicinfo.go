package util

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"net"
	"runtime"
	"strings"
	"kpx/lib"
)

type BasicInfo struct {
	beego.Controller
}

type Basic struct {
	Address   string `json:"address"`   //ip或域名
	Port      string `json:"port"`      //端口号
	Os        string `json:"os"`        //操作系统
	//Osversion string `json:"osversion"` //操作系统版本
	Goversion string `json:"goversion"` //go 版本
	Beegoversion 	string `json:"beegoversion"`  //beego 版本
	Mysql     string `json:"mysql"`     //mysql版本
	Maxmerory string `json:"maxmerory"` //最大上传文件大小
}



// @router /basic/getbasicinfo [get]
func (u *BasicInfo) GetBasicInfo() {
	var getinfo lib.GetInfoN
	var basic Basic
	basic.Address = beego.AppConfig.String("domain")
	basic.Port = beego.AppConfig.String("httpport")
	basic.Os = runtime.GOOS
	basic.Goversion = runtime.Version()
	basic.Beegoversion = beego.VERSION
	basic.Maxmerory = beego.AppConfig.String("maxmemory")
	o := orm.NewOrm()
	var maps []orm.Params
	num, _ := o.Raw("select version() as version").Values(&maps)
	if num > 0 {
		basic.Mysql = maps[0]["version"].(string)
	}

	getinfo.Data = basic
	getinfo.Status = 200
	getinfo.Msg = "请求成功"
	u.Data["json"] = getinfo
	u.ServeJSON()
}

//域名或ip
func GetDomain() string {
	ip := GetOutboundIP()
	addr, err := net.LookupAddr(ip)
	if err == nil {
		return addr[0]
	}
	return ip
}
//ip
func GetOutboundIP() string {
	conn, _ := net.Dial("udp", "8.8.8.8:80")
	//HandleError("net.Dial: ",err)
	defer conn.Close()
	localAddr := conn.LocalAddr().String()
	idx := strings.LastIndex(localAddr, ":")
	return localAddr[0:idx]
}
