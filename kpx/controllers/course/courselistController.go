package course

import (
	"github.com/astaxie/beego"
	"kpx/models/course"
)

type CourselistController struct {
	beego.Controller
}

//小栏目小程序端,根据课程id
// @router /courselist/getcourselistbycourseidx [get]
func (u *CourselistController) GetCourselistByCourseIdX() {
	course_id := u.GetString("course_id")
	ss := course.GetCourselistByCourseIdX(course_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//小栏目下拉框,根据课程id
// @router /courselist/getcourselistbycourseid [get]
func (u *CourselistController) GetCourselistByCourseId() {
	course_id := u.GetString("course_id")
	ss := course.GetCourselistByCourseId(course_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得小栏目获取
// @router /courselist/getcourselistbyid [get]
func (u *CourselistController) GetCourselistById() {
	id, _ := u.GetInt("id")
	ss := course.GetCourselistById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//小栏目信息分页
// @router /courselist/getcourselistbypage [get]
func (u *CourselistController) GetCourselistByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := course.GetCourselistByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//小栏目信息删除
// @router /courselist/deletecourselistbyid/:id [delete]
func (u *CourselistController) DeleteCourselistById() {
	id, _ := u.GetInt(":id")
	ss := course.DeleteCourselistByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//小栏目信息修改
// @router /courselist/updatecourselist [post]
func (u *CourselistController) UpdateCourselist() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := course.UpdateCourselist(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增小栏目信息
// @router /courselist/createcourselist [post]
func (u *CourselistController) CreateCourselist() {
	values := u.Ctx.Request.Form
	ss := course.CreateCourselist(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
