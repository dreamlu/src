package course

import (
	"github.com/astaxie/beego"
	"kpx/models/course"
)

type CoursedetailController struct {
	beego.Controller
}

//根据小栏目条件获得听说读写对应视频
// @router /coursedetail/getcoursedetailbycourselistidp [get]
func (u *CoursedetailController) GetCoursedetailByCourselistIdP() {
	courselist_id := u.GetString("courselist_id")
	ss := course.GetCoursedetailByCourselistIdP(courselist_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据小栏目条件获得听说读写对应视频,小程序
// @router /coursedetail/getcoursedetailbycourselistid [get]
func (u *CoursedetailController) GetCoursedetailByCourselistId() {
	user_id := u.GetString("user_id")
	courselist_id := u.GetString("courselist_id")
	isf := u.GetString("isf")
	ss := course.GetCoursedetailByCourselistId(user_id, courselist_id, isf)
	u.Data["json"] = ss
	u.ServeJSON()
}

// @router /coursedetail/getcoursedetailbycourselistidBank [get]
func (u *CoursedetailController) GetCoursedetailByCourselistIdBank() {
	user_id := u.GetString("user_id")
	courselist_id := u.GetString("courselist_id")
	ss := course.GetCoursedetailByCourselistIdBank(user_id, courselist_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得听说读写对应视频获取
// @router /coursedetail/getcoursedetailbyid [get]
func (u *CoursedetailController) GetCoursedetailById() {
	id, _ := u.GetInt("id")
	user_id, _ := u.GetInt("user_id")
	isf, _ := u.GetInt("isf")
	ss := course.GetCoursedetailById(user_id, id, isf)
	u.Data["json"] = ss
	u.ServeJSON()
}

// @router /coursedetail/getcoursedetailbyidBank [get]
func (u *CoursedetailController) GetCoursedetailByIdBank() {
	id, _ := u.GetInt("id")
	// user_id, _ := u.GetInt("user_id")
	ss := course.GetCoursedetailByIdBank(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//听说读写对应视频信息分页
// @router /coursedetail/getcoursedetailbypage [get]
func (u *CoursedetailController) GetCoursedetailByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := course.GetCoursedetailByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//听说读写对应视频信息删除
// @router /coursedetail/deletecoursedetailbyid/:id [delete]
func (u *CoursedetailController) DeleteCoursedetailById() {
	id, _ := u.GetInt(":id")
	ss := course.DeleteCoursedetailByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//听说读写对应视频信息修改
// @router /coursedetail/updatecoursedetail [post]
func (u *CoursedetailController) UpdateCoursedetail() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := course.UpdateCoursedetail(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增听说读写对应视频信息
// @router /coursedetail/createcoursedetail [post]
func (u *CoursedetailController) CreateCoursedetail() {
	values := u.Ctx.Request.Form
	ss := course.CreateCoursedetail(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
