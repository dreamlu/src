package course

import (
	"github.com/astaxie/beego"
	"kpx/models/course"
)

type CommentController struct {
	beego.Controller
}

//根据搜索条件获得评论
// @router /comment/getcommentbycoursedetailid [get]
func (u *CommentController) GetCommentByCoursedetailId() {
	coursedetail_id, _ := u.GetInt("coursedetail_id")
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := course.GetCommentByCoursedetailId(coursedetail_id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得评论获取
// @router /comment/getcommentbyid [get]
func (u *CommentController) GetCommentById() {
	id, _ := u.GetInt("id")
	ss := course.GetCommentById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//评论信息分页
// @router /comment/getcommentbypage [get]
func (u *CommentController) GetCommentByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := course.GetCommentByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//评论信息删除
// @router /comment/deletecommentbyid/:id [delete]
func (u *CommentController) DeleteCommentById() {
	id, _ := u.GetInt(":id")
	ss := course.DeleteCommentByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//评论信息修改
// @router /comment/updatecomment [post]
func (u *CommentController) UpdateComment() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := course.UpdateComment(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增评论信息
// @router /comment/createcomment [post]
func (u *CommentController) CreateComment() {
	values := u.Ctx.Request.Form
	ss := course.CreateComment(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
