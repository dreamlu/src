package course

import (
	"github.com/astaxie/beego"
	"kpx/models/course"
)

type CommentReplyController struct {
	beego.Controller
}

//回复信息分页
// @router /commentreply/getcommentreplybypage [get]
func (u *CommentReplyController) GetCommentReplyByPage() {
	comment_id, _ := u.GetInt("comment_id") //页码
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := course.GetCommentReplyByPage(comment_id,clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//回复信息删除
// @router /commentreply/deletecommentreplybyid/:id [delete]
func (u *CommentReplyController) DeleteCommentReplyById() {
	id, _ := u.GetInt(":id")
	ss := course.DeleteCommentReplyByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增回复信息
// @router /commentreply/createcommentreply [post]
func (u *CommentReplyController) CreateCommentReply() {
	values := u.Ctx.Request.Form
	ss := course.CreateCommentReply(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
