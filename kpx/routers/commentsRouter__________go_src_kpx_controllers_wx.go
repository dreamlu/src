package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["kpx/controllers/wx:WxController"] = append(beego.GlobalControllerRouter["kpx/controllers/wx:WxController"],
        beego.ControllerComments{
            Method: "WxPay",
            Router: `/wx/vipay`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/wx:WxController"] = append(beego.GlobalControllerRouter["kpx/controllers/wx:WxController"],
        beego.ControllerComments{
            Method: "WechatWebNotifyUrl",
            Router: `/wx/vipayinfo`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
