package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["kpx/controllers/carousel:CarouselController"] = append(beego.GlobalControllerRouter["kpx/controllers/carousel:CarouselController"],
        beego.ControllerComments{
            Method: "CreateCarousel",
            Router: `/carousel/createcarousel`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/carousel:CarouselController"] = append(beego.GlobalControllerRouter["kpx/controllers/carousel:CarouselController"],
        beego.ControllerComments{
            Method: "DeleteCarouselById",
            Router: `/carousel/deletecarouselbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/carousel:CarouselController"] = append(beego.GlobalControllerRouter["kpx/controllers/carousel:CarouselController"],
        beego.ControllerComments{
            Method: "GetCarouselById",
            Router: `/carousel/getcarouselbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/carousel:CarouselController"] = append(beego.GlobalControllerRouter["kpx/controllers/carousel:CarouselController"],
        beego.ControllerComments{
            Method: "GetCarouselByPage",
            Router: `/carousel/getcarouselbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/carousel:CarouselController"] = append(beego.GlobalControllerRouter["kpx/controllers/carousel:CarouselController"],
        beego.ControllerComments{
            Method: "UpdateCarousel",
            Router: `/carousel/updatecarousel`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
