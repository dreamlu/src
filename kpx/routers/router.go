package routers

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"github.com/astaxie/beego/plugins/cors"
	"kpx/controllers"
	"kpx/controllers/alipay"
	"kpx/controllers/carousel"
	"kpx/controllers/course"
	"kpx/controllers/coursevip"
	"kpx/controllers/document"
	"kpx/controllers/documentvip"
	"kpx/controllers/examination"
	"kpx/controllers/examinationvip"
	"kpx/controllers/flag"
	"kpx/controllers/message"
	"kpx/controllers/util"
	"kpx/controllers/video"
	"kpx/controllers/wx"
	"strings"
)

//登录验证/api路由权限验证
var FilterUser = func(ctx *context.Context) {
	//预检请求
	if ctx.Input.Method() == "OPTIONS" {
		ctx.Output.SetStatus(200)
		ctx.Output.Header("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PATCH,DELETE")
		ctx.Output.Header("Access-Control-Allow-Origin", "http://192.168.31.173:3000,http://localhost:3000,http://localhost:3001")
		return
	}
	//验证小程序端不验证权限
	if ctx.Input.Header("Authorization") != "wechat" {
		//请求路由
		url := ctx.Input.URL()
		//用户在线id
		uid, ok := ctx.GetSecureCookie(beego.AppConfig.String("secertkey"), "uid")
		fmt.Println("用户id---->" + uid)

		if !ok && strings.Contains(url, "/login") && strings.Contains(url, "/register") && strings.Contains(url, "/smscode") {
			ctx.WriteString("{\"status\": \"203\", \"msg\": \"请求非法\"}")
			return
		}
	}
}

//网页vip权限验证
var VipVali = func(ctx *context.Context) {
	//预检请求
	if ctx.Input.Method() == "OPTIONS" {
		ctx.Output.SetStatus(200)
		ctx.Output.Header("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PATCH,DELETE")
		ctx.Output.Header("Access-Control-Allow-Origin", "http://192.168.31.173:3000,http://localhost:3000,http://localhost:3001")
		return
	}
	//vip检测
	//验证小程序端不验证权限
	if ctx.Input.Header("Authorization") != "wechat" {
		//请求路由
		url := ctx.Input.URL()
		//isvip
		_, ok := ctx.GetSecureCookie(beego.AppConfig.String("secertkey"), "v")

		if !ok && strings.Contains(url, "/login") && strings.Contains(url, "/register") && strings.Contains(url, "/smscode") {
			ctx.WriteString("{\"status\": \"203\", \"msg\": \"请求非法\"}")
			return
		}
	}
}

func init() {
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowOrigins:     []string{"http://192.168.31.173:3000", "http://localhost:3000", "http://localhost:3001"},
		AllowMethods:     []string{"GET", "POST", "OPTIONS", "PATCH", "DELETE"},
		AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "X-Requested-With", "Access-Control-Allow-Credentials"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))
	beego.InsertFilter("/*", beego.BeforeRouter, FilterUser)
	beego.Include(&controllers.UserController{})
	beego.Include(&course.CourseController{})
	beego.Include(&document.DocumentController{})
	beego.Include(&course.CourselistController{})
	beego.Include(&course.CoursedetailController{})
	beego.Include(&course.CommentController{})
	beego.Include(&examination.StudyRecordController{})
	beego.Include(&document.DocumentDownController{})
	beego.Include(&examination.ExaminationController{})
	beego.Include(&examination.ChoiceQuestionController{})
	beego.Include(&util.FileController{})
	beego.Include(&controllers.LoginController{})
	beego.Include(&examination.ExaminationrecordController{})
	beego.Include(&course.UserCourseController{})
	beego.Include(&util.OsskeysecretController{})
	beego.Include(&util.BasicInfo{})
	beego.Include(&util.GetWxEncryptedData{})
	beego.Include(&course.CommentReplyController{})
	beego.Include(&carousel.CarouselController{})
	beego.Include(&message.SignnameController{})
	beego.Include(&message.SmscodeController{})
	beego.Include(&message.SendstatusController{})
	beego.Include(&message.MessageController{})
	// vip add
	//beego.InsertFilter("/*vip", beego.BeforeRouter, VipVali) //vip 验证
	beego.Include(&coursevip.CourseController{})
	beego.Include(&coursevip.CommentController{})
	beego.Include(&coursevip.CommentReplyController{})
	beego.Include(&coursevip.CoursedetailController{})
	beego.Include(&coursevip.CourselistController{})
	beego.Include(&coursevip.UserCourseController{})
	beego.Include(&documentvip.DocumentController{})
	beego.Include(&documentvip.DocumentController{})
	beego.Include(&flag.FlagController{})
	beego.Include(&examinationvip.StudyRecordController{})
	// vip 支付
	beego.Include(&wx.WxController{})
	beego.Include(&alipay.AliController{})
	// v2.0 udpate
	beego.Include(&video.UserVideoPrivilegeController{})
}
