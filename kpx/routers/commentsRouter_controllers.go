package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["kpx/controllers:LoginController"] = append(beego.GlobalControllerRouter["kpx/controllers:LoginController"],
		beego.ControllerComments{
			Method: "Login",
			Router: `/login/login`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers:LoginController"] = append(beego.GlobalControllerRouter["kpx/controllers:LoginController"],
		beego.ControllerComments{
			Method: "Getopenid",
			Router: `/openid/getopenid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers:UserController"] = append(beego.GlobalControllerRouter["kpx/controllers:UserController"],
		beego.ControllerComments{
			Method: "CreateUser",
			Router: `/user/createuser`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers:UserController"] = append(beego.GlobalControllerRouter["kpx/controllers:UserController"],
		beego.ControllerComments{
			Method: "DeleteUserById",
			Router: `/user/deleteuserbyid/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers:UserController"] = append(beego.GlobalControllerRouter["kpx/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetUserById",
			Router: `/user/getuserbyid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers:UserController"] = append(beego.GlobalControllerRouter["kpx/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetUserByPage",
			Router: `/user/getuserbypage`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers:UserController"] = append(beego.GlobalControllerRouter["kpx/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetUserBySearch",
			Router: `/user/getuserbysearch`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers:UserController"] = append(beego.GlobalControllerRouter["kpx/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetUserCheckById",
			Router: `/user/getusercheckbyid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers:UserController"] = append(beego.GlobalControllerRouter["kpx/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetUserIdByOpenId",
			Router: `/user/getuseridbyopenid`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers:UserController"] = append(beego.GlobalControllerRouter["kpx/controllers:UserController"],
		beego.ControllerComments{
			Method: "UpdateUser",
			Router: `/user/updateuser`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

}
