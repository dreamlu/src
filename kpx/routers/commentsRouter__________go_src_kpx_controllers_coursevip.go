package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentController"],
        beego.ControllerComments{
            Method: "CreateComment",
            Router: `/coursevip/createcomment`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentController"],
        beego.ControllerComments{
            Method: "DeleteCommentById",
            Router: `/coursevip/deletecommentbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentController"],
        beego.ControllerComments{
            Method: "GetCommentByCoursedetailId",
            Router: `/coursevip/getcommentbycoursedetailid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentController"],
        beego.ControllerComments{
            Method: "GetCommentById",
            Router: `/coursevip/getcommentbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentController"],
        beego.ControllerComments{
            Method: "GetCommentByPage",
            Router: `/coursevip/getcommentbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentController"],
        beego.ControllerComments{
            Method: "UpdateComment",
            Router: `/coursevip/updatecomment`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentReplyController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentReplyController"],
        beego.ControllerComments{
            Method: "CreateCommentReply",
            Router: `/coursevip/createcommentreply`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentReplyController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentReplyController"],
        beego.ControllerComments{
            Method: "DeleteCommentReplyById",
            Router: `/coursevip/deletecommentreplybyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentReplyController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CommentReplyController"],
        beego.ControllerComments{
            Method: "GetCommentReplyByPage",
            Router: `/coursevip/getcommentreplybypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"],
        beego.ControllerComments{
            Method: "CreateCourse",
            Router: `/coursevip/createcourse`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"],
        beego.ControllerComments{
            Method: "DeleteCourseById",
            Router: `/coursevip/deletecoursebyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"],
        beego.ControllerComments{
            Method: "GetCourseById",
            Router: `/coursevip/getcoursebyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"],
        beego.ControllerComments{
            Method: "GetCourseByPage",
            Router: `/coursevip/getcoursebypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"],
        beego.ControllerComments{
            Method: "GetCourseBySearch",
            Router: `/coursevip/getcoursebysearch`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"],
        beego.ControllerComments{
            Method: "GetCourseBySearchV2",
            Router: `/coursevip/getcoursebysearchv2`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"],
        beego.ControllerComments{
            Method: "GetCourseBySearchV3",
            Router: `/coursevip/getcoursebysearchv3`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourseController"],
        beego.ControllerComments{
            Method: "UpdateCourse",
            Router: `/coursevip/updatecourse`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"],
        beego.ControllerComments{
            Method: "CreateCoursedetail",
            Router: `/coursevip/createcoursedetail`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"],
        beego.ControllerComments{
            Method: "DeleteCoursedetailById",
            Router: `/coursevip/deletecoursedetailbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"],
        beego.ControllerComments{
            Method: "GetCoursedetailByCourselistId",
            Router: `/coursevip/getcoursedetailbycourselistid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"],
        beego.ControllerComments{
            Method: "GetCoursedetailByCourselistIdP",
            Router: `/coursevip/getcoursedetailbycourselistidp`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"],
        beego.ControllerComments{
            Method: "GetCoursedetailById",
            Router: `/coursevip/getcoursedetailbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"],
        beego.ControllerComments{
            Method: "GetCoursedetailByPage",
            Router: `/coursevip/getcoursedetailbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CoursedetailController"],
        beego.ControllerComments{
            Method: "UpdateCoursedetail",
            Router: `/coursevip/updatecoursedetail`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"],
        beego.ControllerComments{
            Method: "CreateCourselist",
            Router: `/coursevip/createcourselist`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"],
        beego.ControllerComments{
            Method: "DeleteCourselistById",
            Router: `/coursevip/deletecourselistbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"],
        beego.ControllerComments{
            Method: "GetCourselistByCourseId",
            Router: `/coursevip/getcourselistbycourseid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"],
        beego.ControllerComments{
            Method: "GetCourselistByCourseIdX",
            Router: `/coursevip/getcourselistbycourseidx`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"],
        beego.ControllerComments{
            Method: "GetCourselistById",
            Router: `/coursevip/getcourselistbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"],
        beego.ControllerComments{
            Method: "GetCourselistByPage",
            Router: `/coursevip/getcourselistbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:CourselistController"],
        beego.ControllerComments{
            Method: "UpdateCourselist",
            Router: `/coursevip/updatecourselist`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:UserCourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:UserCourseController"],
        beego.ControllerComments{
            Method: "CreateUserCourse",
            Router: `/coursevip/createusercourse`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:UserCourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:UserCourseController"],
        beego.ControllerComments{
            Method: "DeleteUserCourseById",
            Router: `/coursevip/deleteusercoursebyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:UserCourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:UserCourseController"],
        beego.ControllerComments{
            Method: "GetCouselistByCourseId",
            Router: `/coursevip/getcourselistbycourseid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:UserCourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:UserCourseController"],
        beego.ControllerComments{
            Method: "GetUserCourseByUserId",
            Router: `/coursevip/getusercoursebyuserid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/coursevip:UserCourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/coursevip:UserCourseController"],
        beego.ControllerComments{
            Method: "UpdateUserCourse",
            Router: `/coursevip/updateusercourse`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
