package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["kpx/controllers/util:BasicInfo"] = append(beego.GlobalControllerRouter["kpx/controllers/util:BasicInfo"],
        beego.ControllerComments{
            Method: "GetBasicInfo",
            Router: `/basic/getbasicinfo`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/util:FileController"] = append(beego.GlobalControllerRouter["kpx/controllers/util:FileController"],
        beego.ControllerComments{
            Method: "CreateSomething",
            Router: `/file/createsomething`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/util:FileController"] = append(beego.GlobalControllerRouter["kpx/controllers/util:FileController"],
        beego.ControllerComments{
            Method: "ExportUser",
            Router: `/file/exportuser`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/util:FileController"] = append(beego.GlobalControllerRouter["kpx/controllers/util:FileController"],
        beego.ControllerComments{
            Method: "GetFilePath",
            Router: `/file/getfilepath`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/util:GetWxEncryptedData"] = append(beego.GlobalControllerRouter["kpx/controllers/util:GetWxEncryptedData"],
        beego.ControllerComments{
            Method: "GetWxEncryptedData",
            Router: `/wx/getwxencrypteddata`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/util:OsskeysecretController"] = append(beego.GlobalControllerRouter["kpx/controllers/util:OsskeysecretController"],
        beego.ControllerComments{
            Method: "Getossinfo",
            Router: `/oss/getossinfo`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
