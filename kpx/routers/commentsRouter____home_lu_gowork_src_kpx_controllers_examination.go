package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["kpx/controllers/examination:ChoiceQuestionController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ChoiceQuestionController"],
		beego.ControllerComments{
			Method: "CreateChoiceQuestion",
			Router: `/choicequestion/createchoicequestion`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ChoiceQuestionController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ChoiceQuestionController"],
		beego.ControllerComments{
			Method: "DeleteChoiceQuestionById",
			Router: `/choicequestion/deletechoicequestionbyid/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ChoiceQuestionController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ChoiceQuestionController"],
		beego.ControllerComments{
			Method: "GetChoicequestionByExaminationId",
			Router: `/choicequestion/getchoicequestionbyexaminationid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ChoiceQuestionController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ChoiceQuestionController"],
		beego.ControllerComments{
			Method: "GetChoiceQuestionById",
			Router: `/choicequestion/getchoicequestionbyid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ChoiceQuestionController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ChoiceQuestionController"],
		beego.ControllerComments{
			Method: "GetChoiceQuestionByPage",
			Router: `/choicequestion/getchoicequestionbypage`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ChoiceQuestionController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ChoiceQuestionController"],
		beego.ControllerComments{
			Method: "UpdateChoiceQuestion",
			Router: `/choicequestion/updatechoicequestion`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"],
		beego.ControllerComments{
			Method: "GetExaminationBySearch",
			Router: `/document/getexaminationbysearch`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"],
		beego.ControllerComments{
			Method: "CreateExamination",
			Router: `/examination/createexamination`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"],
		beego.ControllerComments{
			Method: "DeleteExaminationById",
			Router: `/examination/deleteexaminationbyid/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"],
		beego.ControllerComments{
			Method: "GetExaminationByCategory",
			Router: `/examination/getexaminationbycategory`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"],
		beego.ControllerComments{
			Method: "GetExaminationById",
			Router: `/examination/getexaminationbyid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"],
		beego.ControllerComments{
			Method: "GetExaminationByPage",
			Router: `/examination/getexaminationbypage`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationController"],
		beego.ControllerComments{
			Method: "UpdateExamination",
			Router: `/examination/updateexamination`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"],
		beego.ControllerComments{
			Method: "CreateExaminationrecord",
			Router: `/examinationrecord/createexaminationrecord`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"],
		beego.ControllerComments{
			Method: "DeleteExaminationrecordById",
			Router: `/examinationrecord/deleteexaminationrecordbyid/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"],
		beego.ControllerComments{
			Method: "GetErrorExam",
			Router: `/examinationrecord/geterrorexam`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"],
		beego.ControllerComments{
			Method: "GetExaminationrecordByUserId",
			Router: `/examinationrecord/getexaminationrecordbyuserid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"],
		beego.ControllerComments{
			Method: "GetExaminationrecordByUserIdPC",
			Router: `/examinationrecord/getexaminationrecordbyuseridpc`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"],
		beego.ControllerComments{
			Method: "GetExaminationrecordByUserIdx",
			Router: `/examinationrecord/getexaminationrecordbyuseridx`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:ExaminationrecordController"],
		beego.ControllerComments{
			Method: "UpdateExaminationrecord",
			Router: `/examinationrecord/updateexaminationrecord`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:StudyRecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:StudyRecordController"],
		beego.ControllerComments{
			Method: "CreateStudyRecord",
			Router: `/studyrecord/createstudyrecord`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:StudyRecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:StudyRecordController"],
		beego.ControllerComments{
			Method: "GetStudyRecordByPage",
			Router: `/studyrecord/getstudyrecordbypage`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/examination:StudyRecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examination:StudyRecordController"],
		beego.ControllerComments{
			Method: "GetStudyRecordUser",
			Router: `/studyrecord/getstudyrecordbyuser`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

}
