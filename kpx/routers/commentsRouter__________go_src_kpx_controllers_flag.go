package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["kpx/controllers/flag:FlagController"] = append(beego.GlobalControllerRouter["kpx/controllers/flag:FlagController"],
        beego.ControllerComments{
            Method: "CreateFlag",
            Router: `/flag/createflag`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/flag:FlagController"] = append(beego.GlobalControllerRouter["kpx/controllers/flag:FlagController"],
        beego.ControllerComments{
            Method: "DeleteFlagById",
            Router: `/flag/deleteflagbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/flag:FlagController"] = append(beego.GlobalControllerRouter["kpx/controllers/flag:FlagController"],
        beego.ControllerComments{
            Method: "GetFlagById",
            Router: `/flag/getflagbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/flag:FlagController"] = append(beego.GlobalControllerRouter["kpx/controllers/flag:FlagController"],
        beego.ControllerComments{
            Method: "GetFlagByPage",
            Router: `/flag/getflagbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/flag:FlagController"] = append(beego.GlobalControllerRouter["kpx/controllers/flag:FlagController"],
        beego.ControllerComments{
            Method: "UpdateFlag",
            Router: `/flag/updateflag`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
