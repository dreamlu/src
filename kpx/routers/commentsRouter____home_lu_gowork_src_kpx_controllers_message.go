package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["kpx/controllers/message:MessageController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:MessageController"],
		beego.ControllerComments{
			Method: "FindSendstatus",
			Router: `/message/find`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/message:MessageController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:MessageController"],
		beego.ControllerComments{
			Method: "SmsSend",
			Router: `/message/send`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/message:SendstatusController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:SendstatusController"],
		beego.ControllerComments{
			Method: "GetSendstatusByPage",
			Router: `/sendstatus/getbypage`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/message:SignnameController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:SignnameController"],
		beego.ControllerComments{
			Method: "CreateSignname",
			Router: `/signname/create`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/message:SignnameController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:SignnameController"],
		beego.ControllerComments{
			Method: "DeleteSignnameById",
			Router: `/signname/deletebyid/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/message:SignnameController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:SignnameController"],
		beego.ControllerComments{
			Method: "GetSignnameById",
			Router: `/signname/getbyid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/message:SignnameController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:SignnameController"],
		beego.ControllerComments{
			Method: "GetSignnameByPage",
			Router: `/signname/getbypage`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/message:SignnameController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:SignnameController"],
		beego.ControllerComments{
			Method: "UpdateSignname",
			Router: `/signname/update`,
			AllowHTTPMethods: []string{"patch"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/message:SmscodeController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:SmscodeController"],
		beego.ControllerComments{
			Method: "CreateSmscode",
			Router: `/smscode/create`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/message:SmscodeController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:SmscodeController"],
		beego.ControllerComments{
			Method: "DeleteSmscodeById",
			Router: `/smscode/deletebyid/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/message:SmscodeController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:SmscodeController"],
		beego.ControllerComments{
			Method: "GetSmscodeById",
			Router: `/smscode/getbyid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/message:SmscodeController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:SmscodeController"],
		beego.ControllerComments{
			Method: "GetSmscodeByPage",
			Router: `/smscode/getbypage`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/message:SmscodeController"] = append(beego.GlobalControllerRouter["kpx/controllers/message:SmscodeController"],
		beego.ControllerComments{
			Method: "UpdateSmscode",
			Router: `/smscode/update`,
			AllowHTTPMethods: []string{"patch"},
			MethodParams: param.Make(),
			Params: nil})

}
