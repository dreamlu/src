package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["kpx/controllers/video:UserVideoPrivilegeController"] = append(beego.GlobalControllerRouter["kpx/controllers/video:UserVideoPrivilegeController"],
        beego.ControllerComments{
            Method: "CreateUserVideoPrivilege",
            Router: `/userVP/create`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/video:UserVideoPrivilegeController"] = append(beego.GlobalControllerRouter["kpx/controllers/video:UserVideoPrivilegeController"],
        beego.ControllerComments{
            Method: "DeleteUserVideoPrivilegeById",
            Router: `/userVP/delete/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/video:UserVideoPrivilegeController"] = append(beego.GlobalControllerRouter["kpx/controllers/video:UserVideoPrivilegeController"],
        beego.ControllerComments{
            Method: "GetUserVideoPrivilegeById",
            Router: `/userVP/id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/video:UserVideoPrivilegeController"] = append(beego.GlobalControllerRouter["kpx/controllers/video:UserVideoPrivilegeController"],
        beego.ControllerComments{
            Method: "GetUserVideoPrivilegeBySearch",
            Router: `/userVP/search`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/video:UserVideoPrivilegeController"] = append(beego.GlobalControllerRouter["kpx/controllers/video:UserVideoPrivilegeController"],
        beego.ControllerComments{
            Method: "UpdateUserVideoPrivilege",
            Router: `/userVP/update`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
