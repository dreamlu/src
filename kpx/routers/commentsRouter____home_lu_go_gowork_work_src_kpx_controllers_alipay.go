package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["kpx/controllers/alipay:AliController"] = append(beego.GlobalControllerRouter["kpx/controllers/alipay:AliController"],
        beego.ControllerComments{
            Method: "Alipay",
            Router: `/ali/vipay`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/alipay:AliController"] = append(beego.GlobalControllerRouter["kpx/controllers/alipay:AliController"],
        beego.ControllerComments{
            Method: "AlipayNotifyUrl",
            Router: `/ali/vipayinfo`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
