package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["kpx/controllers/document:DocumentController"] = append(beego.GlobalControllerRouter["kpx/controllers/document:DocumentController"],
        beego.ControllerComments{
            Method: "CreateDocument",
            Router: `/document/createdocument`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/document:DocumentController"] = append(beego.GlobalControllerRouter["kpx/controllers/document:DocumentController"],
        beego.ControllerComments{
            Method: "DeleteDocumentById",
            Router: `/document/deletedocumentbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/document:DocumentController"] = append(beego.GlobalControllerRouter["kpx/controllers/document:DocumentController"],
        beego.ControllerComments{
            Method: "GetDocumentById",
            Router: `/document/getdocumentbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/document:DocumentController"] = append(beego.GlobalControllerRouter["kpx/controllers/document:DocumentController"],
        beego.ControllerComments{
            Method: "GetDocumentByPage",
            Router: `/document/getdocumentbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/document:DocumentController"] = append(beego.GlobalControllerRouter["kpx/controllers/document:DocumentController"],
        beego.ControllerComments{
            Method: "GetDocumentBySearch",
            Router: `/document/getdocumentbysearch`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/document:DocumentController"] = append(beego.GlobalControllerRouter["kpx/controllers/document:DocumentController"],
        beego.ControllerComments{
            Method: "UpdateDocument",
            Router: `/document/updatedocument`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/document:DocumentDownController"] = append(beego.GlobalControllerRouter["kpx/controllers/document:DocumentDownController"],
        beego.ControllerComments{
            Method: "CreateDocumentDown",
            Router: `/documentdown/createdocumentdown`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/document:DocumentDownController"] = append(beego.GlobalControllerRouter["kpx/controllers/document:DocumentDownController"],
        beego.ControllerComments{
            Method: "GetDocumentDownByPage",
            Router: `/documentdown/getdocumentdownbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/document:DocumentDownController"] = append(beego.GlobalControllerRouter["kpx/controllers/document:DocumentDownController"],
        beego.ControllerComments{
            Method: "GetDocumentDownByUserId",
            Router: `/documentdown/getdocumentdownbyuserid`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/document:DocumentDownController"] = append(beego.GlobalControllerRouter["kpx/controllers/document:DocumentDownController"],
        beego.ControllerComments{
            Method: "UpdateDocumentDown",
            Router: `/documentdown/updatedocumentdown`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
