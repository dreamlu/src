package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["kpx/controllers/examinationvip:StudyRecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examinationvip:StudyRecordController"],
        beego.ControllerComments{
            Method: "CreateStudyRecord",
            Router: `/studyvip/createstudyrecord`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/examinationvip:StudyRecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examinationvip:StudyRecordController"],
        beego.ControllerComments{
            Method: "GetStudyRecordByPage",
            Router: `/studyvip/getstudyrecordbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/examinationvip:StudyRecordController"] = append(beego.GlobalControllerRouter["kpx/controllers/examinationvip:StudyRecordController"],
        beego.ControllerComments{
            Method: "GetStudyRecordUser",
            Router: `/studyvip/getstudyrecordbyuser`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
