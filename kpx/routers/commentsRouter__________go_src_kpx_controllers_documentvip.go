package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentController"] = append(beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentController"],
        beego.ControllerComments{
            Method: "CreateDocument",
            Router: `/documentvip/createdocument`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentController"] = append(beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentController"],
        beego.ControllerComments{
            Method: "DeleteDocumentById",
            Router: `/documentvip/deletedocumentbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentController"] = append(beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentController"],
        beego.ControllerComments{
            Method: "GetDocumentById",
            Router: `/documentvip/getdocumentbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentController"] = append(beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentController"],
        beego.ControllerComments{
            Method: "GetDocumentByPage",
            Router: `/documentvip/getdocumentbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentController"] = append(beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentController"],
        beego.ControllerComments{
            Method: "GetDocumentBySearch",
            Router: `/documentvip/getdocumentbysearch`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentController"] = append(beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentController"],
        beego.ControllerComments{
            Method: "UpdateDocument",
            Router: `/documentvip/updatedocument`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentDownController"] = append(beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentDownController"],
        beego.ControllerComments{
            Method: "CreateDocumentDown",
            Router: `/documentvip/createdocumentdown`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentDownController"] = append(beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentDownController"],
        beego.ControllerComments{
            Method: "GetDocumentDownByPage",
            Router: `/documentvip/getdocumentdownbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentDownController"] = append(beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentDownController"],
        beego.ControllerComments{
            Method: "GetDocumentDownByUserId",
            Router: `/documentvip/getdocumentdownbyuserid`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentDownController"] = append(beego.GlobalControllerRouter["kpx/controllers/documentvip:DocumentDownController"],
        beego.ControllerComments{
            Method: "UpdateDocumentDown",
            Router: `/documentvip/updatedocumentdown`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
