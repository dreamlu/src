package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["kpx/controllers/course:CommentController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CommentController"],
        beego.ControllerComments{
            Method: "CreateComment",
            Router: `/comment/createcomment`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CommentController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CommentController"],
        beego.ControllerComments{
            Method: "DeleteCommentById",
            Router: `/comment/deletecommentbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CommentController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CommentController"],
        beego.ControllerComments{
            Method: "GetCommentByCoursedetailId",
            Router: `/comment/getcommentbycoursedetailid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CommentController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CommentController"],
        beego.ControllerComments{
            Method: "GetCommentById",
            Router: `/comment/getcommentbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CommentController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CommentController"],
        beego.ControllerComments{
            Method: "GetCommentByPage",
            Router: `/comment/getcommentbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CommentController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CommentController"],
        beego.ControllerComments{
            Method: "UpdateComment",
            Router: `/comment/updatecomment`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CommentReplyController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CommentReplyController"],
        beego.ControllerComments{
            Method: "CreateCommentReply",
            Router: `/commentreply/createcommentreply`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CommentReplyController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CommentReplyController"],
        beego.ControllerComments{
            Method: "DeleteCommentReplyById",
            Router: `/commentreply/deletecommentreplybyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CommentReplyController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CommentReplyController"],
        beego.ControllerComments{
            Method: "GetCommentReplyByPage",
            Router: `/commentreply/getcommentreplybypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourseController"],
        beego.ControllerComments{
            Method: "CreateCourse",
            Router: `/course/createcourse`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourseController"],
        beego.ControllerComments{
            Method: "DeleteCourseById",
            Router: `/course/deletecoursebyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourseController"],
        beego.ControllerComments{
            Method: "GetCourseById",
            Router: `/course/getcoursebyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourseController"],
        beego.ControllerComments{
            Method: "GetCourseByPage",
            Router: `/course/getcoursebypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourseController"],
        beego.ControllerComments{
            Method: "GetCourseBySearch",
            Router: `/course/getcoursebysearch`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourseController"],
        beego.ControllerComments{
            Method: "GetCourseBySearchV2",
            Router: `/course/getcoursebysearchv2`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourseController"],
        beego.ControllerComments{
            Method: "GetCourseBySearchV3",
            Router: `/course/getcoursebysearchv3`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourseController"],
        beego.ControllerComments{
            Method: "UpdateCourse",
            Router: `/course/updatecourse`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"],
        beego.ControllerComments{
            Method: "CreateCoursedetail",
            Router: `/coursedetail/createcoursedetail`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"],
        beego.ControllerComments{
            Method: "DeleteCoursedetailById",
            Router: `/coursedetail/deletecoursedetailbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"],
        beego.ControllerComments{
            Method: "GetCoursedetailByCourselistId",
            Router: `/coursedetail/getcoursedetailbycourselistid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"],
        beego.ControllerComments{
            Method: "GetCoursedetailByCourselistIdP",
            Router: `/coursedetail/getcoursedetailbycourselistidp`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"],
        beego.ControllerComments{
            Method: "GetCoursedetailById",
            Router: `/coursedetail/getcoursedetailbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"],
        beego.ControllerComments{
            Method: "GetCoursedetailByPage",
            Router: `/coursedetail/getcoursedetailbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CoursedetailController"],
        beego.ControllerComments{
            Method: "UpdateCoursedetail",
            Router: `/coursedetail/updatecoursedetail`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"],
        beego.ControllerComments{
            Method: "CreateCourselist",
            Router: `/courselist/createcourselist`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"],
        beego.ControllerComments{
            Method: "DeleteCourselistById",
            Router: `/courselist/deletecourselistbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"],
        beego.ControllerComments{
            Method: "GetCourselistByCourseId",
            Router: `/courselist/getcourselistbycourseid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"],
        beego.ControllerComments{
            Method: "GetCourselistByCourseIdX",
            Router: `/courselist/getcourselistbycourseidx`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"],
        beego.ControllerComments{
            Method: "GetCourselistById",
            Router: `/courselist/getcourselistbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"],
        beego.ControllerComments{
            Method: "GetCourselistByPage",
            Router: `/courselist/getcourselistbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:CourselistController"],
        beego.ControllerComments{
            Method: "UpdateCourselist",
            Router: `/courselist/updatecourselist`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:UserCourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:UserCourseController"],
        beego.ControllerComments{
            Method: "CreateUserCourse",
            Router: `/usercourse/createusercourse`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:UserCourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:UserCourseController"],
        beego.ControllerComments{
            Method: "DeleteUserCourseById",
            Router: `/usercourse/deleteusercoursebyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:UserCourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:UserCourseController"],
        beego.ControllerComments{
            Method: "GetCouselistByCourseId",
            Router: `/usercourse/getcourselistbycourseid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:UserCourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:UserCourseController"],
        beego.ControllerComments{
            Method: "GetUserCourseByUserId",
            Router: `/usercourse/getusercoursebyuserid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["kpx/controllers/course:UserCourseController"] = append(beego.GlobalControllerRouter["kpx/controllers/course:UserCourseController"],
        beego.ControllerComments{
            Method: "UpdateUserCourse",
            Router: `/usercourse/updateusercourse`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
