package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["kpx/controllers/util:FileController"] = append(beego.GlobalControllerRouter["kpx/controllers/util:FileController"],
		beego.ControllerComments{
			Method: "CreateSomething",
			Router: `/file/createsomething`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/util:FileController"] = append(beego.GlobalControllerRouter["kpx/controllers/util:FileController"],
		beego.ControllerComments{
			Method: "GetFilePath",
			Router: `/file/getfilepath`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["kpx/controllers/util:OsskeysecretController"] = append(beego.GlobalControllerRouter["kpx/controllers/util:OsskeysecretController"],
		beego.ControllerComments{
			Method: "Getossinfo",
			Router: `/oss/getossinfo`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

}
