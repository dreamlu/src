module kpx

go 1.12

require (
	github.com/Dreamlu/aliyun-sms-go-sdk v0.0.0-20180108012719-fcc9f11de968
	github.com/Dreamlu/wechatpay v0.0.0-20181107133324-3cfb8515d372
	github.com/GiterLab/urllib v0.0.0-20160731084542-ea0e875f9030 // indirect
	github.com/KenmyZhang/aliyun-communicate v0.0.0-20180308134849-7997edc57454
	github.com/astaxie/beego v1.12.0
	github.com/clbanning/mxj v1.8.4
	github.com/go-sql-driver/mysql v1.4.1
	github.com/google/uuid v1.1.1
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/sdbaiguanghe/glog v0.0.0-20160701133742-ca74c069d4e1 // indirect
	github.com/shiena/ansicolor v0.0.0-20151119151921-a422bbe96644 // indirect
	github.com/skip2/go-qrcode v0.0.0-20190110000554-dc11ecdae0a9
	github.com/smartwalle/alipay v1.0.2
	github.com/tealeg/xlsx v1.0.3
	github.com/tobyzxj/uuid v0.0.0-20140223123307-aa0153c14395 // indirect
	github.com/xlstudio/wxbizdatacrypt v0.0.0-20190418111444-c3e116880c11
	google.golang.org/appengine v1.6.2 // indirect
)
