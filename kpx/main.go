package main

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	_ "kpx/routers"
	)

func main() {
	//orm.Debug = true
	beego.BConfig.MaxMemory = 1 << 33	//小于5G文件
	//logs.SetLogger(logs.AdapterFile, `{"filename":"log/logs.log"}`)
	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", "root:"+beego.AppConfig.String("db.password")+"@tcp(127.0.0.1:3306)/"+beego.AppConfig.String("db.database")+"?charset=utf8")
	beego.Run()
}