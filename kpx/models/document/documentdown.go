package document

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
	"strconv"
)

/*文档下载记录表*/
type DocumentDown struct {
	Id           int    `json:"id"`
	Document_id  int    `json:"document_id"`  //文档id
	Filename     string `json:"filename"`     //文档名称
	User_id      string `json:"user_id"`      //用户id
	Downloadtime string `json:"downloadtime"` //下载事假
}

//获得文档下载记录,根据用户id
func GetDocumentDownByUserId(args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := lib.SearchTableSql("documentdown", args)

	var DocumentDown []*DocumentDown
	var getinfo lib.GetInfo
	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps) //有数据是返回相应信息
	if err == nil && num2 > 0 {
		num, _ := o.Raw(sql).QueryRows(&DocumentDown)
		if num > 0 {
			for _, v := range DocumentDown {
				num, _ := o.Raw("select category,filename from `document` where id=?", v.Document_id).Values(&maps)
				if num > 0 {
					v.Filename = maps[0]["category"].(string) + "/" + maps[0]["filename"].(string)
				}
			}
		}
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = DocumentDown //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得文档下载记录,分页
func GetDocumentDownByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `documentdown`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var DocumentDown []*DocumentDown
		var getinfo lib.GetInfo
		o.Raw("select *from `documentdown` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&DocumentDown)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = DocumentDown //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//修改文档下载记录
func UpdateDocumentDown(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("documentdown", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建文档下载记录
func CreateDocumentDown(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("documentdown", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
