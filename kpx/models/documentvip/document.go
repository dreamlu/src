package documentvip

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
	"strconv"
)

/*文档*/
type Document struct {
	Id          int    `json:"id"`
	Category    string `json:"category"`    //类别
	Filename    string `json:"filename"`    //文件名
	Fileaddress string `json:"fileaddress"` //文件地址
	Imageurl    string `json:"imageurl"`    //封面图
	Publishdate string `json:"publishdate"` //上传日期
	Introduce   string `json:"introduce"`   //文档简介
}

//根据搜索条件获得文档,分页
func GetDocumentBySearch(args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := lib.SearchTableSql("documentvip", args)

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Document []*Document
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&Document)
		//统计页码等状态

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Document //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得所有文档,根据id
func GetDocumentById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Document []*Document
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `documentvip` where id=?", id).QueryRows(&Document)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Document //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得文档,分页
func GetDocumentByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `documentvip`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Document []*Document
		var getinfo lib.GetInfo
		o.Raw("select *from `documentvip` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Document)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Document //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除文档,根据id
func DeleteDocumentByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `documentvip` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		//同时删除对应的下载记录
		o.Raw("delete from `documentdown` where document_id=?", id).Exec()
		info = lib.MapDelete
	}
	return info
}

//修改文档
func UpdateDocument(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("documentvip", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建文档
func CreateDocument(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("documentvip", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
