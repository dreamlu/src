package course

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
)

/*小栏目--听说读写*/
type Courselist struct {
	Id             int    `json:"id"`
	Course_id      int    `json:"course_id"`      //对应的小栏目id
	Courselistname string `json:"courselistname"` //小栏目下的分类名称,第几期之听说读写
	Teacher        string `json:"teacher"`         //讲师
	Imageurl       string `json:"imageurl"`       //封面图
	Publishdate    string `json:"publishdate"`    //上传时间
	Coursenum      string `json:"coursenum"`      //总课程数目
	//Duration     string `json:"duration"`       //总时长
}

/*小栏目下拉框*/
type CourselistN struct {
	Id             int    `json:"id"`
	Courselistname string `json:"courselistname"`
}

//小栏目小程序端,根据课程id
func GetCourselistByCourseIdX(course_id string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Courselist []*Courselist
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `courselist` where course_id=? order by id desc", course_id).QueryRows(&Courselist)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计课程总数等数据
		var maps []orm.Params
		for _, v := range Courselist {
			num, _ := o.Raw("select count(id) as num from `coursedetail` where courselist_id=?", v.Id).Values(&maps)
			var strnum = "0"
			if num > 0 {
				strnum = maps[0]["num"].(string)
			}
			v.Coursenum = strnum
		}
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Courselist //数据
		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//小栏目下拉框,根据课程id
func GetCourselistByCourseId(course_id string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Courselist []*CourselistN
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `courselist` where course_id=? order by id desc", course_id).QueryRows(&Courselist)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Courselist //数据
		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得所有小栏目对应的听说读写,根据id
func GetCourselistById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Courselist []*Courselist
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `courselist` where id=?", id).QueryRows(&Courselist)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Courselist //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得小栏目对应的听说读写,分页,类型(草稿/已发送)
func GetCourselistByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `courselist`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Courselist []*Courselist
		var getinfo lib.GetInfo
		num, _ := o.Raw("select *from `courselist` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Courselist)
		//统计页码等状态
		var SumPage = "0"
		if num > 0 {
			SumPage = maps[0]["num"].(string)
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Courselist //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除小栏目对应的听说读写,根据id
func DeleteCourselistByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `courselist` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		//删除对应视频(文件)表
		o.Raw("delete from `coursedetail` where courselist_id=?", id).Exec()
		info = lib.MapDelete
	}
	return info
}

//修改小栏目对应的听说读写
func UpdateCourselist(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("courselist", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建小栏目对应的听说读写
func CreateCourselist(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("courselist", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
