package course

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
	"strconv"
)

/*回复回复*/
type CommentReply struct {
	Id          int    `json:"id"`
	Comment_id  int    `json:"comment_id"`  //回复id
	Nickname    string `json:"nickname"`    //昵称
	Reply       string `json:"reply"`       //回复
	Publishdate string `json:"publishdate"` //发布时间
}

//获得回复,分页
func GetCommentReplyByPage(comment_id, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT id FROM `commentreply` where comment_id=?", comment_id).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var CommentReply []*CommentReply
		var getinfo lib.GetInfo
		o.Raw("select *from `commentreply` where comment_id=? limit ?,?", comment_id, (clientPage-1)*everyPage, everyPage).QueryRows(&CommentReply)
		//统计页码等状态

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = CommentReply //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除回复,根据id
func DeleteCommentReplyByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `commentreply` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//创建回复
func CreateCommentReply(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("commentreply", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
