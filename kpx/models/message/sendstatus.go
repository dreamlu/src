package message

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
)

/*发送状态*/
type Sendstatus struct {
	Id       int    `json:"id"`
	Phone    string `json:"phone"`    //手机号
	Status   int    `json:"status"`   //发送状态
	Senddate string `json:"senddate"` //发送时间
}

//获得发送状态,分页
func GetSendstatusByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT id as num FROM `sendstatus`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Sendstatus []*Sendstatus
		var getinfo lib.GetInfo
		o.Raw("select *from `sendstatus` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Sendstatus)

		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Sendstatus //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}
