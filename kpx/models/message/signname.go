package message

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
)

/*签名*/
type Signname struct {
	Id       int    `json:"id"`
	Signname string `json:"signname"` //签名
}

//获得签名图,根据id
func GetSignnameById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Signname []*Signname
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `signname` where id=?", id).QueryRows(&Signname)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Signname //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得签名图,分页
func GetSignnameByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT id as num FROM `signname`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Signname []*Signname
		var getinfo lib.GetInfo
		o.Raw("select *from `signname` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Signname)

		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Signname //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除签名图,根据id
func DeleteSignnameByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `signname` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改签名图
func UpdateSignname(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("signname", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建签名图
func CreateSignname(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("signname", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.GetMapDataError(500,err.Error())
	} else {
		info = lib.MapCreate
	}
	return info
}

