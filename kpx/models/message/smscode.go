package message

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
)

/*模板code*/
type Smscode struct {
	Id       int    `json:"id"`
	Smscode  string `json:"smscode"`  //模板code
	Describe string `json:"describe"` //描述
}

//获得模板code,根据id
func GetSmscodeById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Smscode []*Smscode
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `smscode` where id=?", id).QueryRows(&Smscode)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Smscode //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得模板code,分页
func GetSmscodeByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT id as num FROM `smscode`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Smscode []*Smscode
		var getinfo lib.GetInfo
		o.Raw("select *from `smscode` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Smscode)

		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Smscode //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除模板code,根据id
func DeleteSmscodeByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `smscode` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改模板code
func UpdateSmscode(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("smscode", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建模板code
func CreateSmscode(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("smscode", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.GetMapDataError(500, err.Error())
	} else {
		info = lib.MapCreate
	}
	return info
}
