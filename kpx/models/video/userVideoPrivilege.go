package video

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"kpx/lib"
	"strconv"
)

// 用户视频权限
type UserVideoPrivilege struct {
	Id       int    `json:"id"`
	UserId   int    `json:"user_id"`
	Category string `json:"category"`
}

//根据搜索条件获得用户视频权限,分页
func GetUserVideoPrivilegeBySearch(args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := SearchTableSql("user_video_privilege", args)

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var UserVideoPrivilege []*UserVideoPrivilege
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&UserVideoPrivilege)
		//统计页码等状态

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = UserVideoPrivilege //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得所有用户视频权限,根据id
func GetUserVideoPrivilegeById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var UserVideoPrivilege []*UserVideoPrivilege
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `user_video_privilege` where id=?", id).QueryRows(&UserVideoPrivilege)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = UserVideoPrivilege //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除用户视频权限,根据id
func DeleteUserVideoPrivilegeByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `user_video_privilege` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		//同时删除对应的下载记录
		//o.Raw("delete from `documentdown` where document_id=?", id).Exec()
		info = lib.MapDelete
	}
	return info
}

//修改用户视频权限
func UpdateUserVideoPrivilege(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("user_video_privilege", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建用户视频权限
func CreateUserVideoPrivilege(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("user_video_privilege", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}


// temp
/*传入表名,查询语句拼接*/
func SearchTableSql(tablename string, args map[string][]string) (sqlnolimit, sql string, clientPage, everyPage int) {

	//页码,每页数量
	clientPageStr := beego.AppConfig.String("clientPage") //默认第1页
	everyPageStr := beego.AppConfig.String("everyPage")   //默认10页

	sql = "select * from `" + tablename + "` where 1=1 and "
	for k, v := range args {
		if k == "clientPage" {
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" {
			everyPageStr = v[0]
			continue
		}
		if v[0] == "" { //条件为空,舍弃
			continue
		}
		sql += k + " = '" + v[0] + "' and "
	}

	clientPage, _ = strconv.Atoi(clientPageStr)
	everyPage, _ = strconv.Atoi(everyPageStr)

	c := []byte(sql)
	sql = string(c[:len(c)-4]) //去and
	sqlnolimit = sql
	sql += "order by id desc limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr

	return sqlnolimit, sql, clientPage, everyPage
}
