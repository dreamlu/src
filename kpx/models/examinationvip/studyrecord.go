package examinationvip

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
	"strconv"
)

/*学习记录表*/
type StudyRecord struct {
	Id              int    `json:"id"`
	Studydate       string `json:"studydate"`       //学习进入时间
	User_id         string `json:"user_id"`         //用户id
	Coursedetail_id int    `json:"coursedetail_id"` //对应视频id
	Filename        string `json:"filename"`        //文件名称
	Studytime       string `json:"studytime"`       //学习时长
}

//获得学习记录,根据用户id
func GetStudyRecordUser(user_id,clientPage,everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var StudyRecord []*StudyRecord
	var getinfo lib.GetInfo
	var maps []orm.Params
	num2, err := o.Raw("select a.id from `studyrecordvip` a inner join `coursedetailvip` b on a.coursedetail_id=b.id where a.user_id=?",user_id).Values(&maps) //有数据是返回相应信息
	if err == nil && num2 > 0 {
		num,_ := o.Raw("select a.id as id,studydate,user_id,coursedetail_id,studytime from `studyrecordvip` a inner join `coursedetailvip` b on a.coursedetail_id=b.id " +
			"where a.user_id=? order by a.id desc limit ?,?",user_id, (clientPage-1)*everyPage, everyPage).QueryRows(&StudyRecord)
		if num > 0{
			for _,v := range StudyRecord{
				num,_ := o.Raw("select coursename,courselistname,filename from `coursedetailvip` a inner join `courselistvip` b inner join `coursevip` c on a.courselist_id=b.id and b.course_id=c.id where a.id=?",v.Coursedetail_id).Values(&maps)
				if num > 0{
					v.Filename = maps[0]["coursename"].(string)+"/"+ maps[0]["courselistname"].(string)+"/"+ maps[0]["filename"].(string)
				}
			}
		}
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = StudyRecord //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得学习记录,分页
func GetStudyRecordByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `studyrecordvip`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var StudyRecord []*StudyRecord
		var getinfo lib.GetInfo
		o.Raw("select *from `studyrecordvip` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&StudyRecord)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = StudyRecord //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//创建学习记录
func CreateStudyRecord(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("studyrecordvip", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
