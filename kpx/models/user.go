package models

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	"kpx/lib"
	"strconv"
)

/*用户*/
type User struct {
	Id          int     `json:"id"`
	Openid      string  `json:"openid"`      //小程序openid
	Nickname    string  `json:"nickname"`    //昵称
	Headimg     string  `json:"headimg"`     //用户头像
	Wechatcard  string  `json:"wechatcard"`  //微信号
	Username    string  `json:"username"`    //用户姓名
	Sex         string  `json:"sex"`         //性别
	Area        string  `json:"area"`        //地区
	School      string  `json:"school"`      //院校
	Grade       string  `json:"grade"`       //年级
	Number      string  `json:"number"`      //学号
	Phone       string  `json:"phone"`       //手机号
	Scard       string  `json:"scard"`       //学生证图片地址或数字
	Joindate    string  `json:"joindate"`    //加入日期
	Ischeck     string  `json:"ischeck"`     //审核状态,0,1,2
	Userorigin  string  `json:"userorigin"`  //用户来源
	Openumber   int     `json:"openumber"`   //打开次数
	Staytime    float64 `json:"staytime"`    //停留时间
	Sharenumber int     `json:"sharenumber"` //分享次数
	IsVip       int     `json:"is_vip"`      //0否，1是
	Vipprice    float64 `json:"vipprice"`    //vip用户支付价格
	Viporigin   string  `json:"viporigin"`   //vip用户来源
}

/*用户审核状态*/
type UserCheck struct {
	Id      int    `json:"id"`
	Ischeck string `json:"ischeck"` //审核状态,0,1,2
}

func (u *User) StaytimeChange() {
	u.Staytime, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", u.Staytime*1.0/6e4), 64)
}

//获得用户id,根据openid
func GetUserIdByOpenId(openid string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var User []*User
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select id from `user` where openid=?", openid).QueryRows(&User)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = map[string]int{"id": User[0].Id} //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//根据搜索条件获得用户,分页
func GetUserBySearch(args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := lib.SearchTableSql("user", args)

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var User []*User
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&User)

		//时间穿换成分钟
		for _, v := range User {
			v.StaytimeChange()
		}

		//统计页码等状态

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = User //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得用户审核状态,根据id
func GetUserCheckById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var User []*UserCheck
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `user` where id=?", id).QueryRows(&User)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = User //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得用户,根据id
func GetUserById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var User []*User
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `user` where id=?", id).QueryRows(&User)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//时间穿换成分钟
		for _, v := range User {
			v.StaytimeChange()
		}
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = User //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得用户,分页
func GetUserByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `user`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var User []*User
		var getinfo lib.GetInfo
		o.Raw("select *from `user` where id > 1 order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&User)

		//时间穿换成分钟
		for _, v := range User {
			v.StaytimeChange()
		}
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = User //数据
		sn, _ := strconv.Atoi(SumPage)
		sn-- //排除管理员
		getinfo.Pager.SumPage = strconv.Itoa(sn)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除用户,根据id
func DeleteUserByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `user` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改用户
func UpdateUser(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("user", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建用户
func CreateUser(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("user", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}

//修改账号密码
func UpdateAccount(uid, nickname, userpassword string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw("update `user` a inner join `useraccount` b on a.id = b.user_id set nickname=?,userpassword=? where a.id=?", nickname, userpassword, uid).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}
