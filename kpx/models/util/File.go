package util

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	"github.com/tealeg/xlsx"
	"kpx/lib"
	"strings"
	"kpx/models"
	"reflect"
)

/*文件上传管理
type File struct {
	Id           int
	File_address string
	Describe     string
}*/

//获得刚上传的文件路径,富文本
func GetFilePath(path string) interface{} {
	var info interface{}
	//上传成功
	if strings.Index(path, ".") > 0 {
		info = map[string]string{"status": "201", "msg": "创建成功", "filename": path}
	} else {
		info = lib.MapError
	}
	return info
}

/*试卷批量新增*/
func CreateSomething(table, path, examination_id string) interface{} {
	var info interface{}
	o := orm.NewOrm()

	sql := "insert `" + table + "`(examination_id,"

	excelFileName := path
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		fmt.Printf("通用批量新增文件打开失败: %s\n", err)
	}
	for _, sheet := range xlFile.Sheets { //一个文件中的多张表
		for i := 1; i < len(sheet.Rows); i++ {
			row := sheet.Rows[i]
			//字段名
			if i == 1 {
				for _, cell := range row.Cells {
					text := cell.String()
					sql += text + ","
				}
				c := []byte(sql)
				sql = string(c[:len(c)-1]) + ") value(" //去掉点
				continue
			}
			sql += "'" + examination_id + "',"

			reading := "" //每次重置
			//插入值
			//遇到阅读理解时,合并到下一个题目中
			if row.Cells[1].String() == "" {
				//阅读理解
				reading = row.Cells[0].String()
				i++
				row = sheet.Rows[i]
			}

			for k, cell := range row.Cells {
				text := cell.String()
				//单引号拼接问题
				text = strings.Replace(text,"'","\\'",-1)
				//选择题目
				if k == 0 && reading != "" {
					text = reading + "\\n\\n" + text
				}
				//答案
				if k == 5 {
					switch text {
					case "A":
						text = "0"
					case "B":
						text = "1"
					case "C":
						text = "2"
					case "D":
						text = "3"
					}
				}
				sql += "'" + text + "',"
			}
			c := []byte(sql)
			sql = string(c[:len(c)-1]) + "),(" //去掉点
		}
		c := []byte(sql)
		sql = string(c[:len(c)-2]) //去掉点(
	}
	var num int64
	//删除之前上传的进行覆盖
	o.Raw("delete from `choicequestion` where examination_id=?", examination_id).Exec()
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}

/*用户数据导出*/
func ExportUser() interface{} {
	var info interface{}
	o := orm.NewOrm()
	var user []*models.User
	num,_ := o.Raw("select *from `user`").QueryRows(&user)
	if num > 0{
		var file *xlsx.File
		var sheet *xlsx.Sheet
		var row *xlsx.Row
		var cell *xlsx.Cell
		var err error

		file = xlsx.NewFile()
		sheet, err = file.AddSheet("Sheet1")
		if err != nil {
			fmt.Printf(err.Error())
		}
		for _,v := range user{
			row = sheet.AddRow()
			ref := reflect.ValueOf(v).Elem()
			col := reflect.TypeOf(v).Elem()
			for i := 2; i < ref.NumField() ; i++ {
				column := col.Field(i).Name
				value := ref.Field(i).String()
				switch column {
				case "Headimg" :
					continue
				case "Wechatcard":
					continue
				case "Sex":
					if value == "1"{
						value = "男"
					}else if value == "2"{
						value = "女"
					}else{
						value = "未知"
					}
				case "Scard":
					continue
				case "Ischeck":
					continue
				case "Openumber":
					continue
				case "Staytime":
					continue
				case "Sharenumber":
					continue
				case "Userorigin":
					continue

				}

				cell = row.AddCell()
				cell.Value = value
			}
		}

		path := "static/file/用户数据.xlsx"
		err = file.Save(path)
		if err != nil {
			fmt.Printf(err.Error())
		}
		info = map[string]interface{}{"status":200,"msg":"导出成功","filepath":path}
	}else {
		info = lib.MapError
	}

	return info
}