package coursevip

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
	"strconv"
)

/*用户课程*/
type UserCourse struct {
	Id            int    `json:"id"`
	Courselist_id int    `json:"courselist_id"` //课程栏目id
	Joindate      string `json:"joindate"`      //加入日期
}

/*小栏目--听说读写
type CourselistInfo struct {
	Id             int    `json:"id"`
	Course_id      int    `json:"course_id"`      //对应的小栏目id
	Courselistname string `json:"courselistname"` //小栏目下的分类名称,第几期之听说读写
	Teacher        string `json:"teacher"`         //讲师
	Imageurl       string `json:"imageurl"`       //封面图
	Publishdate    string `json:"publishdate"`    //上传时间
	Coursenum      string `json:"coursenum"`      //加入的课程数目
}
*/
//获得用户选中的栏目(听说读写),根据用户id和课程id
func GetCouselistByCourseId(user_id, course_id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var getinfo lib.GetInfoN
	var maps []orm.Params
	num2, err := o.Raw("select a.id as id from `usercoursevip` a inner join `courselistvip` b inner join `coursevip` c on a.courselist_id=b.id and b.course_id=c.id where a.user_id=? and c.id=?", user_id, course_id).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var courselist []*Courselist
		num, _ := o.Raw("select b.id as id,category,courselistname,teacher,b.imageurl,b.publishdate from `usercoursevip` a inner join `courselistvip` b inner join `coursevip` c on a.courselist_id=b.id and b.course_id=c.id where a.user_id=? and c.id=? order by id", user_id, course_id).QueryRows(&courselist)
		if num > 0 {
			for _, v := range courselist {
				num, _ := o.Raw("select count(id) as num from `coursedetailvip` where courselist_id=?", v.Id).Values(&maps)
				var strnum = "0"
				if num > 0 {
					strnum = maps[0]["num"].(string)
				}
				v.Coursenum = strnum
			}
		}
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = courselist //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得用户课程,根据用户id
func GetUserCourseByUserId(user_id int, category string, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var getinfo lib.GetInfo
	var maps []orm.Params
	num2, err := o.Raw("select a.id as id from `usercoursevip` a inner join `courselistvip` b inner join `coursevip` c on a.courselist_id=b.id and b.course_id=c.id where a.user_id=? and c.category=?", user_id, category).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Course []*Course
		o.Raw("select c.id as id,category,coursename,courseintroduce,c.imageurl,c.publishdate from `usercoursevip` a inner join `courselistvip` b inner join `coursevip` c on a.courselist_id=b.id and b.course_id=c.id where a.user_id=? and c.category=? order by id desc limit ?,?", user_id, category, (clientPage-1)*everyPage, everyPage).QueryRows(&Course)

		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Course //数据
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除用户课程,根据id
func DeleteUserCourseByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `usercoursevip` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改用户课程
func UpdateUserCourse(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("usercoursevip", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建用户课程
func CreateUserCourse(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("usercoursevip", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
