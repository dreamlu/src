package coursevip

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
	"strconv"
)

/*评论表*/
type Comment struct {
	Id              int             `json:"id"`
	User_id         int             `json:"user_id"`         //用户id
	Nickname        string          `json:"nickname"`        //用户昵称
	Headimg         string          `json:"headimg"`         //用户头像
	Coursedetail_id int             `json:"coursedetail_id"` //对应的评论id
	Comment         string          `json:"comment"`         //评论
	Commentdate     string          `json:"commentdate"`     //评论时间
	Score           int             `json:"score"`           //评论1-5颗小星星
	CommentReply    []*CommentReply `json:"comment_reply"`   //评论对应的回复
}

//根据具体课程id查看评论
func GetCommentByCoursedetailId(coursedetail_id, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Comment []*Comment
	var getinfo lib.GetInfo
	var maps []orm.Params
	num2, err := o.Raw("SELECT id FROM `commentvip` where coursedetail_id=?", coursedetail_id).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		num, _ := o.Raw("select *from `commentvip` where coursedetail_id=? order by id desc limit ?,?", coursedetail_id, (clientPage-1)*everyPage, everyPage).QueryRows(&Comment)
		if num > 0 {
			for _, v := range Comment {
				num, _ := o.Raw("select nickname,headimg from `user` where id=?", v.User_id).Values(&maps)
				if num > 0 {
					if maps[0]["nickname"] == nil{
						continue
					}
					v.Nickname = maps[0]["nickname"].(string)
					v.Headimg = maps[0]["headimg"].(string)
				}
				//查看对应评论的回复
				o.Raw("select *from `commentreplyvip` where comment_id=?",v.Id).QueryRows(&v.CommentReply)
			}
		}
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Comment //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.EveryPage = everyPage
		getinfo.Pager.ClientPage = clientPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得评论,根据id
func GetCommentById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Comment []*Comment
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `commentvip` where id=?", id).QueryRows(&Comment)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {

		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Comment //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得评论,分页
func GetCommentByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `commentvip`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Comment []*Comment
		var getinfo lib.GetInfo
		o.Raw("select *from `commentvip` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Comment)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Comment //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除评论,根据id
func DeleteCommentByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `commentvip` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改评论
func UpdateComment(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("commentvip", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建评论
func CreateComment(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("commentvip", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
