package coursevip

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
	"strconv"
)

/*课程*/
type Course struct {
	Id              int    `json:"id"`
	Category        string `json:"category"`        //类别
	Coursename      string `json:"coursename"`      //课程姓名
	Courseintroduce string `json:"courseintroduce"` //课程介绍
	Imageurl        string `json:"imageurl"`        //封面图
	Publishdate     string `json:"publishdate"`     //上传时间
}

/*小栏目--听说读写2.0*/
type CourselistV2 struct {
	Id             int    `json:"id"`
	Coursename     string `json:"coursename"`     //课程姓名
	Courselistname string `json:"courselistname"` //小栏目下的分类名称,第几期之听说读写
	Teacher        string `json:"teacher"`        //讲师
	Imageurl       string `json:"imageurl"`       //封面图
	Coursenum      string `json:"coursenum"`      //总课程数目
}

/*网站排布方式v3.0*/
/*课程排布*/
type CourseV3 struct {
	Id		int `json:"id"`
	Coursename	string `json:"coursename"`
	Courselist 	[]CourselistV3
}

/*小栏目--听说读写2.0*/
type CourselistV3 struct {
	Id             int    `json:"id"`
	Courselistname string `json:"courselistname"` //小栏目下的分类名称,第几期之听说读写
	Teacher        string `json:"teacher"`        //讲师
	Imageurl       string `json:"imageurl"`       //封面图
	Coursenum      string `json:"coursenum"`      //总课程数目
}


//根据搜索条件获得课程,分页,v3.0
func GetCourseBySearchV3(category string, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("select id from `coursevip` where category=?", category).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Course []*CourseV3
		var getinfo lib.GetInfo
		num, _ := o.Raw("select id,coursename from `coursevip` where category=? order by id limit ?,?", category, (clientPage-1)*everyPage, everyPage).QueryRows(&Course)
		if num > 0 {
			for _, v2 := range Course {
				o.Raw(`select a.id,courselistname,teacher,imageurl,count(b.id) as coursenum
					from courselistvip a
					inner join coursedetailvip b
					on a.id=b.courselist_id
					where course_id = ?
					group by a.id,courselistname,teacher,imageurl`,v2.Id).QueryRows(&v2.Courselist)
			}
		}
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Course //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//根据搜索条件获得课程,分页,v2.0
func GetCourseBySearchV2(category string, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("select b.id from `coursevip` a inner join `courselistvip` b on a.id=b.course_id where category=?", category).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var CourselistV2 []*CourselistV2
		var getinfo lib.GetInfo
		num, _ := o.Raw("select b.id,coursename,courselistname,teacher,b.imageurl from `coursevip` a inner join `courselistvip` b on a.id=b.course_id  where category=? order by a.id,b.id limit ?,?", category, (clientPage-1)*everyPage, everyPage).QueryRows(&CourselistV2)
		if num > 0 {
			var maps []orm.Params
			for _, v2 := range CourselistV2 {
				num, _ := o.Raw("select count(id) as num from `coursedetailvip` where courselist_id=?", v2.Id).Values(&maps)
				var strnum = "0"
				if num > 0 {
					strnum = maps[0]["num"].(string)
				}
				v2.Coursenum = strnum
			}
		}
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = CourselistV2 //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//根据搜索条件获得课程,分页
func GetCourseBySearch(args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := lib.SearchTableSql("coursevip", args)

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Course []*Course
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&Course)
		//统计页码等状态

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Course //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得课程,根据id
func GetCourseById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Course []*Course
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `coursevip` where id=?", id).QueryRows(&Course)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Course //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得课程,分页
func GetCourseByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `coursevip`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Course []*Course
		var getinfo lib.GetInfo
		o.Raw("select *from `coursevip` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Course)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Course //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除课程,根据id
func DeleteCourseByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `coursevip` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		var maps []orm.Params
		//先删除栏目下面视频文件
		num, _ := o.Raw("select id from `courselistvip` where course_id=?", id).Values(&maps)
		if num > 0 {
			for _, v := range maps {
				o.Raw("delete from `coursedetailvip` where courselist_id=?", v["id"]).Exec()
			}
		}
		//删除对应栏目,听说读写
		o.Raw("delete from `courselistvip` where course_id=?", id).Exec()

		info = lib.MapDelete
	}
	return info
}

//修改课程
func UpdateCourse(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("coursevip", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建课程
func CreateCourse(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("coursevip", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
