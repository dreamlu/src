package coursevip

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
	)

/*听说读写对应的文件/视频*/
type Coursedetail struct {
	Id            int    `json:"id"`
	Courselist_id int    `json:"courselist_id"` //id
	Filename      string `json:"filename"`      //文件(视频)名
	Fileaddress   string `json:"fileaddress"`   //文件(视频)地址
	Publishdate   string `json:"publishdate"`   //上传时间
}

/*无分页数据信息*/
/*分页数据信息*/
type GetInfoNH struct {
	Status int         `json:"status"`
	Msg    string      `json:"msg"`
	Data   interface{} `json:"data"`   //数据,通用接口
	Isjoin int64       `json:"isjoin"` //0/1表示是否已经加入课程
}

//栏目听说读写视频文件,根据小栏目id
func GetCoursedetailByCourselistIdP(courselist_id string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Coursedetail []*Coursedetail
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `coursedetailvip` where courselist_id=?", courselist_id).QueryRows(&Coursedetail)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		/*//遍历添加视频文件地址前缀
		for _,v := range Coursedetail{
			v.Fileaddress = beego.AppConfig.String("oss.path")+"/" + beego.AppConfig.String("oss.upload_dir") + v.Fileaddress
		}*/

		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Coursedetail //数据
		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}

	return info
}

//栏目听说读写视频文件,根据小栏目id,小程序端
func GetCoursedetailByCourselistId(user_id, courselist_id string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Coursedetail []*Coursedetail
	var getinfo GetInfoNH
	num2, err := o.Raw("select *from `coursedetailvip` where courselist_id=?", courselist_id).QueryRows(&Coursedetail)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		/*//遍历添加视频文件地址前缀
		for _,v := range Coursedetail{
			v.Fileaddress = beego.AppConfig.String("oss.path")+"/"+ beego.AppConfig.String("oss.upload_dir") + v.Fileaddress
		}*/

		var maps []orm.Params
		num2, _ := o.Raw("select id from `usercoursevip` where user_id=? and courselist_id=?", user_id, courselist_id).Values(&maps)
		if num2 > 0 {
			getinfo.Isjoin = 1
		}

		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Coursedetail //数据
		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得所有栏目听说读写视频文件,根据id
func GetCoursedetailById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Coursedetail []*Coursedetail
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `coursedetailvip` where id=?", id).QueryRows(&Coursedetail)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		/*//遍历添加视频文件地址前缀
		for _,v := range Coursedetail{
			v.Fileaddress = beego.AppConfig.String("oss.path")+"/"+ beego.AppConfig.String("oss.upload_dir") +v.Fileaddress
		}*/
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Coursedetail //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得栏目听说读写视频文件,分页,类型
func GetCoursedetailByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `coursedetailvip`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Coursedetail []*Coursedetail
		var getinfo lib.GetInfo
		o.Raw("select *from `coursedetailvip` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Coursedetail)
		/*if num > 0{
			//遍历添加视频文件地址前缀
			for _,v := range Coursedetail{
				v.Fileaddress = beego.AppConfig.String("oss.path")+"/"+ beego.AppConfig.String("oss.upload_dir") + v.Fileaddress
			}
		}*/
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Coursedetail //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除栏目听说读写视频文件,根据id
func DeleteCoursedetailByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `coursedetailvip` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		//删除对应的学习记录
		o.Raw("delete from `studyrecordvip` where coursedetail_id=?", id).Exec()
		info = lib.MapDelete
	}
	return info
}

//修改栏目听说读写视频文件
func UpdateCoursedetail(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("coursedetailvip", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建栏目听说读写视频文件
func CreateCoursedetail(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("coursedetailvip", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
