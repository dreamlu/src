package carousel

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
)

/*轮播图*/
type Carousel struct {
	Id     int    `json:"id"`
	Imgurl string `json:"imgurl"`
}

//获得轮播图,根据id
func GetCarouselById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Carousel []*Carousel
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `carousel` where id=?", id).QueryRows(&Carousel)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Carousel //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得轮播图,分页
func GetCarouselByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `carousel`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Carousel []*Carousel
		var getinfo lib.GetInfo
		o.Raw("select *from `carousel` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Carousel)

		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Carousel //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除轮播图,根据id
func DeleteCarouselByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `carousel` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改轮播图
func UpdateCarousel(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("carousel", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建轮播图
func CreateCarousel(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("carousel", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
