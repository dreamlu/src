package flag

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
)

/*flag*/
type Flag struct {
	Id    int    `json:"id"`
	Flag  int    `json:"flag"`	//0vip价格
	Value string `json:"value"` //价格
}

//获得flag,根据id
func GetFlagById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Flag 	*Flag
	var getinfo lib.GetInfoN
	err := o.Raw("select *from `flag` where id=?", id).QueryRow(&Flag)
	//有数据是返回相应信息
	if err == nil  {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Flag //数据

		info = getinfo
	} else {
		info = lib.MapNoResult
	}
	return info
}

//获得flag,分页
func GetFlagByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `flag`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Flag []*Flag
		var getinfo lib.GetInfo
		o.Raw("select *from `flag` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Flag)

		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Flag //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除flag,根据id
func DeleteFlagByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `flag` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改flag
func UpdateFlag(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("flag", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建flag
func CreateFlag(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("flag", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
