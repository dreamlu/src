package examination

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
	"strconv"
)

/*试卷*/
type Examination struct {
	Id          int    `json:"id"`
	Category    string `json:"category"`    //类别
	Name        string `json:"name"`        //试卷名称
	Time        string `json:"time"`        //时间限制
	Publishdate string `json:"publishdate"` //上传时间
	Imageurl	string `json:"imageurl"`	//预览图
}

//根据搜索条件获得试卷,分页
func GetExaminationBySearch(args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := lib.SearchTableSql("examination", args)

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Examination []*Examination
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&Examination)
		//统计页码等状态

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Examination //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得试卷,根据类别,分页
func GetExaminationByCategory(category string,clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT id as num FROM `examination` where category=?",category).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Examination []*Examination
		var getinfo lib.GetInfo
		o.Raw("select *from `examination` where category=? order by id desc limit ?,?", category, (clientPage-1)*everyPage, everyPage).QueryRows(&Examination)
		//统计页码等状态

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Examination //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得所有试卷,根据id
func GetExaminationById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Examination []*Examination
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `examination` where id=?", id).QueryRows(&Examination)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Examination //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得试卷,分页
func GetExaminationByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `examination`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Examination []*Examination
		var getinfo lib.GetInfo
		o.Raw("select *from `examination` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Examination)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Examination //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除试卷,根据id
func DeleteExaminationByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `examination` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		//同时删除对应的下载记录
		o.Raw("delete from `examinationdown` where examination_id=?", id).Exec()
		info = lib.MapDelete
	}
	return info
}

//修改试卷
func UpdateExamination(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("examination", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建试卷
func CreateExamination(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("examination", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
