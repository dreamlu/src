package examination

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
)

/*选择题表*/
type Choicequestion struct {
	Id             int `json:"id"`
	Examination_id int `json:"examination_id"` //试卷id
	//Number         int    `json:"number"`         //题号
	Title   string `json:"title"`   //题目
	Option1 string `json:"option1"` //选项1
	Option2 string `json:"option2"` //选项2
	Option3 string `json:"option3"` //选项3
	Option4 string `json:"option4"` //选项4
	Answer  string `json:"answer"`  //答案
}

type GetInfoNN struct {
	Status int         `json:"status"`
	Msg    string      `json:"msg"`
	Data   interface{} `json:"data"`  //数据,通用接口
	Title  string      `json:"title"` //试卷名称
	Time   string      `json:"time"`  //时间限制
}

//获得选择题,根据测试(试卷)id
func GetChoicequestionByExaminationId(examination_id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Choicequestion []*Choicequestion
	var getinfo GetInfoNN
	num2, err := o.Raw("select *from `choicequestion` where examination_id=?", examination_id).QueryRows(&Choicequestion)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var maps []orm.Params
		num, _ := o.Raw("select name,time from examination where id=?", examination_id).Values(&maps)
		if num > 0 {
			getinfo.Title = maps[0]["name"].(string)
			getinfo.Time = maps[0]["time"].(string)
		}
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Choicequestion //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得选择题,根据id
func GetChoicequestionById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Choicequestion []*Choicequestion
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `choicequestion` where id=?", id).QueryRows(&Choicequestion)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {

		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Choicequestion //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得选择题,分页
func GetChoicequestionByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `choicequestion`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Choicequestion []*Choicequestion
		var getinfo lib.GetInfo
		o.Raw("select *from `choicequestion` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Choicequestion)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Choicequestion //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除选择题,根据id
func DeleteChoicequestionByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `choicequestion` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改选择题
func UpdateChoicequestion(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("choicequestion", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建选择题
func CreateChoicequestion(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("choicequestion", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
