package examination

import (
	"github.com/astaxie/beego/orm"
	"kpx/lib"
	"strconv"
	"strings"
)

/*测试记录(包含错题)*/
type Examinationrecord struct {
	Id              int            `json:"id"`
	Examination_id  int            `json:"examination_id"`  //试卷id
	Name            string         `json:"name"`            //试卷名称
	User_id         int            `json:"user_id"`         //用户id
	Examinationdate string         `json:"examinationdate"` //测试开始时间
	Answers         string         `json:"answers"`         //用户答案数组
	Score           string         `json:"score"`           //分数
	Examination     []*Examination `json:"examination"`     //试卷信息
}

/*测试记录(包含错题),,小程序端*/
type Examinationrecordx struct {
	Id             int            `json:"id"`
	Examination_id int            `json:"examination_id"`
	ExaminationX    []*ExaminationX `json:"examination"` //试卷信息
}

/*试卷,测试记录*/
type ExaminationX struct {
	Id         			 int    `json:"id"`
	//Examinationrecord_id int    `json:"examinationrecord_id"` //测试记录id
	Category             string `json:"category"`             //类别
	Name                 string `json:"name"`                 //试卷名称
	Time                 string `json:"time"`                 //时间限制
	Publishdate          string `json:"publishdate"`          //上传时间
	Imageurl             string `json:"imageurl"`             //预览图
}

/*试卷,测试记录,pc端*/
type ExaminationPC struct {
	Id         			 int    `json:"id"`
	Category             string `json:"category"`             //类别
	Name                 string `json:"name"`                 //试卷名称
	Time                 string `json:"time"`                 //时间限制
	Imageurl             string `json:"imageurl"`             //预览图
	Score				 string `json:"score"`				  //分数
}

type GetInfoNX struct {
	Status  int         `json:"status"`
	Msg     string      `json:"msg"`
	Data    interface{} `json:"data"`    //数据,通用接口
	Answers []int       `json:"answers"` //用户测试答案数组
}

//根据用户id和测试记录id获得对应测试选择题
func GetErrorExam(user_id, id int) interface{} {
	var info interface{}
	var getinfo GetInfoNX
	var Choicequestion []*Choicequestion
	o := orm.NewOrm()
	var maps []orm.Params
	num, err := o.Raw("select answers from `examinationrecord` where user_id=? and id=?", user_id, id).Values(&maps)
	if err == nil && num > 0 {
		//答案解析成数组
		var ans = maps[0]["answers"].(string)
		ansStr := strings.Split(ans, ",")
		for _, v := range ansStr {
			answer, _ := strconv.Atoi(v)
			getinfo.Answers = append(getinfo.Answers, answer)
		}
		//对应的选择题
		o.Raw("select *from `choicequestion` a inner join `examinationrecord` b on a.examination_id = b.examination_id where b.id=?", id).QueryRows(&Choicequestion)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Choicequestion
		info = getinfo
	} else if err == nil && num == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//根据用户id获得试卷试题记录,分页,PC端
func GetExaminationrecordByUserIdPC(user_id, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT a.id as num FROM `examinationrecord` a "+
		"inner join `examination` b "+
		"on a.examination_id=b.id "+
		"where user_id=?", user_id).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Examinationpc []*ExaminationPC
		var getinfo lib.GetInfo
		o.Raw("select b.id as id,a.category,a.name,a.time,a.publishdate,a.imageurl,score " +
			"from `examination` a inner join `examinationrecord` b on a.id=b.examination_id " +
			"where user_id=? " +
			"order by id desc limit ?,?", user_id, (clientPage-1)*everyPage, everyPage).QueryRows(&Examinationpc)

		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Examinationpc //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//根据用户id获得试卷试题记录,分页
func GetExaminationrecordByUserIdx(user_id, clientPage, everyPage int, category string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT a.id as num FROM `examinationrecord` a "+
		"inner join `examination` b "+
		"on a.examination_id=b.id "+
		"where user_id=? and category=?", user_id, category).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Examinationx []*ExaminationX
		var getinfo lib.GetInfo
		o.Raw("select b.id as id,a.category,a.name,a.time,a.publishdate,a.imageurl " +
			"from `examination` a inner join `examinationrecord` b on a.id=b.examination_id " +
			"where user_id=? and category=? " +
			"order by id desc limit ?,?", user_id, category, (clientPage-1)*everyPage, everyPage).QueryRows(&Examinationx)

		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Examinationx //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//根据用户id获得试卷试题记录,分页
func GetExaminationrecordByUserId(user_id, clientPage, everyPage int, category string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT a.id as num FROM `examinationrecord` a "+
		"inner join `examination` b "+
		"on a.examination_id=b.id "+
		"where user_id=? and category=?", user_id, category).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Examinationrecord []*Examinationrecord
		var getinfo lib.GetInfo
		num, _ := o.Raw("select a.id as id,examination_id,name,user_id,examinationdate,answers,score "+
			"from `examinationrecord` a "+
			"inner join `examination` b "+
			"on a.examination_id=b.id "+
			"where user_id=? and category=? order by id desc limit ?,?", user_id, category, (clientPage-1)*everyPage, everyPage).QueryRows(&Examinationrecord)
		if num > 0 {
			for _, v := range Examinationrecord {
				o.Raw("select *from `examination` where id=?", v.Examination_id).QueryRows(&v.Examination)
			}
		}
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Examinationrecord //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除试卷试题记录,根据id
func DeleteExaminationrecordByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `examinationrecord` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改试卷试题记录
func UpdateExaminationrecord(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("examinationrecord", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建试卷试题记录
func CreateExaminationrecord(args map[string][]string, answers, examination_id string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数
	var id string
	score := 0 //分数

	sql := lib.GetInsertSqlById("examinationrecord", args)
	o := orm.NewOrm()
	o.Using("default")
	somerr := o.Begin()
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		//分数统计
		var choicequestion []*Choicequestion
		//查找答案
		num, _ := o.Raw("select answer from `choicequestion` where examination_id=?", examination_id).QueryRows(&choicequestion)
		if num > 0 {
			ans := strings.Split(answers, ",")
			for i := 0; i < len(choicequestion) && i < len(ans); i++ { //防止答案数目与题目数不相等
				if choicequestion[i].Answer == ans[i] {
					score += 2
				}
			}
			//更新分数
			var maps []orm.Params
			num, _ := o.Raw("select max(id) as id from `examinationrecord`").Values(&maps)
			if num > 0 {
				id = maps[0]["id"].(string)
				o.Raw("update `examinationrecord` set score=? where id=?", score, id).Exec()
			}
		}
		info = map[string]string{"status": "201", "msg": "创建成功", "score": strconv.Itoa(score), "id": id}
	}
	if somerr != nil {
		err = o.Rollback()
	} else {
		err = o.Commit()
	}
	return info
}
