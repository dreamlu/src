package controllers

import (
	"github.com/astaxie/beego"
	"bm/models"
)

type FileInfo struct {
	beego.Controller
}

// @router /file/getfileinfo [get]
func (u *FileInfo) GetFileInfo(){
	dir := u.GetString("dir")

	ss := models.GetFileInfo(dir)
	u.Data["json"] = ss
	u.ServeJSON()
}

