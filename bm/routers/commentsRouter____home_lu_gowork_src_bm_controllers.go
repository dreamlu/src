package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["bm/controllers:FileInfo"] = append(beego.GlobalControllerRouter["bm/controllers:FileInfo"],
		beego.ControllerComments{
			Method: "GetFileInfo",
			Router: `/file/getfileinfo`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

}
