
package routers

import (
		"github.com/astaxie/beego"
	"bm/controllers"
	"github.com/astaxie/beego/plugins/cors"
)

func init() {
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"get", "post", "options", "patch", "delete", "*"},
		AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "X-Requested-With", "Access-Control-Allow-Credentials"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))
	beego.Include(&controllers.FileInfo{})
}
