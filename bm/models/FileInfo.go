package models

import (
		"io/ioutil"
)

type GetInfo struct {
	Msg    string      `json:"msg"`
	Status string      `json:"status"`
	Data   interface{} `json:"data"`
}

/*文件*/
type Fileinfo struct {
	Fileame []string//文件名
	Size    int    //文件数量
}

func GetFileInfo(dir string) interface{} {
	var info interface{}
	var getinfo GetInfo
	var fileinfo Fileinfo
	myfolder := dir

	files, _ := ioutil.ReadDir(myfolder)
	if len(files) == 0 {
		info = map[string]string{"status": "204", "msg": "暂无数据"}
	}else{
		fileinfo.Size = len(files)
		for _, file := range files {
			if file.IsDir() {
				continue
			} else {
				fileinfo.Fileame = append(fileinfo.Fileame,file.Name())
			}
		}
		getinfo.Status = "200"
		getinfo.Msg = "请求成功"
		getinfo.Data = fileinfo
		info = getinfo
	}
	return info
}
