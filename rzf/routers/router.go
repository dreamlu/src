package routers

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/redis"
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/controllers"
	"rzf/controllers/alipay"
	"rzf/controllers/basic"
	"rzf/controllers/carousel"
	"rzf/controllers/client"
	"rzf/controllers/device"
	"rzf/controllers/distribute"
	"rzf/controllers/exctAct"
	"rzf/controllers/groupmeal"
	"rzf/controllers/hirecar"
	"rzf/controllers/label"
	"rzf/controllers/order"
	"rzf/controllers/other"
	"rzf/controllers/protocol"
	"rzf/controllers/proxyer"
	"rzf/controllers/sharer"
	"rzf/controllers/venue"
	"rzf/controllers/venueprice"
	"rzf/controllers/venuer"
	"rzf/controllers/wx"
	"rzf/util/file"
	"rzf/util/lib"
	"rzf/util/str"
	"strings"
)

func SetRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	router := gin.New()
	str.MaxUploadMemory = router.MaxMultipartMemory
	//router.Use(CorsMiddleware())

	router.Use(CheckLogin()) //简单登录验证

	store, _ := redis.NewStore(10, "tcp", "localhost:6379", "", []byte("secret"))
	router.Use(sessions.Sessions("mysession", store))

	// load the casbin model and policy from files, database is also supported.
	//权限中间件
	//e := casbin.NewEnforcer("conf/authz_model.conf", "conf/authz_policy.csv")
	//router.Use(controllers.NewAuthorizer(e))

	//静态目录
	router.Static("api/v2/static", "static")

	// Ping test
	router.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
	//组的路由,version
	v2 := router.Group("/api/v2")
	{
		v := v2
		//用户登录
		v.POST("/login/login", controllers.Login)
		//网站基本信息
		v.GET("/basic/getbasic", basic.GetBasicInfo)
		//文件上传
		v.POST("/file/upload", file.UpoadFile)
		//验证码
		/*captchas := v.Group("/captcha")
		{
			captchas.GET("/getkey", captcha.GetKey)
			captchas.GET("/showimage", captcha.ShowImage)
			//captchas.GET("/verify", captcha.Verify)
		}*/
		//场馆
		venues := v.Group("/venue")
		{
			venues.GET("/getmonthdata", venue.GetVenueMonthData)
			venues.GET("/gets", venue.GetVenues)
			venues.GET("/labels", venue.GetVenueLabels)
			venues.GET("/getbysearch", venue.GetVenueBySearch)
			venues.GET("/getbyid", venue.GetVenueById)
			venues.DELETE("/deletebyid/:id", venue.DeleteVenueById)
			venues.POST("/create", venue.CreateVenue)
			venues.POST("/update", venue.UpdateVenue)
			venues.GET("/getbysearchx", venue.GetVenueBySearchX)
		}
		//场馆对应时间段
		venueprices := v.Group("/venueprice")
		{
			venueprices.GET("/getbysearch", venueprice.GetVenuepriceBySearch)
			venueprices.GET("/getbysearchx", venueprice.GetVenuepriceBySearchX)
			venueprices.GET("/getbyid", venueprice.GetVenuepriceById)
			venueprices.DELETE("/deletebyid/:id", venueprice.DeleteVenuepriceById)
			venueprices.POST("/create", venueprice.CreateVenueprice)
			venueprices.POST("/update", venueprice.UpdateVenueprice)
		}
		//场馆对应特殊时间段
		venuepricetss := v.Group("/venuepricets")
		{
			venuepricetss.GET("/searchX", venueprice.GetVenuepricetsBySearchX)
			venuepricetss.GET("/getbysearch", venueprice.GetVenuepricetsBySearch)
			venuepricetss.GET("/getbyid", venueprice.GetVenuepricetsById)
			venuepricetss.DELETE("/deletebyid/:id", venueprice.DeleteVenuepricetsById)
			venuepricetss.POST("/create", venueprice.CreateVenuepricets)
			venuepricetss.POST("/update", venueprice.UpdateVenuepricets)
		}
		//馆主
		venuers := v.Group("/venuer")
		{
			venuers.GET("/gets", venuer.GetVenuers)
			venuers.GET("/getbysearch", venuer.GetVenuerBySearch)
			venuers.GET("/getbyid", venuer.GetVenuerById)
			venuers.GET("/getbyidx", venuer.GetVenuerByIdX)
			venuers.DELETE("/deletebyid/:id", venuer.DeleteVenuerById)
			venuers.POST("/create", venuer.CreateVenuer)
			venuers.POST("/update", venuer.UpdateVenuer)
		}
		//馆主或代理分销
		distributes := v.Group("/distribute")
		{
			distributes.GET("/gets", distribute.GetDistributes)
		}
		//代理
		proxyers := v.Group("/proxyer")
		{
			proxyers.GET("/getbysearch", proxyer.GetProxyerBySearch)
			proxyers.GET("/getbyid", proxyer.GetProxyerById)
			proxyers.GET("/getbyidx", proxyer.GetProxyerByIdX)
			proxyers.DELETE("/deletebyid/:id", proxyer.DeleteProxyerById)
			proxyers.POST("/create", proxyer.CreateProxyer)
			proxyers.POST("/update", proxyer.UpdateProxyer)
			proxyers.GET("/sendsms", proxyer.SendSMS)
			proxyers.POST("/register", proxyer.Register)
		}
		//服务管理
		//团餐
		groupmeals := v.Group("/groupmeal")
		{
			groupmeals.GET("/getbysearch", groupmeal.GetGroupmealBySearch)
			groupmeals.GET("/getbyid", groupmeal.GetGroupmealById)
			groupmeals.DELETE("/deletebyid/:id", groupmeal.DeleteGroupmealById)
			groupmeals.POST("/create", groupmeal.CreateGroupmeal)
			groupmeals.POST("/update", groupmeal.UpdateGroupmeal)
		}
		//包车
		hirecars := v.Group("/hirecar")
		{
			hirecars.GET("/getbysearch", hirecar.GetHirecarBySearch)
			hirecars.GET("/getbyid", hirecar.GetHirecarById)
			hirecars.DELETE("/deletebyid/:id", hirecar.DeleteHirecarById)
			hirecars.POST("/create", hirecar.CreateHirecar)
			hirecars.POST("/update", hirecar.UpdateHirecar)
		}
		//设备
		devices := v.Group("/device")
		{
			devices.GET("/getbysearch", device.GetDeviceBySearch)
			devices.GET("/getbyid", device.GetDeviceById)
			devices.DELETE("/deletebyid/:id", device.DeleteDeviceById)
			devices.POST("/create", device.CreateDevice)
			devices.POST("/update", device.UpdateDevice)
		}
		//其他
		others := v.Group("/other")
		{
			others.GET("/get", other.GetOther)
			others.POST("/update", other.UpdateOther)
		}
		//订单
		orders := v.Group("/order")
		{
			orders.GET("/getbysearchx", order.GetOrderBySearchX)
			orders.GET("/getbysearch", order.GetOrderBySearch)
			orders.GET("/getbyid", order.GetOrderById)
			orders.DELETE("/deletebyid/:id", order.DeleteOrderById)
			orders.POST("/create", order.CreateOrder)
			orders.POST("/createx", order.CreateOrderX)
			orders.POST("/update", order.UpdateOrder)
			orders.POST("/updateb", order.UpdateOrderB)
			orders.POST("/creategp", order.CreateOrdergp)
			orders.GET("/getgp", order.GetOrdergp)
			orders.GET("/getisgp", order.GetIsGp)
			orders.POST("/createcgp", order.CreateCartgp)
			orders.GET("/getcgp", order.GetCartgp)
		}
		//小程序
		wxs := v.Group("/wx")
		{
			wxs.POST("/wxlogin", wx.WxLogin)
			wxs.POST("/wxdecrypt", wx.WxDecrypt)
			wxs.POST("/wxwithdraw", wx.WxWithDraw)
			wxs.POST("/wxpay", wx.WxPay)
			wxs.GET("/getaccesstoken", wx.GetAccessToken)
			wxs.GET("/getqrcode", wx.GetQRCode)

			// update 2019/09/04
			wxs.GET("/call", wx.WxCall)
			wxs.POST("/call", wx.WxCall)
			wxs.GET("/gQrcode", wx.WxQrCode)

			wxs.GET("/pQrcode", wx.PQrcode)
		}
		//客户
		clients := v.Group("/client")
		{
			clients.GET("/getdata", client.GetClientData)
			clients.GET("/getid", client.GetClientId)
			clients.GET("/getbyidx", client.GetClientByIdX)
			clients.POST("/update", client.UpdateClient)
		}
		//轮播
		carousels := v.Group("/carousel")
		{
			carousels.GET("/getbysearch", carousel.GetCarouselBySearch)
			carousels.GET("/getbyid", carousel.GetCarouselById)
			carousels.DELETE("/deletebyid/:id", carousel.DeleteCarouselById)
			carousels.POST("/create", carousel.CreateCarousel)
			carousels.POST("/update", carousel.UpdateCarousel)
		}
		//支付
		alipays := v.Group("/alipay")
		{
			alipays.POST("/pay", alipay.Alipay)
			alipays.GET("/payres", alipay.AlipayRes)
		}
		//标签
		labels := v.Group("/label")
		{
			labels.GET("/getx", label.GetLabelX)
			labels.GET("/getbysearch", label.GetLabelBySearch)
			labels.GET("/getbyid", label.GetLabelById)
			labels.DELETE("/deletebyid/:id", label.DeleteLabelById)
			labels.POST("/create", label.CreateLabel)
			labels.POST("/update", label.UpdateLabel)
		}
		//协议
		protocols := v.Group("/protocol")
		{
			protocols.GET("/id", protocol.GetById)
			protocols.PUT("/update", protocol.Update)
		}

		// update
		// 推广服务号
		sharers := v.Group("/sharer")
		{
			sharers.GET("/search", sharer.GetSharerBySearch)
			sharers.GET("/id", sharer.GetSharerById)
			sharers.DELETE("/delete/:id", sharer.DeleteSharerById)
			sharers.POST("/create", sharer.CreateSharer)
			sharers.POST("/update", sharer.UpdateSharer)
		}

		// 精彩活动
		exctActs := v.Group("/exctAct")
		{
			exctActs.GET("/search", exctAct.GetExctActBySearch)
			exctActs.GET("/id", exctAct.GetExctActById)
			exctActs.DELETE("/delete/:id", exctAct.DeleteExctActById)
			exctActs.POST("/create", exctAct.CreateExctAct)
			exctActs.POST("/update", exctAct.UpdateExctAct)
		}
	}
	//不存在路由
	router.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{
			"status": 404,
			"msg":    "接口不存在->('.')/请求方法错误",
		})
	})

	return router
}

/*登录失效验证*/
func CheckLogin() gin.HandlerFunc {
	return func(c *gin.Context) {
		//小程序端
		if c.Request.Header.Get("Authorization") == "wechat" {
			return
		}
		path := c.Request.URL.String()
		if !strings.Contains(path, "login") &&
			!strings.Contains(path, "/static/file") &&
			!strings.Contains(path, "/alipay/payres") &&
			!strings.Contains(path, "/wx/") {
			_, err := c.Cookie("uid")
			if err != nil {
				c.Abort()
				c.JSON(http.StatusOK, lib.MapNoToken)
			}
		}
	}
}

// 处理跨域请求,支持options访问
func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		//fmt.Println(method)
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization, Token")
		c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, PATCH, DELETE")
		c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
		c.Header("Access-Control-Allow-Credentials", "true")

		// 放行所有OPTIONS方法，因为有的模板是要请求两次的
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		// 处理请求
		c.Next()
	}
}
