package main

import (
	"github.com/gin-gonic/gin"
	"rzf/conf"
	"rzf/routers"
	_ "rzf/util/db"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	router := routers.SetRouter()
	// Listen and Server in 0.0.0.0:8080
	router.Run(":" + conf.GetConfigValue("http_port"))
}
