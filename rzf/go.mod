module rzf

go 1.12

require (
	github.com/KenmyZhang/aliyun-communicate v0.0.0-20180308134849-7997edc57454
	github.com/astaxie/beego v1.12.0
	github.com/casbin/casbin v1.9.1
	github.com/chanxuehong/wechat v0.0.0-20190521093015-fafb751f9916
	github.com/gin-contrib/sessions v0.0.1
	github.com/gin-gonic/gin v1.4.0
	github.com/go-ini/ini v1.46.0
	github.com/hanguofeng/config v1.0.0
	github.com/hanguofeng/freetype-go-mirror v0.0.0-20140928112427-cfb10e2cb6de // indirect
	github.com/hanguofeng/gocaptcha v1.0.7
	github.com/jinzhu/gorm v1.9.10
	github.com/kr/pretty v0.1.0 // indirect
	github.com/medivhzhan/weapp v1.5.1
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/skip2/go-qrcode v0.0.0-20190110000554-dc11ecdae0a9
	github.com/smartwalle/alipay v1.0.2
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	gopkg.in/bufio.v1 v1.0.0-20140618132640-567b2bfa514e // indirect
	gopkg.in/ini.v1 v1.46.0 // indirect
	gopkg.in/redis.v2 v2.3.2 // indirect
)
