package protocol

import (
	"rzf/util/db"
)

/*protocol model*/
type Protocol struct {
	ID       uint   `json:"id" gorm:"primary_key"`
	Protocol string `json:"protocol"`
}

// get protocol, by id
func (c *Protocol) GetById(id string) interface{} {

	db.DB.AutoMigrate(&Protocol{})
	var protocol = Protocol{}
	return db.GetDataById(&protocol, id)
}

// update protocol
func (c *Protocol) Update(args map[string][]string) interface{} {

	return db.UpdateData("protocol", args)
}
