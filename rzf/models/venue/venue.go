package venue

import (
	"fmt"
	"rzf/conf"
	"rzf/models/venueprice"
	"rzf/util/db"
	"rzf/util/lib"
	"strconv"
	"strings"
	"time"
)

/*场馆管理模型*/
type Venue struct {
	ID        int     `json:"id"`
	Name      string  `json:"name"`       //场馆名
	Venuer_id int     `json:"venuer_id"`  //馆主
	Category  string  `json:"category"`   //类型
	Address   string  `json:"address"`    //地址
	Map       string  `json:"map"`        //地图
	Introduce string  `json:"introduce"`  //介绍
	Star      int     `json:"star"`       //星级
	Carousels string  `json:"carousels"`  //封面轮播图,','分割
	Adarea    string  `json:"adarea"`     //行政区
	County    string  `json:"county"`     //下辖市县
	PersonNum int     `json:"person_num"` //人数限制
	Venuepro  string  `json:"venuepro"`   //场馆分销比例
	Platpro   string  `json:"platpro"`    //平台分销比例
	Imageurl  string  `json:"imageurl"`   //分页封面
	Label     string  `json:"label"`      //标签
	Shelf     int     `json:"shelf"`      //0上架,1下架
	Ranknum   float64 `json:"ranknum"`    //排序数字，精度两位
	Headline  int64   `json:"headline"`   //人头费线
	Headprice float64 `json:"headprice"`  //单人头费
}

/*场馆管理*/
type VenueInfo struct {
	ID          int     `json:"id"`
	Name        string  `json:"name"`        //场馆名
	Venuer_id   int     `json:"venuer_id"`   //馆主id
	Venuer_name string  `json:"venuer_name"` //馆主
	Category    string  `json:"category"`    //类型
	Address     string  `json:"address"`     //地址
	Map         string  `json:"map"`         //地图
	Introduce   string  `json:"introduce"`   //介绍
	Star        int     `json:"star"`        //星级
	Carousels   string  `json:"carousels"`   //封面轮播图,','分割
	Adarea      string  `json:"adarea"`      //行政区
	County      string  `json:"county"`      //下辖市县
	PersonNum   int     `json:"person_num"`  //人数限制
	Venuepro    string  `json:"venuepro"`    //场馆分销比例
	Platpro     string  `json:"platpro"`     //平台分销比例
	Imageurl    string  `json:"imageurl"`    //分页封面
	Label       string  `json:"label"`       //标签
	Lng         float64 `json:"lng"`         //经度
	Lat         float64 `json:"lat"`         //经度
	Shelf       int     `json:"shelf"`       //0上架,1下架
	Ranknum     float64 `json:"ranknum"`     //排序数字，精度两位
	Headline    int64   `json:"headline"`    //人头费线
	Headprice   float64 `json:"headprice"`   //单人头费
}

/*场馆,小程序*/
type VenueX struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`     //场馆名
	Category string `json:"category"` //类型
	Price    string `json:"price"`    //价格,从数据库查询周几,赋值
	Oldprice string `json:"oldprice"` //原价
	//IsContact string  `json:"is_contact"` // 联系客服, 默认0不联系,1联系客服
	Address   string  `json:"address"`    //地址
	Map       string  `json:"map"`        //地图
	Introduce string  `json:"introduce"`  //介绍
	Star      int     `json:"star"`       //星级
	Carousels string  `json:"carousels"`  //封面轮播图,','分割
	Distance  float64 `json:"distance"`   //离用户距离
	PersonNum int     `json:"person_num"` //人数限制
	Imageurl  string  `json:"imageurl"`   //分页封面
	Label     string  `json:"label"`      //标签
	Shelf     int     `json:"shelf"`      //0上架,1下架
	Headline  int64   `json:"headline"`   //人头费线
	Headprice float64 `json:"headprice"`  //单人头费
}

/*场馆下拉框*/
type Venues struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

/*场馆标签*/
type VenueLabels struct {
	ID    int64  `json:"id"`
	Label string `json:"label"`
}

/*场馆年月统计数据*/
type VenueMonthData struct {
	VenueMonth []VenueMonth `json:"venue_month"` //当月中每天的场次数据统计
	SellNum    int64        `json:"sell_num"`    //本月已出售场次
	VenuePrice float64      `json:"venue_price"` //本月实际收入
}

/*当月中每天的场次数据统计*/
type VenueMonth struct {
	//Year	string `json:"year"`	//年份
	//Month	string `json:"month"`	//
	Day   int64 `json:"day"`
	Time1 int64 `json:"time1"` //场次123
	Time2 int64 `json:"time2"`
	Time3 int64 `json:"time3"`
}

//获得场馆标签
func GetVenueLabels() interface{} {

	var venue []Venues
	sql := "select id,label from `venue` order by rand() limit 10"
	return db.GetDataBySql(&venue, sql)
}

//按照年月统计数据
func GetVenueMonthData(venue_id, year, month string) interface{} {
	var vemd VenueMonthData
	sql := `select sum(time1)+sum(time2)+sum(time3) sell_num,
			(select case when sum(aa.price) is null then 0 else sum(aa.price) end from distribute aa inner join ` + "`" + `order` + "`" + ` bb on aa.order_id=bb.id where introducer=0 and introducer_id=a.venuer_id and bb.venue_id=a.id
			and month(playtime)=` + month + ` and year(playtime)=` + year + `
			)
			+(select case when sum(price) is null then 0 else sum(price) end from ` + "`" + `order` + "`" + ` where locate('录入',origin)>0 and status = 0 and venue_id=a.id
			and month(playtime)=` + month + ` and year(playtime)=` + year + `
			) as venue_price
			from (
			select
			b.id,
			b.venuer_id,
			year(playtime) year,
			month(playtime) month,
			day(playtime) day,
			#时间段统计1，2，3
			if(count(if(locate('1',period)>0||locate('4',period)>0||locate('6',period)>0,true,null)) > 0, 1, 0) time1,
			if(count(if(locate('2',period)>0||locate('4',period)>0||locate('5',period)>0||locate('6',period)>0,true,null)) > 0, 1, 0) time2,
			if(count(if(locate('3',period)>0||locate('4',period)>0||locate('5',period)>0||locate('6',period)>0,true,null)) > 0, 1, 0) time3
			from ` + "`" + `order` + "`" + ` a
			inner join venue b
			on a.venue_id=b.id
			where a.status=0 and b.id=? and month(playtime)=` + month + ` and year(playtime)=` + year + `
			group by b.id,year,month,day) a`
	info := db.GetDataBySql(&vemd, sql, venue_id)

	sql = `select
		b.id,
		b.name,
		year(playtime) year,
		month(playtime) month,
		day(playtime) day,
		a.period,
		#时间段统计1，2，3
		if(count(if(locate('1',period)>0||locate('4',period)>0||locate('6',period)>0,true,null)) > 0, 1, 0) time1,
		if(count(if(locate('2',period)>0||locate('4',period)>0||locate('5',period)>0||locate('6',period)>0,true,null)) > 0, 1, 0) time2,
		if(count(if(locate('3',period)>0||locate('4',period)>0||locate('5',period)>0||locate('6',period)>0,true,null)) > 0, 1, 0) time3
		from ` + "`" + `order` + "`" + ` a
		inner join venue b
		on a.venue_id=b.id
		where a.status=0 and b.id=? and month(playtime)=` + month + ` and year(playtime)=` + year + `
		group by b.id,year,month,day`
	db.GetDataBySql(&vemd.VenueMonth, sql, venue_id)
	return info
}

//获得场馆下拉框
func GetVenues() interface{} {
	var venue []Venues
	sql := "select id,name from `venue`" // order by rand() limit 10"
	return db.GetDataBySql(&venue, sql)
}

//小程序搜索场馆、筛选
func GetVenueBySearchX(args map[string][]string) interface{} {
	var info interface{}
	var venue []VenueX
	//dest = dest.(reflect.TypeOf(dest).Elem())//type != type?
	var getinfo lib.GetInfo

	//表名
	table1 := "venue"
	table2 := "venuer"
	//经纬度
	lng := ""
	lat := ""
	//价格范围
	price1 := ""
	price2 := ""
	//日期筛选
	playtime := ""
	//返回所有
	flag := ""
	//距离范围
	distance := ""
	//场次
	period := ""

	//页码,每页数量
	clientPageStr := conf.GetConfigValue("clientPage") //默认第1页
	everyPageStr := conf.GetConfigValue("everyPage")   //默认10页

	//尝试将select* 变为对应的字段名
	sql := fmt.Sprintf("select distinct %s from "+table1+" "+
		"inner join "+table2+" "+
		"on "+table1+"."+table2+"_id="+table2+".id where shelf=0 and ", db.GetDoubleTableColumnsql(VenueInfo{}, table1, table2))

	sqlnolimit := "select count(distinct " + table1 + ".id) as sum_page from " + table1 + " " +
		"inner join " + table2 + " " +
		"on " + table1 + "." + table2 + "_id=" + table2 + ".id where shelf=0 and "

	for k, v := range args {
		switch k {
		case "clientPage":
			clientPageStr = v[0]
			continue
		case "everyPage":
			everyPageStr = v[0]
			continue
		case "lng":
			lng = v[0]
			continue
		case "lat":
			lat = v[0]
			continue
		case "price1":
			price1 = v[0]
			continue
		case "price2":
			price2 = v[0]
			continue
		case "person_num":
			sql += table1 + "." + k + " >=" + v[0] + " and "
			sqlnolimit += table1 + "." + k + " >=" + v[0] + " and "
			continue
		case "playtime":
			playtime = v[0]
			continue
		case "flag":
			flag = v[0]
			continue
		case "distance":
			distance = v[0]
			continue
		case "period":
			period = v[0]
			continue
		}

		if v[0] == "" { //条件值为空,舍弃
			continue
		}

		v[0] = strings.Replace(v[0], "'", "\\'", -1) //转义
		sql += table1 + "." + k + " like '%" + v[0] + "%' and "
		sqlnolimit += table1 + "." + k + " like '%" + v[0] + "%' and "
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	sql = string([]byte(sql)[:len(sql)-4])                      //去and
	sqlnolimit = string([]byte(sqlnolimit)[:len(sqlnolimit)-4]) //去and
	//sqlnolimit = strings.Replace(sql, GetSqlColumnsSql(model), "count("+table1+".id) as sum_page", -1)
	if flag == "all" {
		sql += "order by star desc,ranknum desc"
	} else {
		sql += "order by star desc,ranknum desc limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr
	}

	pt, _ := time.Parse("2006-01-02", playtime)
	week := pt.Weekday()

	//价格排序,筛选分割后的最小价格进行价格筛选即可(算法)
	if price1 != "" && price2 != "" {
		//sql = strings.Replace(sql,"from",",ROUND(6378.138*2*ASIN(SQRT(POW(SIN(("+lat+"*PI()/180-lat*PI()/180)/2),2)+COS("+lat+"*PI()/180)*COS(lat*PI()/180)*POW(SIN(("+lng+"*PI()/180-lng*PI()/180)/2),2)))) AS distance from",-1)
		sqlnolimit += " and (SELECT " + `min(if(substring_index(substring_index(t.price, ',', b.help_topic_id + 1), ',', -1) != '' 
	and substring_index(substring_index(t.price, ',', b.help_topic_id + 1), ',', -1) != '*', 
	substring_index(substring_index(t.price, ',', b.help_topic_id + 1), ',', -1),
	NULL))` + " FROM venueprice t join mysql.help_topic b ON b.help_topic_id <  (LENGTH(t.price) - LENGTH(REPLACE(t.price, ',', '')) + 1) where t.venue_id=" + table1 + ".id) " +
			"between " + price1 + " and " + price2
		sql = strings.Replace(sql, "order", "and (SELECT "+`min(if(substring_index(substring_index(t.price, ',', b.help_topic_id + 1), ',', -1) != '' 
	and substring_index(substring_index(t.price, ',', b.help_topic_id + 1), ',', -1) != '*', 
	substring_index(substring_index(t.price, ',', b.help_topic_id + 1), ',', -1),
	NULL))`+" FROM venueprice t join mysql.help_topic b ON b.help_topic_id <  (LENGTH(t.price) - LENGTH(REPLACE(t.price, ',', '')) + 1) where t.venue_id="+table1+".id) "+
			"between "+price1+" and "+price2+" order", -1)
	}
	//日期筛选,筛选掉订单中日期满的
	if playtime != "" {
		//sql = strings.Replace(sql,"from",",ROUND(6378.138*2*ASIN(SQRT(POW(SIN(("+lat+"*PI()/180-lat*PI()/180)/2),2)+COS("+lat+"*PI()/180)*COS(lat*PI()/180)*POW(SIN(("+lng+"*PI()/180-lng*PI()/180)/2),2)))) AS distance from",-1)
		sqlnolimit += " and (SELECT count(substring_index(substring_index(t.period,',', b.help_topic_id + 1), ',', -1)) FROM `order` t join mysql.help_topic b ON b.help_topic_id <  (LENGTH(t.period) - LENGTH(REPLACE(t.period, ',', '')) + 1) where t.venue_id=" + table1 + ".id) " +
			"< 42 "
		sql = strings.Replace(sql, "order", " and (SELECT count(substring_index(substring_index(t.period,',', b.help_topic_id + 1), ',', -1)) FROM `order` t join mysql.help_topic b ON b.help_topic_id <  (LENGTH(t.period) - LENGTH(REPLACE(t.period, ',', '')) + 1) where t.venue_id="+table1+".id and DATE_FORMAT(t.playtime, '%Y%m%d') = DATE_FORMAT('"+playtime+"', '%Y%m%d')) "+
			"< 42 order", -1)
	}
	//时间段筛选,反向筛选
	if period != "" && playtime != "" {
		sql = strings.Replace(sql, "from venue", "from venue "+`inner join venueprice on
	venue.id = venueprice.venue_id
	and ((
	SELECT
		substring_index(substring_index(t.price, ',', b.help_topic_id + 1), ',', -1)
	FROM
		venueprice t
	join mysql.help_topic b ON
		b.help_topic_id < (LENGTH(t.price) - LENGTH(REPLACE(t.price,
		',',
		'')) + 1)
	where
		venue.id = t.venue_id and t.week = `+strconv.Itoa(int(week))+` limit `+period+`,1) not in ('*','')) `, -1)

		sqlnolimit = strings.Replace(sqlnolimit, "from venue", "from venue "+`inner join venueprice on
	venue.id = venueprice.venue_id
	and ((
	SELECT
		substring_index(substring_index(t.price, ',', b.help_topic_id + 1), ',', -1)
	FROM
		venueprice t
	join mysql.help_topic b ON
		b.help_topic_id < (LENGTH(t.price) - LENGTH(REPLACE(t.price,
		',',
		'')) + 1)
	where
		venue.id = t.venue_id and t.week = `+strconv.Itoa(int(week))+` limit `+period+`,1) not in ('*','')) `, -1)
	}

	//距离筛选放最后，因sqlnolimit可能会整体替换
	//地图距离排序,单位(km)
	if lng != "" && lat != "" {
		sql = strings.Replace(sql, "from", ",ROUND(6378.138*2*ASIN(SQRT(POW(SIN(("+lat+"*PI()/180-lat*PI()/180)/2),2)+COS("+lat+"*PI()/180)*COS(lat*PI()/180)*POW(SIN(("+lng+"*PI()/180-lng*PI()/180)/2),2)))) AS distance from", -1)
		sql = strings.Replace(sql, "order by star desc", "having distance <= "+distance+" order by distance ", -1)
		sqlnolimit = "select count(id) as sum_page from (" + sql + ") a"
	}

	dba := db.DB.Raw(sqlnolimit).Scan(&getinfo.Pager)
	num := getinfo.Pager.SumPage
	//有数据是返回相应信息
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapNoResult
	} else {
		//DB.Debug().Find(&dest)
		dba = db.DB.Raw(sql).Scan(&venue)
		if dba.Error != nil {
			info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
			return info
		}

		//获取价格
		if playtime != "" {

			for k, v := range venue {
				var vp venueprice.Venueprice
				db.DB.Where("week = ?", week).Where("venue_id = ?", v.ID).Find(&vp)
				venue[k].Price = vp.Price
				venue[k].Oldprice = vp.Oldprice
				//venue[k].IsContact = vp.IsContact

				////判断时间段是否占用
				//var ors []OrderW
				//db.DB.Where("playtime = ? and venue_id = ? and status = 0", args["playtime"][0], args["venue_id"][0]).Find(&ors)
				//for _, v := range ors {
				//	pds := strings.Split(v.Period, ",")
				//	for _, v2 := range pds {
				//		//时间段已下订单,删除对应时间段价格
				//		ps := strings.Split(vp.Price, ",")
				//		for k,v3 := range ps{
				//			if strconv.Itoa(k) == v2 {
				//				v3 = ""
				//				continue
				//			}
				//			venue[k].Price += v3 + ","
				//		}
				//		venue[k].Price = string([]byte(venue[k].Price)[:len(venue[k].Price)-1])
				//	}
				//}
			}
		}

		//统计页码等状态
		getinfo.Status = lib.CodeSuccess
		getinfo.Msg = lib.MsgSuccess
		getinfo.Data = venue //数据
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	}
	return info
}

//获得场馆,根据id
func GetVenueById(id string) interface{} {

	db.DB.AutoMigrate(&VenueInfo{})
	var venue = VenueInfo{}
	//db.DB.Raw("select name as venuer_name from `venuer` where id=?",venue.Venuer_id).Scan(&venue)
	return db.GetDoubleTableDataById(VenueInfo{}, &venue, id, "venue", "venuer")
}

//获得场馆,分页/查询
func GetVenueBySearch(args map[string][]string) interface{} {
	//页码,每页数量
	clientPageStr := conf.GetConfigValue("clientPage") //默认第1页
	everyPageStr := conf.GetConfigValue("everyPage")   //默认10页

	table1 := "venue"
	table2 := "venuer"
	//尝试将select* 变为对应的字段名
	sql := fmt.Sprintf("select %s from "+table1+" "+
		"inner join "+table2+" "+
		"on "+table1+"."+table2+"_id="+table2+".id where 1=1 and ", db.GetDoubleTableColumnsql(VenueInfo{}, table1, table2))

	sqlnolimit := "select count(" + table1 + ".id) as sum_page from " + table1 + " " +
		"inner join " + table2 + " " +
		"on " + table1 + "." + table2 + "_id=" + table2 + ".id where 1=1 and "
	for k, v := range args {
		if k == "clientPage" {
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" {
			everyPageStr = v[0]
			continue
		}
		if v[0] == "" { //条件值为空,舍弃
			continue
		}
		if k == "label" {
			v[0] = strings.Replace(v[0], "'", "\\'", -1) //转义
			sql += table1 + "." + k + " like '%" + v[0] + "%' and "
			sqlnolimit += table1 + "." + k + " like '%" + v[0] + "%' and "
			continue
		}

		//表2值查询
		if strings.Contains(k, table2+"_") && !strings.Contains(k, table2+"_id") {
			sql += table2 + ".`" + string([]byte(k)[len(table2)+1:]) + "`" + " = '" + v[0] + "' and " //string([]byte(tag)[len(table2+1-1):])
			sqlnolimit += table2 + ".`" + string([]byte(k)[len(table2)+1:]) + "`" + " = '" + v[0] + "' and "
			continue
		}

		v[0] = strings.Replace(v[0], "'", "\\'", -1) //转义
		sql += table1 + "." + k + " = '" + v[0] + "' and "
		sqlnolimit += table1 + "." + k + " = '" + v[0] + "' and "
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	sql = string([]byte(sql)[:len(sql)-4])                      //去and
	sqlnolimit = string([]byte(sqlnolimit)[:len(sqlnolimit)-4]) //去and
	//sqlnolimit = strings.Replace(sql, GetSqlColumnsSql(model), "count("+table1+".id) as sum_page", -1)
	sql += "order by " + table1 + ".id desc limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr

	var vinfo []VenueInfo
	return db.GetDataBySqlSearch(&vinfo, sql, sqlnolimit, clientPage, everyPage) //匿名Venue{}
}

//删除场馆,根据id
func DeleteVenueByid(id string) interface{} {

	return db.DeleteDataByName("venue", "id", id)
}

//修改场馆
func UpdateVenue(args map[string][]string) interface{} {

	return db.UpdateData("venue", args)
}

//创建场馆
func CreateVenue(args map[string][]string) interface{} {

	// 默认添加场馆标签
	if _, ok := args["label"]; ok {
		args["label"][0] += "," + args["name"][0]
	}
	return db.CreateData("venue", args)
}
