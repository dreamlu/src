package groupmeal

import "rzf/util/db"

/*团餐管理模型*/
type Groupmeal struct {
	ID           int     `json:"id"`
	Name         string  `json:"name"`         //团餐名
	Person_num   int     `json:"person_num"`   //适合人数
	Price        float64 `json:"price"`        //价格
	Carousel     string  `json:"carousel"`     //轮播
	Introduce    string  `json:"introduce"`    //介绍
	Groupmealpro string  `json:"groupmealpro"` //团餐分销比例
	Imageurl     string  `json:"imageurl"`     //分页封面
}

//获得团餐,根据id
func GetGroupmealById(id string) interface{} {

	db.DB.AutoMigrate(&Groupmeal{})
	var groupmeal = Groupmeal{}
	return db.GetDataById(&groupmeal, id)
}

//获得团餐,分页/查询
func GetGroupmealBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Groupmeal{})
	var groupmeals = []*Groupmeal{}
	return db.GetDataBySearch(Groupmeal{}, &groupmeals, "groupmeal", args) //匿名Groupmeal{}
}

//删除团餐,根据id
func DeleteGroupmealByid(id string) interface{} {

	return db.DeleteDataByName("groupmeal", "id", id)
}

//修改团餐
func UpdateGroupmeal(args map[string][]string) interface{} {

	return db.UpdateData("groupmeal", args)
}

//创建团餐
func CreateGroupmeal(args map[string][]string) interface{} {

	return db.CreateData("groupmeal", args)
}
