package client

import (
	"rzf/conf"
	"rzf/util/db"
	"strconv"
	"strings"
	"time"
)

/*客户,用户管理*/
type Client struct {
	ID            int64  `json:"id"`
	Nickname      string `json:"nickname"`      //昵称
	Username      string `json:"username"`      //用户名
	Openid        string `json:"openid"`        //微信应用唯一标示
	Headimg       string `json:"headimg"`       //头像
	Phone         string `json:"phone"`         //手机号
	Idcard        string `json:"idcard"`        //身份证
	Introducer    int    `json:"introducer"`    //介绍人种类,0/1标示馆主/代理
	Introducer_id int    `json:"introducer_id"` //介绍人id
}

type ClientX struct {
	Username string `json:"username"` //用户名
	Phone    string `json:"phone"`    //手机号
	Idcard   string `json:"idcard"`   //身份证
}

//客户id
type ClientID struct {
	ID int64 `json:"id"`
}

/*客户管理数据*/
type ClientData struct {
	ID           int64  `json:"id"`
	Username     string `json:"username"`       //用户名
	Phone        string `json:"phone"`          //联系方式
	Idcard       string `json:"idcard"`         //身份证
	OrderNum     int64  `json:"order_num"`      //参数订单数
	JoinOrderNum int64  `json:"join_order_num"` //参与订单数
	Origin       string `json:"origin"`         //客户来源
}

//客户管理数据
func GetClientData(args map[string][]string) interface{} {
	sql := `select
		distinct a.id,a.username,a.phone,a.idcard,
		(select count(*) from ` + "`" + `order` + "`" + ` where client_id=a.id and status=0) order_num,
		(select count(distinct order_id) from ` + "`" + `ordergp` + "`" + ` where client_id=a.id) join_order_num,
		case a.origin
		when 0 then '代理'
		when 1 then '馆主'
		when 2 then '平台'
		when 3 then '组队'
		end as origin
		from client a
		left join ` + "`" + `order` + "`" + ` b
		on a.id=b.client_id
		where a.username != '' and `
	sqlnolimit := `select
		count(distinct a.id) as sum_page	
		from client a
		left join ` + "`" + `order` + "`" + ` b
		on a.id=b.client_id
		where a.username != '' and `
	var cd []ClientData

	//页码,每页数量
	clientPageStr := conf.GetConfigValue("clientPage") //默认第1页
	everyPageStr := conf.GetConfigValue("everyPage")   //默认10页

	//尝试将select* 变为对应的字段名
	for k, v := range args {
		if k == "clientPage" {
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" {
			everyPageStr = v[0]
			continue
		}
		if v[0] == "" { //条件值为空,舍弃
			continue
		}
		v[0] = strings.Replace(v[0], "'", "\\'", -1) //转义
		sql += "a." + k + " = '" + v[0] + "' and "
		sqlnolimit += "a." + k + " = '" + v[0] + "' and "
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	c := []byte(sql)
	sql = string(c[:len(c)-4]) //去and
	sqlnolimit = string([]byte(sqlnolimit)[:len(sqlnolimit)-4])
	sql += "order by id desc limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr

	return db.GetDataBySqlSearch(&cd, sql, sqlnolimit, clientPage, everyPage)
}

//获得客户,根据unionid 获得id
func GetClientId(openid string) interface{} {

	db.DB.AutoMigrate(&ClientID{})
	var client = ClientID{}
	sql := "select a.id from `client` where openid=?"
	return db.GetDataBySql(&client, sql, openid)
}

//获得客户,用户,根据id
func GetClientByIdX(id string) interface{} {

	var client ClientX
	sql := "select username,phone,idcard from `client` where id=?"
	return db.GetDataBySql(&client, sql, id)
}

//获得客户,用户,分页/查询
func GetClientBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Client{})
	var clients = []*Client{}
	return db.GetDataBySearch(Client{}, &clients, "client", args) //匿名Client{}
}

//删除客户,用户,根据id
func DeleteClientByid(id string) interface{} {

	return db.DeleteDataByName("client", "id", id)
}

//修改客户,用户
func UpdateClient(args map[string][]string) interface{} {

	if _, ok := args["introducer"]; ok { //替换上级代理(或馆主),更新时间
		args["introduce_time"] = append(args["introduce_time"], time.Now().Format("2006-01-02 15:04:05"))
	}

	return db.UpdateData("client", args)
}
