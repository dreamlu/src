package proxyer

import (
	"fmt"
	"github.com/KenmyZhang/aliyun-communicate"
	"rzf/util"
	"rzf/util/db"
	"rzf/util/lib"
	"strconv"
)

/*代理管理*/
type Proxyer struct {
	ID         int     `json:"id"`
	Name       string  `json:"name"`       //代理名
	Phone      string  `json:"phone"`      //手机号
	Nickname   string  `json:"nickname"`   //账号昵称
	Password   string  `json:"password"`   //密码
	Proprice   float64 `json:"proprice"`   //分销总佣金
	Withdrawed float64 `json:"withdrawed"` //已提现
	Ispass     int64   `json:"ispass"`     //审核状态,0待审核
}

/*代理管理*/
type ProxyerX struct {
	ID         int     `json:"id"`
	Name       string  `json:"name"`       //代理名
	Phone      string  `json:"phone"`      //手机号
	Nickname   string  `json:"nickname"`   //账号昵称
	Password   string  `json:"password"`   //密码
	Proprice   float64 `json:"proprice"`   //分销总佣金
	Withdrawed float64 `json:"withdrawed"` //已提现
	ClientNum  int     `json:"client_num"` //客户数量
	Ispass     int64   `json:"ispass"`     //审核状态,0待审核
}

//获得馆主以及客户数量,根据id
func GetProxyerByIdX(id string) interface{} {

	var pr ProxyerX
	sql := "select " + db.GetColumnsSql(Proxyer{}, "a") + ", count(distinct b.id) as client_num from `proxyer` a inner join `client` b on a.id=b.introducer_id where introducer=1 and introducer_id=" + id
	return db.GetDataBySql(&pr, sql)
}

//获得代理,根据id
func GetProxyerById(id string) interface{} {

	db.DB.AutoMigrate(&Proxyer{})
	var proxyer = Proxyer{}
	return db.GetDataById(&proxyer, id)
}

//获得代理,分页/查询
func GetProxyerBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Proxyer{})
	var proxyers = []*Proxyer{}
	return db.GetDataBySearch(Proxyer{}, &proxyers, "proxyer", args) //匿名Proxyer{}
}

//删除代理,根据id
func DeleteProxyerByid(id string) interface{} {

	return db.DeleteDataByName("proxyer", "id", id)
}

//修改代理
func UpdateProxyer(args map[string][]string) interface{} {

	if _, ok := args["ispass"]; ok {
		var pr Proxyer
		db.DB.First(&pr, args["id"][0])
		if pr.Ispass == 1{//审核通过,短信发送
			templateCode := "SMS_152287001"
			phoneNumbers := pr.Phone
			templateParam := "{\"name\":\"代理\",\"nickname\":\""+pr.Nickname+"\"}"
			SendMsg(phoneNumbers, templateCode, templateParam)
		}
	}

	//提现金额,存在
	if _, ok := args["withdrawed"]; ok {
		//计算提现金额
		var pr Proxyer
		db.DB.First(&pr, args["id"][0])
		//判断余额金额提现是否足够
		wd, _ := strconv.ParseFloat(args["withdrawed"][0], 64) //提现金额
		if (pr.Proprice - pr.Withdrawed) < wd {
			return lib.GetMapDataError(lib.CodeWxWithDraw, "余额不足")
		}

		args["withdrawed"][0] = strconv.FormatFloat(wd+pr.Withdrawed, 'f', -1, 64)
	}

	//密码
	if _, ok := args["password"]; ok {
		password := util.AesEn(args["password"][0])
		args["password"][0] = string(password)
	}

	return db.UpdateData("proxyer", args)
}

//创建代理
func CreateProxyer(args map[string][]string) interface{} {
	//密码
	password := util.AesEn(args["password"][0])

	//delete(args,args["password"][0])
	args["password"][0] = password //append(args["password"],string(password))

	return db.CreateData("proxyer", args)
}


var (
	gatewayUrl      = "http://dysmsapi.aliyuncs.com/"
	accessKeyId     = "LTAIobL132oxzG7B"
	accessKeySecret = "mQU4R454RLFPEDb52dImkfwCQzuJIe"
	//phoneNumbers    = "15869190407"
	signName     = "墅懒日租"
	//templateCode = "SMS_151880459"
	//templateParam   = "{\"code\":\"2018\"}"
)

//发送短信
func SendMsg(phoneNumbers,templateCode,templateParam string) interface{}{
	var info interface{}
	smsClient := aliyunsmsclient.New(gatewayUrl)
	result, err := smsClient.Execute(accessKeyId, accessKeySecret, phoneNumbers, signName, templateCode, templateParam)
	fmt.Println("Got raw response from server:", string(result.RawResponse))
	if err != nil {
		panic("Failed to send Message: " + err.Error())
	}

	//resultJson, err := json.Marshal(result)
	if err != nil {
		panic(err)
	}
	if result.IsSuccessful() {
		//fmt.Println("A SMS is sent successfully:", resultJson)
		info = map[string]interface{}{"status": 271, "msg": "验证码发送成功"}

	} else {
		//fmt.Println("Failed to send a SMS:", resultJson)
		info = map[string]interface{}{"status": 271, "msg": "验证码发送失败"}
	}
	return info
}