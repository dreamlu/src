package label

import "rzf/util/db"

/*标签管理模型*/
type Label struct {
	ID     int    `json:"id"`
	Label  string `json:"label"`   //标签名
	IsShow int64  `json:"is_show"` //0不显示,1显示
}

/*标签小程序*/
type LabelX struct {
	ID     int    `json:"id"`
	Label  string `json:"label"`   //标签名
}

//获得场馆下拉框
func GetLabels() interface{} {

	var label []LabelX
	sql := "select id,label from `label` where is_show=1" //显示
	return db.GetDataBySql(&label, sql)
}

//获得标签,根据id
func GetLabelById(id string) interface{} {

	db.DB.AutoMigrate(&Label{})
	var label = Label{}
	return db.GetDataById(&label, id)
}

//获得标签,分页/查询
func GetLabelBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Label{})
	var labels = []*Label{}
	return db.GetDataBySearch(Label{}, &labels, "label", args) //匿名Label{}
}

//删除标签,根据id
func DeleteLabelByid(id string) interface{} {

	return db.DeleteDataByName("label", "id", id)
}

//修改标签
func UpdateLabel(args map[string][]string) interface{} {

	return db.UpdateData("label", args)
}

//创建标签
func CreateLabel(args map[string][]string) interface{} {

	return db.CreateData("label", args)
}
