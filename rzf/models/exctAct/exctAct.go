package exctAct

import (
	"rzf/util"
	"rzf/util/db"
	"time"
)

// 精彩活动
// 图文介绍
type ExctAct struct {
	ID         uint          `json:"id"`
	Img        string        `json:"img"`        // 封面
	Title      string        `json:"title"`      // 标题
	Introduce  string        `json:"introduce"`  // 介绍
	Content    string        `json:"content"`    // 内容
	Createtime util.JsonDate `json:"createtime"` // 创建时间
}

//获得设备,根据id
func GetExctActById(id string) interface{} {

	var exctAct = ExctAct{}
	return db.GetDataBySql(&exctAct, "select * from exctAct where id = ?", id)
}

//获得设备,分页/查询
func GetExctActBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&ExctAct{})
	var exctActs = []*ExctAct{}
	return db.GetDataBySearch(ExctAct{}, &exctActs, "exctAct", args) //匿名ExctAct{}
}

//删除设备,根据id
func DeleteExctActByid(id string) interface{} {

	return db.DeleteDataByName("exctAct", "id", id)
}

//修改设备
func UpdateExctAct(args map[string][]string) interface{} {

	return db.UpdateData("exctAct", args)
}

//创建设备
func CreateExctAct(args map[string][]string) interface{} {

	createtime := time.Now().Format("2006-01-02 15:04:05")
	args["createtime"] = append(args["createtime"], createtime)
	return db.CreateData("exctAct", args)
}
