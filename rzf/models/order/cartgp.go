package order

import (
	"rzf/util/db"
	"rzf/util/lib"
	"rzf/util/str"
	"strconv"
	"strings"
)

/*购物车组队*/
type Cartgp struct {
	ID       int64 `json:"id"`
	CartID   int64 `json:"cart_id"`
	ClientID int64 `json:"client_id"`
}

/*购物车详细信息*/
type CartgpInfo struct {
	ClientID int64  `json:"client_id"`
	Nickname string `json:"nickname"`
	Headimg  string `json:"headimg"`
}

type CartGpData struct {
	Cartgp     []CartgpInfo `json:"cartgp"`
	GroupedNum int64        `json:"grouped_num"` //组队多少人
	Discount   float64        `json:"discount"`    //优惠了xx
}

// 创建组队信息
func (c *Cartgp) CreateCartgp(client_ids, cart_id, venue_id, discounts string) interface{} {
	// 检测人数是否超过人数限制
	var value str.Value
	db.DB.Raw("select person_num as value from `venue` where id=?", venue_id).Scan(&value)
	if value.Value != "" {
		maxNum,_ := strconv.Atoi(value.Value)
		db.DB.Raw("select count(*) as value from `cartgp` where cart_id=?", cart_id).Scan(&value)
		nowNum,_:= strconv.Atoi(value.Value)
		if nowNum >= maxNum {
			return lib.GetMapDataError(lib.CodeText, "超过人数限制")
		}
	}

	sql := "select sum(discount) as value from `cartgp` where cart_id=?"
	db.DB.Raw(sql, cart_id).Scan(&value)
	if value.Value != "" {
		discount,_ := strconv.Atoi(value.Value)
		if discount >= 100 {
			return lib.GetMapDataError(lib.CodeText, "优惠超过100元限制")
		}
	}

	cli_ids := strings.Split(client_ids, ",")
	dis_ts := strings.Split(discounts, ",")
	sql = "insert `cartgp`(cart_id, client_id, discount) value"
	for k, v := range cli_ids {
		sql += "('" + cart_id + "'," + v + "," + dis_ts[k] + "),"
	}
	sql = string([]byte(sql)[:len(sql)-1])

	var info interface{}
	dba := db.DB.Exec(sql)

	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else {
		info = lib.MapCreate
	}
	return info
}

//获得对应组队购物车信息
func (c *Cartgp) GetCartgp(cart_ids string) interface{} {

	var getinfo lib.GetInfoN
	var cgd []CartGpData
	cids := strings.Split(cart_ids, ",")

	sql := "select count(distinct cart_id) as grouped_num from `cartgp` where cart_id in ("
	for _,v :=range cids {
		sql += "'"+v+"',"
	}
	sql = string([]byte(sql)[:len(sql)-1])+")"
	//info := db.GetDataBySql(&cgd, sql)

	sql3 := "select a.client_id,b.nickname,b.headimg from `cartgp` a inner join client b on a.client_id=b.id where a.cart_id=?"

	sql2 := "select count(*) as grouped_num,sum(discount) as discount from `cartgp` where cart_id=?"

	var cgd2 CartGpData
	for _,v := range cids {

		db.GetDataBySql(&cgd2, sql2, v)
		db.GetDataBySql(&cgd2.Cartgp, sql3, v)
		//cgd2.Discount = cgd2.GroupedNum*5
		cgd = append(cgd, cgd2)
	}
	//统计页码等状态
	getinfo.Status = lib.CodeSuccess
	getinfo.Msg = lib.MsgSuccess
	getinfo.Data = cgd //数据

	return getinfo
}
