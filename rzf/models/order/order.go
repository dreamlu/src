package order

import (
	"github.com/jinzhu/gorm"
	"rzf/conf"
	"rzf/models/client"
	"rzf/models/device"
	"rzf/models/distribute"
	"rzf/models/groupmeal"
	"rzf/models/hirecar"
	"rzf/models/proxyer"
	"rzf/models/venue"
	"rzf/models/venueprice"
	"rzf/models/venuer"
	"rzf/util"
	"rzf/util/db"
	"rzf/util/lib"
	"strconv"
	"strings"
	"time"
)

/*订单管理模型*/
type Order struct {
	ID           int           `json:"id"`
	Username     string        `json:"username"`     //客户姓名
	Phone        string        `json:"phone"`        //客户电话
	Idcard       string        `json:"idcard"`       //身份证
	ClientId     int64         `json:"client_id"`    //客户id(表示所属)
	VenueId      int64         `json:"venue_id"`     //场馆id
	Period       string        `json:"period"`       //时间段
	Service      string        `json:"service"`      //服务管理
	Price        float64       `json:"price"`        //价格
	Origin       string        `json:"origin"`       //来源
	Status       int64         `json:"status"`       //状态
	Playtime     util.JsonDate `json:"playtime"`     //游玩日期
	Remark       string        `json:"remark"`       //备注
	Clientremark string        `json:"clientremark"` //客户备注
	Pnum         int64         `json:"pnum"`         //人数
	Createtime   util.JsonTime `json:"createtime"`   //创建时间
	Disprice     float64       `json:"disprice"`     //优惠后总价格

	VenuePrice   float64 `json:"venue_price"`   //场馆价格
	HeadPrice    float64 `json:"head_price"`    //人头费
	ServicePrice float64 `json:"service_price"` //服务总价格
	Proprice     float64 `json:"proprice"`      //应分销总价格
}

/*订单详细内容展示*/
type OrderInfo struct {
	ID           int64         `json:"id"`
	Username     string        `json:"username"`      //客户姓名
	Phone        string        `json:"phone"`         //客户电话
	Idcard       string        `json:"idcard"`        //身份证
	VenueId      int64         `json:"venue_id"`      //场馆id
	VenueName    string        `json:"venue_name"`    //场馆名
	VenuerName   string        `json:"venuer_name"`   //馆主名
	Category     string        `json:"category"`      //类型
	Period       string        `json:"period"`        //时间段
	Service      string        `json:"service"`       //服务管理
	Price        float64       `json:"price"`         //价格
	Origin       string        `json:"origin"`        //来源
	Status       int64         `json:"status"`        //状态
	Playtime     util.JsonDate `json:"playtime"`      //游玩日期
	Remark       string        `json:"remark"`        //备注
	Clientremark string        `json:"clientremark"`  //客户备注
	Pnum         int64         `json:"pnum"`          //人数
	Imageurl     string        `json:"imageurl"`      //分页封面
	Createtime   util.JsonTime `json:"createtime"`    //创建时间
	Proxyname    string        `json:"proxyname"`     //上级代理或馆主名
	IsGp         int64         `json:"is_gp"`         //是否参团组队
	Disprice     float64       `json:"disprice"`      //优惠后总价格
	VenuePrice   float64       `json:"venue_price"`   //场馆价格
	HeadPrice    float64       `json:"head_price"`    //人头费
	ServicePrice float64       `json:"service_price"` //服务总价格
	Proprice     float64       `json:"proprice"`      //应分销总价格
}

//获得订单,根据id
func GetOrderById(id string) interface{} {

	//db.DB.AutoMigrate(&Order{})
	var order OrderInfo
	sql := `select
		a.id,a.username,a.phone,a.idcard,c.id as venue_id,c.name as venue_name,d.name as venuer_name,category,period,service,a.price,a.origin,status,playtime,remark,imageurl,clientremark,createtime,e.username as proxyname,pnum,disprice,
		venue_price,head_price,service_price,a.proprice
		from ` + "`" + `order` + "`" + ` a
		
		inner join venue c
		on a.venue_id=c.id
		inner join venuer d
		on c.venuer_id=d.id
		left join client e
		on a.client_id=e.id
		where a.id=?`
	return db.GetDataBySql(&order, sql, id)
}

//获得订单,分页/查询
func GetOrderBySearch(args map[string][]string) interface{} {

	var order []OrderInfo
	sql := `select
		a.id,a.username,a.phone,a.idcard,c.id as venue_id,c.name as venue_name,d.name as venuer_name,category,period,service,a.price,a.origin,status,playtime,remark,imageurl,clientremark,createtime,e.username as proxyname,pnum,disprice,
		venue_price,head_price,service_price,a.proprice,
		(select count(*)  from ordergp where order_id=a.id) as is_gp
		from ` + "`" + `order` + "`" + ` a
		
		inner join venue c
		on a.venue_id=c.id
		inner join venuer d
		on c.venuer_id=d.id
		left join client e
		on a.client_id=e.id
		where 1=1 and `
	sqlnolimit := `select
		count(a.id) as sum_page
		from ` + "`" + `order` + "`" + ` a
		
		inner join venue c
		on a.venue_id=c.id
		inner join venuer d
		on c.venuer_id=d.id
		where 1=1 and `
	//页码,每页数量
	clientPageStr := conf.GetConfigValue("clientPage") //默认第1页
	everyPageStr := conf.GetConfigValue("everyPage")   //默认10页
	for k, v := range args {
		if k == "clientPage" {
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" {
			everyPageStr = v[0]
			continue
		}
		if k == "service" && v[0] == "1" {
			sql += "a." + k + " != '' and "
			sqlnolimit += "a." + k + " != '' and "
			continue
		}
		if v[0] == "" { //条件值为空,舍弃
			continue
		}
		switch k {
		case "username", "phone", "service", "origin":
			v[0] = strings.Replace(v[0], "'", "\\'", -1) //转义
			sql += "a." + k + " like '%" + v[0] + "%' and "
			sqlnolimit += "a." + k + " like '%" + v[0] + "%' and "
			continue
		}

		v[0] = strings.Replace(v[0], "'", "\\'", -1) //转义
		sql += "a." + k + " = '" + v[0] + "' and "
		sqlnolimit += "a." + k + " = '" + v[0] + "' and "
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	c := []byte(sql)
	sql = string(c[:len(c)-4]) //去and
	sqlnolimit = string([]byte(sqlnolimit)[:len(sqlnolimit)-4])
	sql += "order by a.id desc limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr

	return db.GetDataBySqlSearch(&order, sql, sqlnolimit, clientPage, everyPage)
}

//获得订单,分页/查询
func GetOrderBySearchX(args map[string][]string) interface{} {

	var order []OrderInfo
	sql := `select
		a.id,a.username,a.phone,a.idcard,c.id as venue_id,c.name as venue_name,d.name as venuer_name,category,period,service,a.price,a.origin,status,playtime,remark,imageurl,clientremark,createtime,e.username as proxyname,pnum,disprice,
		venue_price,head_price,service_price,a.proprice,
		(select count(*)  from ordergp where order_id=a.id) as is_gp
		from ` + "`" + `order` + "`" + ` a
		
		inner join venue c
		on a.venue_id=c.id
		inner join venuer d
		on c.venuer_id=d.id
		left join client e
		on a.client_id=e.id
		where a.status in (0,1,3) and `
	sqlnolimit := `select
		count(a.id) as sum_page
		from ` + "`" + `order` + "`" + ` a
		
		inner join venue c
		on a.venue_id=c.id
		inner join venuer d
		on c.venuer_id=d.id
		where a.status in (0,1,3) and `
	//页码,每页数量
	clientPageStr := conf.GetConfigValue("clientPage") //默认第1页
	everyPageStr := conf.GetConfigValue("everyPage")   //默认10页
	for k, v := range args {
		if k == "clientPage" {
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" {
			everyPageStr = v[0]
			continue
		}
		if k == "status" {
			sql += "a.status in (" + v[0] + ") and "
			sqlnolimit += "a.status in (" + v[0] + ") and "
			continue
		}
		if v[0] == "" { //条件值为空,舍弃
			continue
		}
		v[0] = strings.Replace(v[0], "'", "\\'", -1) //转义
		sql += "a." + k + " = '" + v[0] + "' and "
		sqlnolimit += "a." + k + " = '" + v[0] + "' and "
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	c := []byte(sql)
	sql = string(c[:len(c)-4]) //去and
	sqlnolimit = string([]byte(sqlnolimit)[:len(sqlnolimit)-4])
	sql += "order by a.id desc limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr

	return db.GetDataBySqlSearch(&order, sql, sqlnolimit, clientPage, everyPage)
}

//删除订单,根据id
func DeleteOrderByid(id string) interface{} {

	return db.DeleteDataByName("order", "id", id)
}

//修改订单
func UpdateOrder(args map[string][]string) interface{} {

	//开启事务
	tx := db.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := db.GetUpdateSqlById("order", args)

	dba := tx.Exec(sql)
	num = dba.RowsAffected
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapExistOrNo
	} else {
		if _, ok := args["status"]; ok { //修改订单状态时,分销逻辑
			//支付成功,进行分销,事务
			DestributeMoney(tx, args["id"][0])
		}
		//if _, ok := args["service"]; ok { //订单服务补充
		//	//支付成功,订单补充,事务
		//	OrderBMoney(tx, args["id"][0])
		//}
		info = lib.MapUpdate
	}

	if tx.Error != nil {
		tx.Rollback()
	}

	if _, ok := args["status"]; ok {
		if args["status"][0] == "0" { //下单成功
			var or Order
			db.DB.Find(&or, args["id"][0]) //当前数据库订单状态
			//状态变化
			if or.Status != 0 {
				SendOrderMsg(args["id"][0]) //失败会导致commit 提交失败why?
			}

		}
	}

	tx.Commit()

	return info
}

//补充订单
func UpdateOrderB(args map[string][]string) interface{} {

	//开启事务
	tx := db.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := db.GetUpdateSqlById("order", args)

	dba := tx.Exec(sql)
	num = dba.RowsAffected
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapExistOrNo
	} else {
		if _, ok := args["service"]; ok { //订单服务补充
			//支付成功,订单补充,事务
			OrderBMoney(tx, args["id"][0])
		}
		info = lib.MapUpdate
	}

	if tx.Error != nil {
		tx.Rollback()
	}
	tx.Commit()
	return info
}

//订单补充，分销
func OrderBMoney(tx *gorm.DB, id string) {

	var or Order
	tx.Find(&or, id)
	//判断周几
	//t, _ := time.Parse("2006-01-02", or.Playtime)
	week := time.Time(or.Playtime).Weekday() //0,1,2,3,4,5,6,见源代码

	//撤销订单中服务分销
	{
		var ct client.Client
		var oldor Order
		db.DB.Find(&oldor, id)

		//分销记录创建,先查询日期有效性
		db.DB.Where("TIMESTAMPDIFF(day, introduce_time,now()) <= 30").Find(&ct, or.ClientId)

		//============查询之前的服务，先进行撤销分销====================
		//根据该条订单记录查询计算分销记录总金额
		//多种服务价格+服务名称
		var serprice []float64 //服务价格数组,动态判断服务数量
		var sername []string
		if oldor.Service != "" { //无旧服务,不进行撤销分销动作
			services := strings.Split(oldor.Service, ",")
			for _, v := range services {
				v = strings.Trim(v, " ") //前后空格
				//服务价格
				vprices := strings.Split(v, "￥")
				vprice, _ := strconv.ParseFloat(strings.Trim(vprices[1], " "), 64)
				serprice = append(serprice, vprice)
				//服务名称
				vnames := strings.Split(v, "x")
				vname := strings.Trim(vnames[0], " ")
				sername = append(sername, vname)
			}

			//分销值计算
			var disvalue float64

			//服务分销计算
			for k, v := range sername {
				var gl groupmeal.Groupmeal //for中定义为了重置
				var hc hirecar.Hirecar
				var dv device.Device

				//分销比例
				db.DB.Where("name = ?", v).Find(&gl)
				glps := strings.Split(gl.Groupmealpro, ",")
				if gl.ID != 0 && glps[week] != "" {
					glp, _ := strconv.ParseFloat(glps[week], 64)
					disvalue += serprice[k] * glp
					continue
				}

				db.DB.Where("category = ?", v).Find(&hc)
				hcps := strings.Split(hc.Hirecarpro, ",")
				if hc.ID != 0 && hcps[week] != "" {
					hcp, _ := strconv.ParseFloat(hcps[week], 64)
					disvalue += serprice[k] * hcp
					continue
				}

				db.DB.Where("name = ?", v).Find(&dv)
				dvps := strings.Split(dv.Devicepro, ",")
				if dv.ID != 0 && dvps[week] != "" {
					dvp, _ := strconv.ParseFloat(dvps[week], 64)
					disvalue += serprice[k] * dvp
					continue
				}
			}

			//分销记录中,订单代理对应的分销记录
			if err := tx.Exec("delete from distribute where order_id=? and introducer=? and introducer_id=?", or.ID, ct.Introducer, ct.Introducer_id).Error; err != nil {
				tx.Rollback()
			}

			//该条订单应该分销总价格
			or.Proprice -= disvalue
			if err := tx.Save(&or).Error; err != nil {
				tx.Rollback()
			}

			//有效期范围内,记录分销值
			if ct.Openid != "" {
				//查询介绍人种类和id
				dis := distribute.Distribute{
					Order_id:      or.ID,
					Introducer:    ct.Introducer,
					Introducer_id: ct.Introducer_id,
					Price:         disvalue,
				}
				//if err := tx.Create(&dis).Error; err != nil {
				//	tx.Rollback()
				//}

				//更新分销总价格
				switch ct.Introducer {
				case 0:
					var ver venuer.Venuer
					tx.First(&ver, dis.Introducer_id)
					ver.Proprice -= dis.Price
					if err := tx.Save(&ver).Error; err != nil {
						tx.Rollback()
					}
				case 1:
					var por proxyer.Proxyer
					tx.First(&por, dis.Introducer_id)
					por.Proprice -= dis.Price
					if err := tx.Save(&por).Error; err != nil {
						tx.Rollback()
					}
				}
			}
		}
	}
	//分销比例，周几
	{
		var ct client.Client
		//============将订单记录加入各自场馆的分销记录中以便提现金额====================
		//根据该条订单记录查询计算分销记录总金额
		//多种服务价格+服务名称
		var serprice []float64 //服务价格数组,动态判断服务数量
		var sername []string
		if or.Service != "" {
			services := strings.Split(or.Service, ",")
			for _, v := range services {
				v = strings.Trim(v, " ") //前后空格
				//服务价格
				vprices := strings.Split(v, "￥")
				vprice, _ := strconv.ParseFloat(strings.Trim(vprices[1], " "), 64)
				serprice = append(serprice, vprice)
				//服务名称
				vnames := strings.Split(v, "x")
				vname := strings.Trim(vnames[0], " ")
				sername = append(sername, vname)
			}
		}

		//分销值计算
		var disvalue float64
		//服务分销计算
		for k, v := range sername {
			var gl groupmeal.Groupmeal //for中定义为了重置
			var hc hirecar.Hirecar
			var dv device.Device

			//分销比例
			db.DB.Where("name = ?", v).Find(&gl)
			glps := strings.Split(gl.Groupmealpro, ",")
			if gl.ID != 0 && glps[week] != "" {
				glp, _ := strconv.ParseFloat(glps[week], 64)
				disvalue += serprice[k] * glp
				continue
			}

			db.DB.Where("category = ?", v).Find(&hc)
			hcps := strings.Split(hc.Hirecarpro, ",")
			if hc.ID != 0 && hcps[week] != "" {
				hcp, _ := strconv.ParseFloat(hcps[week], 64)
				disvalue += serprice[k] * hcp
				continue
			}

			db.DB.Where("name = ?", v).Find(&dv)
			dvps := strings.Split(dv.Devicepro, ",")
			if dv.ID != 0 && dvps[week] != "" {
				dvp, _ := strconv.ParseFloat(dvps[week], 64)
				disvalue += serprice[k] * dvp
				continue
			}
		}

		//该条订单应该分销总价格
		or.Proprice += disvalue
		if err := tx.Save(&or).Error; err != nil {
			tx.Rollback()
		}

		//分销记录创建,先查询日期有效性
		db.DB.Where("TIMESTAMPDIFF(day, introduce_time,now()) <= 30").Find(&ct, or.ClientId)
		//有效期范围内,记录分销值
		if ct.Openid != "" {
			//查询介绍人种类和id
			dis := distribute.Distribute{
				Order_id:      or.ID,
				Introducer:    ct.Introducer,
				Introducer_id: ct.Introducer_id,
				Price:         disvalue,
			}
			if err := tx.Create(&dis).Error; err != nil {
				//tx.Rollback()
			}

			//更新分销总价格
			switch ct.Introducer {
			case 0:
				var ver venuer.Venuer
				tx.First(&ver, dis.Introducer_id)
				ver.Proprice += dis.Price
				if err := tx.Save(&ver).Error; err != nil {
					//tx.Rollback()
				}
			case 1:
				var por proxyer.Proxyer
				tx.First(&por, dis.Introducer_id)
				por.Proprice += dis.Price
				if err := tx.Save(&por).Error; err != nil {
					//tx.Rollback()
				}
			}
		}
	}
}

//分销逻辑,传入事务,提交事务
func DestributeMoney(tx *gorm.DB, id string) {
	//当订单完成时,并且支付成功后,进行分销记录操作
	//同时进行日期判断是否过期30天
	//如果是自己的场馆,则100%
	var or Order
	var or2 Order
	var vr venue.Venue
	tx.Find(&or, id)     //修改后数据库订单状态
	or2.Status = -1
	db.DB.Find(&or2, id) //当前数据库订单状态
	//状态未变化，不进行分销判断
	//退出分销
	if or.Status == or2.Status {
		return
	}
	//判断周几
	//t, _ := time.Parse("2006-01-02", or.Playtime)
	week := time.Time(or.Playtime).Weekday() //0,1,2,3,4,5,6,见源代码

	//分销场馆比例查询
	db.DB.Find(&vr, or.VenueId)
	//分销比例，周几
	venueps := strings.Split(vr.Venuepro, ",")
	pps := strings.Split(vr.Platpro, ",")
	//分销比例检测
	//没有对应分销比例时,bu'fen'xiao
	//if vr.Venuepro == "" {
	//	return
	//}
	//for k,v := range venueps {
	//	if k != int(week) {
	//		continue
	//	}
	//	if k == int(week) && v == ""{
	//		return
	//	}
	//}

	//如果是馆主,且下单是自己场馆的订单,获得分销值包括平台比例
	p1, _ := strconv.ParseFloat(venueps[int(week)], 64) //场馆分销比例
	p2, _ := strconv.ParseFloat(pps[int(week)], 64)     //平台分销比例
	//h状态0,进行分销的累加
	if or.Status == 0 {
		var ct client.Client
		//============将订单记录加入各自场馆的分销记录中以便提现金额====================
		//根据该条订单记录查询计算分销记录总金额
		//多种服务价格+服务名称
		var serprice []float64 //服务价格数组,动态判断服务数量
		var sername []string
		if or.Service != "" {
			services := strings.Split(or.Service, ",")
			for _, v := range services {
				v = strings.Trim(v, " ") //前后空格
				//服务价格
				vprices := strings.Split(v, "￥")
				vprice, _ := strconv.ParseFloat(strings.Trim(vprices[1], " "), 64)
				serprice = append(serprice, vprice)
				//服务名称
				vnames := strings.Split(v, "x")
				vname := strings.Trim(vnames[0], " ")
				sername = append(sername, vname)
			}
		}

		//订单：场馆价格+人头费价格=总价格-服务总价格
		venuePrice := or.Price
		for _, v := range serprice {
			venuePrice -= v
		}
		//分销值计算
		var disvalue float64

		//判断是否是馆主自己作为代理,别人下单的也是该馆
		//查询客户对应的馆主id,如果是馆主的话
		var ctv client.Client
		db.DB.First(&ctv, or.ClientId)

		if ctv.Introducer == 0 && ctv.Introducer_id == vr.Venuer_id {
			disvalue = venuePrice * (p1 + p2) // + 服务比例
		} else {
			disvalue = venuePrice * p1 //+ 服务比例
		}

		//服务分销计算
		for k, v := range sername {
			var gl groupmeal.Groupmeal //for中定义为了重置
			var hc hirecar.Hirecar
			var dv device.Device

			//分销比例
			db.DB.Where("name = ?", v).Find(&gl)
			glps := strings.Split(gl.Groupmealpro, ",")
			if gl.ID != 0 && glps[week] != "" {
				glp, _ := strconv.ParseFloat(glps[week], 64)
				disvalue += serprice[k] * glp
				continue
			}

			db.DB.Where("category = ?", v).Find(&hc)
			hcps := strings.Split(hc.Hirecarpro, ",")
			if hc.ID != 0 && hcps[week] != "" {
				hcp, _ := strconv.ParseFloat(hcps[week], 64)
				disvalue += serprice[k] * hcp
				continue
			}

			db.DB.Where("name = ?", v).Find(&dv)
			dvps := strings.Split(dv.Devicepro, ",")
			if dv.ID != 0 && dvps[week] != "" {
				dvp, _ := strconv.ParseFloat(dvps[week], 64)
				disvalue += serprice[k] * dvp
				continue
			}
		}

		//创建馆主自己的订单(合并到分销记录中)
		var dis = distribute.Distribute{
			Order_id:      or.ID,
			Introducer:    0,                          //0表示馆主
			Introducer_id: vr.Venuer_id,               //馆主id
			Price:         venuePrice * (1 - p1 - p2), //场馆应得价格 = 总价格-（场馆+人头费）分销价格-平台分销价格
		}
		//将场馆订单先加入到各自的馆主分销记录中,以便提现
		if err := tx.Create(&dis).Error; err != nil {
			tx.Rollback()
		}
		//更新馆主分销(包括订单)总价格
		var ver venuer.Venuer
		tx.First(&ver, vr.Venuer_id)
		ver.Proprice += dis.Price
		if err := tx.Save(&ver).Error; err != nil {
			tx.Rollback()
		}

		//该条订单应该分销总价格
		orProprice := disvalue + dis.Price
		or.Proprice = orProprice
		if err := tx.Save(&or).Error; err != nil {
			tx.Rollback()
		}

		//分销记录创建,先查询日期有效性
		db.DB.Where("TIMESTAMPDIFF(day, introduce_time,now()) <= 30").Find(&ct, or.ClientId)
		//有效期范围内,记录分销值
		if ct.Openid != "" {
			//查询介绍人种类和id
			dis = distribute.Distribute{
				Order_id:      or.ID,
				Introducer:    ct.Introducer,
				Introducer_id: ct.Introducer_id,
				Price:         disvalue,
			}
			if err := tx.Create(&dis).Error; err != nil {
				//tx.Rollback()
			}

			//更新分销总价格
			switch ct.Introducer {
			case 0:
				var ver venuer.Venuer
				tx.First(&ver, dis.Introducer_id)
				ver.Proprice += dis.Price
				if err := tx.Save(&ver).Error; err != nil {
					//tx.Rollback()
				}
			case 1:
				var por proxyer.Proxyer
				tx.First(&por, dis.Introducer_id)
				por.Proprice += dis.Price
				if err := tx.Save(&por).Error; err != nil {
					//tx.Rollback()
				}
			}
		}
	}

	//状态2,撤销分销,分销的给吐出来
	if or.Status == 2 {
		var ct client.Client
		var vr venue.Venue

		//============将订单记录加入各自场馆的分销记录中以便提现金额====================
		//根据该条订单记录查询计算分销记录总金额
		//多种服务价格+服务名称
		var serprice []float64 //服务价格数组,动态判断服务数量
		var sername []string
		if or.Service != "" {
			services := strings.Split(or.Service, ",")
			for _, v := range services {
				v = strings.Trim(v, " ") //前后空格
				//服务价格
				vprices := strings.Split(v, "￥")
				vprice, _ := strconv.ParseFloat(strings.Trim(vprices[1], " "), 64)
				serprice = append(serprice, vprice)
				//服务名称
				vnames := strings.Split(v, "x")
				vname := strings.Trim(vnames[0], " ")
				sername = append(sername, vname)
			}
		}

		//订单：场馆价格=总价格-服务总价格
		venuePrice := or.Price
		for _, v := range serprice {
			venuePrice -= v
		}
		//分销值计算
		var disvalue float64
		//判断是否是馆主自己作为代理,别人下单的2也是该馆
		//查询客户对应的馆主id,如果是馆主的话
		var ctv client.Client
		db.DB.First(&ctv, or.ClientId)
		//如果是馆主,且下单是自己场馆的订单,获得分销值包括平台比例
		if ctv.Introducer == 0 && ctv.Introducer_id == vr.Venuer_id {
			disvalue = venuePrice * (p1 + p2) // + 服务比例
		} else {
			disvalue = venuePrice * p1 //+ 服务比例
		}

		//服务分销计算
		for k, v := range sername {
			var gl groupmeal.Groupmeal //for中定义为了重置
			var hc hirecar.Hirecar
			var dv device.Device

			//分销比例
			db.DB.Where("name = ?", v).Find(&gl)
			glps := strings.Split(gl.Groupmealpro, ",")
			if gl.ID != 0 && glps[week] != "" {
				glp, _ := strconv.ParseFloat(glps[week], 64)
				disvalue += serprice[k] * glp
				continue
			}

			db.DB.Where("category = ?", v).Find(&hc)
			hcps := strings.Split(hc.Hirecarpro, ",")
			if hc.ID != 0 && hcps[week] != "" {
				hcp, _ := strconv.ParseFloat(hcps[week], 64)
				disvalue += serprice[k] * hcp
				continue
			}

			db.DB.Where("name = ?", v).Find(&dv)
			dvps := strings.Split(dv.Devicepro, ",")
			if dv.ID != 0 && dvps[week] != "" {
				dvp, _ := strconv.ParseFloat(dvps[week], 64)
				disvalue += serprice[k] * dvp
				continue
			}
		}

		//创建馆主自己的订单(合并到分销记录中)
		var dis = distribute.Distribute{
			Order_id:      or.ID,
			Introducer:    0,                          //0表示馆主
			Introducer_id: vr.Venuer_id,               //馆主id
			Price:         venuePrice * (1 - p1 - p2), //总价格-场馆分销价格-平台分销价格
		}
		//分销记录中,删除该订单所有的分销记录
		if err := tx.Where("order_id = ?", or.ID).Delete(&dis).Error; err != nil {
			tx.Rollback()
		}
		//更新馆主分销(包括订单)总价格
		var ver venuer.Venuer
		tx.First(&ver, vr.Venuer_id)
		ver.Proprice -= dis.Price
		if err := tx.Save(&ver).Error; err != nil {
			tx.Rollback()
		}

		//分销记录创建,先查询日期有效性
		db.DB.Where("TIMESTAMPDIFF(day, introduce_time,now()) <= 30").Find(&ct, or.ClientId)
		//有效期范围内,记录分销值
		if ct.Openid != "" {
			//查询介绍人种类和id
			dis = distribute.Distribute{
				Order_id:      or.ID,
				Introducer:    ct.Introducer,
				Introducer_id: ct.Introducer_id,
				Price:         disvalue,
			}
			//if err := tx.Create(&dis).Error; err != nil {
			//	tx.Rollback()
			//}

			//更新分销总价格
			switch ct.Introducer {
			case 0:
				var ver venuer.Venuer
				tx.First(&ver, dis.Introducer_id)
				ver.Proprice -= dis.Price
				if err := tx.Save(&ver).Error; err != nil {
					tx.Rollback()
				}
			case 1:
				var por proxyer.Proxyer
				tx.First(&por, dis.Introducer_id)
				por.Proprice -= dis.Price
				if err := tx.Save(&por).Error; err != nil {
					tx.Rollback()
				}
			}
		}
	}
	//状态3, 已作废
	if or.Status == 4 {
		//创建馆主自己的订单(合并到分销记录中)
		//var dis = distribute.Distribute{
		//	Order_id:      or.ID,
		//}
		////分销记录中,删除该订单所有的分销记录
		//if err := tx.Where("order_id = ?", or.ID).Delete(&dis).Error; err != nil {
		//	tx.Rollback()
		//}
	}
}

//创建订单
func CreateOrder(args map[string][]string) interface{} {

	//创建订单时,判断是否已存在重复时间段订单
	var ors []Order
	db.DB.Where("playtime = ? and venue_id = ? and status = 0", args["playtime"][0], args["venue_id"][0]).Find(&ors)
	for _, v := range ors {
		pds := strings.Split(v.Period, ",")
		for _, v2 := range pds {
			if strings.Contains(args["period"][0], v2) {
				return lib.GetMapDataError(lib.CodeOrder, "时间段已被占用")
			}
		}
	}

	//后台添加创建时间
	createtime := time.Now().Format("2006-01-02 15:04:05")
	args["createtime"] = append(args["createtime"], createtime)

	//db.CreateData("order", args)
	var info interface{}
	var num int64 //返回影响的行数

	//开启事务
	tx := db.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	sql := db.GetInsertSql("order", args)
	dba := tx.Exec(sql)
	num = dba.RowsAffected

	var or Order
	tx.Raw("select max(id) as id from `order`").Scan(&or)

	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapError
	} else {

		info = map[string]interface{}{"status": 201, "msg": "创建成功", "id": or.ID}
	}

	if tx.Error != nil {
		tx.Rollback()
	}

	tx.Commit()
	return info
}

//创建订单
func CreateOrderX(args map[string][]string) interface{} {

	//创建订单时,判断是否已存在重复时间段订单
	var ors []Order
	db.DB.Where("playtime = ? and venue_id = ? and status = 0", args["playtime"][0], args["venue_id"][0]).Find(&ors)
	for _, v := range ors {
		pds := strings.Split(v.Period, ",")
		for _, v2 := range pds {
			if strings.Contains(args["period"][0], v2) {
				return lib.GetMapDataError(lib.CodeOrder, "时间段已被占用")
			}
		}
	}


	//后台添加创建时间
	createtime := time.Now().Format("2006-01-02 15:04:05")
	args["createtime"] = append(args["createtime"], createtime)

	//db.CreateData("order", args)
	var info interface{}
	var num int64 //返回影响的行数

	//开启事务
	tx := db.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	sql := db.GetInsertSql("order", args)
	dba := tx.Exec(sql)
	num = dba.RowsAffected

	var or Order
	tx.Raw("select max(id) as id from `order`").Scan(&or)

	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else if num == 0 && dba.Error == nil {
		info = lib.MapError
	} else {
		if _, ok := args["status"]; ok { //修改订单状态时,分销逻辑
			//支付成功,进行分销,事务
			DestributeMoney(tx, strconv.Itoa(or.ID))
		}
		info = map[string]interface{}{"status": 201, "msg": "创建成功", "id": or.ID}
	}

	if tx.Error != nil {
		tx.Rollback()
	}

	tx.Commit()

	if args["status"][0] == "0" { //下单成功
		SendOrderMsg(strconv.Itoa(or.ID)) //
	}
	return info
}

//短信发送,订单下单成功
func SendOrderMsg(order_id string) {
	//确定操作成功结束后
	var or Order
	var vr venuer.Venuer
	var ve venue.Venue
	db.DB.Find(&or, order_id)
	db.DB.Raw("select a.* from venuer a inner join venue b on a.id=b.venuer_id where b.id=?", or.VenueId).Scan(&vr)
	db.DB.Raw("select * from venue where id=?", or.VenueId).Scan(&ve)
	//发送给馆主
	templateCode := "SMS_152289177"
	phoneNumbers := vr.Phone
	//var t  time.Time
	//t = time.Time(or.Playtime)
	datetime1 := time.Time(or.Playtime).Format("2006-01-02")
	createtime := time.Time(or.Createtime).Format("2006-01-02 15:04:05")

	//当天场馆价格
	var vp venueprice.VenuepriceInfo
	//判断playtime是否有特殊价格
	sql := "select price,oldprice from `venuepricets` where venue_id='" + strconv.FormatInt(or.VenueId, 10) + "' and playtime='" + datetime1 + "'"
	db.GetDataBySql(&vp, sql)
	if vp.Price == "" {
		sql = "select price,oldprice from `venueprice` where venue_id='" + strconv.FormatInt(or.VenueId, 10) + "' and week='" + strconv.Itoa(int(time.Time(or.Playtime).Weekday())) + "'"
		db.GetDataBySql(&vp, sql)
	}
	//价格分割
	if vp.Price == "" {
		vp.Price = "0,0,0,0,0,0" //6个时间段价格
	}
	vps := strings.Split(vp.Price, ",")
	money := 0

	//场次判断
	perds := strings.Split(or.Period, ",")
	for _, v := range perds {
		if v == "" {
			break
		}
		switch v {
		case "1":
			datetime1 += "上午场"
		case "2":
			datetime1 += "下午场"
		case "3":
			datetime1 += "夜间场"
		case "4":
			datetime1 += "白天场"
		case "5":
			datetime1 += "通宵场"
		case "6":
			datetime1 += "全天场"
		}
		//累加价格
		k, _ := strconv.Atoi(v)
		if len(vps) < k -1 {
			continue
		}
		my, _ := strconv.Atoi(vps[k-1])
		money += my
	}

	templateParam := "{\"name1\":\"" + vr.Name + "\",\"venue_name\":\"" + ve.Name + "\",\"datetime1\":\"" + datetime1 + "\",\"time2\":\"" + createtime + "\"," +
		"\"name2\":\"" + or.Username + "\",\"phone\":\"" + or.Phone + "\",\"idcard\":\"" + or.Idcard + "\",\"person_num\":\"" + strconv.FormatInt(or.Pnum, 10) + "\"," +
		"\"money\":\"" + strconv.Itoa(money) + "\",\"remark\":\"" + or.Clientremark + "\"}"
	//fmt.Println(templateParam)
	proxyer.SendMsg(phoneNumbers, templateCode, templateParam)
	//发送给客户
	templateCode = "SMS_152284337"
	phoneNumbers = or.Phone
	templateParam = "{\"name1\":\"" + or.Username + "\",\"datetime\":\"" + datetime1 + "\",\"venue_name\":\"" + ve.Name + "\"," +
		"\"name2\":\"" + or.Username + "\",\"phone\":\"" + or.Phone + "\",\"idcard\":\"" + or.Idcard + "\",\"person_num\":\"" + strconv.FormatInt(or.Pnum, 10) + "\"," +
		"}"
	proxyer.SendMsg(phoneNumbers, templateCode, templateParam)
}
