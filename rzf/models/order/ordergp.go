package order

import (
	"rzf/util/db"
	"rzf/util/lib"
	"rzf/util/str"
	"strconv"
	"strings"
)

/*订单组队*/
type Ordergp struct {
	ID       int64 `json:"id"`
	OrderID  int64 `json:"order_id"`
	ClientID int64 `json:"client_id"`
}

/*订单详细信息*/
type OrdergpInfo struct {
	ClientID int64  `json:"client_id"`
	Nickname string `json:"nickname"`
	Headimg  string `json:"headimg"`
}

type OrderGpData struct {
	Cartgp     []Ordergp `json:"cartgp"`      //为了结构保持一致
	GroupedNum int64     `json:"grouped_num"` //组队多少人
	Discount   float64     `json:"discount"`    //优惠了xx
}

type IsGp struct {
	IsGp int64 `json:"is_gp"`
}

// 创建组队信息
func (c *Ordergp) CreateOrdergp(client_ids, order_id, venue_id, discounts string) interface{} {
	// 检测人数是否超过人数限制
	var value str.Value
	db.DB.Raw("select person_num as value from `venue` where id=?", venue_id).Scan(&value)
	if value.Value != "" {
		maxNum,_ := strconv.Atoi(value.Value)
		db.DB.Raw("select count(*) as value from `ordergp` where order_id=?", order_id).Scan(&value)
		nowNum,_:= strconv.Atoi(value.Value)
		if nowNum >= maxNum{
			return lib.GetMapDataError(lib.CodeText, "超过人数限制")
		}
	}

	sql := "select sum(discount) as value from `ordergp` where order_id=?"
	db.DB.Raw(sql, order_id).Scan(&value)
	if value.Value != "" {
		discount,_ := strconv.Atoi(value.Value)
		if discount >= 100 {
			return lib.GetMapDataError(lib.CodeText, "优惠超过100元限制")
		}
	}

	cli_ids := strings.Split(client_ids, ",")
	dis_ts := strings.Split(discounts, ",")
	sql = "insert `ordergp`(order_id, client_id, discount) value"
	for k, v := range cli_ids {
		sql += "(" + order_id + "," + v + "," + dis_ts[k] + "),"
	}
	sql = string([]byte(sql)[:len(sql)-1])

	var info interface{}
	dba := db.DB.Exec(sql)

	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
	} else {
		info = lib.MapCreate
	}
	return info
}

//获得对应组队订单信息
func (c *Ordergp) GetOrdergp(order_id string) interface{} {

	//var oi []OrdergpInfo
	//sql := "select a.client_id,b.nickname,b.headimg from `ordergp` a inner join client b on a.client_id=b.id where a.order_id=?"
	//return db.GetDataBySql(&oi, sql, order_id)

	var getinfo lib.GetInfoN
	var cgd []OrderGpData
	cids := strings.Split(order_id, ",")

	sql := "select count(distinct order_id) as grouped_num from `ordergp` where order_id in ("
	for _, v := range cids {
		sql += "'" + v + "',"
	}
	sql = string([]byte(sql)[:len(sql)-1]) + ")"
	//info := db.GetDataBySql(&cgd, sql)

	sql3 := "select a.client_id,b.nickname,b.headimg from `ordergp` a inner join client b on a.client_id=b.id where a.order_id=?"

	sql2 := "select count(*) as grouped_num,sum(discount) as discount from `ordergp` where order_id=?"

	var cgd2 OrderGpData
	for _, v := range cids {

		db.GetDataBySql(&cgd2, sql2, v)
		db.GetDataBySql(&cgd2.Cartgp, sql3, v)
		//cgd2.Discount = cgd2.GroupedNum * 5
		cgd = append(cgd, cgd2)
	}
	//统计页码等状态
	getinfo.Status = lib.CodeSuccess
	getinfo.Msg = lib.MsgSuccess
	getinfo.Data = cgd //数据

	return getinfo
}

//查询组队信息
func (c *Ordergp) GetIsGp(client_id, key_id string) interface{} {
	var ip, ip2 IsGp
	var getinfo lib.GetInfoN
	sql := "select count(*) as is_gp from `ordergp` where client_id=" + client_id + " and order_id=? "
	sql2 := "select count(*) as is_gp from `cartgp` where client_id=" + client_id + " and cart_id=? "
	db.GetDataBySql(&ip, sql, key_id)
	db.GetDataBySql(&ip2, sql2, key_id)
	getinfo.Msg = lib.MsgSuccess
	getinfo.Status = lib.CodeSuccess
	getinfo.Data = ip
	if ip.IsGp != 0 {
		getinfo.Data = ip
	}
	if ip2.IsGp != 0 {
		getinfo.Data = ip2
	}
	return getinfo
}
