package hirecar

import "rzf/util/db"

/*包车管理模型*/
type Hirecar struct {
	ID         int     `json:"id"`
	Category   string  `json:"category"`   //类型
	Person_num string  `json:"person_num"` //适合人数
	Price      float64 `json:"price"`      //价格
	Imageurl   string  `json:"imageurl"`   //封面
	Hirecarpro string `json:"hirecarpro"` //包车分销比例
}

//获得包车,根据id
func GetHirecarById(id string) interface{} {

	db.DB.AutoMigrate(&Hirecar{})
	var hirecar = Hirecar{}
	return db.GetDataById(&hirecar, id)
}

//获得包车,分页/查询
func GetHirecarBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Hirecar{})
	var hirecars = []*Hirecar{}
	return db.GetDataBySearch(Hirecar{}, &hirecars, "hirecar", args) //匿名Hirecar{}
}

//删除包车,根据id
func DeleteHirecarByid(id string) interface{} {

	return db.DeleteDataByName("hirecar", "id", id)
}

//修改包车
func UpdateHirecar(args map[string][]string) interface{} {

	return db.UpdateData("hirecar", args)
}

//创建包车
func CreateHirecar(args map[string][]string) interface{} {

	return db.CreateData("hirecar", args)
}
