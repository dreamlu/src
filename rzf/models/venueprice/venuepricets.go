package venueprice

import (
	"rzf/conf"
	"rzf/util"
	"rzf/util/db"
	"strconv"
	"strings"
)

/*特殊日期价格模型*/
type Venuepricets struct {
	ID       int           `json:"id"`
	Oldprice string        `json:"oldprice"`
	Price    string        `json:"price"` //时间段，逗号分割
	Playtime util.JsonDate `json:"playtime"`
	VenueID  int           `json:"venue_id"` //对应场馆id
}

// 后台 特价
type VpsInfo struct {
	Venuepricets
	VenueName  string `json:"venue_name"`  //场馆名
	VenuerName string `json:"venuer_name"` //馆主
}

// 小程序所需字段(venue)
type VpsInfoXV struct {
	Category  string  `json:"category"`   //类型
	Address   string  `json:"address"`    //地址
	Map       string  `json:"map"`        //地图
	Introduce string  `json:"introduce"`  //介绍
	Star      int     `json:"star"`       //星级
	Carousels string  `json:"carousels"`  //封面轮播图,','分割
	PersonNum int     `json:"person_num"` //人数限制
	Imageurl  string  `json:"imageurl"`   //分页封面
	Label     string  `json:"label"`      //标签
	Shelf     int     `json:"shelf"`      //0上架,1下架
	Headline  int64   `json:"headline"`   //人头费线
	Headprice float64 `json:"headprice"`  //单人头费
}

// 小程序 特价
type VpsInfoX struct {
	VpsInfo
	VpsInfoXV
	Distance float64 `json:"distance"` //离用户距离
}

//获得时间段,根据id
func GetVenuepricetsById(id string) interface{} {

	var vsi VpsInfo
	sql := "select a.*,b.name as venue_name,c.name as venuer_name from venuepricets a inner join venue b on a.venue_id=b.id inner join venuer c on b.venuer_id=c.id where a.id=?"
	return db.GetDataBySql(&vsi, sql, id)
}

//获得时间段,分页/查询
func GetVenuepricetsBySearchX(args map[string][]string) interface{} {

	//经纬度
	lng := ""
	lat := ""

	sql := `select a.*,b.name as venue_name,c.name as venuer_name,` + db.GetColumnsSql(VpsInfoXV{}, "b") + ` from venuepricets a inner join venue b on a.venue_id=b.id inner join venuer c on b.venuer_id=c.id
		where DATE_FORMAT(a.playtime, '%Y%m%d') in (DATE_FORMAT(now(), '%Y%m%d'), DATE_FORMAT(date_add(now(), interval 1 day), '%Y%m%d')) and `
	sqlnolimit := `select
		count(distinct a.id) as sum_page	
		from venuepricets a inner join venue b on a.venue_id=b.id inner join venuer c on b.venuer_id=c.id
		where DATE_FORMAT(a.playtime, '%Y%m%d') in (DATE_FORMAT(now(), '%Y%m%d'), DATE_FORMAT(date_add(now(), interval 1 day), '%Y%m%d')) and `
	var vsi []VpsInfoX

	//页码,每页数量
	clientPageStr := conf.GetConfigValue("clientPage") //默认第1页
	everyPageStr := conf.GetConfigValue("everyPage")   //默认10页

	//尝试将select* 变为对应的字段名
	for k, v := range args {
		if k == "clientPage" {
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" {
			everyPageStr = v[0]
			continue
		}
		if k == "lng" {
			lng = v[0]
			continue
		}
		if k == "lat" {
			lat = v[0]
			continue
		}
		if v[0] == "" { //条件值为空,舍弃
			continue
		}
		v[0] = strings.Replace(v[0], "'", "\\'", -1) //转义
		sql += "a." + k + " = '" + v[0] + "' and "
		sqlnolimit += "a." + k + " = '" + v[0] + "' and "
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	c := []byte(sql)
	sql = string(c[:len(c)-4]) //去and
	sqlnolimit = string([]byte(sqlnolimit)[:len(sqlnolimit)-4])
	sql += "order by id desc limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr

	//距离筛选放最后，因sqlnolimit可能会整体替换
	//地图距离排序,单位(km)
	if lng != "" && lat != "" {
		sql = strings.Replace(sql, "from", ",ROUND(6378.138*2*ASIN(SQRT(POW(SIN(("+lat+"*PI()/180-lat*PI()/180)/2),2)+COS("+lat+"*PI()/180)*COS(lat*PI()/180)*POW(SIN(("+lng+"*PI()/180-lng*PI()/180)/2),2)))) AS distance from", -1)
		// sqlnolimit = "select count(id) as sum_page from (" + sql + ") a"
	}

	return db.GetDataBySqlSearch(&vsi, sql, sqlnolimit, clientPage, everyPage)
}

//获得时间段,分页/查询
func GetVenuepricetsBySearch(args map[string][]string) interface{} {
	sql := `select a.*,b.name as venue_name,c.name as venuer_name from venuepricets a inner join venue b on a.venue_id=b.id inner join venuer c on b.venuer_id=c.id
		where 1=1 and `
	sqlnolimit := `select
		count(distinct a.id) as sum_page	
		from venuepricets a inner join venue b on a.venue_id=b.id inner join venuer c on b.venuer_id=c.id
		where 1=1 and `
	var vsi []VpsInfo

	//页码,每页数量
	clientPageStr := conf.GetConfigValue("clientPage") //默认第1页
	everyPageStr := conf.GetConfigValue("everyPage")   //默认10页

	//尝试将select* 变为对应的字段名
	for k, v := range args {
		if k == "clientPage" {
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" {
			everyPageStr = v[0]
			continue
		}
		if v[0] == "" { //条件值为空,舍弃
			continue
		}
		v[0] = strings.Replace(v[0], "'", "\\'", -1) //转义
		sql += "a." + k + " = '" + v[0] + "' and "
		sqlnolimit += "a." + k + " = '" + v[0] + "' and "
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	c := []byte(sql)
	sql = string(c[:len(c)-4]) //去and
	sqlnolimit = string([]byte(sqlnolimit)[:len(sqlnolimit)-4])
	sql += "order by id desc limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr

	return db.GetDataBySqlSearch(&vsi, sql, sqlnolimit, clientPage, everyPage)
}

//删除时间段,根据id
func DeleteVenuepricetsByid(id string) interface{} {

	return db.DeleteDataByName("venuepricets", "id", id)
}

//修改时间段
func UpdateVenuepricets(args map[string][]string) interface{} {

	return db.UpdateData("venuepricets", args)
}

//创建时间段
func CreateVenuepricets(args map[string][]string) interface{} {

	return db.CreateData("venuepricets", args)
}
