package venueprice

import (
	"fmt"
	"rzf/util/db"
	"rzf/util/lib"
	"strconv"
	"strings"
)

/*时间段价格模型*/
type Venueprice struct {
	ID        int    `json:"id"`
	Oldprice  string `json:"oldprice"`
	Price     string `json:"price"`      //时间段，逗号分割
	Week      string `json:"week"`       //周几
	VenueID   int    `json:"venue_id"`   //对应场馆id
	IsContact string `json:"is_contact"` // 联系客服, 默认'*,*,*,*,*,*'不联系,1联系客服
}

/*小程序返回价格模型*/
type VenuepriceInfo struct {
	Oldprice  string `json:"oldprice"`
	Price     string `json:"price"`      //时间段，逗号分割
	IsContact string `json:"is_contact"` // 联系客服, 默认'*,*,*,*,*,*'不联系,1联系客服
}

/*订单判断变灰消失*/
type OrderW struct {
	Period string `json:"period"` //时间段
}

//获得时间段,根据id
func GetVenuepriceById(id string) interface{} {

	db.DB.AutoMigrate(&Venueprice{})
	var venueprice = Venueprice{}
	return db.GetDataById(&venueprice, id)
}

//获得时间段,分页/查询
func GetVenuepriceBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Venueprice{})
	var venueprices = []*Venueprice{}
	sqlnolimit, sql, clientPage, everyPage := db.SearchTableSql(Venueprice{}, "venueprice", args)
	sql = strings.Replace(sql, "order by id desc", "order by week", -1)
	return db.GetDataBySqlSearch(&venueprices, sql, sqlnolimit, clientPage, everyPage) //匿名Venueprice{}
}

//获得时间段,分页/查询
func GetVenuepriceBySearchX(venue_id, playtime, week string) interface{} {

	var vp, vp2, vpt VenuepriceInfo
	//判断playtime是否有特殊价格
	sql := fmt.Sprintf("select price,oldprice from `venuepricets` where venue_id='%s' and playtime='%s'", venue_id, playtime)
	info := db.GetDataBySql(&vp, sql)
	if vp.Price != "" {
		//标识特价
		vps := strings.Split(vp.Price, ",")
		vp.Price = ""
		for _, v := range vps {
			if v != "*" {
				v += "-t"
			}
			vp.Price += v + ","
		}
		vp.Price = strings.Trim(vp.Price, ",") // 去除前后","
	}

	// 非特价查询
	sql = fmt.Sprintf("select price,oldprice,is_contact from `venueprice` where venue_id='%s' and week='%s'", venue_id, week)
	db.GetDataBySql(&vp2, sql)

	//特价时间段与非特价的合并
	vp2s := strings.Split(vp2.Price, ",")
	vps := strings.Split(vp.Price, ",")

	vp.IsContact = vp2.IsContact

	if vp.Price != "" && vp2.Price != "" {
		vp.Price = ""
		for k, v := range vp2s {
			if vps[k] == "*" { //如果特价对应时间段价格为空(*),用非特价的覆盖
				vps[k] = v
			}
			vp.Price += vps[k] + ","
		}
		vp.Price = strings.Trim(vp.Price, ",") // 去除前后","
		//原价覆盖
		vp.Oldprice = vp2.Oldprice

	} else if vp.Price == "" && vp2.Price != "" {
		vp.Price = vp2.Price
		vp.Oldprice = vp2.Oldprice
	}

	//if vp.Price == "" {
	//	sql = fmt.Sprintf("select price,oldprice from `venueprice` where venue_id='%s' and week='%s'",venue_id, week)
	//	info = db.GetDataBySql(&vp, sql)
	//}

	//判断时间段是否占用
	var ors []OrderW
	db.DB.Raw("select period from `order` where date(playtime) = ? and venue_id = ? and status in (0,1)", playtime, venue_id).Scan(&ors)

	//返回的时间段,查询其中已下订单的时间段,删除对应时间段价格
	vps = strings.Split(vp.Price, ",")
	for k, v3 := range vps {
		for _, v := range ors {
			//订单中时间段分割
			pds := strings.Split(v.Period, ",")
			//订单中对应日期的时间段
			for _, v2 := range pds {
				v2i, _ := strconv.Atoi(v2)
				if k == v2i-1 { //时间段和下标相同012345时,删除对应价格，显示已预定
					vpt.Price += v3 + "-order," //标识预定

					goto into
				}
			}
		}
		vpt.Price += v3 + ","
	into:
	}
	vp.Price = string([]byte(vpt.Price)[:len(vpt.Price)-1])

	if vp.Price != "" {
		var getinfo lib.GetInfoN
		getinfo.Status = lib.CodeSuccess
		getinfo.Msg = lib.MsgSuccess
		getinfo.Data = vp
		return getinfo
	}

	return info
}

//删除时间段,根据id
func DeleteVenuepriceByid(id string) interface{} {

	return db.DeleteDataByName("venueprice", "id", id)
}

//修改时间段
func UpdateVenueprice(args map[string][]string) interface{} {

	return db.UpdateData("venueprice", args)
}

//创建时间段
func CreateVenueprice(args map[string][]string) interface{} {

	return db.CreateData("venueprice", args)
}
