package other

import (
	"rzf/util/db"
)

/*其他管理*/
type Other struct {
	Value	  string `json:"value"` //用户须知、团建定制或联系方式
}

//获得其他,分页/查询
func GetOther(flag string) interface{} {
	var other Other
	var sql string
	id := "1"

	switch flag {
	case "0":
		sql = "select notice as value from `other` where id="+id
	case "1":
		sql = "select gbuild as value from `other` where id="+id
	default:
		sql = "select phone as value from `other` where id="+id
	}

	return db.GetDataBySql(&other,sql)
}
//修改其他
func UpdateOther(args map[string][]string) interface{} {

	return db.UpdateData("other", args)
}

