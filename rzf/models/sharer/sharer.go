package sharer

import (
	"rzf/util/db"
)

/*分销推广管理模型*/
type Sharer struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	ViewNum int    `json:"view_num"`
}

//获得分销推广,根据id
func GetSharerById(id string) interface{} {

	db.DB.AutoMigrate(&Sharer{})
	var sharer = Sharer{}
	return db.GetDataById(&sharer, id)
}

//获得分销推广,分页/查询
func GetSharerBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Sharer{})
	var sharers = []*Sharer{}
	return db.GetDataBySearch(Sharer{}, &sharers, "sharer", args) //匿名Sharer{}
}

//删除分销推广,根据id
func DeleteSharerByid(id string) interface{} {

	return db.DeleteDataByName("sharer", "id", id)
}

//修改分销推广
func UpdateSharer(args map[string][]string) interface{} {

	//if ok := args["view_num"]; ok == nil {
	//	view_num, _ := strconv.Itoa()
	//	args["view_num"] = append(args["view_num"], "")
	//}
	return db.UpdateData("sharer", args)
}

//创建分销推广
func CreateSharer(args map[string][]string) interface{} {

	return db.CreateData("sharer", args)
}
