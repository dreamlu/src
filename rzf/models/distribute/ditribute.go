package distribute

import (
	"fmt"
	"rzf/util"
	"rzf/util/db"
)

/*分销模型*/
type Distribute struct {
	ID            int     `json:"id"`
	Order_id      int     `json:"order_id"`
	Introducer    int     `json:"introducer"`    //介绍人种类0/1,表示馆主/代理
	Introducer_id int     `json:"introducer_id"` //介绍人id
	Price         float64 `json:"price"`         //该条分销记录价格
}

/*分销详情*/
type DistributeInfo struct {
	Username  string        `json:"username"`   //用户名
	Playtime  util.JsonTime `json:"playtime"`   //游玩日期
	Period    string        `json:"period"`     //时间段
	VenueName string        `json:"venue_name"` //场馆名
	Service   string        `json:"service"`    //服务
	Price     float64       `json:"price"`      //该条分销记录价格
}

//根据馆主或代理id获得分销记录
func GetDistributes(introducer, introducer_id string, clientPage, everyPage int) interface{} {
	var distribute []DistributeInfo

	sqlnolimit := fmt.Sprintf(`select 
		count(a.id) as sum_page
		from distribute a 
		inner join `+"`"+`order`+"`"+` b 
		on a.order_id=b.id 
		right join venue c 
		on b.venue_id=c.id 
		where introducer=%s and introducer_id=%s`,introducer,introducer_id)

	sql := fmt.Sprintf(`select 
		username,playtime,period,c.name as venue_name,service,a.price 
		from distribute a 
		inner join `+"`"+`order`+"`"+` b 
		on a.order_id=b.id 
		right join venue c 
		on b.venue_id=c.id 
		where introducer=%s and introducer_id=%s
		order by a.id desc limit %d,%d`,introducer,introducer_id,(clientPage-1)*everyPage,everyPage)

	return db.GetDataBySqlSearch(&distribute, sql, sqlnolimit, clientPage, everyPage)
}
