package device

import "rzf/util/db"

/*设备管理*/
type Device struct {
	ID        int     `json:"id"`
	Name      string  `json:"name"`      //设备名
	Price     float64 `json:"price"`     //价格
	Imageurl  string  `json:"imageurl"`  //封面
	Devicepro string  `json:"devicepro"` //设备分销比例
}

//获得设备,根据id
func GetDeviceById(id string) interface{} {

	db.DB.AutoMigrate(&Device{})
	var device = Device{}
	return db.GetDataById(&device, id)
}

//获得设备,分页/查询
func GetDeviceBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Device{})
	var devices = []*Device{}
	return db.GetDataBySearch(Device{}, &devices, "device", args) //匿名Device{}
}

//删除设备,根据id
func DeleteDeviceByid(id string) interface{} {

	return db.DeleteDataByName("device", "id", id)
}

//修改设备
func UpdateDevice(args map[string][]string) interface{} {

	return db.UpdateData("device", args)
}

//创建设备
func CreateDevice(args map[string][]string) interface{} {

	return db.CreateData("device", args)
}
