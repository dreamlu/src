package venuer

import (
	"rzf/models/venue"
	"rzf/util"
	"rzf/util/db"
	"rzf/util/lib"
	"strconv"
)

/*馆主管理*/
type Venuer struct {
	ID         int     `json:"id"`
	Name       string  `json:"name"`       //馆主名
	Phone      string  `json:"phone"`      //手机号
	Nickname   string  `json:"nickname"`   //账号昵称
	Password   string  `json:"password"`   //密码
	Proprice   float64 `json:"proprice"`   //分销总佣金
	Withdrawed float64 `json:"withdrawed"` //已提现
}

/*馆主管理*/
type VenuerX struct {
	ID         int     `json:"id"`
	Name       string  `json:"name"`       //馆主名
	Phone      string  `json:"phone"`      //手机号
	Nickname   string  `json:"nickname"`   //账号昵称
	Password   string  `json:"password"`   //密码
	Proprice   float64 `json:"proprice"`   //分销总佣金
	Withdrawed float64 `json:"withdrawed"` //已提现
	ClientNum  int     `json:"client_num"` //客户数量
}

/*馆主下拉框*/
type Venuers struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

//获得馆主下拉框
func GetVenuers() interface{} {

	var venuer []Venuers
	sql := "select id,name from `venuer`"
	return db.GetDataBySql(&venuer, sql)
}

//获得馆主以及客户数量,根据id
func GetVenuerByIdX(id string) interface{} {

	var vr VenuerX
	sql := "select "+db.GetColumnsSql(Venuer{},"a")+", count(distinct b.id) as client_num from `venuer` a inner join `client` b on a.id=b.introducer_id where introducer=0 and introducer_id="+id
	return db.GetDataBySql(&vr,sql)
}

//获得馆主,根据id
func GetVenuerById(id string) interface{} {

	db.DB.AutoMigrate(&Venuer{})
	var venuer = Venuer{}
	return db.GetDataById(&venuer, id)
}

//获得馆主,分页/查询
func GetVenuerBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Venuer{})
	var venuers = []*Venuer{}
	return db.GetDataBySearch(Venuer{}, &venuers, "venuer", args) //匿名Venuer{}
}

//删除馆主,根据id
func DeleteVenuerByid(id string) interface{} {

	//ids := []string{"1","2","3","4","5"}
	//for _,v := range ids{
	//	if v == id {
	//		return lib.GetMapDataError(222,"根管理禁止删除")
	//	}
	//}

	switch id {
	case "1","2","3","4","5":
		return lib.GetMapDataError(222,"根管理禁止删除")
	}

	var ve venue.Venue
	db.DB.Raw("select id from `venue` where venuer_id=?",id).Scan(&ve)
	if ve.ID != 0{
		return lib.GetMapDataError(lib.CodeText,"馆主下面存在场馆,禁止删除")
	}
	return db.DeleteDataByName("venuer", "id", id)
}

//修改馆主
func UpdateVenuer(args map[string][]string) interface{} {

	//提现金额,存在
	if _, ok := args["withdrawed"]; ok {
		//计算提现金额
		var vr Venuer
		db.DB.First(&vr,args["id"][0])
		//判断余额金额提现是否足够
		wd,_ := strconv.ParseFloat(args["withdrawed"][0],64)//提现金额
		if (vr.Proprice-vr.Withdrawed) < wd {
			return lib.GetMapDataError(lib.CodeWxWithDraw,"余额不足")
		}

		args["withdrawed"][0] = strconv.FormatFloat(wd + vr.Withdrawed,'f',-1,64)
	}

	//密码
	if _, ok := args["password"]; ok {
		password := util.AesEn(args["password"][0])
		args["password"][0] = string(password)
	}

	return db.UpdateData("venuer", args)
}

//创建馆主
func CreateVenuer(args map[string][]string) interface{} {
	//密码
	if _, ok := args["password"]; ok {
		password := util.AesEn(args["password"][0])
		args["password"][0] = string(password)
	}

	return db.CreateData("venuer", args)
}
