package carousel

import "rzf/util/db"

/*轮播图管理模型*/
type Carousel struct {
	ID         int     `json:"id"`
	Address		string `json:"address"`
}

//获得轮播图,根据id
func GetCarouselById(id string) interface{} {

	db.DB.AutoMigrate(&Carousel{})
	var carousel = Carousel{}
	return db.GetDataById(&carousel, id)
}

//获得轮播图,分页/查询
func GetCarouselBySearch(args map[string][]string) interface{} {
	//相当于注册类型,https://github.com/jinzhu/gorm/issues/857
	db.DB.AutoMigrate(&Carousel{})
	var carousels = []*Carousel{}
	return db.GetDataBySearch(Carousel{}, &carousels, "carousel", args) //匿名Carousel{}
}

//删除轮播图,根据id
func DeleteCarouselByid(id string) interface{} {

	return db.DeleteDataByName("carousel", "id", id)
}

//修改轮播图
func UpdateCarousel(args map[string][]string) interface{} {

	return db.UpdateData("carousel", args)
}

//创建轮播图
func CreateCarousel(args map[string][]string) interface{} {

	return db.CreateData("carousel", args)
}
