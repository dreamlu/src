package hirecar

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/hirecar"
)

//根据id获得包车
func GetHirecarById(u *gin.Context) {
	id := u.Query("id")
	ss := hirecar.GetHirecarById(id)
	u.JSON(http.StatusOK, ss)
}

//包车信息分页
func GetHirecarBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := hirecar.GetHirecarBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//包车信息删除
func DeleteHirecarById(u *gin.Context) {
	id := u.Param("id")
	ss := hirecar.DeleteHirecarByid(id)
	u.JSON(http.StatusOK, ss)
}

//包车信息修改
func UpdateHirecar(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := hirecar.UpdateHirecar(values)
	u.JSON(http.StatusOK, ss)
}

//新增包车信息
func CreateHirecar(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := hirecar.CreateHirecar(values)
	u.JSON(http.StatusOK, ss)
}

/*//修改包车账号密码
func UpdateAccount(u *gin.Context) {

}*/
