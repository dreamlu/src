package wx

import (
	"encoding/json"
	"fmt"
	"github.com/chanxuehong/wechat/mp/core"
	"github.com/chanxuehong/wechat/mp/menu"
	"github.com/chanxuehong/wechat/mp/message/callback/request"
	"github.com/chanxuehong/wechat/mp/message/callback/response"
	"github.com/chanxuehong/wechat/mp/qrcode"
	"github.com/gin-gonic/gin"
	"github.com/medivhzhan/weapp"
	"github.com/medivhzhan/weapp/code"
	"github.com/medivhzhan/weapp/payment"
	qrcode2 "github.com/skip2/go-qrcode"
	"io/ioutil"
	"log"
	"net/http"
	"rzf/models/client"
	"rzf/util/db"
	"rzf/util/lib"
	"strconv"
	"time"
)

var AppID = "wxbda06dec21c6807c"
var Secret = "e0aa97fc0a544a0dacc2b5edf98e8842"
var PaySecret = "x29B0tOuBu1i7dy8I6rgoVp8Ouh91ALL"
var MchID = "1515254021"

//小程序access_token
type AccessToken struct {
	AccessToken string `json:"access_token"`
}

//小程序登录
func WxLogin(u *gin.Context) {
	var getinfo lib.GetInfoN
	code := u.PostForm("code")

	//var info interface{}
	res, err := weapp.Login(AppID, Secret, code)
	if err != nil {
		u.JSON(http.StatusOK, lib.MapDataError{lib.CodeWx, err.Error()})
		return
	}

	//fmt.Printf("返回结果: %#v", res)
	//新用户新增操作
	var clienta client.Client
	db.DB.Where("openid = ?", res.OpenID).First(&clienta)
	if clienta.Openid == "" && res.OpenID != "" {
		client := client.Client{
			Openid: res.OpenID,
		}
		db.DB.Create(&client)
	}
	//查询对应用户信息
	db.DB.Where("openid = ?", res.OpenID).First(&clienta)

	getinfo.Data = map[string]interface{}{"client_id": clienta.ID, "openid": res.OpenID}
	getinfo.Msg = lib.MsgSuccess
	getinfo.Status = lib.CodeSuccess

	u.JSON(http.StatusOK, getinfo)
}

//解密
func WxDecrypt(u *gin.Context) {
	var getinfo lib.GetInfoN

	rawData := u.PostForm("rawData")
	fmt.Println(rawData)
	encryptedData := u.PostForm("encryptedData")
	signature := u.PostForm("signature")
	iv := u.PostForm("iv")
	session_key := u.PostForm("session_key")

	//var info interface{}
	// 解密用户信息
	//
	// @rawData 不包括敏感信息的原始数据字符串, 用于计算签名。
	// @encryptedData 包括敏感数据在内的完整用户信息的加密数据
	// @signature 使用 sha1( rawData + session_key ) 得到字符串, 用于校验用户信息
	// @iv 加密算法的初始向量
	// @ssk 微信 session_key
	userinfo, err := weapp.DecryptUserInfo(rawData, encryptedData, signature, iv, session_key)
	if err != nil {
		u.JSON(http.StatusOK, lib.MapDataError{lib.CodeEcrypt, err.Error()})
		return
	}
	//phone , err := weapp.DecryptPhoneNumber(session_key, encryptedData, iv)

	getinfo.Data = userinfo
	getinfo.Msg = lib.MsgSuccess
	getinfo.Status = lib.CodeSuccess

	u.JSON(http.StatusOK, getinfo)
}

//提现
func WxWithDraw(u *gin.Context) {

	var getinfo lib.GetInfoN
	//生成时间戳
	nanos := time.Now().UnixNano()
	tradeNo := strconv.FormatInt(nanos, 10)

	// 新建退款订单
	form := payment.Transferer{
		// 必填 ...
		AppID:  "APPID",
		MchID:  MchID,
		Amount: 100, //"总金额(分)",
		//OutRefundNo: "商户退款单号",
		OutTradeNo: tradeNo, //"商户订单号", // or TransactionID: "微信订单号",
		ToUser:     "ozjfE5O5hFU0cQBW4eJeaWhvIjTc",
		Desc:       "转账描述", // 若商户传入, 会在下发给用户的退款消息中体现退款原因

		/*// 选填 ...
		IP: "发起转账端 IP 地址", // 若商户传入, 会在下发给用户的退款消息中体现退款原因
		CheckName: "校验用户姓名选项 true/false",
		RealName: "收款用户真实姓名", // 如果 CheckName 设置为 true 则必填用户真实姓名
		Device:   "发起转账设备信息",*/
	}

	// 需要证书
	res, err := form.Transfer(PaySecret, "conf/cert/apiclient_cert.pem", "conf/cert/apiclient_key.pem")
	if err != nil {
		u.JSON(http.StatusOK, lib.GetMapDataError(lib.CodeWxWithDraw, err.Error()))
		return
	}

	//fmt.Printf("返回结果: %#v", res)
	getinfo.Data = res
	getinfo.Msg = lib.MsgSuccess
	getinfo.Status = lib.CodeSuccess
	u.JSON(http.StatusOK, getinfo)
}

//支付,范围对应支付的多个(5)参数
func WxPay(u *gin.Context) {

	var getinfo lib.GetInfoN

	openid := u.PostForm("openid")
	price, _ := strconv.Atoi(u.PostForm("price"))

	//生成时间戳
	nanos := time.Now().UnixNano()
	tradeNo := strconv.FormatInt(nanos, 10)
	// 新建支付订单
	form := payment.Order{
		// 必填
		AppID:      AppID,
		MchID:      MchID,
		Body:       "墅懒日租",
		NotifyURL:  "通知地址",
		OpenID:     openid,
		OutTradeNo: tradeNo, //"商户订单号",
		TotalFee:   price,

		// 选填 ...
		/*IP:        "发起支付终端IP",
		NoCredit:  "是否允许使用信用卡",
		StartedAt: "交易起始时间",
		ExpiredAt: "交易结束时间",
		Tag:       "订单优惠标记",
		Detail:    "商品详情",
		Attach:    "附加数据",*/
	}

	res, err := form.Unify(PaySecret)
	if err != nil {
		u.JSON(http.StatusOK, lib.GetMapDataError(lib.CodeWxPay, err.Error()))
		return
	}

	//fmt.Printf("返回结果: %#v", res)

	// 获取小程序前点调用支付接口所需参数
	params, err := payment.GetParams(res.AppID, PaySecret, res.NonceStr, res.PrePayID)
	if err != nil {
		u.JSON(http.StatusOK, lib.GetMapDataError(lib.CodeWxPay, err.Error()))
		return
	}

	getinfo.Data = params
	getinfo.Msg = lib.MsgSuccess
	getinfo.Status = lib.CodeSuccess
	u.JSON(http.StatusOK, getinfo)
}

//获得access_token
func GetAccessToken(u *gin.Context) {
	var getinfo lib.GetInfoN

	te_uri := "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + AppID + "&secret=" + Secret
	res, _ := http.Get(te_uri)
	body, _ := ioutil.ReadAll(res.Body)
	//fmt.Printf("返回结果: %#v", res)
	json.Unmarshal(body, &getinfo.Data)
	getinfo.Msg = lib.MsgSuccess
	getinfo.Status = lib.CodeSuccess
	u.JSON(http.StatusOK, getinfo)
}

//二维码,业务量多的情况
func GetQRCode(u *gin.Context) {
	//var getinfo lib.GetInfoN

	scene := u.Query("scene")
	page := u.Query("page")

	te_uri := "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + AppID + "&secret=" + Secret
	res, _ := http.Get(te_uri)
	body, _ := ioutil.ReadAll(res.Body)
	//fmt.Printf("返回结果: %#v", res)
	var at AccessToken
	json.Unmarshal(body, &at)

	coder := code.QRCoder{
		Scene:     scene, // 参数数据
		Page:      page,  // 识别二维码后进入小程序的页面链接
		Width:     430,   // 图片宽度
		IsHyaline: false, // 是否需要透明底色
		AutoColor: true,  // 自动配置线条颜色, 如果颜色依然是黑色, 则说明不建议配置主色调
		LineColor: code.Color{ //  AutoColor 为 false 时生效, 使用 rgb 设置颜色 十进制表示
			R: "50",
			G: "50",
			B: "50",
		},
	}

	// token: 微信 access_token
	resu, err := coder.UnlimitedAppCode(at.AccessToken)
	defer resu.Body.Close()
	if err != nil {
		u.JSON(http.StatusOK, lib.GetMapDataError(lib.CodeWx, err.Error()))
		return
	}

	bodyu, _ := ioutil.ReadAll(resu.Body)
	u.Writer.Header().Add("Content-Type", "image/png")
	u.Writer.Write(bodyu)
}

// =================================
// ==== 公众号开发
// ==============================
//const (
//	wxAppId     = "wx3a5a6f00a5450bef"
//	wxAppSecret = "4df4b09f71249f092b7e9dc7253ff3dd"
//
//	wxOriId         = "gh_9c5d6cebb3f7"
//	wxToken         = "test"
//	wxEncodedAESKey = "kdcerQmN8beu3runEETgPEWMo6Ae9BDDClyyK3uIJ3V"
//)

const (
	wxAppId     = "wx16d99df58aee063c"
	wxAppSecret = "fd7526ec069b4fe062919325ae54078c"

	wxOriId         = ""
	wxToken         = "test"
	wxEncodedAESKey = ""
)

var (
	// 下面两个变量不一定非要作为全局变量, 根据自己的场景来选择.
	msgHandler core.Handler
	msgServer  *core.Server
)

func init() {
	mux := core.NewServeMux()
	mux.DefaultMsgHandleFunc(defaultMsgHandler)
	mux.DefaultEventHandleFunc(defaultEventHandler)
	mux.MsgHandleFunc(request.MsgTypeText, textMsgHandler)
	mux.EventHandleFunc(menu.EventTypeClick, menuClickEventHandler)

	msgHandler = mux
	msgServer = core.NewServer(wxOriId, wxAppId, wxToken, wxEncodedAESKey, msgHandler, nil)
}

func textMsgHandler(ctx *core.Context) {
	log.Printf("收到文本消息:\n%s\n", ctx.MsgPlaintext)

	msg := request.GetText(ctx.MixedMsg)
	resp := response.NewText(msg.FromUserName, msg.ToUserName, msg.CreateTime, msg.Content)
	ctx.RawResponse(resp) // 明文回复
	//ctx.AESResponse(resp, 0, "测试", nil) // aes密文回复
}

func defaultMsgHandler(ctx *core.Context) {
	log.Printf("收到消息:\n%s\n", ctx.MsgPlaintext)
	ctx.NoneResponse()
}

func menuClickEventHandler(ctx *core.Context) {
	log.Printf("收到菜单 click 事件:\n%s\n", ctx.MsgPlaintext)

	//menu.Create()
	event := menu.GetClickEvent(ctx.MixedMsg)
	resp := response.NewText(event.FromUserName, event.ToUserName, event.CreateTime, "收到 click 类型的事件")
	//ctx.RawResponse(resp) // 明文回复
	ctx.AESResponse(resp, 0, "", nil) // aes密文回复
}

func defaultEventHandler(ctx *core.Context) {
	log.Printf("收到事件:\n%s\n", ctx.MsgPlaintext)
	log.Println("事件类型", ctx.MixedMsg.EventKey)
	ctx.NoneResponse()
}

// wxCallbackHandler 是处理回调请求的 http handler.
//  1. 不同的 web 框架有不同的实现
//  2. 一般一个 handler 处理一个公众号的回调请求(当然也可以处理多个, 这里我只处理一个)
func WxCall(c *gin.Context) {
	r := c.Request
	w := c.Writer
	msgServer.ServeHTTP(w, r, nil)
}

// === 公众号 api 调用
var (
	accessTokenServer core.AccessTokenServer = core.NewDefaultAccessTokenServer(wxAppId, wxAppSecret, nil)
	wechatClient      *core.Client           = core.NewClient(accessTokenServer, nil)
)

// 获得微信服务号二维码
func WxQrCode(c *gin.Context) {

	qr, err := qrcode.CreatePermQrcode(wechatClient, 2)
	if err != nil {
		log.Println("二维码生成错误: ", err)
		return
	}
	// cookie 验证访问唯一性
	// 简单判断用户访问唯一性

	_, err = c.Cookie("view")
	if err != nil {
		// 设置cookie
		log.Println("初次访问")
		// 访问数加一
		id := c.Query("id")
		if id != "" {
			db.DB.Exec("update `sharer` set view_num = view_num + 1 where id = ?", id)
		}
		// 设置cookie
		c.SetCookie("view", "view", 0, "/", "", false, true)
	} else {
		log.Println("已访问")
	}

	qrcode.DownloadToWriter(qr.Ticket, c.Writer, wechatClient.HttpClient)
}

// 普通二维码生成
func PQrcode(u *gin.Context) {
	url := u.Query("url")
	w := u.Writer

	var err error
	defer func() {
		if err != nil {
			w.WriteHeader(500)
			return
		}
	}()
	png, err := qrcode2.Encode(url, qrcode2.Medium, 256)
	if err != nil {
		return
	}
	w.Header().Set("Content-Type", "image/png")
	w.Header().Set("Content-Length", fmt.Sprintf("%d", len(png)))
	w.Write(png)
}
