package distribute

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/distribute"
	"strconv"
)

//获得馆主对应的分销记录
func GetDistributes(u *gin.Context) {
	introducer := u.Query("introducer")
	introducer_id := u.Query("introducer_id")
	clientPage,_ := strconv.Atoi(u.Query("clientPage"))
	everyPage,_ := strconv.Atoi(u.Query("everyPage"))
	ss := distribute.GetDistributes(introducer,introducer_id,clientPage,everyPage)
	u.JSON(http.StatusOK, ss)
}