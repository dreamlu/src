package venue

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/venue"
)

//获得场馆标签
func GetVenueLabels(u *gin.Context) {
	ss := venue.GetVenueLabels()
	u.JSON(http.StatusOK, ss)
}

//根据年月统计数据
func GetVenueMonthData(u *gin.Context){
	venue_id := u.Query("venue_id")
	year := u.Query("year")
	month := u.Query("month")
	ss := venue.GetVenueMonthData(venue_id,year,month)
	u.JSON(http.StatusOK, ss)
}

//获得场馆下拉框
func GetVenues(u *gin.Context) {
	ss := venue.GetVenues()
	u.JSON(http.StatusOK, ss)
}

//场馆信息分页
func GetVenueBySearchX(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venue.GetVenueBySearchX(values)
	u.JSON(http.StatusOK, ss)
}

//根据id获得场馆
func GetVenueById(u *gin.Context) {
	id := u.Query("id")
	ss := venue.GetVenueById(id)
	u.JSON(http.StatusOK, ss)
}

//场馆信息分页
func GetVenueBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venue.GetVenueBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//场馆信息删除
func DeleteVenueById(u *gin.Context) {
	id := u.Param("id")
	ss := venue.DeleteVenueByid(id)
	u.JSON(http.StatusOK, ss)
}

//场馆信息修改
func UpdateVenue(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venue.UpdateVenue(values)
	u.JSON(http.StatusOK, ss)
}

//新增场馆信息
func CreateVenue(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venue.CreateVenue(values)
	u.JSON(http.StatusOK, ss)
}
