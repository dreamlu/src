package order

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/order"
)

//根据id获得订单
func GetOrderById(u *gin.Context) {
	id := u.Query("id")
	ss := order.GetOrderById(id)
	u.JSON(http.StatusOK, ss)
}

//订单信息分页
func GetOrderBySearchX(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := order.GetOrderBySearchX(values)
	u.JSON(http.StatusOK, ss)
}

//订单信息分页
func GetOrderBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := order.GetOrderBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//订单信息删除
func DeleteOrderById(u *gin.Context) {
	id := u.Param("id")
	ss := order.DeleteOrderByid(id)
	u.JSON(http.StatusOK, ss)
}

//订单信息修改
func UpdateOrder(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := order.UpdateOrder(values)
	u.JSON(http.StatusOK, ss)
}

//订单信息补充
func UpdateOrderB(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := order.UpdateOrderB(values)
	u.JSON(http.StatusOK, ss)
}

//新增订单信息
func CreateOrder(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := order.CreateOrder(values)
	u.JSON(http.StatusOK, ss)
}

//新增订单信息
func CreateOrderX(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := order.CreateOrderX(values)
	u.JSON(http.StatusOK, ss)
}