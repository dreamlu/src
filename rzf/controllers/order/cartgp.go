package order

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/order"
)

var pp order.Cartgp

//创建组队购物车信息
func CreateCartgp(u *gin.Context){
	client_ids := u.PostForm("client_ids")
	cart_id := u.PostForm("cart_id")
	venue_id := u.PostForm("venue_id")
	discounts := u.PostForm("discounts")
	ss := pp.CreateCartgp(client_ids, cart_id, venue_id, discounts)
	u.JSON(http.StatusOK, ss)
}

//获得对应组队购物车信息
func GetCartgp(u *gin.Context) {
	cart_ids := u.Query("cart_ids")
	ss := pp.GetCartgp(cart_ids)
	u.JSON(http.StatusOK, ss)
}