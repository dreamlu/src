package order

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/order"
)

var p order.Ordergp

//创建组队订单信息
func CreateOrdergp(u *gin.Context){
	client_ids := u.PostForm("client_ids")
	order_id := u.PostForm("order_id")
	venue_id := u.PostForm("venue_id")
	discounts := u.PostForm("discounts")
	ss := p.CreateOrdergp(client_ids, order_id, venue_id, discounts)
	u.JSON(http.StatusOK, ss)
}

//获得对应组队订单信息
func GetOrdergp(u *gin.Context) {
	order_id := u.Query("order_id")
	ss := p.GetOrdergp(order_id)
	u.JSON(http.StatusOK, ss)
}

//获得对应组队订单信息
func GetIsGp(u *gin.Context) {
	key_id := u.Query("key_id")
	client_id := u.Query("client_id")
	ss := p.GetIsGp(client_id, key_id)
	u.JSON(http.StatusOK, ss)
}