package device

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/device"
)

//根据id获得设备
func GetDeviceById(u *gin.Context) {
	id := u.Query("id")
	ss := device.GetDeviceById(id)
	u.JSON(http.StatusOK, ss)
}

//设备信息分页
func GetDeviceBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := device.GetDeviceBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//设备信息删除
func DeleteDeviceById(u *gin.Context) {
	id := u.Param("id")
	ss := device.DeleteDeviceByid(id)
	u.JSON(http.StatusOK, ss)
}

//设备信息修改
func UpdateDevice(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := device.UpdateDevice(values)
	u.JSON(http.StatusOK, ss)
}

//新增设备信息
func CreateDevice(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := device.CreateDevice(values)
	u.JSON(http.StatusOK, ss)
}