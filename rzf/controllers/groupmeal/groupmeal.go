package groupmeal

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/groupmeal"
)

//根据id获得场馆
func GetGroupmealById(u *gin.Context) {
	id := u.Query("id")
	ss := groupmeal.GetGroupmealById(id)
	u.JSON(http.StatusOK, ss)
}

//场馆信息分页
func GetGroupmealBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := groupmeal.GetGroupmealBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//场馆信息删除
func DeleteGroupmealById(u *gin.Context) {
	id := u.Param("id")
	ss := groupmeal.DeleteGroupmealByid(id)
	u.JSON(http.StatusOK, ss)
}

//场馆信息修改
func UpdateGroupmeal(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := groupmeal.UpdateGroupmeal(values)
	u.JSON(http.StatusOK, ss)
}

//新增场馆信息
func CreateGroupmeal(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := groupmeal.CreateGroupmeal(values)
	u.JSON(http.StatusOK, ss)
}