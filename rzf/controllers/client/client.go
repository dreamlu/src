package client

import (
	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/client"
	"rzf/util/lib"
)

//客户管理数据
func GetClientData(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form
	ss := client.GetClientData(values)
	u.JSON(http.StatusOK, ss)
}

//根据unionid获得客户、用户id
func GetClientId(u *gin.Context) {
	openid := u.Query("openid")
	ss := client.GetClientId(openid)
	u.JSON(http.StatusOK, ss)
}

//根据id获得客户、用户
func GetClientByIdX(u *gin.Context) {
	id := u.Query("id")
	ss := client.GetClientByIdX(id)
	u.JSON(http.StatusOK, ss)
}

//客户、用户信息分页
func GetClientBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := client.GetClientBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//客户、用户信息删除
func DeleteClientById(u *gin.Context) {
	id := u.Param("id")
	ss := client.DeleteClientByid(id)
	u.JSON(http.StatusOK, ss)
}

//客户、用户信息修改
func UpdateClient(u *gin.Context) {
	phone := u.PostForm("phone")
	valid := validation.Validation{}
	valid.Mobile(phone, "phone")

	if valid.HasErrors() && phone != "" {
		u.JSON(http.StatusOK, lib.MapPhone)
	}else {
		u.Request.ParseForm()
		values := u.Request.Form //在使用之前需要调用ParseForm方法
		ss := client.UpdateClient(values)
		u.JSON(http.StatusOK, ss)
	}
}