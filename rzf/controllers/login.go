package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/util"
	"rzf/util/db"
	"rzf/util/lib"
	"strconv"
)

type User struct {
	ID       int    `json:"id"`
	Nickname string `json:"nickname"`
	Password string `json:"password"`
}

//登录
func Login(u *gin.Context) {
	var info interface{}
	var user User
	var sql string

	nickname := u.PostForm("nickname")
	password := u.PostForm("password")

	flag := u.PostForm("flag")
	switch flag {
	//馆主、管理员
	case "0":
		sql = "SELECT id,nickname,password FROM `venuer` WHERE nickname = ?"
	case "1": //代理
		sql = "SELECT id,nickname,password FROM `proxyer` WHERE nickname = ? and ispass=1"//审核通过
	}
	dba := db.DB.Raw(sql, nickname).Scan(&user)
	num := dba.RowsAffected
	if dba.Error == nil && num > 0 {
		password = util.AesEn(password)
		if user.Password == password {
			//name	string	cookie_key
			//value	string	cookie_val
			//maxAge	int	生存期（秒）
			//path	string	有效域
			//domain	string	有效域名
			//secure	bool	是否安全传输 是则只走https
			//httpOnly	bool	是否仅网络使用 是则js无法获取
			// encript
			//字符为16的倍数
			strID := strconv.Itoa(user.ID)
			key := "-Iloveyouchinese"
			user_id, err := util.Encrypt([]byte(strID + string([]byte(key)[:len(key)-len(strID)])))
			if err != nil {
				fmt.Println("cookie加密错误Encrypt: ", err)
				return
			}
			//u.SetCookie("uid", strconv.Itoa(user.Id), 1800, "/", "*", false, true)
			u.SetCookie("uid", string(user_id), 24*3600, "/", "*", false, true)
			//this.SetSecureCookie(beego.AppConfig.String("secertkey"), "uid", strconv.Itoa(v.Id))
			info = map[string]interface{}{"status": lib.CodeSuccess, "msg": "请求成功", "uid": strconv.Itoa(user.ID)}
		} else {
			info = lib.MapCountErr
		}
	} else {
		info = lib.MapNoCount
	}

	u.JSON(http.StatusOK, info)
}
