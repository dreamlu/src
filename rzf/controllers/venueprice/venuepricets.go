package venueprice

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/venueprice"
)

//根据id获得特殊日期
func GetVenuepricetsById(u *gin.Context) {
	id := u.Query("id")
	ss := venueprice.GetVenuepricetsById(id)
	u.JSON(http.StatusOK, ss)
}

//特殊日期信息分页
func GetVenuepricetsBySearchX(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venueprice.GetVenuepricetsBySearchX(values)
	u.JSON(http.StatusOK, ss)
}

//特殊日期信息分页
func GetVenuepricetsBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venueprice.GetVenuepricetsBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//特殊日期信息删除
func DeleteVenuepricetsById(u *gin.Context) {
	id := u.Param("id")
	ss := venueprice.DeleteVenuepricetsByid(id)
	u.JSON(http.StatusOK, ss)
}

//特殊日期信息修改
func UpdateVenuepricets(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venueprice.UpdateVenuepricets(values)
	u.JSON(http.StatusOK, ss)
}

//新增特殊日期信息
func CreateVenuepricets(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venueprice.CreateVenuepricets(values)
	u.JSON(http.StatusOK, ss)
}
