package venueprice

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/venueprice"
)

//根据id获得时间段
func GetVenuepriceById(u *gin.Context) {
	id := u.Query("id")
	ss := venueprice.GetVenuepriceById(id)
	u.JSON(http.StatusOK, ss)
}

//时间段信息分页,小程序端
func GetVenuepriceBySearchX(u *gin.Context) {
	venue_id := u.Query("venue_id")
	playtime := u.Query("playtime")
	week := u.Query("week")
	ss := venueprice.GetVenuepriceBySearchX(venue_id,playtime,week)
	u.JSON(http.StatusOK, ss)
}

//时间段信息分页
func GetVenuepriceBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venueprice.GetVenuepriceBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//时间段信息删除
func DeleteVenuepriceById(u *gin.Context) {
	id := u.Param("id")
	ss := venueprice.DeleteVenuepriceByid(id)
	u.JSON(http.StatusOK, ss)
}

//时间段信息修改
func UpdateVenueprice(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venueprice.UpdateVenueprice(values)
	u.JSON(http.StatusOK, ss)
}

//新增时间段信息
func CreateVenueprice(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venueprice.CreateVenueprice(values)
	u.JSON(http.StatusOK, ss)
}
