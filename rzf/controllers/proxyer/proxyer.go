package proxyer

import (
	"fmt"
	"github.com/astaxie/beego/validation"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"math/rand"
	"net/http"
	"rzf/models/proxyer"
	"rzf/util"
	"rzf/util/db"
	"rzf/util/lib"
	"strings"
	"time"
)

//根据id获得代理以及客户数量
func GetProxyerByIdX(u *gin.Context) {
	id := u.Query("id")
	ss := proxyer.GetProxyerByIdX(id)
	u.JSON(http.StatusOK, ss)
}

//根据id获得代理
func GetProxyerById(u *gin.Context) {
	id := u.Query("id")
	ss := proxyer.GetProxyerById(id)
	u.JSON(http.StatusOK, ss)
}

//代理信息分页
func GetProxyerBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := proxyer.GetProxyerBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//代理信息删除
func DeleteProxyerById(u *gin.Context) {
	id := u.Param("id")
	ss := proxyer.DeleteProxyerByid(id)
	u.JSON(http.StatusOK, ss)
}

//代理信息修改
func UpdateProxyer(u *gin.Context) {
	phone := u.PostForm("phone")
	valid := validation.Validation{}
	valid.Mobile(phone, "phone")

	if valid.HasErrors() && phone != "" {
		u.JSON(http.StatusOK, lib.MapPhone)
	}else {
		u.Request.ParseForm()
		values := u.Request.Form //在使用之前需要调用ParseForm方法
		ss := proxyer.UpdateProxyer(values)
		u.JSON(http.StatusOK, ss)
	}
}

//新增代理信息
func CreateProxyer(u *gin.Context) {
	phone := u.PostForm("phone")
	valid := validation.Validation{}
	valid.Mobile(phone, "phone")

	if valid.HasErrors() && phone != "" {
		u.JSON(http.StatusOK, lib.MapPhone)
	}else {
		u.Request.ParseForm()
		values := u.Request.Form //在使用之前需要调用ParseForm方法
		ss := proxyer.CreateProxyer(values)
		u.JSON(http.StatusOK, ss)
	}
}

//获取短信验证码
func SendSMS(u *gin.Context) {
	session := sessions.Default(u)
	var info interface{}
	templateCode := "SMS_151880459"
	phoneNumbers := u.Query("phone")

	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	vcode := fmt.Sprintf("%06v", rnd.Int31n(1000000))
	templateParam := "{\"code\":\"" + vcode + "\"}"

	pcode := phoneNumbers+"&"+vcode
	//key := "-IloveyouchineseIIloveyouchinese"
	//pcodeb, err := util.Encrypt([]byte(pcode + string([]byte(key)[:len(key)-len(pcode)])))
	//if err != nil {
	//	fmt.Println("cookie加密错误Encrypt: ", err)
	//	return
	//}
	//u.SetCookie("phocode", string(pcodeb), 24*3600, "/", "*", false, true)

	info = proxyer.SendMsg(phoneNumbers, templateCode, templateParam)

	session.Set("pcode", pcode)
	session.Save()
	u.JSON(http.StatusOK, info)
}

//根据短信验证码注册身份
func Register(u *gin.Context) {
	session := sessions.Default(u)
	var info interface{}

	//验证码
	code := u.PostForm("code")
	password := u.PostForm("password")
	name := u.PostForm("name")
	password = util.AesEn(password)
	//用户phone和code验证码
	//cookie,err := u.Request.Cookie("phocode")
	//if err != nil{
	//	fmt.Println("cookie-->uid不存在")
	//}
	//ss, _ := url.QueryUnescape(cookie.Value)
	//// 解密
	//cs, err := util.Decrypt([]byte(ss))
	//phcode := strings.Split(string(cs),"-")
	//if err != nil {
	//	fmt.Println("cookie解密失败: ", err)
	//	return
	//}

	pcode := session.Get("pcode")
	pco := strings.Split(pcode.(string), "&")
	if len(pco) > 0 {
		phone := pco[0]
		pcode := pco[1]
		//短信验证码判断
		if pcode != code {
			info = map[string]interface{}{"status": 271, "msg": "验证码不正确"}
		} else {
			db.DB.Exec("insert `proxyer`(phone,nickname,password,name) value(?,?,?,?)",phone,phone,password,name) //账号默认phone
			info = map[string]interface{}{"status":271,"msg":"注册成功"}
		}
	}else {
		info = map[string]interface{}{"status":271,"msg":"请注册或登录"}
	}
	u.JSON(http.StatusOK, info)
}
