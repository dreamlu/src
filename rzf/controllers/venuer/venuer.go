package venuer

import (
	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/venuer"
	"rzf/util/lib"
)

//获得馆主下拉框
func GetVenuers(u *gin.Context) {
	ss := venuer.GetVenuers()
	u.JSON(http.StatusOK, ss)
}

//根据id获得馆主
func GetVenuerByIdX(u *gin.Context) {
	id := u.Query("id")
	ss := venuer.GetVenuerByIdX(id)
	u.JSON(http.StatusOK, ss)
}

//根据id获得馆主
func GetVenuerById(u *gin.Context) {
	id := u.Query("id")
	ss := venuer.GetVenuerById(id)
	u.JSON(http.StatusOK, ss)
}

//馆主信息分页
func GetVenuerBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venuer.GetVenuerBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//馆主信息删除
func DeleteVenuerById(u *gin.Context) {
	id := u.Param("id")
	ss := venuer.DeleteVenuerByid(id)
	u.JSON(http.StatusOK, ss)
}

//馆主信息修改
func UpdateVenuer(u *gin.Context) {
	//phone := u.PostForm("phone")
	//valid := validation.Validation{}
	//valid.Mobile(phone, "phone")

	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := venuer.UpdateVenuer(values)
	u.JSON(http.StatusOK, ss)
}

//新增馆主信息
func CreateVenuer(u *gin.Context) {
	phone := u.PostForm("phone")
	valid := validation.Validation{}
	valid.Mobile(phone, "phone")

	if valid.HasErrors() && phone != "" {
		u.JSON(http.StatusOK, lib.MapPhone)
	}else {
		u.Request.ParseForm()
		values := u.Request.Form //在使用之前需要调用ParseForm方法
		ss := venuer.CreateVenuer(values)
		u.JSON(http.StatusOK, ss)
	}
}
