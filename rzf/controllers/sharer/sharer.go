package sharer

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/sharer"
)

//根据id获得分销推广
func GetSharerById(u *gin.Context) {
	id := u.Query("id")
	ss := sharer.GetSharerById(id)
	u.JSON(http.StatusOK, ss)
}

//分销推广信息分页
func GetSharerBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := sharer.GetSharerBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//分销推广信息删除
func DeleteSharerById(u *gin.Context) {
	id := u.Param("id")
	ss := sharer.DeleteSharerByid(id)
	u.JSON(http.StatusOK, ss)
}

//分销推广信息修改
func UpdateSharer(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := sharer.UpdateSharer(values)
	u.JSON(http.StatusOK, ss)
}

//新增分销推广信息
func CreateSharer(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := sharer.CreateSharer(values)
	u.JSON(http.StatusOK, ss)
}

