package alipay

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/smartwalle/alipay"
	"net/http"
	url2 "net/url"
	"rzf/conf"
	"rzf/models/order"
	"rzf/util"
	"rzf/util/db"
	"rzf/util/lib"
	"strconv"
	"strings"
	"time"
)

var appId = "2018102361789243"
var aliPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwO42GOKcV0nA1A6lDWo2T60J8RE12fN8fS24iKqDr92HvNMjjYVkniEnnrSC40ApyyPJ1f2ECJcaGKUrLkBp3RwKuZoF4guOKuHa6+xd8yvjZ4IcnmTPqHHKlQUIkSbk/tT3ALygJGsyFGUZwGUoQXNtl1x3O8pWi5+pS1HJ+4yLkfDrMrfR+kjm1CMuqtyf1ZjQ1wsAoH25/iN4BY0sPa0J1p1MsrkSmJoUgP0PtvJh+1byDJrdkNf0MvH+nYZxijaPa2ODQL7aDjZowE/NCOYy0LEZycAp69/uv2774bPbxgRWkLN8rZsZChrp/8f0PbTU0IP5oExhVaSMgYQ2IwIDAQAB"
var privateKey = "MIIEpAIBAAKCAQEAwO42GOKcV0nA1A6lDWo2T60J8RE12fN8fS24iKqDr92HvNMjjYVkniEnnrSC40ApyyPJ1f2ECJcaGKUrLkBp3RwKuZoF4guOKuHa6+xd8yvjZ4IcnmTPqHHKlQUIkSbk/tT3ALygJGsyFGUZwGUoQXNtl1x3O8pWi5+pS1HJ+4yLkfDrMrfR+kjm1CMuqtyf1ZjQ1wsAoH25/iN4BY0sPa0J1p1MsrkSmJoUgP0PtvJh+1byDJrdkNf0MvH+nYZxijaPa2ODQL7aDjZowE/NCOYy0LEZycAp69/uv2774bPbxgRWkLN8rZsZChrp/8f0PbTU0IP5oExhVaSMgYQ2IwIDAQABAoIBABJE/H5yxxdE9queD2crzaTR4AUh+hV/hyRbdgQLEZlPwxDQdadrIFIRFa1jrplSdCK2auCGt0AQ/vmiempQ0zWK+EEHItN7AKVy6cY5hdQHLeuZWJhvN3LeA68E4QDNzBWNmS5KrmeEdyOTR1L+wUchZRlWOjxBNtzV67QkNJpLVFV2kZ6Ad7cJxmGGgwe1B+oPc5EEzIhMfMSDnNHKdTSQCUS9ClWN9Xn2y/p9jgqpXJOzTUb1gMhC/OxCZoJ8Ruu0z4csMe9qfoatRYMhSJ9K0cVprfBm1L7nKdLVohqqbePQEIj74V0YApGL1hwz8iEWdl/Gku792eCfRPT9sWECgYEA/0Mul1rmr4lmW16hPD/FstmmNy0GUbb326kA2h0v9LL9jutSbVWqApIv0rLDfkojEBrOwCk0NKGrZ+M/FrJ6TKpJulk0Hgono4WWq/4qRI/MAXWXPI9aZMf7NTKQH+dLGn1/dq5eYHSqF0nf47CVu400dy8/JEeOPTTmkTR830sCgYEAwXzsHFu7bYl9wLNtyzit1dbEFc72rTeG0IN8KF5evk5Tf6wViGIUTNTSIMCMirvJqJ/GHNb+Y7nOM2gQgzsKb58CEQzAYtY2RuctoYQ8+TpXnK4JeSa7B1EbjtCgSPMLbHZkMJvxL/uBETJlHT4y3iS4fQ2Npv/GRdnovAMUxYkCgYEA5nGjNTb2PZCgXGpgy0CTXTRKb1NxegHhX2gCgeaJrbKNYCIn5tbIRcvt6pxCCtgpMVfSQP6xx7YQRRiNkxMLf/zX1KjFeT0XrfNjI+CwDfd3Z/Xd0+li5Mr5iXwdSwAG+GJF2UHvp8qK571kp2x6sLf4noSDz1/eFHmWkY4+EWMCgYEAoR8TxvGkfCyYKVXBQg9qwr1bAAYeaFPq0cwMIdmj77lt0s1MCQwTd39pk5iD8Cs8XEzgPrfRkt3xFeG8jmsl35l7sRL7SdKz1AtCdlOWKrwopPJQNTDJYehqmFymfNjXVHSqLchN6fT3n4hlpxpaZZ4SeyBEKyKmyxgQtC+kpnECgYALmbNZ8X68o20PRFauRsEkbcnhnLNvrvdblsN/A7m1VI1yqx0iYdO+sWBWtkZ7sXzsktahO6DimpDDxEeXysDxTnn/DqJjvJnOnH3rODStLSXfuLxi8B669dydpzAR2IdLcFBHAXVWYDJTBJlNdNFZm2hntGPOhm1FXvPUv/nebQ=="
var client = alipay.New(appId, aliPublicKey, privateKey, true)

//支付宝支付
func Alipay(u *gin.Context) {

	money := u.PostForm("money")
	order_id := u.PostForm("order_id")

	key := "-Iloveyouchinese"
	res, errs := util.Encrypt([]byte(order_id + string([]byte(key)[:len(key)-len(order_id)])))
	if errs != nil {
		fmt.Println("cookie加密错误Encrypt: ", errs)
		return
	}

	//生成时间戳
	nanos := time.Now().UnixNano()
	tradeNo := strconv.FormatInt(nanos, 10)

	var p = alipay.AliPayTradeWapPay{}
	//p.NotifyURL = "http://xxx"
	p.ReturnURL = conf.GetConfigValue("api")+"/alipay/payres?order_id="+url2.QueryEscape(string(res))
	p.Subject = "树懒日租"
	p.OutTradeNo = tradeNo //"传递一个唯一单号"
	p.TotalAmount = money
	p.ProductCode = "FAST_INSTANT_TRADE_PAY"

	var url, err = client.TradeWapPay(p)
	if err != nil {
		u.JSON(http.StatusOK,lib.GetMapDataError(lib.CodeAliPay,err.Error()))
		return
	}

	var payURL = url.String()
	u.JSON(http.StatusOK,map[string]interface{}{"status": lib.CodeSuccess, "msg": lib.MsgSuccess,"pay_url":payURL})
}

//支付宝通知
func AlipayRes(u *gin.Context) {
	var info interface{}
	order_id,_ := url2.QueryUnescape(u.Query("order_id"))
	//key := "-Iloveyouchinese"
	res, err := util.Decrypt([]byte(order_id))
	if err != nil {
		fmt.Println("cookie加密错误Encrypt: ", err)
		return
	}
	order_id = string(res)
	oids := strings.Split(order_id,"-")

	//开启事务
	tx := db.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	dba := tx.Exec("update `order` set status=0 where id=?",oids[0])
	num := dba.RowsAffected
	if dba.Error != nil {
		info = lib.GetMapDataError(lib.CodeSql, dba.Error.Error())
		u.JSON(http.StatusOK,info)
		if tx.Error != nil {
			tx.Rollback()
		}
		tx.Commit()
	} else if num == 0 && dba.Error == nil {
		info = lib.MapExistOrNo
		if tx.Error != nil {
			tx.Rollback()
		}
		tx.Commit()
		u.JSON(http.StatusOK,info)
	} else {
		//支付成功,进行分销,事务
		order.DestributeMoney(tx,oids[0])
		if tx.Error != nil {
			tx.Rollback()
		}
		tx.Commit()
		u.Redirect(http.StatusMovedPermanently,conf.GetConfigValue("domain")+"/wx.html")
	}
}