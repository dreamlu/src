package other

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/other"
)

//其他信息分页
func GetOther(u *gin.Context) {
	flag := u.Query("flag")
	ss := other.GetOther(flag)
	u.JSON(http.StatusOK, ss)
}

//其他信息修改
func UpdateOther(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := other.UpdateOther(values)
	u.JSON(http.StatusOK, ss)
}