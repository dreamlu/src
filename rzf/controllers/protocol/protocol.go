package protocol

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/protocol"
)

var p protocol.Protocol

//根据id
func GetById(u *gin.Context) {
	id := u.Query("id")
	ss := p.GetById(id)
	u.JSON(http.StatusOK, ss)
}

//修改
func Update(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form

	ss := p.Update(values)
	u.JSON(http.StatusOK, ss)
}
