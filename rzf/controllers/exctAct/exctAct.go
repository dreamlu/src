package exctAct

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/exctAct"
)

//根据id获得设备
func GetExctActById(u *gin.Context) {
	id := u.Query("id")
	ss := exctAct.GetExctActById(id)
	u.JSON(http.StatusOK, ss)
}

//设备信息分页
func GetExctActBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := exctAct.GetExctActBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//设备信息删除
func DeleteExctActById(u *gin.Context) {
	id := u.Param("id")
	ss := exctAct.DeleteExctActByid(id)
	u.JSON(http.StatusOK, ss)
}

//设备信息修改
func UpdateExctAct(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := exctAct.UpdateExctAct(values)
	u.JSON(http.StatusOK, ss)
}

//新增设备信息
func CreateExctAct(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := exctAct.CreateExctAct(values)
	u.JSON(http.StatusOK, ss)
}