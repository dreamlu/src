package carousel

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/carousel"
)

//根据id获得轮播图
func GetCarouselById(u *gin.Context) {
	id := u.Query("id")
	ss := carousel.GetCarouselById(id)
	u.JSON(http.StatusOK, ss)
}

//轮播图信息分页
func GetCarouselBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := carousel.GetCarouselBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//轮播图信息删除
func DeleteCarouselById(u *gin.Context) {
	id := u.Param("id")
	ss := carousel.DeleteCarouselByid(id)
	u.JSON(http.StatusOK, ss)
}

//轮播图信息修改
func UpdateCarousel(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := carousel.UpdateCarousel(values)
	u.JSON(http.StatusOK, ss)
}

//新增轮播图信息
func CreateCarousel(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := carousel.CreateCarousel(values)
	u.JSON(http.StatusOK, ss)
}

/*//修改轮播图账号密码
func UpdateAccount(u *gin.Context) {

}*/
