package label

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"rzf/models/label"
)

//根据id获得标签
func GetLabelX(u *gin.Context) {
	ss := label.GetLabels()
	u.JSON(http.StatusOK, ss)
}

//根据id获得标签
func GetLabelById(u *gin.Context) {
	id := u.Query("id")
	ss := label.GetLabelById(id)
	u.JSON(http.StatusOK, ss)
}

//标签信息分页
func GetLabelBySearch(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := label.GetLabelBySearch(values)
	u.JSON(http.StatusOK, ss)
}

//标签信息删除
func DeleteLabelById(u *gin.Context) {
	id := u.Param("id")
	ss := label.DeleteLabelByid(id)
	u.JSON(http.StatusOK, ss)
}

//标签信息修改
func UpdateLabel(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := label.UpdateLabel(values)
	u.JSON(http.StatusOK, ss)
}

//新增标签信息
func CreateLabel(u *gin.Context) {
	u.Request.ParseForm()
	values := u.Request.Form //在使用之前需要调用ParseForm方法
	ss := label.CreateLabel(values)
	u.JSON(http.StatusOK, ss)
}