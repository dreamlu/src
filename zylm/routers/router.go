package routers

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"github.com/astaxie/beego/plugins/cors"
	"zylm/controllers"
	"zylm/controllers/wx"
		)

//登录验证/api路由权限验证
var FilterUser = func(ctx *context.Context) {
	//预检请求
	if ctx.Input.Method() == "OPTIONS" {
		ctx.Output.SetStatus(200)
		ctx.Output.Header("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PATCH,DELETE")
		ctx.Output.Header("Access-Control-Allow-Origin", "http://localhost:8080")
		return
	}
	//验证小程序端不验证权限
	if ctx.Input.Header("Authorization") != "wechat" {
		//请求路由
		url := ctx.Input.URL()
		//用户在线id
		uid, ok := ctx.GetSecureCookie(beego.AppConfig.String("secertkey"), "uid")
		fmt.Println("用户id---->" + uid)

		if !ok && url != "/login/login" {
			ctx.WriteString("{\"status\": \"203\", \"msg\": \"请求非法\"}")
		}
	}
}

func init() {
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowOrigins:     []string{"http://localhost:8080"},
		AllowMethods:     []string{"GET", "POST", "OPTIONS", "PATCH", "DELETE"},
		AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "X-Requested-With", "Access-Control-Allow-Credentials"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))
	//beego.InsertFilter("*", beego.BeforeRouter, authz.NewAuthorizer(casbin.NewEnforcer("casbin/authz_model.conf", "casbin/authz_policy.csv")))
	beego.InsertFilter("/*", beego.BeforeRouter, FilterUser)
	beego.Include(&controllers.GameController{})
	beego.Include(&controllers.UserController{})
	beego.Include(&controllers.LoginController{})
	beego.Include(&controllers.FileController{})
	beego.Include(&controllers.UserGameController{})
	beego.Include(&controllers.BasicInfo{})
	beego.Include(&wx.GetWxEncryptedData{})
}
