package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["zylm/controllers:BasicInfo"] = append(beego.GlobalControllerRouter["zylm/controllers:BasicInfo"],
		beego.ControllerComments{
			Method: "GetBasicInfo",
			Router: `/basic/getbasicinfo`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:FileController"] = append(beego.GlobalControllerRouter["zylm/controllers:FileController"],
		beego.ControllerComments{
			Method: "GetFilePath",
			Router: `/file/getfilepath`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:GameController"] = append(beego.GlobalControllerRouter["zylm/controllers:GameController"],
		beego.ControllerComments{
			Method: "CreateGame",
			Router: `/game/creategame`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:GameController"] = append(beego.GlobalControllerRouter["zylm/controllers:GameController"],
		beego.ControllerComments{
			Method: "DeleteGameById",
			Router: `/game/deletegamebyid/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:GameController"] = append(beego.GlobalControllerRouter["zylm/controllers:GameController"],
		beego.ControllerComments{
			Method: "GetGameById",
			Router: `/game/getgamebyid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:GameController"] = append(beego.GlobalControllerRouter["zylm/controllers:GameController"],
		beego.ControllerComments{
			Method: "GetGameByPage",
			Router: `/game/getgamebypage`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:GameController"] = append(beego.GlobalControllerRouter["zylm/controllers:GameController"],
		beego.ControllerComments{
			Method: "GetGameBySearch",
			Router: `/game/getgamebysearch`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:GameController"] = append(beego.GlobalControllerRouter["zylm/controllers:GameController"],
		beego.ControllerComments{
			Method: "UpdateGame",
			Router: `/game/updategame`,
			AllowHTTPMethods: []string{"patch"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:LoginController"] = append(beego.GlobalControllerRouter["zylm/controllers:LoginController"],
		beego.ControllerComments{
			Method: "Login",
			Router: `/login/login`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:LoginController"] = append(beego.GlobalControllerRouter["zylm/controllers:LoginController"],
		beego.ControllerComments{
			Method: "Getopenid",
			Router: `/openid/getopenid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserController"],
		beego.ControllerComments{
			Method: "CreateUser",
			Router: `/user/createuser`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserController"],
		beego.ControllerComments{
			Method: "DeleteUserById",
			Router: `/user/deleteuserbyid/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetUserById",
			Router: `/user/getuserbyid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetUserByPage",
			Router: `/user/getuserbypage`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetUserBySearch",
			Router: `/user/getuserbysearch`,
			AllowHTTPMethods: []string{"patch"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetUserIdByOpenId",
			Router: `/user/getuseridbyopenid`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserController"],
		beego.ControllerComments{
			Method: "UpdateAccount",
			Router: `/user/updateaccount`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserController"],
		beego.ControllerComments{
			Method: "UpdateUser",
			Router: `/user/updateuser`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserController"],
		beego.ControllerComments{
			Method: "WithdrawMoney",
			Router: `/user/withdrawmoney`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserGameController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserGameController"],
		beego.ControllerComments{
			Method: "CreateUserGame",
			Router: `/usergame/createusergame`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserGameController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserGameController"],
		beego.ControllerComments{
			Method: "DeleteUserGameById",
			Router: `/usergame/deleteusergamebyid/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserGameController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserGameController"],
		beego.ControllerComments{
			Method: "ExportExcelByGameData",
			Router: `/usergame/exportexcelgamedata`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserGameController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserGameController"],
		beego.ControllerComments{
			Method: "GetUserGameByGameId",
			Router: `/usergame/getusergamebygameid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserGameController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserGameController"],
		beego.ControllerComments{
			Method: "GetUserGameByUserId",
			Router: `/usergame/getusergamebyuserid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["zylm/controllers:UserGameController"] = append(beego.GlobalControllerRouter["zylm/controllers:UserGameController"],
		beego.ControllerComments{
			Method: "UpdateUserGame",
			Router: `/usergame/updateusergame`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

}
