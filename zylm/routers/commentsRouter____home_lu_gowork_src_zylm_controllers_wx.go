package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["zylm/controllers/wx:GetWxEncryptedData"] = append(beego.GlobalControllerRouter["zylm/controllers/wx:GetWxEncryptedData"],
		beego.ControllerComments{
			Method: "GetWxEncryptedData",
			Router: `/wx/getwxencrypteddata`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

}
