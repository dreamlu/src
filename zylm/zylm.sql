/*
 Navicat MySQL Data Transfer

 Source Server         : lu_掌游联盟
 Source Server Type    : MySQL
 Source Server Version : 50722
 Source Host           : localhost:3306
 Source Schema         : zylm

 Target Server Type    : MySQL
 Target Server Version : 50722
 File Encoding         : 65001

 Date: 25/08/2018 16:44:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for game
-- ----------------------------
DROP TABLE IF EXISTS `game`;
CREATE TABLE `game`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '游戏信息',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '游戏名称',
  `category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '类别',
  `imageurl` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '封面图',
  `logo` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'logo图片',
  `introduce` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '游戏介绍',
  `modifyperson` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '修改人气',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '游戏地址',
  `targetappid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '目标appid',
  `publishdate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '上传时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of game
-- ----------------------------
INSERT INTO `game` VALUES (39, '你这是要上天', '展示区', 'static/file/20180822121646.jpeg', '', '疯狂冲击，小球从底层一直往上冲！', '400', '不知', 'wxa42102e20105cd25', '2018-08-21');
INSERT INTO `game` VALUES (45, '说唱变变变', '展示区', 'static/file/20180824170330.png', '', '带你的偶像小哥哥回家吧\n', '86', '', 'wxc6eb406d8a25c3b9', '2018-08-24');
INSERT INTO `game` VALUES (46, '脑力生存大逃杀', '展示区', 'static/file/20180824171844.png', '', '一款脑力生存游戏，大吉大利，今晚吃鸡', '92', '', 'wxe626160017bef7b1', '2018-08-24');
INSERT INTO `game` VALUES (52, '猜歌名来挑战', '轮播区', 'static/file/20180825152855.png', 'static/file/20180825152859.png', '你能猜出多少BGM？ ', '69', 'game/gamePage.html?channel=caigesgm', 'wx57546e463a82e4af ', '2018-08-24');
INSERT INTO `game` VALUES (53, '三国杀', '展示区', 'static/file/20180824173449.png', '', '一分钟也能杀个痛快！', '42', '', 'wxe928e6e9fb9318d1', '2018-08-24');
INSERT INTO `game` VALUES (55, '西游后传', '展示区', 'static/file/20180824173944.jpg', '', '神兽，炼妖，变异，打造最强召唤兽', '158', '\'?channel=mengmob212\'', 'wx78caa30cd32c16b9', '2018-08-24');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '小程序openid',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户昵称',
  `joindate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '加入日期',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '电话',
  `totalcoin` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '累计金币',
  `nowcoin` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '现有金币',
  `withdrawcoin` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '待提现',
  `withdrawcoined` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '已提现',
  `issigntime` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `isfive` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `isfivetime` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `isshare` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `tasktime` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `task` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `openid`(`openid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1489 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '1', 'admin', '2018-01-01', '15869190407', '2', '1', '4', '3', '', '', '', '', '', '');
INSERT INTO `user` VALUES (1475, 'oW9ee4nwqigFRdD-dSsLHMgWG8b0', '', '', '15968176721', '400', '400', '0.4', '0', 'Sat Aug 25 2018', '53,46', '', 'Sat Aug 25 2018', '', '');
INSERT INTO `user` VALUES (1476, 'oW9ee4oCe0luqNzJEsxLN1Mi7iXc', '', '', '15869197786', '1000', '1000', '0.9999999999999999', '0', 'Sat Aug 25 2018', '', 'Sat Aug 25 2018', 'Sat Aug 25 2018', 'Sat Aug 25 2018', '{\"num\":0,\"data\":[true,false,false,false,false,false,false]}');
INSERT INTO `user` VALUES (1477, 'oW9ee4p0xC1w0hzKd_HLWWJsGZQA', '', '', '', '0', '0', '0', '0', '', '55,53', '', '', '', '');
INSERT INTO `user` VALUES (1479, 'oW9ee4uzggWPmnAVHZVZr5Ro2zfo', '', '', '15869190407', '700', '700', '0.7', '0', 'Sat Aug 25 2018', '', 'Sat Aug 25 2018', '', '', '');
INSERT INTO `user` VALUES (1486, 'oW9ee4kMMdjXJ_2AAyk73HJYsAp4', '', '2018-08-25 16:26:59', '', '0', '0', '0', '0', '', '39', '', '', '', '');
INSERT INTO `user` VALUES (1488, 'oW9ee4m4dxTsuwavDOBuaTy4FG8o', '', '2018-08-25 16:43:24', '', '0', '0', '0', '0', '', '53', '', '', '', '');

-- ----------------------------
-- Table structure for useraccount
-- ----------------------------
DROP TABLE IF EXISTS `useraccount`;
CREATE TABLE `useraccount`  (
  `id` int(11) NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `userpassword` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of useraccount
-- ----------------------------
INSERT INTO `useraccount` VALUES (1, 1, '7c6fa7db2f2e5bc8');

-- ----------------------------
-- Table structure for usergame
-- ----------------------------
DROP TABLE IF EXISTS `usergame`;
CREATE TABLE `usergame`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `game_id` int(11) NULL DEFAULT NULL,
  `jointime` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '开始时间',
  `playtime` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '游戏时长',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 888 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of usergame
-- ----------------------------
INSERT INTO `usergame` VALUES (2, 1, 1, '2018-01-01 11::10 59', '30分钟');
INSERT INTO `usergame` VALUES (3, 1, 1, '2018-01-01 11::10 59', '30分钟');
INSERT INTO `usergame` VALUES (4, 1, 1, '2018-01-01 11::10 59', '30分钟');
INSERT INTO `usergame` VALUES (5, 1, 1, '2018-01-01 11::10 59', '30分钟');
INSERT INTO `usergame` VALUES (6, 1, 1, '2018-01-01 11::10 59', '30分钟');
INSERT INTO `usergame` VALUES (7, 8, 39, '2018-8-22', '');
INSERT INTO `usergame` VALUES (8, 8, 39, '2018-11-12', '');
INSERT INTO `usergame` VALUES (9, 8, 25, '2018-8-22', '');
INSERT INTO `usergame` VALUES (10, 8, 25, '2018-11-12', '');
INSERT INTO `usergame` VALUES (11, 8, 38, '2018-11-12', '');
INSERT INTO `usergame` VALUES (12, 8, 36, '2018-11-12', '');
INSERT INTO `usergame` VALUES (13, 8, 40, '2018-11-12', '');
INSERT INTO `usergame` VALUES (14, 53, 35, '2018-8-22', '');
INSERT INTO `usergame` VALUES (15, 53, 25, '2018-8-22', '');
INSERT INTO `usergame` VALUES (16, 53, 37, '2018-8-22', '');
INSERT INTO `usergame` VALUES (17, 8, 40, '2018-8-22', '');
INSERT INTO `usergame` VALUES (18, 53, 40, '2018-8-22', '');
INSERT INTO `usergame` VALUES (19, 53, 38, '2018-8-22', '');
INSERT INTO `usergame` VALUES (20, 53, 39, '2018-8-22', '');
INSERT INTO `usergame` VALUES (21, 53, 25, '2018-8-22', '');
INSERT INTO `usergame` VALUES (22, 53, 38, '2018-8-22', '');
INSERT INTO `usergame` VALUES (23, 53, 39, '2018-8-22', '');
INSERT INTO `usergame` VALUES (24, 8, 39, '2018-11-12', '');
INSERT INTO `usergame` VALUES (25, 8, 39, '2018-8-22', '');
INSERT INTO `usergame` VALUES (26, 8, 38, '2018-11-12', '');
INSERT INTO `usergame` VALUES (27, 8, 39, '2018-11-12', '');
INSERT INTO `usergame` VALUES (28, 8, 38, '2018-11-12', '');
INSERT INTO `usergame` VALUES (29, 8, 40, '2018-11-12', '');
INSERT INTO `usergame` VALUES (30, 8, 39, '2018-11-12', '');
INSERT INTO `usergame` VALUES (31, 8, 36, '2018-11-12', '');
INSERT INTO `usergame` VALUES (32, 8, 37, '2018-11-12', '');
INSERT INTO `usergame` VALUES (33, 53, 40, '2018-8-22', '');
INSERT INTO `usergame` VALUES (34, 53, 39, '2018-8-22', '');
INSERT INTO `usergame` VALUES (35, 8, 40, '2018-11-12', '');
INSERT INTO `usergame` VALUES (36, 53, 35, '2018-8-22', '');
INSERT INTO `usergame` VALUES (37, 53, 37, '2018-8-22', '');
INSERT INTO `usergame` VALUES (38, 53, 35, '2018-8-22', '');
INSERT INTO `usergame` VALUES (39, 53, 39, '2018-8-22', '');
INSERT INTO `usergame` VALUES (40, 53, 38, '2018-8-22', '');
INSERT INTO `usergame` VALUES (41, 108, 25, '2018-8-22', '');
INSERT INTO `usergame` VALUES (42, 108, 40, '2018-8-22', '');
INSERT INTO `usergame` VALUES (43, 108, 37, '2018-8-22', '');
INSERT INTO `usergame` VALUES (44, 108, 37, '2018-8-22', '');
INSERT INTO `usergame` VALUES (45, 982, 39, '2018-8-22', '');
INSERT INTO `usergame` VALUES (46, 982, 35, '2018-8-22', '');
INSERT INTO `usergame` VALUES (47, 982, 38, '2018-8-22', '');
INSERT INTO `usergame` VALUES (48, 982, 36, '2018-8-22', '');
INSERT INTO `usergame` VALUES (49, 982, 40, '2018-8-22', '');
INSERT INTO `usergame` VALUES (50, 982, 37, '2018-8-22', '');
INSERT INTO `usergame` VALUES (51, 982, 25, '2018-8-22', '');
INSERT INTO `usergame` VALUES (52, 988, 39, '2018-8-22', '');
INSERT INTO `usergame` VALUES (53, 988, 35, '2018-8-22', '');
INSERT INTO `usergame` VALUES (54, 988, 38, '2018-8-22', '');
INSERT INTO `usergame` VALUES (55, 988, 36, '2018-8-22', '');
INSERT INTO `usergame` VALUES (56, 988, 40, '2018-8-22', '');
INSERT INTO `usergame` VALUES (57, 988, 37, '2018-8-22', '');
INSERT INTO `usergame` VALUES (58, 988, 25, '2018-8-22', '');
INSERT INTO `usergame` VALUES (59, 108, 38, '2018-8-23', '');
INSERT INTO `usergame` VALUES (60, 108, 25, '2018-8-23', '');
INSERT INTO `usergame` VALUES (61, 108, 39, '2018-8-23', '');
INSERT INTO `usergame` VALUES (62, 108, 39, '2018-8-23', '');
INSERT INTO `usergame` VALUES (63, 342, 40, '2018-8-23', '');
INSERT INTO `usergame` VALUES (64, 342, 40, '2018-8-23', '');
INSERT INTO `usergame` VALUES (65, 108, 40, '2018-8-23', '');
INSERT INTO `usergame` VALUES (66, 108, 39, '2018-8-23', '');
INSERT INTO `usergame` VALUES (67, 108, 25, '2018-8-23', '');
INSERT INTO `usergame` VALUES (68, 1001, 38, '2018-8-23', '');
INSERT INTO `usergame` VALUES (69, 1001, 35, '2018-8-23', '');
INSERT INTO `usergame` VALUES (70, 1001, 36, '2018-8-23', '');
INSERT INTO `usergame` VALUES (71, 1001, 39, '2018-8-23', '');
INSERT INTO `usergame` VALUES (72, 1001, 37, '2018-8-23', '');
INSERT INTO `usergame` VALUES (73, 1001, 25, '2018-8-23', '');
INSERT INTO `usergame` VALUES (74, 1006, 39, '2018-8-23', '');
INSERT INTO `usergame` VALUES (75, 1006, 38, '2018-8-23', '');
INSERT INTO `usergame` VALUES (76, 1006, 37, '2018-8-23', '');
INSERT INTO `usergame` VALUES (77, 857, 35, '2018-8-23', '');
INSERT INTO `usergame` VALUES (78, 857, 36, '2018-8-23', '');
INSERT INTO `usergame` VALUES (79, 857, 35, '2018-8-23', '');
INSERT INTO `usergame` VALUES (80, 857, 37, '2018-8-23', '');
INSERT INTO `usergame` VALUES (81, 857, 38, '2018-8-23', '');
INSERT INTO `usergame` VALUES (82, 1009, 36, '2018-8-23', '');
INSERT INTO `usergame` VALUES (83, 1009, 36, '2018-8-23', '');
INSERT INTO `usergame` VALUES (84, 857, 39, '2018-8-23', '');
INSERT INTO `usergame` VALUES (85, 1010, 39, '2018-8-23', '');
INSERT INTO `usergame` VALUES (86, 1009, 39, '2018-8-23', '');
INSERT INTO `usergame` VALUES (87, 1010, 38, '2018-8-23', '');
INSERT INTO `usergame` VALUES (88, 1010, 36, '2018-8-23', '');
INSERT INTO `usergame` VALUES (89, 1010, 37, '2018-8-23', '');
INSERT INTO `usergame` VALUES (90, 1010, 37, '2018-8-23', '');
INSERT INTO `usergame` VALUES (91, 1010, 25, '2018-8-23', '');
INSERT INTO `usergame` VALUES (92, 108, 25, '2018-8-23', '');
INSERT INTO `usergame` VALUES (93, 108, 36, '2018-8-23', '');
INSERT INTO `usergame` VALUES (94, 1012, 37, '2018-8-23', '');
INSERT INTO `usergame` VALUES (95, 108, 37, '2018-8-23', '');
INSERT INTO `usergame` VALUES (96, 1022, 25, '2018-8-23', '');
INSERT INTO `usergame` VALUES (97, 108, 39, '2018-8-23', '');
INSERT INTO `usergame` VALUES (98, 1025, 37, '2018-8-23', '');
INSERT INTO `usergame` VALUES (99, 1025, 38, '2018-8-23', '');
INSERT INTO `usergame` VALUES (100, 1025, 37, '2018-8-23', '');
INSERT INTO `usergame` VALUES (101, 1025, 36, '2018-8-23', '');
INSERT INTO `usergame` VALUES (102, 1025, 35, '2018-8-23', '');
INSERT INTO `usergame` VALUES (103, 1025, 25, '2018-8-23', '');
INSERT INTO `usergame` VALUES (104, 1022, 37, '2018-8-23', '');
INSERT INTO `usergame` VALUES (105, 1022, 25, '2018-8-23', '');
INSERT INTO `usergame` VALUES (106, 1022, 39, '2018-8-23', '');
INSERT INTO `usergame` VALUES (107, 1022, 38, '2018-8-23', '');
INSERT INTO `usergame` VALUES (108, 1022, 39, '2018-8-23', '');
INSERT INTO `usergame` VALUES (109, 1022, 36, '2018-8-23', '');
INSERT INTO `usergame` VALUES (110, 1021, 35, '2018-8-23', '');
INSERT INTO `usergame` VALUES (111, 1021, 25, '2018-8-23', '');
INSERT INTO `usergame` VALUES (112, 1021, 36, '2018-8-23', '');
INSERT INTO `usergame` VALUES (113, 1028, 35, '2018-8-23', '');
INSERT INTO `usergame` VALUES (114, 1028, 25, '2018-8-23', '');
INSERT INTO `usergame` VALUES (115, 1028, 37, '2018-8-23', '');
INSERT INTO `usergame` VALUES (116, 1028, 36, '2018-8-23', '');
INSERT INTO `usergame` VALUES (117, 1028, 38, '2018-8-23', '');
INSERT INTO `usergame` VALUES (118, 1023, 25, '2018-8-23', '');
INSERT INTO `usergame` VALUES (119, 1023, 37, '2018-8-23', '');
INSERT INTO `usergame` VALUES (120, 1023, 36, '2018-8-23', '');
INSERT INTO `usergame` VALUES (121, 1023, 35, '2018-8-23', '');
INSERT INTO `usergame` VALUES (842, 1475, 53, '2018-8-25 15:47:13', '');
INSERT INTO `usergame` VALUES (843, 1477, 55, '2018-8-25 15:50:0', '');
INSERT INTO `usergame` VALUES (844, 1476, 55, '2018-8-25 15:54:12', '');
INSERT INTO `usergame` VALUES (845, 1476, 52, '2018-8-25 15:54:23', '');
INSERT INTO `usergame` VALUES (846, 1476, 46, '2018-8-25 15:54:25', '');
INSERT INTO `usergame` VALUES (847, 1476, 39, '2018-8-25 15:54:31', '');
INSERT INTO `usergame` VALUES (848, 1476, 46, '2018-8-25 15:54:44', '');
INSERT INTO `usergame` VALUES (849, 1476, 55, '2018-8-25 15:54:50', '');
INSERT INTO `usergame` VALUES (850, 1476, 45, '2018-8-25 15:55:8', '');
INSERT INTO `usergame` VALUES (851, 1476, 45, '2018-8-25 15:55:37', '');
INSERT INTO `usergame` VALUES (852, 1476, 45, '2018-8-25 15:55:46', '');
INSERT INTO `usergame` VALUES (853, 1476, 55, '2018-8-25 15:55:58', '');
INSERT INTO `usergame` VALUES (854, 1476, 45, '2018-8-25 15:56:12', '');
INSERT INTO `usergame` VALUES (855, 1476, 55, '2018-8-25 15:56:19', '');
INSERT INTO `usergame` VALUES (856, 1476, 55, '2018-8-25 15:59:58', '');
INSERT INTO `usergame` VALUES (857, 1476, 39, '2018-8-25 16:0:12', '');
INSERT INTO `usergame` VALUES (858, 1476, 53, '2018-8-25 16:0:46', '');
INSERT INTO `usergame` VALUES (859, 1479, 55, '2018-8-25 16:1:19', '');
INSERT INTO `usergame` VALUES (860, 1479, 46, '2018-8-25 16:1:25', '');
INSERT INTO `usergame` VALUES (861, 1479, 45, '2018-8-25 16:1:39', '');
INSERT INTO `usergame` VALUES (862, 1476, 39, '2018-8-25 16:1:41', '');
INSERT INTO `usergame` VALUES (863, 1479, 39, '2018-8-25 16:1:46', '');
INSERT INTO `usergame` VALUES (864, 1479, 55, '2018-8-25 16:2:24', '');
INSERT INTO `usergame` VALUES (865, 1476, 55, '2018-8-25 16:2:34', '');
INSERT INTO `usergame` VALUES (866, 1475, 46, '2018-8-25 16:2:57', '');
INSERT INTO `usergame` VALUES (867, 1479, 55, '2018-8-25 16:2:58', '');
INSERT INTO `usergame` VALUES (868, 1479, 53, '2018-8-25 16:3:4', '');
INSERT INTO `usergame` VALUES (877, 1477, 53, '2018-8-25 16:20:46', '');
INSERT INTO `usergame` VALUES (878, 1486, 39, '2018-8-25 16:27:6', '');
INSERT INTO `usergame` VALUES (879, 1479, 55, '2018-8-25 16:41:9', '');
INSERT INTO `usergame` VALUES (880, 1479, 53, '2018-8-25 16:41:39', '');
INSERT INTO `usergame` VALUES (881, 1476, 55, '2018-8-25 16:42:0', '');
INSERT INTO `usergame` VALUES (882, 1479, 55, '2018-8-25 16:42:6', '');
INSERT INTO `usergame` VALUES (883, 1479, 55, '2018-8-25 16:42:8', '');
INSERT INTO `usergame` VALUES (884, 1476, 53, '2018-8-25 16:42:12', '');
INSERT INTO `usergame` VALUES (885, 1479, 53, '2018-8-25 16:42:29', '');
INSERT INTO `usergame` VALUES (886, 1479, 53, '2018-8-25 16:42:49', '');
INSERT INTO `usergame` VALUES (887, 1488, 53, '2018-8-25 16:43:25', '');

SET FOREIGN_KEY_CHECKS = 1;
