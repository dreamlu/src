package models

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	"github.com/tealeg/xlsx"
	"strconv"
	"zylm/lib"
)

/*用户游戏数据*/
type UserGame struct {
	Id      int `json:"id"`
	User_id int `json:"user_id"`
	//Game_id  int    `json:"game_id"`
	//Nickname string `json:"nickname"` //昵称
	Phone    string `json:"phone"`    //手机号
	Jointime string `json:"jointime"` //加入时间
	Playtime string `json:"playtime"` //游戏时长
}

func ExportExcelByGameData(game_id int) interface{} {
	var info interface{}
	var filename string
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	var UserGame []*UserGame
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select a.id as id,a.game_id,user_id,phone,jointime,playtime from `usergame` a inner join `user` b inner join `game` c on a.user_id=b.id and a.game_id=c.id where game_id=? order by a.user_id", game_id).QueryRows(&UserGame)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {

		var file *xlsx.File
		var sheet *xlsx.Sheet
		var row *xlsx.Row
		var cell *xlsx.Cell
		var err error

		file = xlsx.NewFile()
		sheet, err = file.AddSheet("Sheet1")
		if err != nil {
			fmt.Printf("excel导出错误" + err.Error())
		}
		row = sheet.AddRow()
		cell = row.AddCell()
		cell.Value = "用户id"
		cell = row.AddCell()
		cell.Value = "手机号"
		cell = row.AddCell()
		cell.Value = "进入时间"

		for _, v := range UserGame {
			row = sheet.AddRow()
			cell = row.AddCell()
			cell.Value = strconv.Itoa(v.User_id)
			cell = row.AddCell()
			cell.Value = v.Phone
			cell = row.AddCell()
			cell.Value = v.Jointime
		}
		num, _ := o.Raw("select name from `game` where id=?", game_id).Values(&maps)
		if num > 0 {
			filename = "static/file/" + maps[0]["name"].(string) + ".xlsx"
			err = file.Save("static/file/" + maps[0]["name"].(string) + ".xlsx")
			if err != nil {
				fmt.Printf("excel导出失败" + err.Error())
			}
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = filename //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得用户游戏数据,分页,根据用户id
func GetUserGameByUserId(user_id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("select a.id from `usergame` a inner join `user` b inner join `game` c on a.game_id=c.id and a.user_id=b.id where user_id=?", user_id).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var game []*Game
		var getinfo lib.GetInfoN
		num, _ := o.Raw("select game_id,jointime from `usergame` where user_id=? order by jointime desc", user_id).Values(&maps)
		if num > 0 {
			for k := 0; k < len(maps); k++ {
				//去重game_id
				for i := k - 1; i >= 0; i-- {
					if k >= len(maps) {
						break
					}
					if maps[i]["game_id"].(string) == maps[k]["game_id"].(string) {
						//delete(v,"game_id")
						k++ //越过
						i = k //重新初始化,,抵消i--,实际是i = k-1+1
						continue //break的bug
					}
				}
				if k < len(maps) {
					var gameone *Game
					err := o.Raw("select *from `game` where id=?", maps[k]["game_id"]).QueryRow(&gameone)
					if err == nil {
						game = append(game, gameone)
					}
				}
			}
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = game //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得用户游戏数据,分页
func GetUserGameByGameId(game_id, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("select a.id from `usergame` a inner join `user` b on a.user_id=b.id where game_id=?", game_id).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {

		var UserGame []*UserGame
		var getinfo lib.GetInfo
		o.Raw("select a.id as id,user_id,phone,jointime,playtime from `usergame` a inner join `user` b on a.user_id=b.id where game_id=? order by id desc limit ?,?", game_id, (clientPage-1)*everyPage, everyPage).QueryRows(&UserGame)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = UserGame //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除用户游戏数据,根据id
func DeleteUserGameByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `usergame` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		//删除对应栏目,听说读写
		o.Raw("delete from `usergamelist` where usergame_id=?", id).Exec()
		info = lib.MapDelete
	}
	return info
}

//修改用户游戏数据
func UpdateUserGame(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("usergame", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建用户游戏数据
func CreateUserGame(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("usergame", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
