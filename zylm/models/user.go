package models

import (
	"github.com/astaxie/beego/orm"
	"github.com/medivhzhan/weapp/payment"
	"strconv"
	"time"
	"zylm/lib"
)

/*用户*/
type User struct {
	Id             int    `json:"id"`
	Openid         string `json:"openid"`         //小程序openid
	Nickname       string `json:"nickname"`       //昵称
	Joindate       string `json:"joindate"`       //加入日期
	Phone          string `json:"phone"`          //电话
	Totalcoin      string `json:"totalcoin"`      //累计金币
	Nowcoin        string `json:"nowcoin"`        //现有金币
	Withdrawcoin   string `json:"withdrawcoin"`   //待提现
	Withdrawcoined string `json:"withdrawcoined"` //已提现
	//以下6个为签到相关字段,具体见前端
	Issigntime string `json:"issigntime"`
	Isfive     string `json:"isfive"`
	Isfivetime string `json:"isfivetime"`
	Isshare    string `json:"isshare"`
	Tasktime   string `json:"tasktime"`
	Task       string `json:"task"`
}

type GetInfoNN struct {
	Status int    `json:"status"`
	Msg    string `json:"msg"`
}

//用户金币提现
func WithdrawMoney(openid string, money float64) interface{} {

	var info interface{}
	var getinfo GetInfoNN
	o := orm.NewOrm()
	var maps []orm.Params
	num, _ := o.Raw("select nowcoin from `user` where openid=?", openid).Values(&maps)
	if num > 0 {
		//现有金币
		nowcoin, _ := strconv.ParseFloat(maps[0]["nowcoin"].(string), 64)
		if nowcoin*1.0/1000 < money {
			getinfo.Status = 212
			getinfo.Msg = "提现金额小于规定金额"
			info = getinfo
		} else {
			//验证通过
			//限制
			if money > 10 {
				getinfo.Status = 212
				getinfo.Msg = "提现金额超出规定金额"
				info = getinfo
			}

			//生成时间戳
			nanos := time.Now().UnixNano()
			tradeNo := strconv.FormatInt(nanos, 10)

			form := payment.Transferer{
				// 必填 ...
				AppID:      "wx78e01e7996dc6f59",
				MchID:      "1512206151",
				Amount:     int(money * 100), //单位分
				OutTradeNo: tradeNo,          // or TransactionID: "微信订单号",
				ToUser:     openid,           //小程序关联的用户openid
				Desc:       "金币提现",           // 若商户传入, 会在下发给用户的退款消息中体现退款原因

				/*// 选填 ...
				IP: "发起转账端 IP 地址", // 若商户传入, 会在下发给用户的退款消息中体现退款原因
				CheckName: "校验用户姓名选项 true/false",
				RealName: "收款用户真实姓名", // 如果 CheckName 设置为 true 则必填用户真实姓名
				Device:   "发起转账设备信息",*/
			}

			// 需要证书
			// 支付秘钥 ,res
			_, err := form.Transfer("RvKKXPcYlmDnbG84Aj9KATF5u31YSjRK", "cert/apiclient_cert.pem", "cert/apiclient_key.pem")
			if err != nil {
				//fmt.Println(err)
				getinfo.Status = 212
				getinfo.Msg = err.Error()
				info = getinfo
			} else {
				getinfo.Status = 200
				getinfo.Msg = "支付成功"
				info = getinfo
			}
			//fmt.Printf("返回结果: %#v", res)
		}
	} else {
		getinfo.Status = 212
		getinfo.Msg = "用户不存在！"
		info = getinfo
	}

	return info
}

//获得用户id,根据openid
func GetUserIdByOpenId(openid string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var User []*User
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select id from `user` where openid=?", openid).QueryRows(&User)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = map[string]int{"id": User[0].Id} //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//根据搜索条件获得用户,分页
func GetUserBySearch(args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := lib.SearchTableSql("user", args)

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var User []*User
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&User)
		//统计页码等状态

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = User //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得用户,根据id
func GetUserById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var User []*User
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `user` where id=?", id).QueryRows(&User)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = User //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得用户,分页
func GetUserByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `user`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var User []*User
		var getinfo lib.GetInfo
		o.Raw("select *from `user` where id > 1 order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&User)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = User //数据
		num, _ := strconv.Atoi(SumPage)
		num--
		SumPage = strconv.Itoa(num)
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除用户,根据id
func DeleteUserByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `user` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		//删除对应游戏记录
		o.Raw("delete from `usergame` where user_id=?", id).Exec()
		info = lib.MapDelete
	}
	return info
}

//修改用户
func UpdateUser(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("user", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建用户
func CreateUser(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("user", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}

//修改账号密码
func UpdateAccount(uid, nickname, userpassword string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw("update `user` a inner join `useraccount` b on a.id = b.user_id set nickname=?,userpassword=? where a.id=?", nickname, userpassword, uid).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}
