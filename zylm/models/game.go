package models

import (
	"github.com/astaxie/beego/orm"
	"strconv"
	"zylm/lib"
)

/*游戏*/
type Game struct {
	Id           int    `json:"id"`
	Name         string `json:"name"`         //游戏名
	Category     string `json:"category"`     //类别
	Imageurl     string `json:"imageurl"`     //封面图
	Logo         string `json:"logo"`         //logo图片
	Introduce    string `json:"introduce"`    //游戏介绍
	Realperson   string `json:"realperson"`   //真实人气
	Modifyperson string `json:"modifyperson"` //修改人气
	Address      string `json:"address"`      //游戏地址
	Targetappid  string `json:"targetappid"`  //目标appid
	Publishdate  string `json:"publishdate"`  //上传日期
}

//根据搜索条件获得游戏,分页
func GetGameBySearch(args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := lib.SearchTableSql("game", args)

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Game []*Game
		var getinfo lib.GetInfo
		num,_ := o.Raw(sql).QueryRows(&Game)
		if num > 0{
			for _,v := range Game{
				num,_:= o.Raw("select count(distinct user_id) as num from `usergame` where game_id=?",v.Id).Values(&maps)
				if num > 0{
					v.Realperson = maps[0]["num"].(string)
				}
			}
		}
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Game //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得游戏,根据id
func GetGameById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var Game []*Game
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `game` where id=?", id).QueryRows(&Game)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Game //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//获得游戏,分页
func GetGameByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `game`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Game []*Game
		var getinfo lib.GetInfo
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)
		num,_ := o.Raw("select *from `game` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&Game)

		if num > 0{
			for _,v := range Game{
				num,_:= o.Raw("select count(distinct user_id) as num from `usergame` where game_id=?",v.Id).Values(&maps)
				if num > 0{
					v.Realperson = maps[0]["num"].(string)
				}
			}
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Game //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//删除游戏,根据id
func DeleteGameByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `game` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		//删除对应栏目,听说读写
		o.Raw("delete from `usergame` where game_id=?", id).Exec()
		info = lib.MapDelete
	}
	return info
}

//修改游戏
func UpdateGame(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("game", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建游戏
func CreateGame(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("game", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
