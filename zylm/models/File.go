package models

import (
		"strings"
	"zylm/lib"
)

//获得刚上传的文件路径,富文本
func GetFilePath(path string) interface{} {
	var info interface{}
	//上传成功
	if strings.Index(path, ".") > 0 {
		info = map[string]string{"status": "201", "msg": "创建成功", "filename": path}
	} else {
		info = lib.MapError
	}
	return info
}
