package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"io/ioutil"
	"kpx/util"
	"net/http"
	"reflect"
	"strconv"
	"time"
)

type LoginController struct {
	beego.Controller
}

type User struct {
	Id           int
	Openid       string //OpenId不行,只能首字母大写,beego...
	Userpassword string //密码
	Session_key  string
}

// @router /openid/getopenid [get]
func (this *LoginController) Getopenid() {

	code := this.GetString("code")
	appid := this.GetString("appid")
	secret := this.GetString("secret")
	fmt.Println("临时登录凭证：" + code)

	//获取openid接口
	te_uri := "https://api.weixin.qq.com/sns/jscode2session?appid=" + appid + "&secret=" + secret + "&js_code=" + code + "&grant_type=authorization_code"
	res, _ := http.Get(te_uri)
	body, _ := ioutil.ReadAll(res.Body)
	fmt.Println("用户openid等相关数据:" + string(body))
	defer res.Body.Close()

	//json数据的获取与返回
	userInfo := User{}
	json.Unmarshal(body, &userInfo) //反序列化

	//新用户注册(无论新老用户尝试一次openid注册)
	o := orm.NewOrm()
	o.Raw("insert `user`(openid,joindate) value(?,?)", userInfo.Openid,time.Now().Format("2006-01-02 15:04:05")).Exec()

	//反射遍历结构体，赋值给json
	//要求是可导出的字段大写
	k := reflect.ValueOf(userInfo)
	v := reflect.TypeOf(userInfo)
	//返回的json集合
	jsonMap := make(map[string]string)
	for i := 0; i < v.NumField(); i++ {
		typeX := v.Field(i).Type
		switch typeX.String() { //多选语句switch
		case "string":
			jsonMap[v.Field(i).Name] = k.Field(i).Interface().(string)
			//case "int":
			//	this.Data["json"] = map[string]int{v.Field(i).Name: k.Field(i).Interface().(int)}
		}
	}
	this.Data["json"] = jsonMap
	//只返回了最后一个json
	fmt.Println(this.Data["json"])
	this.ServeJSON()
}

//登录
// @router /login/login [post]
func (this *LoginController) Login() {
	var info interface{}
	username := this.GetString("username")
	userpassword := this.GetString("userpassword")
	o := orm.NewOrm()
	o.Using("default")
	var user []*User
	num, err := o.Raw("SELECT a.id as id,userpassword FROM `user` a inner join `useraccount` b on a.id=b.user_id WHERE nickname = ?", username).QueryRows(&user)
	if err == nil && num > 0 {
		userpassword = util.AesEn(userpassword)
		for _,v := range user{
			if v.Userpassword == userpassword{
				this.SetSecureCookie(beego.AppConfig.String("secertkey"), "uid", strconv.Itoa(v.Id))
				info = map[string]string{"status":"200","msg":"请求成功"}
			}else{
				info = map[string]string{"status":"203","msg":"请求非法"}
			}
		}
	}else{
		info = map[string]string{"status":"203","msg":"请求非法"}
	}
	this.Data["json"] = info
	this.ServeJSON()
}
