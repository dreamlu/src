package controllers

import (
	"github.com/astaxie/beego"
	"zylm/models"
	"strings"
	"time"
)

/*文件处理处理*/
type FileController struct {
	beego.Controller
}

//获得上传的文件路径,不对外暴露(小写)
func (u *FileController) filePath() string {
	//获得文件及保存到文件夹
	f, h, _ := u.GetFile("file") //获取上传的文件
	//上传文件重命名
	filenameSplit := strings.Split(h.Filename, ".")
	//防止文件名中多个“.”,获得文件后缀
	filename := "." + filenameSplit[len(filenameSplit)-1]
	filename = time.Now().Format("20060102150405") + filename //时间戳"2006-01-02 15:04:05"是参考格式,具体数字可变(经测试)
	path := beego.AppConfig.String("filepath") + filename     //文件目录
	defer f.Close()                                           //关闭上传的文件，不然的话会出现临时文件不能清除的情况
	u.SaveToFile("file", path)
	return path
}

//返回上传的文件/图片路径,富文本编辑器
// @router /file/getfilepath [post]
func (u *FileController) GetFilePath() {
	path := u.filePath()
	ss := models.GetFilePath(path)
	u.Data["json"] = ss
	u.ServeJSON()
}
