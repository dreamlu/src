package wx

import (
	"github.com/astaxie/beego"
	"github.com/xlstudio/wxbizdatacrypt"
	"zylm/lib"
)

type GetWxEncryptedData struct {
	beego.Controller
}

// @router /wx/getwxencrypteddata [post]
func (u *GetWxEncryptedData) GetWxEncryptedData() {
	var info interface{}
	var getinfo lib.GetInfoN

	appID := u.GetString("appid")
	sessionKey := u.GetString("sessionKey")
	encryptedData := u.GetString("encryptedData")
	iv := u.GetString("iv")

	pc := wxbizdatacrypt.WxBizDataCrypt{AppID: appID, SessionKey: sessionKey}
	result, err := pc.Decrypt(encryptedData, iv, true) //第三个参数解释： 需要返回 JSON 数据类型时 使用 true, 需要返回 map 数据类型时 使用 false
	if err != nil {
		info = lib.MapError
	} else {
		getinfo.Data = result
		getinfo.Msg = "请求成功"
		getinfo.Status = 200
		info = getinfo
	}
	u.Data["json"] = info
	u.ServeJSON()
}
