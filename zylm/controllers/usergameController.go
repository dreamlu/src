package controllers

import (
	"github.com/astaxie/beego"
	"zylm/models"
	)

type UserGameController struct {
	beego.Controller
}

//游戏的用户游戏数据信息导出
// @router /usergame/exportexcelgamedata [get]
func (u *UserGameController) ExportExcelByGameData() {
	game_id,_ := u.GetInt("game_id")
	ss := models.ExportExcelByGameData(game_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//游戏的用户游戏数据信息分页,根据用户id
// @router /usergame/getusergamebyuserid [get]
func (u *UserGameController) GetUserGameByUserId() {
	user_id,_ := u.GetInt("user_id")
	ss := models.GetUserGameByUserId(user_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//游戏的用户游戏数据信息分页
// @router /usergame/getusergamebygameid [get]
func (u *UserGameController) GetUserGameByGameId() {
	game_id,_ := u.GetInt("game_id")
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := models.GetUserGameByGameId(game_id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//用户游戏数据信息删除
// @router /usergame/deleteusergamebyid/:id [delete]
func (u *UserGameController) DeleteUserGameById() {
	id, _ := u.GetInt(":id")
	ss := models.DeleteUserGameByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//用户游戏数据信息修改
// @router /usergame/updateusergame [post]
func (u *UserGameController) UpdateUserGame() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := models.UpdateUserGame(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增用户游戏数据信息
// @router /usergame/createusergame [post]
func (u *UserGameController) CreateUserGame() {
	values := u.Ctx.Request.Form
	ss := models.CreateUserGame(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
