package controllers

import (
	"github.com/astaxie/beego"
	"zylm/models"
)

type GameController struct {
	beego.Controller
}

//根据搜索条件获得游戏
// @router /game/getgamebysearch [post]
func (u *GameController) GetGameBySearch() {
	values := u.Ctx.Request.Form
	ss := models.GetGameBySearch(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得游戏获取
// @router /game/getgamebyid [get]
func (u *GameController) GetGameById() {
	id, _ := u.GetInt("id")
	ss := models.GetGameById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//游戏信息分页
// @router /game/getgamebypage [get]
func (u *GameController) GetGameByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := models.GetGameByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//游戏信息删除
// @router /game/deletegamebyid/:id [delete]
func (u *GameController) DeleteGameById() {
	id, _ := u.GetInt(":id")
	ss := models.DeleteGameByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//游戏信息修改
// @router /game/updategame [patch]
func (u *GameController) UpdateGame() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := models.UpdateGame(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增游戏信息
// @router /game/creategame [post]
func (u *GameController) CreateGame() {
	values := u.Ctx.Request.Form
	ss := models.CreateGame(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
