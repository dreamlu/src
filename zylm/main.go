package main

import (
	_ "zylm/routers"
	_ "github.com/go-sql-driver/mysql"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

func main() {
	//orm.Debug = true
	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", "root:"+beego.AppConfig.String("db.password")+"@tcp(127.0.0.1:3306)/zylm?charset=utf8")
	beego.Run()
}
