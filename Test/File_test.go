package main

import (
	"log"
	"os"
	"testing"
)

func TestFile(t *testing.T){
	fileInfo, err := os.Stat("test*")
	if err != nil {
		if os.IsNotExist(err) {
			log.Fatal("File does not exist.")
		}
	}
	log.Println("File does exist. File information:")
	log.Println(fileInfo)
}