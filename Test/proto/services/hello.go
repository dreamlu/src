package main

import (
	"Test/proto"
	"context"
	"fmt"

	//proto "hello"

	"github.com/micro/go-micro"
)

type Hello struct{}

func (h *Hello) Ping(ctx context.Context, req *hello.Request, res *hello.Response) error {
	res.Msg = "Hello " + req.Name
	return nil
}
func main() {
	service := micro.NewService(
		micro.Name("hellooo"), // 服务名称
	)
	service.Init()
	hello.RegisterHelloHandler(service.Server(), new(Hello))
	if err := service.Run(); err != nil {
		fmt.Println(err)
	}
}