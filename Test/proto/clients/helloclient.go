package main

import (
	"Test/proto"
	"context"
	"fmt"
	"github.com/micro/go-micro"
)

func main() {
	service := micro.NewService(micro.Name("hello.client")) // 客户端服务名称
	service.Init()
	helloservice := hello.NewHelloClient("hellooo", service.Client())
	res, err := helloservice.Ping(context.TODO(), &hello.Request{Name: "World ^_^"})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(res.Msg)
}