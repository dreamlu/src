package main

/*var (
	ccaptcha   *gocaptcha.Captcha
	configFile = flag.String("c", "conf/gocaptcha.conf", "the config file")
)

const (
	DEFAULT_PORT = "80"
	DEFAULT_LOG  = "logs/gocaptcha-server.log"
)

func ShowImageHandler(w http.ResponseWriter, r *http.Request) {
	key := r.FormValue("key")
	if len(key) >= 0 {
		cimg, err := ccaptcha.GetImage(key)
		log.Println("err", err)
		if nil == err {
			w.Header().Add("Content-Type", "image/png")
			png.Encode(w, cimg)
		} else {
			log.Printf("show image error:%s", err.Error())
			w.WriteHeader(500)
		}
	}

	log.Printf("[cmd:showimage][remote_addr:%s][key:%s]", r.RemoteAddr, key)
}

func GetKeyHandler(w http.ResponseWriter, r *http.Request) {
	callback := html.EscapeString(r.FormValue("callback"))

	key, err := ccaptcha.GetKey(4)
	retstr := "{error_no:%d,error_msg:'%s',key:'%s'}"

	error_no := 0
	error_msg := ""

	if nil != err {
		error_no = 1
		error_msg = err.Error()
	}

	if callback != "" {
		retstr = "%s(" + retstr + ")"
		retstr = fmt.Sprintf(retstr, callback, error_no, error_msg, key)
	} else {
		retstr = fmt.Sprintf(retstr, error_no, error_msg, key)
	}
	w.Header().Add("Content-Type", "application/json")
	w.Write([]byte(retstr))

	log.Printf("[cmd:getkey][remote_addr:%s][key:%s]", r.RemoteAddr, key)
}

func VerifyHandler(w http.ResponseWriter, r *http.Request) {
	key := r.FormValue("key")
	code := r.FormValue("code")
	callback := html.EscapeString(r.FormValue("callback"))

	retstr := "{error_no:%d,error_msg:'%s',key:'%s'}"
	error_no := 0
	error_msg := ""

	suc, msg := ccaptcha.Verify(key, code)

	if false == suc {
		error_no = 1
		error_msg = msg
	}

	if callback != "" {
		retstr = "%s(" + retstr + ")"
		retstr = fmt.Sprintf(retstr, callback, error_no, error_msg, key)
	} else {
		retstr = fmt.Sprintf(retstr, error_no, error_msg, key)
	}
	w.Header().Add("Content-Type", "application/json")
	w.Write([]byte(retstr))
	log.Printf("[cmd:verify][remote_addr:%s][key:%s][code:%s]", r.RemoteAddr, key, code)
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	retstr := "<html>"
	retstr += "<body>"
	retstr += "<h1>gocaptcha server</h1>"
	retstr += "<h2>document</h2>"
	retstr += "<p>see:<a href='https://github.com/hanguofeng/gocaptcha/tree/master/samples/gocaptcha-server'>https://github.com/hanguofeng/gocaptcha/tree/master/samples/gocaptcha-server</a></p>"
	retstr += "<h2>interface</h2>"
	retstr += "<p><a href='/getkey'>/getkey</a></p>"
	retstr += "<p><a href='/showimage'>/showimage</a></p>"
	retstr += "<p><a href='/verify'>/verify</a></p>"
	retstr += "</body>"
	retstr += "</html>"
	w.Write([]byte(retstr))
}*/

///*func main() {
//
//	flag.Parse()
//
//	 1.load the config file and assign port/logfile */
//	port := DEFAULT_PORT
//	logfile := DEFAULT_LOG
//
//	if _, err := os.Stat(*configFile); os.IsNotExist(err) {
//		log.Fatalf("config file:%s not exists!", *configFile)
//		os.Exit(1)
//	}
//
//	c, err := config.ReadDefault(*configFile)
//	if nil != err {
//		port = DEFAULT_PORT
//		logfile = DEFAULT_LOG
//	}
//	port, err = c.String("service", "port")
//	if nil != err {
//		port = DEFAULT_PORT
//	}
//	logfile, err = c.String("service", "logfile")
//	if nil != err {
//		logfile = DEFAULT_LOG
//	}
//
//	os.MkdirAll(filepath.Dir(logfile), 0777)
//	f, err := os.OpenFile(logfile, os.O_RDWR|os.O_CREATE, 0666)
//	log.SetOutput(f)
//
//	captcha, err := gocaptcha.CreateCaptchaFromConfigFile(*configFile)
//
//	if nil != err {
//		log.Fatalf("config load failed:%s", err.Error())
//	} else {
//		ccaptcha = captcha
//	}
//
//	/* 2. bind handler */
//	http.HandleFunc("/showimage", ShowImageHandler)
//	http.HandleFunc("/getkey", GetKeyHandler)
//	http.HandleFunc("/verify", VerifyHandler)
//	http.HandleFunc("/", IndexHandler)
//
//	/* 3. run the http server */
//	s := &http.Server{Addr: ":" + port}
//
//	log.Printf("=======ready to serve=======")
//	log.Fatal(s.ListenAndServe())
//	f.Close()
//}*/
/*func main() {
	//form := payment.Transferer{
	//	// 必填 ...
	//	AppID:      "wx78e01e7996dc6f59",
	//	MchID:      "1512206151",
	//	Amount:     100,
	//	OutTradeNo: "test1", // or TransactionID: "微信订单号",
	//	ToUser:     "oW9ee4uzggWPmnAVHZVZr5Ro2zfo", //小程序关联的用户openid
	//	Desc:       "金币提现", // 若商户传入, 会在下发给用户的退款消息中体现退款原因
	//
	//	//// 选填 ...
	//	//IP: "发起转账端 IP 地址", // 若商户传入, 会在下发给用户的退款消息中体现退款原因
	//	//CheckName: "校验用户姓名选项 true/false",
	//	//RealName: "收款用户真实姓名", // 如果 CheckName 设置为 true 则必填用户真实姓名
	//	//Device:   "发起转账设备信息",
	//}
	//
	//// 需要证书
	//res, err := form.Transfer("RvKKXPcYlmDnbG84Aj9KATF5u31YSjRK", "/home/lu/test/apiclient_cert.pem", "/home/lu/test/apiclient_key.pem")
	//if err != nil {
	//	fmt.Println(err)
	//	return
	//}
	//
	//fmt.Printf("返回结果: %#v", res)

	filename := time.Now().Format("20060102150405")
	fmt.Println(filename)
	fmt.Println(3121 * 1.0 / 1000)
	fmt.Println(time.Now().Format("2006-01-02 15:04:05"))
}
*/

/*加密解密*/
/*// key
const aesTable = "abcdefghijklmnopkrstuvwsyz012345"

var (
	block cipher.Block
	mutex sync.Mutex
)

func main() {
	// encript
	encodeBytes, err := Encrypt([]byte("1lloveyouchinese"))
	if err != nil {
		fmt.Println("Encrypt: ", err)
		return
	}

	// encode println
	fmt.Printf("Encrypt code: %x\n", string(encodeBytes))

	// decrypt
	decodeBytes, err := Decrypt(encodeBytes)
	if err != nil {
		fmt.Println("Decrypt: ", err)
		return
	}

	// decode println
	fmt.Println("Decrypt code: ", string(decodeBytes))
}

// AES加密
func Encrypt(src []byte) ([]byte, error) {
	// 验证输入参数
	// 必须为aes.Blocksize的倍数
	if len(src)%aes.BlockSize != 0 {
		return nil, errors.New("crypto/cipher: input not full blocks")
	}

	encryptText := make([]byte, aes.BlockSize+len(src))

	iv := encryptText[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, err
	}

	mode := cipher.NewCBCEncrypter(block, iv)

	mode.CryptBlocks(encryptText[aes.BlockSize:], src)

	return encryptText, nil
}

// AES解密
func Decrypt(src []byte) ([]byte, error) {
	// hex
	decryptText, err := hex.DecodeString(fmt.Sprintf("%x", string(src)))
	if err != nil {
		return nil, err
	}

	// 长度不能小于aes.Blocksize
	if len(decryptText) < aes.BlockSize {
		return nil, errors.New("crypto/cipher: ciphertext too short")
	}

	iv := decryptText[:aes.BlockSize]
	decryptText = decryptText[aes.BlockSize:]

	// 验证输入参数
	// 必须为aes.Blocksize的倍数
	if len(decryptText)%aes.BlockSize != 0 {
		return nil, errors.New("crypto/cipher: ciphertext is not a multiple of the block size")
	}

	mode := cipher.NewCBCDecrypter(block, iv)

	mode.CryptBlocks(decryptText, decryptText)

	return decryptText, nil
}

func init() {
	mutex.Lock()
	defer mutex.Unlock()

	if block != nil {
		return
	}

	cblock, err := aes.NewCipher([]byte(aesTable))
	if err != nil {
		panic("aes.NewCipher: " + err.Error())
	}

	block = cblock
}*/