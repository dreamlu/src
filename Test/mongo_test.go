package main

import (
	"context"
	"github.com/mongodb/mongo-go-driver/mongo"
	"log"
	"testing"
	"time"
)

func TestMongo(t *testing.T)  {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, _ := mongo.Connect(ctx, "mongodb://localhost:27017")

	collection := client.Database("test").Collection("test")
	ctx, _ = context.WithTimeout(context.Background(), 30*time.Second)
	//res, _ := collection.Name()//InsertOne(ctx, bson.M{"test": "pi"})
	//collection.find
	//id := res.InsertedID
	log.Println(collection.Name())
}

