package main

import (
	"github.com/smartwalle/alipay"
	"log"
	"strconv"
	"testing"
	"time"
)

var appId = "2018121162519388"
var aliPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvUIZrG+Dme6iuqwHcCxeXd73j142RGrwjPBPGoifNIciDM1BkOKTJhwim3V6yLIXHzZVGpaoVNPfgjG0Wy8UP6SpulDcFXtGgE3MGZF1HxiM7bu+EOUFPXJYW4DZU4I23wd7i+vuiTVcL9uny6V1+eeAG/l/beZokW4ygHgvAOKj6GgLyUXfuFYsf80/N30qZiML9P22x/dUqlayRcDc665C4nEJmQlXqn4lwi4E7z0Hrt1cWj1fF3Iodo30fBB5KRF+TT3Vgf8/qIpymYojNRv2e/veCn/eeE/g0MTZuId1jJ1uf3SkE6m6YpKfkxmz/t0mcPYKTEtkvkfCWuPhQwIDAQAB"
var privateKey = "MIIEowIBAAKCAQEAvUIZrG+Dme6iuqwHcCxeXd73j142RGrwjPBPGoifNIciDM1BkOKTJhwim3V6yLIXHzZVGpaoVNPfgjG0Wy8UP6SpulDcFXtGgE3MGZF1HxiM7bu+EOUFPXJYW4DZU4I23wd7i+vuiTVcL9uny6V1+eeAG/l/beZokW4ygHgvAOKj6GgLyUXfuFYsf80/N30qZiML9P22x/dUqlayRcDc665C4nEJmQlXqn4lwi4E7z0Hrt1cWj1fF3Iodo30fBB5KRF+TT3Vgf8/qIpymYojNRv2e/veCn/eeE/g0MTZuId1jJ1uf3SkE6m6YpKfkxmz/t0mcPYKTEtkvkfCWuPhQwIDAQABAoIBAG/NK5N08N4vXcw+scovO7PicoYsFozGS8JGd8Rp7vq91XyQiGoJnvnFZyBE/zquEgCrPQ0O8PPjkca8jBnNrv2FzYo2GD1MbLKAwtBb4D91ZlNONBKQ6E6LTO0JKdkuNDH+nppizrAZaJMRBIfF//KYMlLUSdXnZeB7o7PGy6WOONDTsTz+Qzbyh2qerrECPxTI7SZGXrUSejT672hUUxRC5qxJyfgVm1SBuJQnIXkMocrFjst2TNs9RGEDQIzdze3FttyN9extQX1bBgtTkrRKhRz2u31758k0tyDwyiXPkR+RyV3zLbgyEsBCSoixu6tp+HyTeQ82jKWLpADRMkkCgYEA6ee4R+kJ+P9wmQtohvOsaefrfiMFu+SzHfwmDhXFXDkKgUrSC6Sg5MGfItbGo3xAWCow/eBWDEw7VA9CLxk69eJelByW5atyqOO4ANyQX86OueokTdWGMGIB+5uQrlThWYUdkcScwhD+HTZDEPHf+j8kV2RGJDXTmRzoYdW2e2UCgYEAzyK7CEOAxK4bZGE8kSyelTbqScpNKwKTN4zcyWFs4GwIR1qPEv8M2c+6+R7qohWcwO0UyDcfxYW8kzrG52Sak96aEwiArJmzUYauhC89qNHNwgnMjqbOOl50qxtSw7xMAlALgqueLayGa19q/KJZC9WmrZ0QI27syIqu9eYCI4cCgYBJyWFmver5c4tPnDuzJrdjCVhOiDnM1g5zDRHsK44A07Wup62vIpXm0/WhheNPu2iI53ZhAB3k7z9f+xhDx0/ENU7kpkgr13PqNzvaebnJn12C5IvjSdJ0/NXnS2HOvNQB624yh7VzuuyOxolNlR+0z6plLyq5TYmcqXPdOQ/ByQKBgBUGNW21TFCqS2V46SjjGoWSuV0cB5EiBcdRqOdX2DE/RefYLUWEVc2V5Ch9Ftu0zeBXMBDSZ4so8+cNxDqgX4A6y/C6oDTktOrFfNk14lBMW1IipHymObB6/eSQNWpgpmZlsYVl3fxM0qf7W2ShJeCpou1kT8sI60e/q9qRqg3pAoGBAN8JRArxYBwiImM60j6xH+l6rU2gKvqDC9K/7MaqLKOF6L120yEFWIwI30BUlFzixEphlyYi474U1xqextr7eZfn2eK+dA/R3XDC4q8T4YItgjnvlesEot3QJnCn9sm1HluIneI5ufP7KpwR5AtGLBUZ4LRtCBQbhezoLNRZI5Wg"
var client = alipay.New(appId, aliPublicKey, privateKey, true)

//支付宝支付
func TestAlipay(t *testing.T) {

	money := "0.01"
	//order_id := "1"
	//
	//key := "-Iloveyouchinese"
	////res, errs := util.Encrypt([]byte(order_id + string([]byte(key)[:len(key)-len(order_id)])))
	////if errs != nil {
	////	fmt.Println("cookie加密错误Encrypt: ", errs)
	////	return
	////}

	//生成时间戳
	nanos := time.Now().UnixNano()
	tradeNo := strconv.FormatInt(nanos, 10)

	var p = alipay.AliPayTradePagePay{}
	//p.NotifyURL = "http://xxx"
	//p.ReturnURL = conf.GetConfigValue("api")+"/alipay/payres?order_id="+url.QueryEscape(string(res))
	p.Subject = "考培侠"
	p.OutTradeNo = tradeNo //"传递一个唯一单号"
	p.TotalAmount = money
	p.ProductCode = "FAST_INSTANT_TRADE_PAY"

	var url, err = client.TradePagePay(p)
	if err != nil {
		log.Println(err)
		return
	}

	var payURL = url.String()
	log.Println(payURL)
}
