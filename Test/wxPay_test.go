package main

import (
	"fmt"
	"github.com/Dreamlu/wechatpay"
	"log"
	"os"
	"testing"
)

var (
	wechat_cert    = "cert/apiclient_cert.pem"          //证书cert
	wechat_key     = "cert/apiclient_key.pem"           //证书key
	wechat_app_id  = "wx61d6f8124a4222b3" //"wx2b9c8cd08c79022b"                   //应用的appid
	wechat_mch_id  = "1520812131"                       //商户号
	wechat_api_key = "yP5MIIebIPvtaxd6M3E0Zu7yokybYc4Y" //商户支付密钥
)

var wechat_client *wechatpay.WechatPay

func TestMain(m *testing.M) {
	wechat_client = wechatpay.New(wechat_app_id, wechat_mch_id,
		wechat_api_key, []byte(wechat_cert), []byte(wechat_key))
	exitCode := m.Run()
	os.Exit(exitCode)
}

//微信扫码支付
func TestWxPay(t *testing.T) {
	var pay_data wechatpay.UnitOrder
	pay_data.NotifyUrl = "47.94.8.188"
	pay_data.TradeType = "NATIVE"
	pay_data.Body = "测试支付"
	pay_data.SpbillCreateIp = "47.94.8.188"
	pay_data.TotalFee = 1
	pay_data.OutTradeNo = "1000001asfdddddddddddddddddddddd"
	result ,err:= wechat_client.Pay(pay_data)
	if err != nil {
		log.Println(err)
	}
	fmt.Println(result.CodeUrl)
}

//微信扫码回调地址
func TestWechatWebNotifyUrl(t *testing.T) {
	//log.Println("wechat notify start")
	//body, err := ioutil.ReadAll(c.Request.Body)
	//if err != nil {
	//	log.Println( "read http body failed！error msg:"+err.Error())
	//}
	//log.Println("wechat pay notify body :" + string(body))
	//
	//var wx_notify_req wechatpay.PayNotifyResult
	//
	//err = xml.Unmarshal(body, &wx_notify_req)
	//if err != nil {
	//	log.Println( "read http body xml failed! err :"+err.Error())
	//}
	//mv, err := mxj.NewMapXml(body)
	//if err != nil {
	//	log.Println(err,err.Error())
	//}
	//
	////进行签名校验
	//if wechat_client.VerifySign(mv["xml"].(map[string]interface{}), mv["xml"].(map[string]interface{})["sign"].(string)) {
	//	record, err := json.Marshal(wx_notify_req)
	//	if err != nil {
	//		log.Error(err, "wechat pay marshal err :"+err.Error())
	//	}
	//	c.XML(http.StatusOK, gin.H{
	//		"return_code": "SUCCESS",
	//		"return_msg":  "OK",
	//	})
	//} else {
	//	c.XML(http.StatusOK, gin.H{
	//		"return_code": "FAIL",
	//		"return_msg":  "failed to verify sign, please retry!",
	//	})
	//}
	//return
}
