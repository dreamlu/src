package controllers

import (
	"github.com/astaxie/beego"
	"yq_520/models"
)

type LoveController struct {
	beego.Controller
}

// @router /love:openid [get,post]
func (u *LoveController) GetLovesById() {
	openid := u.GetString("openid")
	ss := models.GetLoveById(openid)
	u.Data["json"] = ss
	u.ServeJSON()
}

// @router /createlove
func (u *LoveController) CreateLoveBindId(){
	openid := u.GetString("openid")
	love_nickname := u.GetString("love_nickname")
	love_picture := u.GetString("love_picture")
	models.CreateLoveBindId(openid,love_nickname,love_picture)
	u.Data["json"]=""
	u.ServeJSON()
}
