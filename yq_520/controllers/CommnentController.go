package controllers

import (
	"github.com/astaxie/beego"
	"yq_520/models"
)

type CommentController struct {
	beego.Controller
}

// @router /comment:openid [get,post]
func (u *CommentController) GetCommentsById() {
	openid := u.GetString("openid")
	ss := models.GetCommentById(openid)
	u.Data["json"] = ss
	u.ServeJSON()
}

// @router /createcomment
func (u *CommentController) CreateCommentBindId(){
	openid := u.GetString("openid")
	comment := u.GetString("comment")
	models.CreateCommentBindId(openid,comment)
}
