package controllers

import (
	"github.com/astaxie/beego"
	"yq_520/models"
)

type UserController struct {
	beego.Controller
}

// @router /getuser [get,post]
func (u *UserController) GetUserById() {
	openid := u.GetString("openid")
	ss := models.GetUserById(openid)
	u.Data["json"] = ss
	u.ServeJSON()
}