package controllers

import (
	"github.com/astaxie/beego"
	"net/http"
	"io/ioutil"
	"fmt"
	"encoding/json"
	"bytes"
	"yq_520/models"
	"github.com/astaxie/beego/orm"
)

type GetWxPicture struct {
	beego.Controller
}

// @router /getwxpicture:openid [get,post]
func (u *GetWxPicture) Getwxpicture()  {
	openid := u.GetString("openid")

	//获取token接口
	te_uri := "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx957183cb1d4af567&secret=2f4571a570a2215619f48139046173af"
	res,_ := http.Get(te_uri)
	body,_ := ioutil.ReadAll(res.Body)
	fmt.Println("token相关数据:"+string(body))
	defer res.Body.Close()

	//json数据的获取与返回
	pageInfo := models.PageInfo{}
	json.Unmarshal(body,&pageInfo)//反序列化


	//获取二维码数据接口
	data := "{\"page\":\"pages/biaobai/biaobai\",\"scene\":\""+openid+"\"}"

	te_uri = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token="+pageInfo.Access_token
	//post提交数据
	res,_ = http.Post(te_uri,"application/json",bytes.NewBuffer([]byte(data)))
	body,_ = ioutil.ReadAll(res.Body)

	ioutil.WriteFile("./wxpicture/wx"+openid+".jpg", body, 0666)
	fmt.Println(body)
	//返回小程序二维码地址
	u.Data["json"]=map[string]string{"wxpicture":"/wxpicture/wx"+openid+".jpg"}
	u.ServeJSON()
}

// @router /updatewxpicture
func (u *GetWxPicture) PostPicture(){
	openid := u.GetString("openid")
	user_picture := u.GetString("user_picture")
	user_nickname := u.GetString("user_nickname")
	o := orm.NewOrm()
	o.Raw("update `user` set user_nickname=?,user_picture=? where openid=?",user_nickname,user_picture,openid).Exec()
}