package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["yq_520/controllers:CommentController"] = append(beego.GlobalControllerRouter["yq_520/controllers:CommentController"],
		beego.ControllerComments{
			Method: "GetCommentsById",
			Router: `/comment:openid`,
			AllowHTTPMethods: []string{"get","post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["yq_520/controllers:CommentController"] = append(beego.GlobalControllerRouter["yq_520/controllers:CommentController"],
		beego.ControllerComments{
			Method: "CreateCommentBindId",
			Router: `/createcomment`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["yq_520/controllers:GetWxPicture"] = append(beego.GlobalControllerRouter["yq_520/controllers:GetWxPicture"],
		beego.ControllerComments{
			Method: "Getwxpicture",
			Router: `/getwxpicture:openid`,
			AllowHTTPMethods: []string{"get","post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["yq_520/controllers:GetWxPicture"] = append(beego.GlobalControllerRouter["yq_520/controllers:GetWxPicture"],
		beego.ControllerComments{
			Method: "PostPicture",
			Router: `/updatewxpicture`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["yq_520/controllers:LoveController"] = append(beego.GlobalControllerRouter["yq_520/controllers:LoveController"],
		beego.ControllerComments{
			Method: "CreateLoveBindId",
			Router: `/createlove`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["yq_520/controllers:LoveController"] = append(beego.GlobalControllerRouter["yq_520/controllers:LoveController"],
		beego.ControllerComments{
			Method: "GetLovesById",
			Router: `/love:openid`,
			AllowHTTPMethods: []string{"get","post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["yq_520/controllers:UserController"] = append(beego.GlobalControllerRouter["yq_520/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetUserById",
			Router: `/getuser`,
			AllowHTTPMethods: []string{"get","post"},
			MethodParams: param.Make(),
			Params: nil})

}
