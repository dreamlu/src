package main

import (
	_ "yq_520/routers"
	_ "yq_520/models"
	_ "github.com/go-sql-driver/mysql"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

func main() {
	orm.RegisterDriver("mysql",orm.DRMySQL)
	orm.RegisterDataBase("default","mysql","root:lucheng@tcp(127.0.0.1:3306)/yq_520?charset=utf8")
	beego.SetStaticPath("/wxpicture","wxpicture")

	beego.Run()

}
