package models

import (
	"yq_520/util"
	"github.com/astaxie/beego/orm"
	"fmt"
)

type Love struct {
	Id				int
	User_id			int
	Love_nickname	string
	Love_picture	string
}

//根据openid获得love
func GetLoveById(openid string)  []Love{
	o := orm.NewOrm()
	o.Using("default")
	userid := util.GetIdByOpenid(openid)

	u:=[]Love{}
	_,err := o.Raw("select * from `love` where user_id=?",userid).QueryRows(&u)
	if err == nil {
		fmt.Println("love获取成功")
	}
	return u
}

//插入love,绑定该openid
func CreateLoveBindId(openid,love_nickname,love_picture string) int{
	o := orm.NewOrm()
	userid := util.GetIdByOpenid(openid)
	_,err := o.Raw("insert `love`(user_id,love_nickname,love_picture) value(?,?,?)",userid,love_nickname,love_picture).Exec()
	if err == nil {
		fmt.Println("love插入成功")
		return 1
	}
	return 0
}

func init()  {
	orm.RegisterModel(new(Love))
}

