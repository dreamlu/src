package models

type PageInfo struct {
	Access_token	string
	Expires			string
}
