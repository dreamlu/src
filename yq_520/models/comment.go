package models

import (
	"github.com/astaxie/beego/orm"
	"fmt"
	"yq_520/util"
)

type Comment struct {
	Id				int
	User_id			int
	Comment			string
}

//根据openid获取相关弹幕
func GetCommentById(openid string)  []Comment{
	o := orm.NewOrm()
	o.Using("default")
	userid := util.GetIdByOpenid(openid)

	u:=[]Comment{}
	_,err := o.Raw("select * from `comment` where user_id=?",userid).QueryRows(&u)
	if err == nil {
		fmt.Println("弹幕获取成功")
	}
	return u
}

//插入弹幕,绑定该openid
func CreateCommentBindId(openid,comment string) int{
	o := orm.NewOrm()
	userid := util.GetIdByOpenid(openid)
	_,err := o.Raw("insert `comment`(user_id,comment) value(?,?)",userid,comment).Exec()
	if err == nil {
		fmt.Println("弹幕插入成功")
		return 1
	}
	return 0
}

func init()  {
	orm.RegisterModel(new(Comment))
}
