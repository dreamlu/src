package models

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	"github.com/tealeg/xlsx"
	"strconv"
	"yd_hm/lib"
)

type Customer struct {
	Id           int    `json:"id"`
	Name         string `json:"name"`
	Phone        string `json:"phone"`        //电话
	Brithdate    string `json:"brithdate"`    //生日
	Rechard_card string `json:"rechard_card"` //充值卡余额
	JmuserID     int    `json:"jmuser_id"`    //分店用户id
}

// 客户消费记录
type Record struct {
	Id           int    `json:"id"`
	Dealer       string `json:"dealer"`       //成交人
	Project_name string `json:"project_name"` //项目
	Beautician   string `json:"beautician"`   //美容师
	Money        string `json:"money"`
	Deal_time    string `json:"deal_time"` //时间
	Paymothod    string `json:"paymothod"` //支付方式
	Remark       string `json:"remark"`    //备注
}

//客户消费记录,分页
func GetRecordBySearch(args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := lib.SearchTableSql("record", args)

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Record []*Record
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&Record)
		//统计页码等状态

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Record //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

// 创建客户消费记录
func CreateRecord(args map[string][]string) interface{} {

	o := orm.NewOrm()
	// create
	if _, ok := args["money"]; ok {
		// 充值卡减去金额
		o.Raw("update `customer` set rechard_card=rechard_card-"+args["money"][0]+" where id=?",args["customer_id"][0]).Exec()
	}

	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("record", args)

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}

/*顾客批量新增*/
func CreateSomething(table, path string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	sql := "insert `" + table + "`("

	path = string([]byte(path)[1:])
	excelFileName := path
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		fmt.Printf("通用批量新增文件打开失败: %s\n", err)
	}
	for _, sheet := range xlFile.Sheets { //一个文件中的多张表
		for i := 1; i < len(sheet.Rows); i++ {
			row := sheet.Rows[i]
			//字段名
			if i == 1 {
				for _, cell := range row.Cells {
					text := cell.String()
					sql += text + ","
				}
				c := []byte(sql)
				sql = string(c[:len(c)-1]) + ") value(" //去掉点
				continue
			}
			for _, cell := range row.Cells {
				text := cell.String()
				sql += "'" + text + "',"
			}
			c := []byte(sql)
			sql = string(c[:len(c)-1]) + "),(" //去掉点
		}
		c := []byte(sql)
		sql = string(c[:len(c)-2]) //去掉点(
	}
	var num int64
	//删除之前上传的进行覆盖
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}

//根据id获取客户管理
func GetCustomerById(id int) interface{} {
	var info interface{}
	var getinfo lib.GetInfoN

	o := orm.NewOrm()
	o.Using("default")
	var customer []Customer
	num2, err := o.Raw("select *from `customer` where id=?", id).QueryRows(&customer)
	if err == nil && num2 > 0 {
		//统计页码等状态
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = customer //数据

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoResult
	} else {
		info = lib.MapError
	}
	return info
}

//客户管理,分页
func GetCustomerBySearch(args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := lib.SearchTableSql("customer", args)

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var Customer []*Customer
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&Customer)
		//统计页码等状态

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Customer //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//删除客户管理
func DeleteCustomerById(id string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw("delete from `customer` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改用户
func UpdateCustomer(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("customer", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建用户
func CreateCustomer(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("customer", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
