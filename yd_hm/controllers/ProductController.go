package controllers

import (
	"github.com/astaxie/beego"
	"yd_hm/models"
)

type ProductController struct {
	beego.Controller
}

//根据id获取产品
func (u *ProductController) GetProductById(){
	id,_ := u.GetInt("id")	//每页数量
	ss := models.GetProductById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}
//产品分页,分类
func (u *ProductController) GetProductByPage(){
	pageNum := 1
	num := 10
	category := ""
	category = u.GetString("category")
	pageNum,_ = u.GetInt("pagenum")	//页码
	num,_ = u.GetInt("num")	//每页数量
	ss := models.GetProductByPage(pageNum,num,category)
	u.Data["json"] = ss
	u.ServeJSON()
}

// 产品数据录入,返回状态1/0
func (u *ProductController) CreateProduct(){
	category := u.GetString("category")
	model := u.GetString("model")
	name := u.GetString("name")
	specal := u.GetString("specal")
	introduce := u.GetString("introduce")
	file_id := u.GetString("file_id")
	link1 := u.GetString("link1")
	link2 := u.GetString("link2")

	n := models.InsertProduct(category,model,name,specal,introduce,file_id,link1,link2)
	u.Data["json"]=map[string]int{"status":n}
	u.ServeJSON()
}

//产品用户删除,返回状态1/0
func (u *ProductController) DeleteProductById(){
	id := u.GetString("id")
	n := models.DeleteProductById(id)
	u.Data["json"]=map[string]int64{"status":n}
	u.ServeJSON()
}

// 产品数据更新,返回状态1/0
func (u *ProductController) UpdateProduct(){
	id := u.GetString("id")
	category := u.GetString("category")
	model := u.GetString("model")
	name := u.GetString("name")
	specal := u.GetString("specal")
	introduce := u.GetString("introduce")
	file_id := u.GetString("file_id")
	link1 := u.GetString("link1")
	link2 := u.GetString("link2")

	n := models.UpdateProduct(id,category,model,name,specal,introduce,file_id,link1,link2)
	u.Data["json"]=map[string]int64{"status":n}
	u.ServeJSON()
}

//产品搜索
func (u *ProductController) SearchProduct()  {
	pageNum := 1
	num := 10
	key := ""
	key = u.GetString("key")
	pageNum,_ = u.GetInt("pagenum")	//页码
	num,_ = u.GetInt("num")	//每页数量
	ss := models.SearchProduct(pageNum,num,key)
	u.Data["json"] = ss
	u.ServeJSON()
}

//获得不同产品类别
func (u *ProductController) GetProductName(){
	ss := models.GetProductName()
	u.Data["json"]=ss
	u.ServeJSON()
}

func (u *ProductController) UpdateProductName() {
	new_category := u.GetString("new_category")
	old_category := u.GetString("old_category")
	n := models.UpdateProductName(new_category, old_category)
	u.Data["json"] = map[string]int64{"status": n}
	u.ServeJSON()
}

//是否隐藏
func (u *ProductController) GetProductFlag(){
	ss := models.GetProductFlag()
	u.Data["json"]=ss
	u.ServeJSON()
}

//修改隐藏状态
func (u *ProductController) UpdateProductFlag() {
	flag := u.GetString("flag")
	n := models.UpdateProductFlag(flag)
	u.Data["json"] = map[string]int64{"status": n}
	u.ServeJSON()
}



