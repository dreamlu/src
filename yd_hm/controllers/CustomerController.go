package controllers

import (
	"github.com/astaxie/beego"
	"yd_hm/models"
)

type CustomerController struct {
	beego.Controller
}

//客户管理分页/查询
func (u *CustomerController) GetRecordBySearch(){
	values := u.Ctx.Request.Form
	ss := models.GetRecordBySearch(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

// 客户消费记录创建,返回状态1/0
func (u *CustomerController) CreateRecord(){
	values := u.Ctx.Request.Form
	ss := models.CreateRecord(values)
	u.Data["json"]=ss
	u.ServeJSON()
}

//试卷批量新增,file/createsomething......
func (u *FileController) CreateSomething() {
	table := "customer" //表名
	path := u.filePath()
	ss := models.CreateSomething(table, path)
	u.Data["json"] = ss
	u.ServeJSON()
}

//客户管理,id
func (u *CustomerController) GetCustomerById(){
	id,_ := u.GetInt("id")	//每页数量
	ss := models.GetCustomerById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//客户管理分页/查询
func (u *CustomerController) GetCustomerBySearch(){
	values := u.Ctx.Request.Form
	ss := models.GetCustomerBySearch(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//客户管理用户删除,返回状态1/0
func (u *CustomerController) DeleteCustomerById(){
	id := u.GetString("id")
	ss := models.DeleteCustomerById(id)
	u.Data["json"]=ss
	u.ServeJSON()
}

// 客户管理数据更新,返回状态1/0
func (u *CustomerController) UpdateCustomer(){
	values := u.Ctx.Request.Form
	ss := models.UpdateCustomer(values)
	u.Data["json"]=ss
	u.ServeJSON()
}

// 客户管理数据录入,返回状态1/0
func (u *CustomerController) CreateCustomer(){
	values := u.Ctx.Request.Form
	ss := models.CreateCustomer(values)
	u.Data["json"]=ss
	u.ServeJSON()
}
