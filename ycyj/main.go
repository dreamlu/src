package main

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	_ "ycyj/routers"
)

func main() {
	orm.Debug = true
	beego.BConfig.WebConfig.Session.SessionOn = true
	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", "root:lucheng@tcp(127.0.0.1:3306)/ycyj?charset=utf8")
	beego.Run()
}
