package authz

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"github.com/casbin/casbin"
	"net/http"
	"strings"
)

// NewAuthorizer returns the authorizer.
// Use a casbin enforcer as input
func NewAuthorizer(e *casbin.Enforcer) beego.FilterFunc {
	return func(ctx *context.Context) {
		//获得用户所有角色ids
		role_ids, _ := ctx.GetSecureCookie(beego.AppConfig.String("secertkey"), "role_ids")

		a := &BasicAuthorizer{enforcer: e}

		if  !a.CheckPermission(ctx.Request, role_ids) {
			//a.RequirePermission(ctx.ResponseWriter)
			ctx.WriteString("{\"status\": \"203\", \"msg\": \"没有该路由权限\"}")
		}
	}
}

// BasicAuthorizer stores the casbin handler
type BasicAuthorizer struct {
	enforcer *casbin.Enforcer
}

// GetUserName gets the user name from the request.
// Currently, only HTTP basic authentication is supported
/*func (a *BasicAuthorizer) GetUserName(r *http.Request, role_ids string) string {
	username, _, _ := r.BasicAuth()

	return username
}*/

// CheckPermission checks the user/method/path combination from the request.
// Returns true (permission granted) or false (permission forbidden)
func (a *BasicAuthorizer) CheckPermission(r *http.Request, role_ids string) bool {
	//user := a.GetUserName(r)
	method := r.Method
	path := r.URL.Path
	if strings.Contains(path,"/login") || strings.Contains(path,"/getmenubypage") || strings.Contains(path,"/static/file") {
		return true
	}
	//角色权限是否通过判断
	roles := strings.Split(role_ids,"&")
	for _,v := range roles{
		//根管理员所有接口权限
		if v == "1" || true == a.enforcer.Enforce("role_"+v, path, method){
			return true
		}
	}
	return false
}

// RequirePermission returns the 403 Forbidden to the client
func (a *BasicAuthorizer) RequirePermission(w http.ResponseWriter) {
	w.WriteHeader(403)
	w.Write([]byte("403 Forbidden\n"))
}
