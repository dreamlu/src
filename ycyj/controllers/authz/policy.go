package authz

/*casbin 权限测试*/
import (
	"github.com/astaxie/beego"
	"github.com/casbin/casbin"
	"github.com/casbin/casbin/persist/file-adapter"
)

type Policy struct {
	beego.Controller
}

// @router /policy/getallsubjects [get]
func (u *Policy) GetAllSubjects() {
	a := fileadapter.NewAdapter("conf/authz_policy.csv")
	e := casbin.NewEnforcer("conf/authz_model.conf", a)
	//e.AddGroupingPolicy("role_2","9")
	//e.DeleteRolesForUser("role_2")
	//e.SavePolicy()

	ss := e.GetAllRoles()

	u.Data["json"] = ss
	u.ServeJSON()
}

// @router /policy/getallsubjects [get]
func (u *Policy) GetRoles() {

}
