package announcement

import (
	"github.com/astaxie/beego"
	"ycyj/models/announcement"
)

type AnnouncementController struct {
	beego.Controller
}

//根据搜索条件获取公告信息
// @router /announcement/getannouncementbysearch [patch]
func (u *AnnouncementController) GetAnnouncementBySearch() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	values := u.Ctx.Request.Form
	ss := announcement.GetAnnouncementBySearch(user_id, values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获取公告信息
// @router /announcement/getannouncementbyid [get]
func (u *AnnouncementController) GetAnnouncementById() {
	id, _ := u.GetInt("id") //每页数量
	ss := announcement.GetAnnouncementById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//公告信息分页
// @router /announcement/getannouncementbypage [get]
func (u *AnnouncementController) GetAnnouncementByPage() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := announcement.GetAnnouncementByPage(user_id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//公告信息删除
// @router /announcement/deleteannouncementbyid/:id [delete]
func (u *AnnouncementController) DeleteAnnouncementById() {
	id, _ := u.GetInt(":id")
	ss := announcement.DeleteAnnouncementByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//公告信息修改
// @router /announcement/updateannouncement [patch]
func (u *AnnouncementController) UpdateAnnouncement() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := announcement.UpdateAnnouncement(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增公告信息
//创建时给个对应编号相同的默认密码
// @router /announcement/createannouncement [post]
func (u *AnnouncementController) CreateAnnouncement() {
	values := u.Ctx.Request.Form
	ss := announcement.CreateAnnouncement(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//公告信息修改
// @router /announcement/updatereply [patch]
func (u *AnnouncementController) UpdateReply() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	announcement_id := u.GetString("announcement_id")
	ss := announcement.UpdateReply(user_id, announcement_id)
	u.Data["json"] = ss
	u.ServeJSON()
}
