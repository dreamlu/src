package permission

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"strconv"
	"ycyj/models/lib"
	"ycyj/models/permission"
	"ycyj/util"
)

type UserController struct {
	beego.Controller
}

//根据所有用户对应角色权限
// @router /user/getusers [get]
func (u *UserController) GetUsers() {
	ss := permission.GetUsers()
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据用户id获得对应角色权限
// @router /user/getuserlist [get]
func (u *UserController) GetUserlist() {
	id,_ := u.GetInt("id")
	ss := permission.GetUserlist(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//用户信息删除,会同时删除对应权限
// @router /user/deleteuserbyid/:id [delete]
func (u *UserController) DeleteUserById() {
	id, _ := u.GetInt(":id")
	ss := permission.DeleteUserByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//用户信息修改
// @router /user/updateuser [patch]
func (u *UserController) UpdateUser() {
	id := u.GetString("id")
	user_name := u.GetString("user_name")
	user_password := u.GetString("user_password")
	role_ids := u.GetString("role_ids")
	unit_id := u.GetString("unit_id")
	ss := permission.UpdateUser(id,user_name,user_password,role_ids,unit_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增用户信息,并创建相应的角色
// @router /user/createuser [post]
func (u *UserController) CreateUser() {
	user_name := u.GetString("user_name")
	user_password := u.GetString("user_password")
	role_ids := u.GetString("role_ids")
	unit_id := u.GetString("unit_id")
	ss := permission.CreateUser(user_name,user_password,role_ids,unit_id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//登录
// @router /login/login [post]
func (this *UserController) Login() {
	var info interface{}
	username := this.GetString("username")
	userpassword := this.GetString("userpassword")
	o := orm.NewOrm()
	o.Using("default")
	var user []*permission.User
	num, err := o.Raw("SELECT id,user_password FROM `user` WHERE user_name = ?", username).QueryRows(&user)
	if err == nil && num > 0 {
		userpassword = util.AesEn(userpassword)
		for _, v := range user {
			//用户名相同,比对密码
			if v.User_password == userpassword {
				//查询相应角色
				role_ids := permission.GetRoleIds(v.Id)
				this.SetSecureCookie(beego.AppConfig.String("secertkey"), "role_ids", role_ids)

				//查询对应菜单以及路由,存储本地
				//permission := permission.GetMenuRouters(v.Id)
				//this.SetSecureCookie(beego.AppConfig.String("secertkey"), "permission", permission)

				//id 存储本地cookie
				this.SetSecureCookie(beego.AppConfig.String("secertkey"), "uid", strconv.Itoa(v.Id))
				info = map[string]string{"status": "200", "msg": "请求成功", "uid": strconv.Itoa(v.Id)}
				break
			} else {
				info = lib.MapError
			}
		}
	} else {
		info = lib.MapError
	}
	this.Data["json"] = info
	this.ServeJSON()
}