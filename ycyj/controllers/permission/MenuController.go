package permission

import (
	"github.com/astaxie/beego"
	"strconv"
	"ycyj/models/permission"
)

type MenuController struct {
	beego.Controller
}

//根据id获得菜单
// @router /menu/getmenubyid [get]
func (u *MenuController) GetMenuById() {
	id, _ := u.GetInt("id")
	ss := permission.GetMenuById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//菜单信息,所有
// @router /menu/getallmenu [get]
func (u *MenuController) GetAllMenu() {
	ss := permission.GetAllMenu()
	u.Data["json"] = ss
	u.ServeJSON()
}

//菜单信息分页，权限
// @router /menu/getmenubypage [get]
func (u *MenuController) GetMenuByPage() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	uid,_ := strconv.Atoi(user_id)
	ss := permission.GetMenuByPage(uid)
	u.Data["json"] = ss
	u.ServeJSON()
}

//菜单信息删除
// @router /menu/deletemenubyid/:id [delete]
func (u *MenuController) DeleteMenuById() {
	id, _ := u.GetInt(":id")
	ss := permission.DeleteMenuByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//菜单信息修改
// @router /menu/updatemenu [patch]
func (u *MenuController) UpdateMenu() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := permission.UpdateMenu(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增菜单信息
// @router /menu/createmenu [post]
func (u *MenuController) CreateMenu() {
	values := u.Ctx.Request.Form
	ss := permission.CreateMenu(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
