package permission

import (
	"github.com/astaxie/beego"
	"ycyj/models/permission"
)

type RoleController struct {
	beego.Controller
}

//获得所有角色
// @router /role/getroles [get]
func (u *RoleController) GetRoles() {
	ss := permission.GetRoles()
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得对应菜单权限id,&连接
// @router /role/getmenubyid [get]
func (u *RoleController) GetMenuByRoleId() {
	id, _ := u.GetInt("id")
	ss := permission.GetMenuByRoleId(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//角色信息删除
// @router /role/deleterolebyid/:id [delete]
func (u *RoleController) DeleteRoleById() {
	id, _ := u.GetInt(":id")
	ss := permission.DeleteRoleByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//角色信息修改
// @router /role/updaterole [patch]
func (u *RoleController) UpdateRole() {
	id := u.GetString("id")
	role_name := u.GetString("role_name")
	menu_ids := u.GetString("menu_ids")
	ss := permission.UpdateRole(id,role_name,menu_ids)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增角色信息
// @router /role/createrole [post]
func (u *RoleController) CreateRole() {
	role_name := u.GetString("role_name")
	menu_ids := u.GetString("menu_ids")
	ss := permission.CreateRole(role_name,menu_ids)
	u.Data["json"] = ss
	u.ServeJSON()
}
