package basic

import (
	"github.com/astaxie/beego"
	"ycyj/models/basic"
)

type EmphasispersonController struct {
	beego.Controller
}

//根据id查询本地重点人口信息
// @router /emphasisperson/getemphasispersonbyid [get]
func (u *PoliceController) GetEmphasispersonById() {
	id, _ := u.GetInt("id") //每页数量
	ss := basic.GetEmphasispersonById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//条件搜索本地重点人口信息分页
// @router /emphasisperson/getemphasispersonbysearch [patch]
func (u *PoliceController) GetEmphasispersonBySearch() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	values := u.Ctx.Request.Form
	ss := basic.GetEmphasispersonBySearch(user_id, values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//本地重点人口信息分页
// @router /emphasisperson/getemphasispersonbypage [get]
func (u *PoliceController) GetEmphasispersonByPage() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	clientPage, _ := u.GetInt("clientPage")             //页码
	everyPage, _ := u.GetInt("everyPage")               //每页数量
	ss := basic.GetEmphasispersonByPage(user_id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//本地重点人口信息删除
// @router /emphasisperson/deleteemphasispersonbyid/:id [delete]
func (u *EmphasispersonController) DeleteEmphasispersonById() {
	id, _ := u.GetInt(":id")
	ss := basic.DeleteEmphasispersonByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//本地重点人口信息修改
// @router /emphasisperson/updateemphasisperson [patch]
func (u *EmphasispersonController) UpdateEmphasisperson() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := basic.UpdateEmphasisperson(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增本地重点人口信息
// @router /emphasisperson/createemphasisperson [post]
func (u *EmphasispersonController) CreateEmphasisperson() {
	values := u.Ctx.Request.Form
	ss := basic.CreateEmphasisperson(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
