package basic

import (
	"github.com/astaxie/beego"
	"ycyj/models/basic"
)

type PoliceController struct {
	beego.Controller
}

//警员信息搜索分页
// @router /police/searchpolice [patch]
func (u *PoliceController) SearchPolice() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := basic.SearchPolice(user_id, values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//警员信息分页
// @router /police/getpolicebyid [get]
func (u *PoliceController) GetPoliceById() {
	id, _ := u.GetInt("id") //每页数量
	ss := basic.GetPoliceById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//警员信息分页
// @router /police/getpolicebypage [get]
func (u *PoliceController) GetPoliceByPage() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	clientPage, _ := u.GetInt("clientPage")             //页码
	everyPage, _ := u.GetInt("everyPage")               //每页数量
	ss := basic.GetPoliceByPage(user_id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//警员信息删除
// @router /police/deletepolicebyid/:id [delete]
func (u *PoliceController) DeletePoliceById() {
	id, _ := u.GetInt(":id")
	ss := basic.DeletePoliceByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//警员信息修改
// @router /police/updatepolice [patch]
func (u *PoliceController) UpdatePolice() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := basic.UpdatePolice(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增警员信息
//创建时给个对应编号相同的默认密码
// @router /police/createpolice [post]
func (u *PoliceController) CreatePolice() {
	values := u.Ctx.Request.Form
	ss := basic.CreatePolice(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据警员编号返回id
// @router /police/getidbynumber [get]
func (u *PoliceController) GetIdByNumber() {
	number, _ := u.GetInt("number")
	ss := basic.GetIdByNumber(number)
	u.Data["json"] = ss
	u.ServeJSON()
}
