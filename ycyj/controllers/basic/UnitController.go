package basic

import (
	"github.com/astaxie/beego"
	"ycyj/models/basic"
)

type UnitController struct {
	beego.Controller
}

//获得所有不重复单位名称以及id
// @router /unit/getunitlist [get]
func (u *UnitController) GetUnitList() {
	ss := basic.GetUnitList()
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得单位
// @router /unit/getunitbyid [get]
func (u *UnitController) GetUnitById() {
	id, _ := u.GetInt("id")
	ss := basic.GetUnitById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//单位相关警员信息分页
// @router /unit/getunitpolicebyid [get]
func (u *UnitController) GetUnitPoliceById() {
	clientPage, _ := beego.AppConfig.Int("clientPage") //默认第1页
	everyPage, _ := beego.AppConfig.Int("everyPage")   //默认10页
	clientPage, _ = u.GetInt("clientPage")             //页码
	everyPage, _ = u.GetInt("everyPage")               //每页数量
	id, _ := u.GetInt("id")
	ss := basic.GetUnitPoliceById(id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//单位信息分页
// @router /unit/getunitbypage [get]
func (u *UnitController) GetUnitByPage() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := basic.GetUnitByPage(user_id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//单位信息删除
// @router /unit/deleteunitbyid/:id [delete]
func (u *UnitController) DeleteUnitById() {
	id, _ := u.GetInt(":id")
	ss := basic.DeleteUnitByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//单位信息修改
// @router /unit/updateunit [patch]
func (u *UnitController) UpdateUnit() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := basic.UpdateUnit(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增单位信息
// @router /unit/createunit [post]
func (u *UnitController) CreateUnit() {
	values := u.Ctx.Request.Form
	ss := basic.CreateUnit(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
