package basic

import (
	"github.com/astaxie/beego"
	"ycyj/models/basic"
)

type NativepersonController struct {
	beego.Controller
}

//查找职业下拉框
// @router /nativeperson/getcareer [get]
func (u *PoliceController) GetCareer() {
	ss := basic.GetCareer()
	u.Data["json"] = ss
	u.ServeJSON()
}

//查找政治面貌下拉框
// @router /nativeperson/getpolitical [get]
func (u *PoliceController) GetPolitical() {
	ss := basic.GetPolitical()
	u.Data["json"] = ss
	u.ServeJSON()
}

//查找教育学历下拉框
// @router /nativeperson/geteducation [get]
func (u *PoliceController) GetEducation() {
	ss := basic.GetEducation()
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据户口查找本地人口信息
// @router /nativeperson/getnativepersonbyhousehold [get]
func (u *PoliceController) GetNativepersonByHousehold() {
	household_num := u.GetString("household_num") //每页数量
	ss := basic.GetNativepersonByHousehold(household_num)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id查找本地人口信息
// @router /nativeperson/getnativepersonbyidp [get]
func (u *PoliceController) GetNativepersonByIdP() {
	id := u.GetString("id") //每页数量
	ss := basic.GetNativepersonByIdP(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据搜索条件获得本地人口信息
// @router /nativeperson/getnativepersonbysearch [patch]
func (u *PoliceController) GetNativepersonBySearch() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	values := u.Ctx.Request.Form
	ss := basic.GetNativepersonBySearch(user_id, values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据身份证号码查找本地人口信息
// @router /nativeperson/getnativepersonbyidcard [get]
func (u *PoliceController) GetNativepersonByIdcard() {
	idcard := u.GetString("idcard") //每页数量
	ss := basic.GetNativepersonByIdcard(idcard)
	u.Data["json"] = ss
	u.ServeJSON()
}

//本地人口信息分页
// @router /nativeperson/getnativepersonbypage [get]
func (u *PoliceController) GetNativepersonByPage() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	clientPage, _ := u.GetInt("clientPage")             //页码
	everyPage, _ := u.GetInt("everyPage")               //每页数量
	ss := basic.GetNativepersonByPage(user_id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//本地人口信息删除
// @router /nativeperson/deletenativepersonbyid/:id [delete]
func (u *NativepersonController) DeleteNativepersonById() {
	id, _ := u.GetInt(":id")
	ss := basic.DeleteNativepersonByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//本地人口信息修改
// @router /nativeperson/updatenativeperson [patch]
func (u *NativepersonController) UpdateNativeperson() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := basic.UpdateNativeperson(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增本地人口信息
// @router /nativeperson/createnativeperson [post]
func (u *NativepersonController) CreateNativeperson() {
	values := u.Ctx.Request.Form
	ss := basic.CreateNativeperson(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
