package Graphics

import (
	"github.com/astaxie/beego"
	"ycyj/models/basic/graphics"
)

type GraphicsController struct {
	beego.Controller
}

//获得流动人口动态变化数据
// @router /graphics/getflowchange [get]
func (u *GraphicsController) GetFlowChange() {
	township_id := u.GetString("township_id")
	time1 := u.GetString("time1")
	time2 := u.GetString("time2")

	ss := graphics.GetFlowChange(township_id, time1, time2)
	u.Data["json"] = ss
	u.ServeJSON()
}

//获得流动人口户籍分布数据
// @router /graphics/getflowleave [get]
func (u *GraphicsController) GetFlowLeave() {
	township_id := u.GetString("township_id")
	time1 := u.GetString("time1")
	time2 := u.GetString("time2")

	ss := graphics.GetFlowLeave(township_id, time1, time2)
	u.Data["json"] = ss
	u.ServeJSON()
}

//获得本地户籍人户分离数据
// @router /graphics/getnativeleave [get]
func (u *GraphicsController) GetNativeLeave() {
	township_id := u.GetString("township_id")
	time1 := u.GetString("time1")
	time2 := u.GetString("time2")

	ss := graphics.GetNativeLeave(township_id, time1, time2)
	u.Data["json"] = ss
	u.ServeJSON()
}

//获得本地人口和流动人口比例数据
// @router /graphics/getcomparefngraphics [get]
func (u *GraphicsController) GetCompareFNGraphics() {
	township_id := u.GetString("township_id")
	time1 := u.GetString("time1")
	time2 := u.GetString("time2")

	ss := graphics.GetCompareFNGraphics(township_id, time1, time2)
	u.Data["json"] = ss
	u.ServeJSON()
}

//获得本地人口年龄比例数据
// @router /graphics/getnativeagegraphics [get]
func (u *GraphicsController) GetNativeAgeGraphics() {
	township_id := u.GetString("township_id")
	time1 := u.GetString("time1")
	time2 := u.GetString("time2")

	ss := graphics.GetNativeAgeGraphics(township_id, time1, time2)
	u.Data["json"] = ss
	u.ServeJSON()
}

//获得本地人口性别比例数据
// @router /graphics/getnativesexgraphics [get]
func (u *GraphicsController) GetNativeSexGraphics() {
	township_id := u.GetString("township_id")
	time1 := u.GetString("time1")
	time2 := u.GetString("time2")

	ss := graphics.GetNativeSexGraphics(township_id, time1, time2)
	u.Data["json"] = ss
	u.ServeJSON()
}
