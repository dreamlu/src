package basic

import (
	"github.com/astaxie/beego"
	"ycyj/models/basic"
)

type TownshipController struct {
	beego.Controller
}

//乡镇信息下拉框
// @router /township/gettownshiplistn [get]
func (u *PoliceController) GetTownshipListN() {
	ss := basic.GetTownshipListN()
	u.Data["json"] = ss
	u.ServeJSON()
}

//乡镇以及下面乡村信息下拉框
// @router /township/gettownshiplist [get]
func (u *PoliceController) GetTownshipList() {
	ss := basic.GetTownshipList()
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id查询乡镇信息
// @router /township/gettownshipbyid [get]
func (u *PoliceController) GetTownshipById() {
	id, _ := u.GetInt("id") //每页数量
	ss := basic.GetTownshipById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//乡镇信息分页
// @router /township/gettownshipbypage [get]
func (u *PoliceController) GetTownshipByPage() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	clientPage, _ := u.GetInt("clientPage")             //页码
	everyPage, _ := u.GetInt("everyPage")               //每页数量
	ss := basic.GetTownshipByPage(user_id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//乡镇信息删除
// @router /township/deletetownshipbyid/:id [delete]
func (u *TownshipController) DeleteTownshipById() {
	id, _ := u.GetInt(":id")
	ss := basic.DeleteTownshipByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//乡镇信息修改
// @router /township/updatetownship [patch]
func (u *TownshipController) UpdateTownship() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := basic.UpdateTownship(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增乡镇信息
//创建时给个对应编号相同的默认密码
// @router /township/createtownship [post]
func (u *TownshipController) CreateTownship() {
	values := u.Ctx.Request.Form
	ss := basic.CreateTownship(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
