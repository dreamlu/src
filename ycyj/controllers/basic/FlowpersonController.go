package basic

import (
	"github.com/astaxie/beego"
	"ycyj/models/basic"
)

type FlowpersonController struct {
	beego.Controller
}

//根据搜索条件获得流动人口信息
// @router /flowperson/getflowpersonbysearch [patch]
func (u *PoliceController) GetFlowpersonBySearch() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := basic.GetFlowpersonBySearch(user_id, values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得流动人口信息
// @router /flowperson/getflowpersonbyid [get]
func (u *PoliceController) GetFlowpersonById() {
	id, _ := u.GetInt("id") //每页数量
	ss := basic.GetFlowpersonById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//流动人口信息分页
// @router /flowperson/getflowpersonbypage [get]
func (u *PoliceController) GetFlowpersonByPage() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	clientPage, _ := u.GetInt("clientPage")             //页码
	everyPage, _ := u.GetInt("everyPage")               //每页数量
	ss := basic.GetFlowpersonByPage(user_id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//流动人口信息删除
// @router /flowperson/deleteflowpersonbyid/:id [delete]
func (u *FlowpersonController) DeleteFlowpersonById() {
	id, _ := u.GetInt(":id")
	ss := basic.DeleteFlowpersonByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//流动人口信息修改
// @router /flowperson/updateflowperson [patch]
func (u *FlowpersonController) UpdateFlowperson() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := basic.UpdateFlowperson(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增流动人口信息
// @router /flowperson/createflowperson [post]
func (u *FlowpersonController) CreateFlowperson() {
	values := u.Ctx.Request.Form
	ss := basic.CreateFlowperson(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
