package basic

import (
	"github.com/astaxie/beego"
	"ycyj/models/basic"
)

type EmphasiscontrolController struct {
	beego.Controller
}

//根据本地人口id获取对应的重点人口管控信息
// @router /emphasiscontrol/getemphasiscontrolbynativepersonid [get]
func (u *EmphasiscontrolController) GetEmphasiscontrolByNativepersonId() {
	id, _ := u.GetInt("id")
	ss := basic.GetEmphasiscontrolByNativepersonId(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//本地重点人口信息删除
// @router /emphasiscontrol/deleteemphasiscontrolbyid/:id [delete]
func (u *EmphasiscontrolController) DeleteEmphasiscontrolById() {
	id, _ := u.GetInt(":id")
	ss := basic.DeleteEmphasiscontrolByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增本地重点人口信息
// @router /emphasiscontrol/createemphasiscontrol [post]
func (u *EmphasiscontrolController) CreateEmphasiscontrol() {
	values := u.Ctx.Request.Form
	ss := basic.CreateEmphasiscontrol(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
