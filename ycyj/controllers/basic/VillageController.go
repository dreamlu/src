package basic

import (
	"github.com/astaxie/beego"
	"ycyj/models/basic"
)

type VillageController struct {
	beego.Controller
}

//根据乡镇id获取下面所有人口信息,分页
// @router /village/getnativepersonbytownshipid [get]
func (u *VillageController) GetNativePersonByTownshipId() {
	clientPage, _ := u.GetInt("clientPage")             //页码
	everyPage, _ := u.GetInt("everyPage")               //每页数量
	id, _ := u.GetInt("id")
	ss := basic.GetNativePersonByTownshipId(id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据乡镇id获取村子信息,分页
// @router /village/getvillagebytownshipid [get]
func (u *VillageController) GetVillageByTownshipId() {
	clientPage, _ := u.GetInt("clientPage")             //页码
	everyPage, _ := u.GetInt("everyPage")               //每页数量
	id, _ := u.GetInt("id")
	ss := basic.GetVillageByTownshipId(id, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据村子id获取村子信息
// @router /village/getvillagebyid [get]
func (u *VillageController) GetVillageById() {
	id, _ := u.GetInt("id")
	ss := basic.GetVillageById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//村子信息删除
// @router /village/deletevillagebyid/:id [delete]
func (u *VillageController) DeleteVillageById() {
	id, _ := u.GetInt(":id")
	ss := basic.DeleteVillageByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//村子信息修改
// @router /village/updatevillage [patch]
func (u *VillageController) UpdateVillage() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := basic.UpdateVillage(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增村子信息
//创建时给个对应编号相同的默认密码
// @router /village/createvillage [post]
func (u *VillageController) CreateVillage() {
	values := u.Ctx.Request.Form
	ss := basic.CreateVillage(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
