package news

import (
	"github.com/astaxie/beego"
	"ycyj/models/news"
)

type NewsController struct {
	beego.Controller
}

//置顶新闻获取
// @router /news/getnewsbytop [get]
func (u *NewsController) GetNewsByTop() {
	everyPage, _ := u.GetInt("everyPage") //每页数量
	ss := news.GetNewsByTop(everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据搜索条件获得轮播新闻获取
// @router /news/getnewsbysearch [patch]
func (u *NewsController) GetNewsBySearch() {
	values := u.Ctx.Request.Form
	ss := news.GetNewsBySearch(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得轮播新闻获取
// @router /news/getnewsbyid [get]
func (u *NewsController) GetNewsById() {
	id, _ := u.GetInt("id") //每页数量
	ss := news.GetNewsById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//轮播新闻获取
// @router /news/getnewsbysort [get]
func (u *NewsController) GetNewsBySort() {
	everyPage := 10                      //默认10页
	everyPage, _ = u.GetInt("everyPage") //每页数量
	ss := news.GetNewsBySort(everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新闻信息分页
// @router /news/getnewsbypage [get]
func (u *NewsController) GetNewsByPage() {
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	ss := news.GetNewsByPage(clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新闻信息删除
// @router /news/deletenewsbyid/:id [delete]
func (u *NewsController) DeleteNewsById() {
	id, _ := u.GetInt(":id")
	ss := news.DeleteNewsByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新闻信息修改
// @router /news/updatenews [patch]
func (u *NewsController) UpdateNews() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := news.UpdateNews(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增新闻信息
//创建时给个对应编号相同的默认密码
// @router /news/createnews [post]
func (u *NewsController) CreateNews() {
	values := u.Ctx.Request.Form
	ss := news.CreateNews(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
