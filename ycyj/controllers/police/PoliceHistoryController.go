package police

import (
	"github.com/astaxie/beego"
	"ycyj/models/police"
)

type PoliceHistoryController struct {
	beego.Controller
}

//获得所有警员/行政村/单位的报送数据,每年每月每日
// @router /policeaffairs/getallcountyear [get]
func (u *PoliceHistoryController) GetAllCountYear() {
	category := u.GetString("category")
	clientPage,_ := u.GetInt("clientPage")
	everyPage,_ := u.GetInt("everyPage")
	ss := police.GetAllCountYear(category, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据具体id或编号获得警员/行政村/单位的报送数据
// @router /policeaffairs/getdetailcount [get]
func (u *PoliceHistoryController) GetDetailCount() {
	category := u.GetString("category")
	data := u.GetString("data")
	time1 := u.GetString("time1")
	time2 := u.GetString("time2")
	clientPage,_ := u.GetInt("clientPage")
	everyPage,_ := u.GetInt("everyPage")
	ss := police.GetDetailCount(category, data, time1, time2, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//获得所有警员/行政村/单位的报送数据
// @router /policeaffairs/getallcount [get]
func (u *PoliceHistoryController) GetAllCount() {
	category := u.GetString("category")
	time1 := u.GetString("time1")
	time2 := u.GetString("time2")
	clientPage,_ := u.GetInt("clientPage")
	everyPage,_ := u.GetInt("everyPage")
	ss := police.GetAllCount(category, time1, time2, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}
