package police

import (
	"github.com/astaxie/beego"
	"ycyj/models/police"
)

type ReplyController struct {
	beego.Controller
}

//新增回复或批示信息
// @router /reply/createreply [post]
func (u *ReplyController) CreateReply() {
	values := u.Ctx.Request.Form
	ss := police.CreateReply(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
