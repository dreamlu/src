package police

import (
	"github.com/astaxie/beego"
	"ycyj/models/police"
)

type PoliceCountController struct {
	beego.Controller
}

//根据id获得警务信息
// @router /policeaffairs/getnumbytype [get]
func (u *PoliceCountController) GetNumByType() {
	category := u.GetString("category")
	clientPage,_:= u.GetInt("clientPage")
	everyPage,_:= u.GetInt("everyPage")
	var ss interface{}
	switch category {
	case "police":
		ss = police.GetNumByPolice(clientPage,everyPage)
	case "unit":
		ss = police.GetNumByUnit(clientPage,everyPage)
	case "village":
		ss = police.GetNumByVillage(clientPage,everyPage)
	}
	u.Data["json"] = ss
	u.ServeJSON()
}
