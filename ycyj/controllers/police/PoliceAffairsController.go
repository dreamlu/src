package police

import (
	"github.com/astaxie/beego"
	"ycyj/models/police"
)

type PoliceaffairsController struct {
	beego.Controller
}

//根据id获得警务信息
// @router /policeaffairs/getbasicdata [get]
func (u *PoliceaffairsController) GetBasicData() {
	ss := police.GetBasicData()
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据搜索条件获得警务信息
// @router /policeaffairs/getpoliceaffairsbysearch [patch]
func (u *PoliceaffairsController) GetPoliceaffairsBySearch() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	values := u.Ctx.Request.Form
	ss := police.GetPoliceaffairsBySearch(user_id, values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//根据id获得警务信息
// @router /policeaffairs/getpoliceaffairsbyid [get]
func (u *PoliceaffairsController) GetPoliceaffairsById() {
	id, _ := u.GetInt("id")
	ss := police.GetPoliceaffairsById(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//警务信息分页
// @router /policeaffairs/getpoliceaffairsbypage [get]
func (u *PoliceaffairsController) GetPoliceaffairsByPage() {
	user_id,_ := u.Ctx.GetSecureCookie(beego.AppConfig.String("secertkey"),"uid")
	issend, _ := u.GetInt("issend")         //页码
	clientPage, _ := u.GetInt("clientPage") //页码
	everyPage, _ := u.GetInt("everyPage")   //每页数量
	flag,_ := u.GetInt("flag")
	ss := police.GetPoliceaffairsByPage(user_id, issend, clientPage, everyPage,flag)
	u.Data["json"] = ss
	u.ServeJSON()
}

//警务信息删除
// @router /policeaffairs/deletepoliceaffairsbyid/:id [delete]
func (u *PoliceaffairsController) DeletePoliceaffairsById() {
	id, _ := u.GetInt(":id")
	ss := police.DeletePoliceaffairsByid(id)
	u.Data["json"] = ss
	u.ServeJSON()
}

//警务信息修改
// @router /policeaffairs/updatepoliceaffairs [patch]
func (u *PoliceaffairsController) UpdatePoliceaffairs() {
	values := u.Ctx.Request.Form //源码values 为map[string][]string类型
	ss := police.UpdatePoliceaffairs(values)
	u.Data["json"] = ss
	u.ServeJSON()
}

//新增警务信息
//创建时给个对应编号相同的默认密码
// @router /policeaffairs/createpoliceaffairs [post]
func (u *PoliceaffairsController) CreatePoliceaffairs() {
	values := u.Ctx.Request.Form
	ss := police.CreatePoliceaffairs(values)
	u.Data["json"] = ss
	u.ServeJSON()
}
