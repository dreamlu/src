package police

import (
	"github.com/astaxie/beego"
	"ycyj/models/police"
)

type PoliceRankController struct {
	beego.Controller
}

//获得所有警员/行政村/单位的报送数据,时间范围
// @router /policeaffairs/getallcounttimerank [get]
func (u *PoliceRankController) GetAllCountTimeRank() {
	category := u.GetString("category")
	time1 := u.GetString("time1")
	time2 := u.GetString("time2")
	clientPage,_:= u.GetInt("clientPage")
	everyPage,_:= u.GetInt("everyPage")
	ss := police.GetAllCountTimeRank(category, time1, time2, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}

//获得所有警员/行政村/单位的报送数据,年月数据
// @router /policeaffairs/getallcountrank [get]
func (u *PoliceRankController) GetAllCountRank() {
	category := u.GetString("category")
	year := u.GetString("year")
	month := u.GetString("month")
	clientPage,_:= u.GetInt("clientPage")
	everyPage,_:= u.GetInt("everyPage")
	ss := police.GetAllCountRank(category, year, month, clientPage, everyPage)
	u.Data["json"] = ss
	u.ServeJSON()
}
