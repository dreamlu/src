/*
 Navicat MySQL Data Transfer

 Source Server         : lu
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : ycyj

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 03/12/2018 17:57:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for announcement
-- ----------------------------
DROP TABLE IF EXISTS `announcement`;
CREATE TABLE `announcement`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '公告',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '标题',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '附件',
  `recieve_unit` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '接收单位们的id',
  `publish_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '发布人',
  `publish_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of announcement
-- ----------------------------
INSERT INTO `announcement` VALUES (13, '发布一篇公告', '<p>三所高校领导调整 北大迎来新任党委书记及校长<br/><em>2018-10-23 14:27</em> <em>新华网</em><br/><span style=\"color:#aaaaaa\"><span style=\"font-size:14px\">我要反馈</span></span><br/></p><p></p><p></p><ul><li><a href=\"https://mini.eastday.com/hbzx/hdy.html?hbzxdlqid=03366\" target=\"_blank\">东方头条发百万红包，下载客户端领现金</a></li><li><a href=\"https://www.jikaicai.com/home?from=ch18060825&amp;src=default\" target=\"_blank\">主播帮忙买彩票中了十万</a></li></ul><p></p><p>新华网北京10月23日电据教育部网站消息，23日中共中央组织部在北京大学宣布了中共中央、国务院的任免决定，邱水平任北京大学党委书记（副部长级），郝平由北京大学党委书记转任校长，林建华不再担任北京大学校长职务。<br/>邱水平，男，1962年6月出生，中共党员，1983年8月参加工作，北京大学法学理论专业硕士研究生毕业，二级大法官。曾任北京大学团委书记、学生工作部部长，北京市朝阳区副区长，平谷区委副书记、区长、区委书记，北京市委副秘书长、政法委常务副书记，2017年1月至今任山西省高级人民法院院长。<br/>郝平，男，1959年9月出生，中共党员，1982年8月参加工作，北京大学国际关系学专业在职博士研究生毕业，教授。曾任北京大学副校长，北京外国语大学校长，教育部党组成员、副部长，中国联合国教科文组织全国委员会主任，2016年12月至今任北京大学党委书记。<br/>同日，南京大学与中国药科大学也迎来新任党委书记，胡金波任南京大学党委书记（副部长级），金能明任中国药科大学党委书记。<br/>胡金波，男，1960年1月出生，中共党员，1976年7月参加工作，河海大学技术经济及管理专业在职博士研究生毕业，研究员。曾任南京农业大学党委副书记、副校长，中国药科大学党委书记，江苏省教育厅副厅长，省委教育工委副书记，江苏省委组织部副部长、常务副部长，2018年1月至今任江苏省政协副主席。<br/>金能明，男，1963年10月生，1984年5月入党，1985年7月参加工作，厦门大学工商管理专业硕士研究生毕业。曾任厦门大学副校长、内蒙古自治区巴彦淖尔市委常委、副市长（挂职），2015年5月任宁夏大学党委书记。</p>', '', '82&80', '公告发布者1', '2018-10-31 16:10:64');

-- ----------------------------
-- Table structure for api
-- ----------------------------
DROP TABLE IF EXISTS `api`;
CREATE TABLE `api`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apiurl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `comment` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `apiurl`(`apiurl`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of api
-- ----------------------------
INSERT INTO `api` VALUES (1, '/unit/getunitbypage', '获得单位信息管理,分页');
INSERT INTO `api` VALUES (2, '/unit/deleteunitbyid/:id', '删除单位');
INSERT INTO `api` VALUES (3, '/unit/updateunit', '修改单位信息');
INSERT INTO `api` VALUES (4, '/unit/createunit', '创建单位');
INSERT INTO `api` VALUES (5, '/unit/getunitpolicebyid', '根据单位id查看相应的警员信息,页码');
INSERT INTO `api` VALUES (6, '/unit/getunitbyid', '根据id获得对应的单位');
INSERT INTO `api` VALUES (7, '/police/getpolicebypage', '警员信息分页');
INSERT INTO `api` VALUES (8, '/police/deletepolicebyid/:id', '警员信息删除');
INSERT INTO `api` VALUES (9, '/police/createpolice', '警员信息创建');
INSERT INTO `api` VALUES (10, '/police/getidbynumber', '根据警员编号返回id');
INSERT INTO `api` VALUES (11, '/police/updatepolice', '警员信息修改');
INSERT INTO `api` VALUES (12, '/police/getpolicebyid', '根据id获得警员信息');
INSERT INTO `api` VALUES (13, '/police/searchpolice', '警员信息搜索,模糊搜索,警员创建时的参数,参数任意多个,可省略');
INSERT INTO `api` VALUES (14, '/village/deletevillagebyid/:id', '村子信息删除');
INSERT INTO `api` VALUES (15, '/village/createvillage', '村子信息创建');
INSERT INTO `api` VALUES (16, '/village/updatevillage', '村子信息修改');
INSERT INTO `api` VALUES (17, '/village/getvillagebyid', '根据id获取村子信息');
INSERT INTO `api` VALUES (18, '/village/getvillagebytownshipid', '根据乡镇id获取下面众多乡村,分页');
INSERT INTO `api` VALUES (19, '/township/gettownshipbypage', '乡镇信息获取');
INSERT INTO `api` VALUES (20, '/township/deletetownshipbyid/:id', '乡镇信息删除');
INSERT INTO `api` VALUES (21, '/township/updatetownship', '乡镇信息修改');
INSERT INTO `api` VALUES (22, '/township/createtownship', '乡镇信息创建');
INSERT INTO `api` VALUES (23, '/township/gettownshipbyid', '根据id查询乡镇信息');
INSERT INTO `api` VALUES (24, '/flowperson/getflowpersonbypage', '流动人口信息查询,分页');
INSERT INTO `api` VALUES (25, '/flowperson/createflowperson', '流动人口信息创建');
INSERT INTO `api` VALUES (26, '/flowperson/updateflowperson', '流动人口信息修改');
INSERT INTO `api` VALUES (27, '/flowperson/deleteflowpersonbyid/:id', '流动人口信息删除');
INSERT INTO `api` VALUES (28, '/nativeperson/createnativeperson', '本地人口增加');
INSERT INTO `api` VALUES (29, '/nativeperson/updatenativeperson', '本地人口修改');
INSERT INTO `api` VALUES (30, '/nativeperson/deletenativepersonbyid/:id', '本地人口删除');
INSERT INTO `api` VALUES (31, '/nativeperson/getnativepersonbypage', '本地人口查看,分页');
INSERT INTO `api` VALUES (32, '/nativeperson/getnativepersonbyidcard', '本地人口信息查找,根据身份证号码');
INSERT INTO `api` VALUES (33, '/emphasisperson/createemphasisperson', '重点人口新增');
INSERT INTO `api` VALUES (34, '/emphasisperson/updateemphasisperson', '重点人口修改');
INSERT INTO `api` VALUES (35, '/emphasisperson/deleteemphasispersonbyid/:id', '重点人口删除');
INSERT INTO `api` VALUES (36, '/emphasisperson/getemphasispersonbypage', '重点人口查询,分页');
INSERT INTO `api` VALUES (37, '/announcement/getannouncementbypage', '公告信息获得');
INSERT INTO `api` VALUES (38, '/announcement/deleteannouncementbyid/:id', '公告删除');
INSERT INTO `api` VALUES (39, '/announcement/updateannouncement', '公告修改');
INSERT INTO `api` VALUES (40, '/announcement/createannouncement', '公告新增');
INSERT INTO `api` VALUES (41, '/announcement/updatereply', '公告对应的单位阅读状态修改');
INSERT INTO `api` VALUES (42, '/policeaffairs/createpoliceaffairs', '信息报送(新增警务)');
INSERT INTO `api` VALUES (43, '/policeaffairs/updatepoliceaffairs', '信息(警务)更新相关的几个操作');
INSERT INTO `api` VALUES (44, '/policeaffairs/deletepoliceaffairsbyid/:id', '警务信息删除');
INSERT INTO `api` VALUES (45, '/policeaffairs/getpoliceaffairsbypage', '获得警务信息,分页');
INSERT INTO `api` VALUES (48, '/reply/createreply', '回复或批示新增');
INSERT INTO `api` VALUES (49, '/news/createnews', '轮播/新闻新增');
INSERT INTO `api` VALUES (50, '/news/updatenews', '轮播/新闻修改');
INSERT INTO `api` VALUES (51, '/news/deletenewsbyid/:id', '轮播或新闻删除');
INSERT INTO `api` VALUES (52, '/news/getnewsbypage', '新闻/轮播获取,分页');
INSERT INTO `api` VALUES (53, '/news/getnewsbysort', '轮播获取');
INSERT INTO `api` VALUES (54, '/a/c', '测试');

-- ----------------------------
-- Table structure for emphasiscontrol
-- ----------------------------
DROP TABLE IF EXISTS `emphasiscontrol`;
CREATE TABLE `emphasiscontrol`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '重点人口管控记',
  `nativeperson_id` int(11) NULL DEFAULT NULL COMMENT '本地人口id',
  `remark_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '记录人姓名',
  `record_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '记录日期',
  `related_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '相关记录,管控详情',
  `info_source` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '消息来源',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of emphasiscontrol
-- ----------------------------
INSERT INTO `emphasiscontrol` VALUES (1, 1, '张警官', '2018-7-30', '涉及砍人', '仙人');
INSERT INTO `emphasiscontrol` VALUES (2, 2, '张警官', '2018-7-30', '涉及砍人', '仙人');
INSERT INTO `emphasiscontrol` VALUES (3, 2, '张警官', '2018-7-30', '涉及砍人', '仙人');
INSERT INTO `emphasiscontrol` VALUES (4, 1, '张警官', '2018-7-30', '涉及砍人', '仙人');
INSERT INTO `emphasiscontrol` VALUES (5, 3, '张警官', '2018-7-30', '涉及砍人', '仙人');
INSERT INTO `emphasiscontrol` VALUES (8, 17, '2342', '2018-09-03 12:09:11', '34234', '234324');
INSERT INTO `emphasiscontrol` VALUES (9, 17, '阿萨德', '2018-10-26 10:10:10', '阿萨德', '撒大声地');

-- ----------------------------
-- Table structure for emphasisperson
-- ----------------------------
DROP TABLE IF EXISTS `emphasisperson`;
CREATE TABLE `emphasisperson`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '重点人口',
  `nativeperson_id` int(11) NULL DEFAULT NULL COMMENT '本地人口信息id',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '人员类别',
  `police_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '警员编号',
  `related_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '相关记录,管控详情',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of emphasisperson
-- ----------------------------
INSERT INTO `emphasisperson` VALUES (2, 16, 'B', '0101002', '公司是否');
INSERT INTO `emphasisperson` VALUES (3, 16, 'C', '0101002', '公司是否');
INSERT INTO `emphasisperson` VALUES (4, 16, 'D', '0101002', '公司是否');
INSERT INTO `emphasisperson` VALUES (5, 16, 'B', '0101002', '公司是否');
INSERT INTO `emphasisperson` VALUES (6, 17, 'A', '0101002', '该成员涉恐');
INSERT INTO `emphasisperson` VALUES (7, 49, 'C', '1273971293', '阿斯顿好看了11');

-- ----------------------------
-- Table structure for file
-- ----------------------------
DROP TABLE IF EXISTS `file`;
CREATE TABLE `file`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `describe` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of file
-- ----------------------------
INSERT INTO `file` VALUES (3, '/static/picture/20180711162437.jpg', '测试');

-- ----------------------------
-- Table structure for flowperson
-- ----------------------------
DROP TABLE IF EXISTS `flowperson`;
CREATE TABLE `flowperson`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流动人口信息',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '姓名',
  `sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '性别',
  `born_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '出生日期',
  `idcard` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '身份证号码',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '电话号码',
  `village_id` int(11) NULL DEFAULT NULL COMMENT '所属乡镇乡村id',
  `birthplace` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '籍贯,\"-\"分割',
  `flow_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '流动原因类型',
  `flow_reason` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '流动原因',
  `flow_tothis_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '到此地日期',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idcard`(`idcard`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flowperson
-- ----------------------------
INSERT INTO `flowperson` VALUES (16, '流动人口1', '男', '', '513823199405172881', '15182213701', 42, '山西省-长治市-长治县', '经商', '工作场所1', '2017-10-04', '');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单名称',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '父级菜单',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `menu_name`(`menu_name`, `parent_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (7, '乡镇信息管理', 1);
INSERT INTO `menu` VALUES (9, '人口信息管理', 1);
INSERT INTO `menu` VALUES (47, '人口数据分析', 9);
INSERT INTO `menu` VALUES (4, '信息发布', NULL);
INSERT INTO `menu` VALUES (14, '信息报送', 2);
INSERT INTO `menu` VALUES (15, '信息查阅', 2);
INSERT INTO `menu` VALUES (27, '信息编辑', 14);
INSERT INTO `menu` VALUES (36, '修改', 6);
INSERT INTO `menu` VALUES (31, '修改', 8);
INSERT INTO `menu` VALUES (63, '修改', 10);
INSERT INTO `menu` VALUES (50, '修改', 11);
INSERT INTO `menu` VALUES (58, '修改', 12);
INSERT INTO `menu` VALUES (87, '修改', 18);
INSERT INTO `menu` VALUES (75, '修改', 25);
INSERT INTO `menu` VALUES (71, '修改', 26);
INSERT INTO `menu` VALUES (79, '修改发布信息', 22);
INSERT INTO `menu` VALUES (82, '修改发布信息', 23);
INSERT INTO `menu` VALUES (41, '修改行政村信息', 7);
INSERT INTO `menu` VALUES (80, '停止发布', 22);
INSERT INTO `menu` VALUES (83, '停止发布', 23);
INSERT INTO `menu` VALUES (17, '公告发布', 3);
INSERT INTO `menu` VALUES (37, '删除', 6);
INSERT INTO `menu` VALUES (45, '删除', 7);
INSERT INTO `menu` VALUES (32, '删除', 8);
INSERT INTO `menu` VALUES (64, '删除', 10);
INSERT INTO `menu` VALUES (51, '删除', 11);
INSERT INTO `menu` VALUES (59, '删除', 12);
INSERT INTO `menu` VALUES (92, '删除', 15);
INSERT INTO `menu` VALUES (88, '删除', 18);
INSERT INTO `menu` VALUES (81, '删除', 22);
INSERT INTO `menu` VALUES (84, '删除', 23);
INSERT INTO `menu` VALUES (77, '删除', 25);
INSERT INTO `menu` VALUES (73, '删除', 26);
INSERT INTO `menu` VALUES (90, '删除', 28);
INSERT INTO `menu` VALUES (94, '删除', 65);
INSERT INTO `menu` VALUES (6, '单位信息管理', 1);
INSERT INTO `menu` VALUES (68, '历史统计数据', 66);
INSERT INTO `menu` VALUES (21, '发布板块信息', 4);
INSERT INTO `menu` VALUES (18, '发布记录', 3);
INSERT INTO `menu` VALUES (20, '发布轮播信息', 4);
INSERT INTO `menu` VALUES (33, '图表', 9);
INSERT INTO `menu` VALUES (1, '基本信息', NULL);
INSERT INTO `menu` VALUES (67, '实时统计数据', 66);
INSERT INTO `menu` VALUES (70, '所属乡镇', 47);
INSERT INTO `menu` VALUES (29, '所属单位', 8);
INSERT INTO `menu` VALUES (61, '批量导入流动人口', 10);
INSERT INTO `menu` VALUES (55, '批量导入重点人口', 12);
INSERT INTO `menu` VALUES (39, '批量新增', 6);
INSERT INTO `menu` VALUES (46, '批量新增', 8);
INSERT INTO `menu` VALUES (49, '批量新增', 11);
INSERT INTO `menu` VALUES (16, '报送记录', 2);
INSERT INTO `menu` VALUES (69, '排名情况', 66);
INSERT INTO `menu` VALUES (66, '数据统计', 16);
INSERT INTO `menu` VALUES (48, '新增', 11);
INSERT INTO `menu` VALUES (40, '新增乡镇', 7);
INSERT INTO `menu` VALUES (38, '新增单位', 6);
INSERT INTO `menu` VALUES (60, '新增流动人口', 10);
INSERT INTO `menu` VALUES (78, '新增用户', 25);
INSERT INTO `menu` VALUES (74, '新增角色', 26);
INSERT INTO `menu` VALUES (30, '新增警员', 8);
INSERT INTO `menu` VALUES (54, '新增重点人口', 12);
INSERT INTO `menu` VALUES (85, '是否置顶', 23);
INSERT INTO `menu` VALUES (11, '本地户籍管理', 9);
INSERT INTO `menu` VALUES (100, '查看', 7);
INSERT INTO `menu` VALUES (97, '查看', 10);
INSERT INTO `menu` VALUES (98, '查看', 11);
INSERT INTO `menu` VALUES (99, '查看', 12);
INSERT INTO `menu` VALUES (91, '查看', 15);
INSERT INTO `menu` VALUES (86, '查看', 18);
INSERT INTO `menu` VALUES (95, '查看', 22);
INSERT INTO `menu` VALUES (96, '查看', 23);
INSERT INTO `menu` VALUES (76, '查看', 25);
INSERT INTO `menu` VALUES (72, '查看', 26);
INSERT INTO `menu` VALUES (101, '查看', 47);
INSERT INTO `menu` VALUES (93, '查看', 65);
INSERT INTO `menu` VALUES (42, '查看乡镇详情', 7);
INSERT INTO `menu` VALUES (43, '查看人口详情', 7);
INSERT INTO `menu` VALUES (52, '查看家庭成员', 11);
INSERT INTO `menu` VALUES (34, '查看警员信息', 6);
INSERT INTO `menu` VALUES (62, '查看详情', 10);
INSERT INTO `menu` VALUES (53, '查看详情', 11);
INSERT INTO `menu` VALUES (57, '查看详情', 12);
INSERT INTO `menu` VALUES (89, '查看详情', 28);
INSERT INTO `menu` VALUES (10, '流动人员管理', 9);
INSERT INTO `menu` VALUES (23, '版块信息发布记录', 4);
INSERT INTO `menu` VALUES (26, '用户账号管理', 5);
INSERT INTO `menu` VALUES (56, '管控记录', 12);
INSERT INTO `menu` VALUES (5, '系统维护', NULL);
INSERT INTO `menu` VALUES (28, '草稿管理', 14);
INSERT INTO `menu` VALUES (25, '角色信息维护', 5);
INSERT INTO `menu` VALUES (2, '警务信息', NULL);
INSERT INTO `menu` VALUES (8, '警员信息管理', 1);
INSERT INTO `menu` VALUES (65, '记录查看', 16);
INSERT INTO `menu` VALUES (22, '轮播信息发布记录', 4);
INSERT INTO `menu` VALUES (3, '通知公告', NULL);
INSERT INTO `menu` VALUES (12, '重点人员管理', 9);

-- ----------------------------
-- Table structure for nativeperson
-- ----------------------------
DROP TABLE IF EXISTS `nativeperson`;
CREATE TABLE `nativeperson`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '本地户口信息管理',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '姓名',
  `sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '性别',
  `born_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '出生日期',
  `village_id` int(11) NULL DEFAULT NULL COMMENT '所属乡镇乡村',
  `idcard` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '身份证号码',
  `now_location` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '现居地',
  `education` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '文化程度',
  `political_status` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '政治面貌',
  `ishouseholder` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '否' COMMENT '是否未户主,默认否',
  `household_num` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '户号',
  `householder_relation` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '与户主关系',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '联系方式',
  `career` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '职业',
  `ismarry` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '是否已婚',
  `ismove` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '是否外地迁入',
  `move_address` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '迁入地',
  `move_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '迁入时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idcard`(`idcard`) USING BTREE COMMENT '身份证号码唯一'
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nativeperson
-- ----------------------------
INSERT INTO `nativeperson` VALUES (16, '都撒到', '男', '1996-08-30', 2, '340122199611141900', '北京市-市辖区-西城区', '硕士', '共产党员', '是', '1003', '父子', '18100171122', '公务员', '已婚', '是', '天津市-市辖区-河东区', '2018-08-30');
INSERT INTO `nativeperson` VALUES (17, '18届毕业作品DDD', '女', '1996-08-30', 3, '11112', '河北省-秦皇岛市-北戴河区', '硕士', '共青团员', '否', '1003', '父子', '13899888899', '公务员', '未婚', '否', '山西省-晋城市-泽州县', '2018-08-30');
INSERT INTO `nativeperson` VALUES (18, 'luoyelusheng', '女', '1996-08-30', 12, '522123199599212511', '山西省-阳泉市-山西阳泉经济开发区', '硕士', '共青团员', '否', '1003', '父子', '15869190407', '学生', '未婚', '否', '', '');
INSERT INTO `nativeperson` VALUES (19, 'luoyelusheng', '女', '1996-08-30', 3, '522123199509212511', '天津市-市辖区-河东区', '硕士', '共产党员', '否', '1003', '父子', '13899888899', '学生', '已婚', '否', '', '');
INSERT INTO `nativeperson` VALUES (20, '都撒到', '男', '1996-08-30', 2, '340122199611141001', '北京市-市辖区-西城区', '硕士', '共产党员', '是', '1003', '父子', '18100171122', '公务员', '已婚', '是', '天津市-市辖区-河东区', '2018-01-01');
INSERT INTO `nativeperson` VALUES (21, '5556', '男', '1996-01-02', 2, '340122199611141002', '北京市-市辖区-西城区', '硕士', '共产党员', '是', '1004', '父子', '18100171123', '公务员', '已婚', '是', '天津市-市辖区-河东区', '2018-01-02');
INSERT INTO `nativeperson` VALUES (22, '5557', '男', '1996-01-03', 2, '340122199611141003', '北京市-市辖区-西城区', '硕士', '共产党员', '是', '1005', '父子', '18100171124', '公务员', '已婚', '是', '天津市-市辖区-河东区', '2018-01-03');
INSERT INTO `nativeperson` VALUES (23, '5558', '男', '1996-01-04', 2, '340122199611141004', '北京市-市辖区-西城区', '硕士', '共产党员', '是', '1006', '父子', '18100171125', '公务员', '已婚', '是', '天津市-市辖区-河东区', '2018-01-04');
INSERT INTO `nativeperson` VALUES (28, '陈天飞', '男', '1994-10-16', NULL, '51382199405172883', '四川省-眉山市-彭山区', '硕士', '共青团员', '是', '012231', '本人', '15182213701', '学生', '未婚', '否', '天津市-市辖区-河东区', '2003-10-16');
INSERT INTO `nativeperson` VALUES (32, '人口1', '女', '2018-10-23', 42, '513821199405172883', '天津市-市辖区-河东区', '硕士', '共青团员', '是', '012231', '本人', '15182213701', '学生', '已婚', '否', '', '');
INSERT INTO `nativeperson` VALUES (33, '人口1', '男', '2018-10-04', 42, '513823199405172881', '北京市-市辖区-东城区', '硕士', '群众', '否', '012231', '妻子', '15182213701', '学生', '未婚', '否', '', '');
INSERT INTO `nativeperson` VALUES (34, '陈一', '男', '2013-10-10', 42, '513286201310105320', '河北省-秦皇岛市-北戴河区', '博士', '群众', '是', '02032152', '是', '15355221532', '公务员', '未婚', '是', '河北省-秦皇岛市-山海关区', '2018-10-25');
INSERT INTO `nativeperson` VALUES (36, '萨芬', '男', '2018-11-05', 42, '340122199611141919', '北京市-市辖区-东城区', '博士', '共产党员', '是', '123', '14', '15869190407', '公务员', '已婚', '是', '天津市-市辖区-和平区', '');
INSERT INTO `nativeperson` VALUES (39, '姓名1', '男', '1996-8-9', 1, '340122199611141911', '安徽-合肥-肥西县', '博士', '党员', '是', '10010', '父子', '15869190407', '警员', '是', '是', '安徽-合肥-肥西县', '2002-10-11');
INSERT INTO `nativeperson` VALUES (49, '飞哥', '男', '2018-11-01', 42, '5221231995992125111', '北京市-市辖区-东城区', '博士', '共产党员', '是', '10031', '父子', '18100171144', '公务员', '已婚', '否', '', '');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '轮播新闻信息',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `imageurl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '轮播封面',
  `publish_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '发布人',
  `publish_num` int(11) NULL DEFAULT 0 COMMENT '轮播展示序号,默认0为新闻信息',
  `village_id` int(11) NULL DEFAULT NULL COMMENT '发布来源所属乡村',
  `istop` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '不置顶' COMMENT '默认不置顶',
  `ispublish` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '已发布' COMMENT '默认已发布',
  `read_num` int(11) NULL DEFAULT 0 COMMENT '阅读量,默认0',
  `publish_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES (24, '使用v12312312312312312312313w实现移ff11111sdfsdf', '<p style=\"text-align:start;\">有关于移动端的适配布局一直以来都是众说纷纭，对应的解决方案也是有很多种。在《<a href=\"https://www.w3cplus.com/mobile/lib-flexible-for-html5-layout.html\" target=\"_blank\">使用Flexible实现手淘H5页面的终端适配</a>》提出了Flexible的布局方案，随着<code>viewport</code>单位越来越受到众多浏览器的支持，因此在《<a href=\"https://www.w3cplus.com/css/vw-for-layout.html\" target=\"_blank\">再聊移动端页面的适配</a>》一文中提出了<code>vw</code>来做移动端的适配问题。到目前为止不管是哪一种方案，都还存在一定的缺vv陷。言外之意 ，还没有哪一个方案是完美的。</p><p style=\"text-align:start;\">事实上真的不完美？其实不然。最近为了新项目中能更完美的使用<code>vw</code>来做移动端的适配。探讨出一种能解决不兼容<code>viewport</code>单位的方案。今天整理一下，与大家一起分享。如果方案中存在一定的缺陷，欢迎大家 一起拍正 。</p><h2 style=\"text-align:start;\">准备工作</h2><p style=\"text-align:start;\">对于Flexible或者说<code>vw</code>的布局，其原理不在这篇文章进行阐述。如果你想追踪其中的原委，强烈建议你阅读早前整理的文章《<a href=\"https://www.w3cplus.com/mobile/lib-flexible-for-html5-layout.html\" target=\"_blank\">使用Flexible实现手淘H5页面的终端适配</a>》和《<a href=\"https://www.w3cplus.com/css/vw-for-layout.html\" target=\"_blank\">再聊移动端页面的适配</a>》。</p><blockquote style=\"text-align:start;\">说句题外话，由于Flexible的出现，也造成很多同学对<code>rem</code>的误解。正如当年大家对<code>div</code>的误解一样。也因此，大家都觉得<code>rem</code>是万能的，他能直接解决移动端的适配问题。事实并不是如此，至于为什么，我想大家应该去阅读<code><a href=\"https://github.com/amfe/lib-flexible\" target=\"_blank\">flexible.js</a></code>源码，我相信你会明白其中的原委。</blockquote><p style=\"text-align:start;\">回到我们今天要聊的主题，怎么实现<code>vw</code>的兼容问题。为了解决这个兼容问题，我将  借助Vue官网提供的构建工程以及一些PostCSS插件来完成。在继续后面的内容之前，需要准备一些东西：</p><ul><li><a href=\"https://nodejs.org/en/\" target=\"_blank\">NodeJs</a></li><li><a href=\"https://www.npmjs.com/\" target=\"_blank\">NPM</a></li><li><a href=\"https://webpack.js.org/\" target=\"_blank\">Webpack</a></li><li><a href=\"https://github.com/vuejs/vue-cli\" target=\"_blank\">Vue-cli</a></li><li><a href=\"https://github.com/postcss/postcss-import\" target=\"_blank\">postcss-import</a></li><li><a href=\"https://github.com/postcss/postcss-url\" target=\"_blank\">postcss-url</a></li><li><a href=\"https://github.com/yisibl/postcss-aspect-ratio-mini\" target=\"_blank\">postcss-aspect-ratio-mini</a></li><li><a href=\"https://github.com/MoOx/postcss-cssnext\" target=\"_blank\">postcss-cssnext</a></li><li><a href=\"https://github.com/postcss/autoprefixer\" target=\"_blank\">autoprefixer</a></li><li><a href=\"https://github.com/evrone/postcss-px-to-viewport\" target=\"_blank\">postcss-px-to-viewport</a></li><li><a href=\"https://github.com/jonathantneal/postcss-write-svg\" target=\"_blank\">postcss-write-svg</a></li><li><a href=\"https://github.com/ben-eb/cssnano\" target=\"_blank\">cssnano</a></li><li><a href=\"https://github.com/springuper/postcss-viewport-units\" target=\"_blank\">postcss-viewport-units</a></li><li><a href=\"https://github.com/rodneyrehm/viewport-units-buggyfill\" target=\"_blank\">Viewport Units Buggyfill</a> </li></ul><p style=\"text-align:start;\">对于这些起什么作用，先不阐述，后续我们会聊到上述的一些东西。</p><p><span style=\"color:#404040\"><span style=\"font-size:14px\"><span style=\"background-color:#eeeeee\">著作权归作者所有。</span></span></span></p><p><span style=\"color:#404040\"><span style=\"font-size:14px\"><span style=\"background-color:#eeeeee\">商业转载请联系作者获得授权,非商业转载请注明出处。</span></span></span></p><p><span style=\"color:#404040\"><span style=\"font-size:14px\"><span style=\"background-color:#eeeeee\">原文:</span></span></span><a href=\"https://www.w3cplus.com/mobile/vw-layout-in-vue.html\" target=\"\">https://www.w3cplus.com/mobile/vw-layout-in-vue.html</a><span style=\"color:#404040\"><span style=\"font-size:14px\"><span style=\"background-color:#eeeeee\">©</span></span></span><a href=\"https://www.w3cplus.com/\" target=\"\">w3cplus.com</a></p><p></p>', '/static/file/20181030212957.jpg', '李甜甜', 111, 42, '置顶', '已发布', 6, '2018-11-22');

-- ----------------------------
-- Table structure for police
-- ----------------------------
DROP TABLE IF EXISTS `police`;
CREATE TABLE `police`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '警员编号',
  `police_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '警员姓名',
  `police_sex` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '性别',
  `born_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '出生日期',
  `idcard` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '身份证号码',
  `birthplace` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '籍贯',
  `unit_id` int(11) NULL DEFAULT NULL COMMENT '所属单位,可不填',
  `police_position` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '职务',
  `village_id` int(11) NULL DEFAULT NULL COMMENT '关联乡村',
  `police_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '联系方式',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `number`(`number`) USING BTREE,
  UNIQUE INDEX `idcard`(`idcard`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 194 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of police
-- ----------------------------
INSERT INTO `police` VALUES (190, '1010099', '陈天', '男', '35383-01-01', '340122199611141941', '北京市-市辖区-东城区', 80, '科员', 42, '15869190407');
INSERT INTO `police` VALUES (191, '1010100', '陈一', '男', '2018-10-03', '513823199506233654', '江苏省-徐州市-贾汪区', 84, '科员', 42, '15323658852');
INSERT INTO `police` VALUES (192, '04', 'lu', '男', '2013-01-05', '340122199611141919', '北京市-市辖区-东城区', 80, 'asf', 42, '15869190407');

-- ----------------------------
-- Table structure for policeaffairs
-- ----------------------------
DROP TABLE IF EXISTS `policeaffairs`;
CREATE TABLE `policeaffairs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '警务',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '标题',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `unit_id` int(11) NULL DEFAULT NULL COMMENT '接收单位',
  `village_id` int(11) NULL DEFAULT NULL COMMENT '所属行政村',
  `police_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '报送人警员编号',
  `issend` int(2) NULL DEFAULT 0 COMMENT '默认0,存为草稿,1提交',
  `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '下载的附件地址,多个用&符号分割',
  `isread` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '未读' COMMENT '已读/未,默认未读',
  `publish_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of policeaffairs
-- ----------------------------
INSERT INTO `policeaffairs` VALUES (28, '测试实验', '<p><span style=\"color:#000000\"><span style=\"font-size:16px\"><span style=\"background-color:#ffffff\">实验内容实验内容实验内容实验内容实验内容实验内容实验内容实验内容</span></span></span></p>', 82, 42, '03', 1, '', '已读', '2018-11-06 11:11:56');
INSERT INTO `policeaffairs` VALUES (29, '权威', '<p>123</p>', 80, 42, '03', 0, '', '未读', '2018-11-06 14:11:54');

-- ----------------------------
-- Table structure for readstatus
-- ----------------------------
DROP TABLE IF EXISTS `readstatus`;
CREATE TABLE `readstatus`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '阅读状态',
  `unit_id` int(11) NULL DEFAULT NULL COMMENT '单位id',
  `isread` int(2) NULL DEFAULT 0 COMMENT '0:未读/1:已读',
  `read_date` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '阅读时间',
  `announcement_id` int(11) NULL DEFAULT NULL COMMENT '阅读的公告',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of readstatus
-- ----------------------------
INSERT INTO `readstatus` VALUES (1, 1, 1, '', 10);
INSERT INTO `readstatus` VALUES (2, 2, 0, '', 10);
INSERT INTO `readstatus` VALUES (3, 3, 0, '', 11);
INSERT INTO `readstatus` VALUES (4, 1, 0, '', 11);
INSERT INTO `readstatus` VALUES (5, 7, 0, '', 11);
INSERT INTO `readstatus` VALUES (6, 3, 0, '', 12);
INSERT INTO `readstatus` VALUES (7, 1, 0, '', 12);
INSERT INTO `readstatus` VALUES (8, 7, 0, '', 12);
INSERT INTO `readstatus` VALUES (9, 18, 1, '', 0);
INSERT INTO `readstatus` VALUES (10, 4, 0, '', 0);
INSERT INTO `readstatus` VALUES (11, 3, 0, '', 0);
INSERT INTO `readstatus` VALUES (12, 1, 0, '', 0);
INSERT INTO `readstatus` VALUES (13, 7, 1, '', 0);
INSERT INTO `readstatus` VALUES (15, 1, 0, '', 0);
INSERT INTO `readstatus` VALUES (16, 38, 1, '', 0);
INSERT INTO `readstatus` VALUES (17, 7, 1, '', 0);
INSERT INTO `readstatus` VALUES (18, 9, 0, '', 0);
INSERT INTO `readstatus` VALUES (19, 21, 0, '', 0);
INSERT INTO `readstatus` VALUES (20, 40, 0, '', 18);
INSERT INTO `readstatus` VALUES (21, 3, 0, '', 18);
INSERT INTO `readstatus` VALUES (22, 63, 0, '', 18);
INSERT INTO `readstatus` VALUES (23, 18, 1, '', 18);
INSERT INTO `readstatus` VALUES (24, 5, 1, '', 18);
INSERT INTO `readstatus` VALUES (25, 41, 1, '', 18);
INSERT INTO `readstatus` VALUES (26, 4, 1, '', 18);
INSERT INTO `readstatus` VALUES (27, 83, 1, '', 10);
INSERT INTO `readstatus` VALUES (28, 82, 0, '', 10);
INSERT INTO `readstatus` VALUES (29, 80, 0, '', 10);
INSERT INTO `readstatus` VALUES (30, 88, 0, '', 14);

-- ----------------------------
-- Table structure for reply
-- ----------------------------
DROP TABLE IF EXISTS `reply`;
CREATE TABLE `reply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '批示或回复,根据id顺序当做时间顺序',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '名字',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '批示或回复内容',
  `policeaffairs_id` int(11) NULL DEFAULT NULL COMMENT '批示或回复警务信息',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of reply
-- ----------------------------
INSERT INTO `reply` VALUES (1, '陈天飞', '哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈', 1);
INSERT INTO `reply` VALUES (2, '陈天', '哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈哈', 4);
INSERT INTO `reply` VALUES (3, '陈天飞', '老板大气', 1);
INSERT INTO `reply` VALUES (4, '陈飞', '老板大气', 9);
INSERT INTO `reply` VALUES (5, '韩语第一期测试', '到法律', 9);
INSERT INTO `reply` VALUES (6, ' 实打实地方', '安达市多', 9);
INSERT INTO `reply` VALUES (7, 'werwer', '沃尔沃二玩儿', 9);
INSERT INTO `reply` VALUES (8, '次仁', '很好', 16);
INSERT INTO `reply` VALUES (9, '次仁书记', '这次活动做得很很好，', 17);
INSERT INTO `reply` VALUES (10, '陈天飞', '我们会按照书记的指示进行保持', 17);
INSERT INTO `reply` VALUES (11, '陈一', '很好', 17);
INSERT INTO `reply` VALUES (12, '次仁书记', '希望加强整治力度', 22);
INSERT INTO `reply` VALUES (13, '陈天飞', '明白，书记', 22);
INSERT INTO `reply` VALUES (14, '飞哥', '很好', 21);
INSERT INTO `reply` VALUES (15, '方导', '恩', 21);
INSERT INTO `reply` VALUES (16, '飞哥', '可以啊', 21);
INSERT INTO `reply` VALUES (17, '测试1', '批示测试', 24);
INSERT INTO `reply` VALUES (18, '回复', '回复测试', 24);
INSERT INTO `reply` VALUES (19, '次仁书记', '测试批示功能', 26);
INSERT INTO `reply` VALUES (20, '陈天飞', '收到', 26);
INSERT INTO `reply` VALUES (21, '次仁书记', '已阅', 27);
INSERT INTO `reply` VALUES (22, '陈天飞', '收到，书记', 27);
INSERT INTO `reply` VALUES (23, '次仁书记', '很好', 28);
INSERT INTO `reply` VALUES (24, '飞哥', '123', 28);
INSERT INTO `reply` VALUES (25, '234', '请问', 28);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '超级管理员');
INSERT INTO `role` VALUES (9, '首页信息展示');
INSERT INTO `role` VALUES (12, '查看全部单位信息');
INSERT INTO `role` VALUES (13, '查看全部乡镇信息');
INSERT INTO `role` VALUES (14, '查看全部警员信息');
INSERT INTO `role` VALUES (15, '查询全部本地人口');
INSERT INTO `role` VALUES (16, '查询全部流动人口');
INSERT INTO `role` VALUES (17, '查询全部重点人口');
INSERT INTO `role` VALUES (20, '1号领导');

-- ----------------------------
-- Table structure for rolelist
-- ----------------------------
DROP TABLE IF EXISTS `rolelist`;
CREATE TABLE `rolelist`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL,
  `menu_id` int(11) NULL DEFAULT NULL COMMENT '菜单',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1077 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rolelist
-- ----------------------------
INSERT INTO `rolelist` VALUES (254, 1, 1);
INSERT INTO `rolelist` VALUES (255, 1, 2);
INSERT INTO `rolelist` VALUES (256, 1, 3);
INSERT INTO `rolelist` VALUES (257, 1, 4);
INSERT INTO `rolelist` VALUES (258, 1, 5);
INSERT INTO `rolelist` VALUES (259, 1, 6);
INSERT INTO `rolelist` VALUES (260, 1, 7);
INSERT INTO `rolelist` VALUES (261, 1, 8);
INSERT INTO `rolelist` VALUES (262, 1, 9);
INSERT INTO `rolelist` VALUES (263, 1, 10);
INSERT INTO `rolelist` VALUES (264, 1, 11);
INSERT INTO `rolelist` VALUES (265, 1, 12);
INSERT INTO `rolelist` VALUES (266, 1, 14);
INSERT INTO `rolelist` VALUES (267, 1, 15);
INSERT INTO `rolelist` VALUES (268, 1, 16);
INSERT INTO `rolelist` VALUES (269, 1, 17);
INSERT INTO `rolelist` VALUES (270, 1, 18);
INSERT INTO `rolelist` VALUES (271, 1, 20);
INSERT INTO `rolelist` VALUES (272, 1, 21);
INSERT INTO `rolelist` VALUES (273, 1, 22);
INSERT INTO `rolelist` VALUES (274, 1, 23);
INSERT INTO `rolelist` VALUES (275, 1, 25);
INSERT INTO `rolelist` VALUES (276, 1, 26);
INSERT INTO `rolelist` VALUES (277, 1, 27);
INSERT INTO `rolelist` VALUES (278, 1, 28);
INSERT INTO `rolelist` VALUES (279, 1, 29);
INSERT INTO `rolelist` VALUES (280, 1, 30);
INSERT INTO `rolelist` VALUES (281, 1, 31);
INSERT INTO `rolelist` VALUES (282, 1, 32);
INSERT INTO `rolelist` VALUES (283, 1, 33);
INSERT INTO `rolelist` VALUES (284, 1, 34);
INSERT INTO `rolelist` VALUES (285, 1, 36);
INSERT INTO `rolelist` VALUES (286, 1, 37);
INSERT INTO `rolelist` VALUES (287, 1, 38);
INSERT INTO `rolelist` VALUES (288, 1, 39);
INSERT INTO `rolelist` VALUES (289, 1, 40);
INSERT INTO `rolelist` VALUES (290, 1, 41);
INSERT INTO `rolelist` VALUES (291, 1, 42);
INSERT INTO `rolelist` VALUES (292, 1, 43);
INSERT INTO `rolelist` VALUES (293, 1, 44);
INSERT INTO `rolelist` VALUES (294, 1, 45);
INSERT INTO `rolelist` VALUES (295, 1, 46);
INSERT INTO `rolelist` VALUES (296, 1, 47);
INSERT INTO `rolelist` VALUES (297, 1, 48);
INSERT INTO `rolelist` VALUES (298, 1, 49);
INSERT INTO `rolelist` VALUES (299, 1, 50);
INSERT INTO `rolelist` VALUES (300, 1, 51);
INSERT INTO `rolelist` VALUES (301, 1, 52);
INSERT INTO `rolelist` VALUES (302, 1, 53);
INSERT INTO `rolelist` VALUES (303, 1, 54);
INSERT INTO `rolelist` VALUES (304, 1, 55);
INSERT INTO `rolelist` VALUES (305, 1, 56);
INSERT INTO `rolelist` VALUES (306, 1, 57);
INSERT INTO `rolelist` VALUES (307, 1, 58);
INSERT INTO `rolelist` VALUES (308, 1, 59);
INSERT INTO `rolelist` VALUES (309, 1, 60);
INSERT INTO `rolelist` VALUES (310, 1, 61);
INSERT INTO `rolelist` VALUES (311, 1, 62);
INSERT INTO `rolelist` VALUES (312, 1, 63);
INSERT INTO `rolelist` VALUES (313, 1, 64);
INSERT INTO `rolelist` VALUES (314, 1, 65);
INSERT INTO `rolelist` VALUES (315, 1, 66);
INSERT INTO `rolelist` VALUES (316, 1, 67);
INSERT INTO `rolelist` VALUES (317, 1, 68);
INSERT INTO `rolelist` VALUES (318, 1, 69);
INSERT INTO `rolelist` VALUES (319, 1, 70);
INSERT INTO `rolelist` VALUES (320, 1, 71);
INSERT INTO `rolelist` VALUES (321, 1, 72);
INSERT INTO `rolelist` VALUES (322, 1, 73);
INSERT INTO `rolelist` VALUES (323, 1, 74);
INSERT INTO `rolelist` VALUES (324, 1, 75);
INSERT INTO `rolelist` VALUES (325, 1, 76);
INSERT INTO `rolelist` VALUES (326, 1, 77);
INSERT INTO `rolelist` VALUES (327, 1, 78);
INSERT INTO `rolelist` VALUES (328, 1, 79);
INSERT INTO `rolelist` VALUES (329, 1, 80);
INSERT INTO `rolelist` VALUES (330, 1, 81);
INSERT INTO `rolelist` VALUES (331, 1, 82);
INSERT INTO `rolelist` VALUES (332, 1, 83);
INSERT INTO `rolelist` VALUES (333, 1, 84);
INSERT INTO `rolelist` VALUES (334, 1, 85);
INSERT INTO `rolelist` VALUES (335, 1, 86);
INSERT INTO `rolelist` VALUES (336, 1, 87);
INSERT INTO `rolelist` VALUES (337, 1, 88);
INSERT INTO `rolelist` VALUES (338, 1, 89);
INSERT INTO `rolelist` VALUES (339, 1, 90);
INSERT INTO `rolelist` VALUES (340, 1, 91);
INSERT INTO `rolelist` VALUES (341, 1, 92);
INSERT INTO `rolelist` VALUES (342, 1, 93);
INSERT INTO `rolelist` VALUES (343, 1, 94);
INSERT INTO `rolelist` VALUES (624, 12, 1);
INSERT INTO `rolelist` VALUES (625, 12, 6);
INSERT INTO `rolelist` VALUES (626, 12, 34);
INSERT INTO `rolelist` VALUES (627, 13, 1);
INSERT INTO `rolelist` VALUES (628, 13, 6);
INSERT INTO `rolelist` VALUES (629, 13, 34);
INSERT INTO `rolelist` VALUES (630, 13, 7);
INSERT INTO `rolelist` VALUES (631, 13, 43);
INSERT INTO `rolelist` VALUES (632, 14, 1);
INSERT INTO `rolelist` VALUES (633, 14, 6);
INSERT INTO `rolelist` VALUES (634, 14, 34);
INSERT INTO `rolelist` VALUES (635, 14, 7);
INSERT INTO `rolelist` VALUES (636, 14, 43);
INSERT INTO `rolelist` VALUES (637, 14, 8);
INSERT INTO `rolelist` VALUES (638, 14, 29);
INSERT INTO `rolelist` VALUES (639, 15, 1);
INSERT INTO `rolelist` VALUES (640, 15, 6);
INSERT INTO `rolelist` VALUES (641, 15, 34);
INSERT INTO `rolelist` VALUES (642, 15, 7);
INSERT INTO `rolelist` VALUES (643, 15, 43);
INSERT INTO `rolelist` VALUES (644, 15, 8);
INSERT INTO `rolelist` VALUES (645, 15, 29);
INSERT INTO `rolelist` VALUES (646, 15, 9);
INSERT INTO `rolelist` VALUES (647, 15, 11);
INSERT INTO `rolelist` VALUES (648, 15, 53);
INSERT INTO `rolelist` VALUES (649, 16, 1);
INSERT INTO `rolelist` VALUES (650, 16, 6);
INSERT INTO `rolelist` VALUES (651, 16, 34);
INSERT INTO `rolelist` VALUES (652, 16, 7);
INSERT INTO `rolelist` VALUES (653, 16, 43);
INSERT INTO `rolelist` VALUES (654, 16, 8);
INSERT INTO `rolelist` VALUES (655, 16, 29);
INSERT INTO `rolelist` VALUES (656, 16, 9);
INSERT INTO `rolelist` VALUES (657, 16, 11);
INSERT INTO `rolelist` VALUES (658, 16, 53);
INSERT INTO `rolelist` VALUES (659, 16, 10);
INSERT INTO `rolelist` VALUES (660, 16, 62);
INSERT INTO `rolelist` VALUES (661, 17, 1);
INSERT INTO `rolelist` VALUES (662, 17, 6);
INSERT INTO `rolelist` VALUES (663, 17, 34);
INSERT INTO `rolelist` VALUES (664, 17, 7);
INSERT INTO `rolelist` VALUES (665, 17, 43);
INSERT INTO `rolelist` VALUES (666, 17, 8);
INSERT INTO `rolelist` VALUES (667, 17, 29);
INSERT INTO `rolelist` VALUES (668, 17, 9);
INSERT INTO `rolelist` VALUES (669, 17, 11);
INSERT INTO `rolelist` VALUES (670, 17, 53);
INSERT INTO `rolelist` VALUES (671, 17, 10);
INSERT INTO `rolelist` VALUES (672, 17, 62);
INSERT INTO `rolelist` VALUES (673, 17, 12);
INSERT INTO `rolelist` VALUES (674, 17, 57);
INSERT INTO `rolelist` VALUES (871, 9, 47);
INSERT INTO `rolelist` VALUES (872, 9, 1);
INSERT INTO `rolelist` VALUES (873, 9, 9);
INSERT INTO `rolelist` VALUES (874, 9, 86);
INSERT INTO `rolelist` VALUES (875, 9, 3);
INSERT INTO `rolelist` VALUES (876, 9, 18);
INSERT INTO `rolelist` VALUES (877, 9, 93);
INSERT INTO `rolelist` VALUES (878, 9, 2);
INSERT INTO `rolelist` VALUES (879, 9, 16);
INSERT INTO `rolelist` VALUES (880, 9, 65);
INSERT INTO `rolelist` VALUES (881, 9, 15);
INSERT INTO `rolelist` VALUES (882, 9, 14);
INSERT INTO `rolelist` VALUES (1044, 20, 1);
INSERT INTO `rolelist` VALUES (1045, 20, 2);
INSERT INTO `rolelist` VALUES (1046, 20, 3);
INSERT INTO `rolelist` VALUES (1047, 20, 6);
INSERT INTO `rolelist` VALUES (1048, 20, 9);
INSERT INTO `rolelist` VALUES (1049, 20, 7);
INSERT INTO `rolelist` VALUES (1050, 20, 8);
INSERT INTO `rolelist` VALUES (1051, 20, 10);
INSERT INTO `rolelist` VALUES (1052, 20, 11);
INSERT INTO `rolelist` VALUES (1053, 20, 12);
INSERT INTO `rolelist` VALUES (1054, 20, 14);
INSERT INTO `rolelist` VALUES (1055, 20, 15);
INSERT INTO `rolelist` VALUES (1056, 20, 16);
INSERT INTO `rolelist` VALUES (1057, 20, 18);
INSERT INTO `rolelist` VALUES (1058, 20, 29);
INSERT INTO `rolelist` VALUES (1059, 20, 33);
INSERT INTO `rolelist` VALUES (1060, 20, 34);
INSERT INTO `rolelist` VALUES (1061, 20, 42);
INSERT INTO `rolelist` VALUES (1062, 20, 43);
INSERT INTO `rolelist` VALUES (1063, 20, 53);
INSERT INTO `rolelist` VALUES (1064, 20, 56);
INSERT INTO `rolelist` VALUES (1065, 20, 57);
INSERT INTO `rolelist` VALUES (1066, 20, 62);
INSERT INTO `rolelist` VALUES (1067, 20, 66);
INSERT INTO `rolelist` VALUES (1068, 20, 67);
INSERT INTO `rolelist` VALUES (1069, 20, 68);
INSERT INTO `rolelist` VALUES (1070, 20, 69);
INSERT INTO `rolelist` VALUES (1071, 20, 86);
INSERT INTO `rolelist` VALUES (1072, 20, 91);
INSERT INTO `rolelist` VALUES (1073, 20, 92);
INSERT INTO `rolelist` VALUES (1074, 20, 97);
INSERT INTO `rolelist` VALUES (1075, 20, 98);
INSERT INTO `rolelist` VALUES (1076, 20, 99);

-- ----------------------------
-- Table structure for township
-- ----------------------------
DROP TABLE IF EXISTS `township`;
CREATE TABLE `township`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '编号',
  `township_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '乡镇名称',
  `township_area_num` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '国土面积',
  `township_altitude` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '平均海拔',
  `township_person_num` int(11) NULL DEFAULT 0 COMMENT '户籍人口',
  `township_p_person_num` int(11) NULL DEFAULT 0 COMMENT '重点人口',
  `township_flow_person_num` int(11) NULL DEFAULT 0 COMMENT '流动人口',
  `township_dwsj_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '党委书记',
  `township_dwsj_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '党委书记联系方式',
  `township_leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '乡镇长',
  `township_leader_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '乡镇长联系方式',
  `unit_id` int(11) NULL DEFAULT NULL COMMENT '关联派出所',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of township
-- ----------------------------
INSERT INTO `township` VALUES (42, '01', '通门乡', '576', '3987', 3511, 43, 344, '伟色', '13245432232', '王原', '15322346754', 84);

-- ----------------------------
-- Table structure for unit
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '派出所单位表',
  `number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '编号',
  `unit_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '单位',
  `police_num` int(11) NULL DEFAULT 0 COMMENT '警员人数',
  `police_leader` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '负责人',
  `police_leader_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '负责人联系方式',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `number`(`number`) USING BTREE COMMENT '编号唯一',
  UNIQUE INDEX `unit_name`(`unit_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 177 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unit
-- ----------------------------
INSERT INTO `unit` VALUES (80, '01', '县公安局政工科', 23, '陈天飞', '15182213701');
INSERT INTO `unit` VALUES (82, '02', '县公安局网安大队', 7, '史警官', '15288321921');
INSERT INTO `unit` VALUES (83, '03', '通门乡派出所', 3, '次平', '15311249321');
INSERT INTO `unit` VALUES (84, '04', '卡嘎镇派出所', 5, '卡嘎镇派出所所长', '15323135798');
INSERT INTO `unit` VALUES (87, '05', '二乡镇派出所', 30, '二乡镇派出所所长', '15182213700');
INSERT INTO `unit` VALUES (88, '06', '三乡镇派出所', 23, '三乡镇派出所所长', '15182213600');
INSERT INTO `unit` VALUES (89, '07', '四乡镇派出所所长', 64, '四乡镇派出所所长', '15182213500');
INSERT INTO `unit` VALUES (164, '555', '咖咖科技派出所', 50, '测试', '15869190407');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `user_password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `unit_id` int(11) NULL DEFAULT NULL COMMENT '用户所属单位id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', '2c39f9', 41);
INSERT INTO `user` VALUES (18, 'ctf', '2c39f9867429', 84);
INSERT INTO `user` VALUES (19, 'gaj01', '2c39f9867429', 84);

-- ----------------------------
-- Table structure for userlist
-- ----------------------------
DROP TABLE IF EXISTS `userlist`;
CREATE TABLE `userlist`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of userlist
-- ----------------------------
INSERT INTO `userlist` VALUES (1, 1, 1);
INSERT INTO `userlist` VALUES (88, 18, 9);
INSERT INTO `userlist` VALUES (89, 18, 20);
INSERT INTO `userlist` VALUES (98, 19, 20);
INSERT INTO `userlist` VALUES (99, 19, 9);

-- ----------------------------
-- Table structure for village
-- ----------------------------
DROP TABLE IF EXISTS `village`;
CREATE TABLE `village`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '行政村id',
  `township_id` int(11) NULL DEFAULT NULL COMMENT '所属乡镇',
  `village_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '村名',
  `village_area` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '国土面积',
  `person_num` int(11) NULL DEFAULT 0 COMMENT '户籍人口数量',
  `p_person_num` int(11) NULL DEFAULT 0 COMMENT '重点人口数量',
  `f_person_num` int(11) NULL DEFAULT 0 COMMENT '流动人口数量',
  `village_distance` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '距乡镇距离',
  `village_dzber` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '村党支部',
  `village_dzber_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '村党支部联系方式',
  `village_cwher` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '村委会主任',
  `village_cwher_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '村委会主任联系方式',
  `village_gzder` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '驻村工作队队长',
  `village_gzder_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '驻村工作队队长联系方式',
  `police_id` int(11) NULL DEFAULT NULL COMMENT '关联警员',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of village
-- ----------------------------
INSERT INTO `village` VALUES (1, 1, '村子66', '1', 1, 111, 1, '1', '1', '1', '1', '1', '1', '1', 3);
INSERT INTO `village` VALUES (2, 2, '村子2', '234', 345, 5, 2345, '25', '', '', '', '', '', '', 1);
INSERT INTO `village` VALUES (3, 2, '村子3', '234', 456, 6, 35, '10', '', '', '', '', '', '', 2);
INSERT INTO `village` VALUES (4, 2, '村子66', '1234', 456, 6, 12, '10', '', '', '', '', '', '', 3);
INSERT INTO `village` VALUES (6, 2, '村子66', '4', 456, 6, 324, '10', '', '', '', '', '', '', 3);
INSERT INTO `village` VALUES (7, 1, '村子66', '1234', 456, 6, 443, '10', 'xxxx', '15869190407', 'xxxxx', '15869190407', 'xxxxx', '15869190407', 3);
INSERT INTO `village` VALUES (8, 1, '村子66', '1234', 456, 6, 434, '10', 'xxxx', '15869190407', 'xxxxx', '15869190407', 'xxxxx', '15869190407', 3);
INSERT INTO `village` VALUES (9, 1, '村子66', '1234', 456, 6, 123, '10', 'xxxx', '15869190407', '111', '15869190407', 'xxxxx164', '15869190407', 3);
INSERT INTO `village` VALUES (10, 3, '3', '3', 3, 3, 3, '3', '', '3', '3', '3', '3', '3', 3);
INSERT INTO `village` VALUES (11, 4, '4', '4', 4, 4, 4, '4', '', '4', '1010002', '33', '22', '22', 101002);
INSERT INTO `village` VALUES (12, 4, '4', '4', 4, 4, 4, '4', '4', '4', '4', '4', '4', '4', 101002);
INSERT INTO `village` VALUES (13, 5, '5', '5', 5, 5, 5, '5', '5', '5', '5', '5', '5', '5', 101002);
INSERT INTO `village` VALUES (14, 14, '5665', '32', 2, 2, 2, '2', '2', '2', '2', '2', '2', '2', 101002);
INSERT INTO `village` VALUES (15, 15, '3', '3', 3, 3, 3, '3', '3', '3', '3', '3', '5', '3', 101002);
INSERT INTO `village` VALUES (16, 15, '5', '2', 2, 2, 2, '2', '2', '2', '2', '2', '2', '2', 101002);
INSERT INTO `village` VALUES (17, 17, '3', '3', 3, 3, 3, '3', '3', '3', '3', '3', '3', '3', 101002);
INSERT INTO `village` VALUES (18, 17, '2', '2', 2, 2, 2, '2', '2', '2', '2', '2', '9', '2', 101002);
INSERT INTO `village` VALUES (19, 18, '55', '5', 55, 5, 5, '5', '5', '55', '5', '5', '555', '55', 101002);
INSERT INTO `village` VALUES (20, 0, '3232', '232', 323, 32, 2, '2', '2', '2', '2', '2', '2', '2', 101002);
INSERT INTO `village` VALUES (21, 19, '343', '323', 32, 32, 32, '23', '32', '23', '32', '23', '32', '23', 101002);
INSERT INTO `village` VALUES (23, 21, '111', '565', 56, 56, 56, '5', '65', '65', '65', '56', '56', '5', 21);
INSERT INTO `village` VALUES (24, 23, '6', '6', 6, 6, 6, '6', '6', '6', '66', '6', '6', '66', 21);
INSERT INTO `village` VALUES (25, 23, '5', '5', 5, 5, 5, '5', '5', '5', '55', '5', '5', '5', 21);
INSERT INTO `village` VALUES (26, 25, '1', '1', 1, 1, 1, '1', '1', '1', '1', '1', '1', '1', 1);
INSERT INTO `village` VALUES (27, 26, '1', '1', 1, 1, 1, '1', '1', '1', '1', '1', '1', '1', 7);
INSERT INTO `village` VALUES (28, 27, '1', '1', 1, 1, 1, '1', '1', '1', '1', '1', '1', '1', 1);
INSERT INTO `village` VALUES (29, 29, '1', '1', 1, 1, 1, '1', '1', '1', '1', '1', '1', '1', 21);
INSERT INTO `village` VALUES (30, 30, '1', '1', 1, 1, 1, '1', '1', '1', '1', '1', '1', '1', 21);
INSERT INTO `village` VALUES (33, 32, '6', '6', 6, 6, 6, '6', '6', '6', '6', '6', '66', '6', 21);
INSERT INTO `village` VALUES (35, 33, '77', '7', 7, 7, 7, '7', '77', '7', '71', '77', '77', '7', 21);
INSERT INTO `village` VALUES (39, 0, '123', '12313', 1231, 123, 123123, '12313', '1313', '1231', '1231', '123123', '13123', '11323', 27);
INSERT INTO `village` VALUES (40, 0, '123123123', '123123', 12311, 123, 123, '123', '123', '123', '12312', '1231231', '312', '3123', 27);
INSERT INTO `village` VALUES (41, 1, '村子66', '42', 456, 6, 123, '10', 'xxxx', '15869190407', 'xxxxx', '15869190407', 'xxxxx', '15869190407', NULL);
INSERT INTO `village` VALUES (42, 42, '仲村', '102', 114, 2, 21, '34', '仲村支部书记', '15212345678', '仲村主任', '15212345678', '仲村队长', '15212345678', 191);

-- ----------------------------
-- Function structure for searchParent
-- ----------------------------
DROP FUNCTION IF EXISTS `searchParent`;
delimiter ;;
CREATE FUNCTION `searchParent`(areaId INT)
 RETURNS varchar(4000) CHARSET utf8
BEGIN
DECLARE sTemp VARCHAR(4000);
DECLARE sTempChd VARCHAR(4000);
 
SET sTemp='$';
SET sTempChd = CAST(areaId AS CHAR);
SET sTemp = CONCAT(sTemp,',',sTempChd);
 
SELECT parentId INTO sTempChd FROM t_areainfo WHERE id = sTempChd;
WHILE sTempChd <> 0 DO
SET sTemp = CONCAT(sTemp,',',sTempChd);
SELECT parentId INTO sTempChd FROM t_areainfo WHERE id = sTempChd;
END WHILE;
RETURN sTemp;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
