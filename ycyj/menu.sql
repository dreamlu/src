/*
 Navicat MySQL Data Transfer

 Source Server         : lu
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : localhost:3306
 Source Schema         : ycyj

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 23/10/2018 16:21:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单名称',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '父级菜单',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `menu_name`(`menu_name`, `parent_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 97 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (7, '乡镇信息管理', 1);
INSERT INTO `menu` VALUES (9, '人口信息管理', 1);
INSERT INTO `menu` VALUES (47, '人口数据分析', 9);
INSERT INTO `menu` VALUES (4, '信息发布', NULL);
INSERT INTO `menu` VALUES (14, '信息报送', 2);
INSERT INTO `menu` VALUES (15, '信息查阅', 2);
INSERT INTO `menu` VALUES (27, '信息编辑', 14);
INSERT INTO `menu` VALUES (36, '修改', 6);
INSERT INTO `menu` VALUES (44, '修改', 7);
INSERT INTO `menu` VALUES (31, '修改', 8);
INSERT INTO `menu` VALUES (63, '修改', 10);
INSERT INTO `menu` VALUES (50, '修改', 11);
INSERT INTO `menu` VALUES (58, '修改', 12);
INSERT INTO `menu` VALUES (87, '修改', 18);
INSERT INTO `menu` VALUES (75, '修改', 25);
INSERT INTO `menu` VALUES (71, '修改', 26);
INSERT INTO `menu` VALUES (79, '修改发布信息', 22);
INSERT INTO `menu` VALUES (82, '修改发布信息', 23);
INSERT INTO `menu` VALUES (41, '修改行政村信息', 7);
INSERT INTO `menu` VALUES (80, '停止发布', 22);
INSERT INTO `menu` VALUES (83, '停止发布', 23);
INSERT INTO `menu` VALUES (17, '公告发布', 3);
INSERT INTO `menu` VALUES (37, '删除', 6);
INSERT INTO `menu` VALUES (45, '删除', 7);
INSERT INTO `menu` VALUES (32, '删除', 8);
INSERT INTO `menu` VALUES (64, '删除', 10);
INSERT INTO `menu` VALUES (51, '删除', 11);
INSERT INTO `menu` VALUES (59, '删除', 12);
INSERT INTO `menu` VALUES (92, '删除', 15);
INSERT INTO `menu` VALUES (88, '删除', 18);
INSERT INTO `menu` VALUES (81, '删除', 22);
INSERT INTO `menu` VALUES (84, '删除', 23);
INSERT INTO `menu` VALUES (77, '删除', 25);
INSERT INTO `menu` VALUES (73, '删除', 26);
INSERT INTO `menu` VALUES (90, '删除', 28);
INSERT INTO `menu` VALUES (94, '删除', 65);
INSERT INTO `menu` VALUES (6, '单位信息管理', 1);
INSERT INTO `menu` VALUES (68, '历史统计数据', 66);
INSERT INTO `menu` VALUES (21, '发布板块信息', 4);
INSERT INTO `menu` VALUES (18, '发布记录', 3);
INSERT INTO `menu` VALUES (20, '发布轮播信息', 4);
INSERT INTO `menu` VALUES (33, '图表', 9);
INSERT INTO `menu` VALUES (1, '基本信息', NULL);
INSERT INTO `menu` VALUES (67, '实时统计数据', 66);
INSERT INTO `menu` VALUES (70, '所属乡镇', 47);
INSERT INTO `menu` VALUES (29, '所属单位', 8);
INSERT INTO `menu` VALUES (61, '批量导入流动人口', 10);
INSERT INTO `menu` VALUES (55, '批量导入重点人口', 12);
INSERT INTO `menu` VALUES (39, '批量新增', 6);
INSERT INTO `menu` VALUES (46, '批量新增', 8);
INSERT INTO `menu` VALUES (49, '批量新增', 11);
INSERT INTO `menu` VALUES (16, '报送记录', 2);
INSERT INTO `menu` VALUES (69, '排名情况', 66);
INSERT INTO `menu` VALUES (66, '数据统计', 16);
INSERT INTO `menu` VALUES (48, '新增', 11);
INSERT INTO `menu` VALUES (40, '新增乡镇', 7);
INSERT INTO `menu` VALUES (38, '新增单位', 6);
INSERT INTO `menu` VALUES (60, '新增流动人口', 10);
INSERT INTO `menu` VALUES (78, '新增用户', 25);
INSERT INTO `menu` VALUES (74, '新增角色', 26);
INSERT INTO `menu` VALUES (30, '新增警员', 8);
INSERT INTO `menu` VALUES (54, '新增重点人口', 12);
INSERT INTO `menu` VALUES (85, '是否置顶', 23);
INSERT INTO `menu` VALUES (11, '本地户籍管理', 9);
INSERT INTO `menu` VALUES (91, '查看', 15);
INSERT INTO `menu` VALUES (86, '查看', 18);
INSERT INTO `menu` VALUES (95, '查看', 22);
INSERT INTO `menu` VALUES (96, '查看', 23);
INSERT INTO `menu` VALUES (76, '查看', 25);
INSERT INTO `menu` VALUES (72, '查看', 26);
INSERT INTO `menu` VALUES (93, '查看', 65);
INSERT INTO `menu` VALUES (42, '查看乡镇详情', 7);
INSERT INTO `menu` VALUES (43, '查看人口详情', 7);
INSERT INTO `menu` VALUES (52, '查看家庭成员', 11);
INSERT INTO `menu` VALUES (34, '查看警员信息', 6);
INSERT INTO `menu` VALUES (62, '查看详情', 10);
INSERT INTO `menu` VALUES (53, '查看详情', 11);
INSERT INTO `menu` VALUES (57, '查看详情', 12);
INSERT INTO `menu` VALUES (89, '查看详情', 28);
INSERT INTO `menu` VALUES (10, '流动人员管理', 9);
INSERT INTO `menu` VALUES (23, '版块信息发布记录', 4);
INSERT INTO `menu` VALUES (26, '用户账号管理', 5);
INSERT INTO `menu` VALUES (56, '管控记录', 12);
INSERT INTO `menu` VALUES (5, '系统维护', NULL);
INSERT INTO `menu` VALUES (28, '草稿管理', 14);
INSERT INTO `menu` VALUES (25, '角色信息维护', 5);
INSERT INTO `menu` VALUES (2, '警务信息', NULL);
INSERT INTO `menu` VALUES (8, '警员信息管理', 1);
INSERT INTO `menu` VALUES (65, '记录查看', 16);
INSERT INTO `menu` VALUES (22, '轮播信息发布记录', 4);
INSERT INTO `menu` VALUES (3, '通知公告', NULL);
INSERT INTO `menu` VALUES (12, '重点人员管理', 9);

SET FOREIGN_KEY_CHECKS = 1;
