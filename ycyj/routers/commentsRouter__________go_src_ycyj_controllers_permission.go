package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["ycyj/controllers/permission:MenuController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:MenuController"],
        beego.ControllerComments{
            Method: "CreateMenu",
            Router: `/menu/createmenu`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:MenuController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:MenuController"],
        beego.ControllerComments{
            Method: "DeleteMenuById",
            Router: `/menu/deletemenubyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:MenuController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:MenuController"],
        beego.ControllerComments{
            Method: "GetAllMenu",
            Router: `/menu/getallmenu`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:MenuController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:MenuController"],
        beego.ControllerComments{
            Method: "GetMenuById",
            Router: `/menu/getmenubyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:MenuController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:MenuController"],
        beego.ControllerComments{
            Method: "GetMenuByPage",
            Router: `/menu/getmenubypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:MenuController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:MenuController"],
        beego.ControllerComments{
            Method: "UpdateMenu",
            Router: `/menu/updatemenu`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:RoleController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:RoleController"],
        beego.ControllerComments{
            Method: "CreateRole",
            Router: `/role/createrole`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:RoleController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:RoleController"],
        beego.ControllerComments{
            Method: "DeleteRoleById",
            Router: `/role/deleterolebyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:RoleController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:RoleController"],
        beego.ControllerComments{
            Method: "GetMenuByRoleId",
            Router: `/role/getmenubyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:RoleController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:RoleController"],
        beego.ControllerComments{
            Method: "GetRoles",
            Router: `/role/getroles`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:RoleController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:RoleController"],
        beego.ControllerComments{
            Method: "UpdateRole",
            Router: `/role/updaterole`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:UserController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:UserController"],
        beego.ControllerComments{
            Method: "Login",
            Router: `/login/login`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:UserController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:UserController"],
        beego.ControllerComments{
            Method: "CreateUser",
            Router: `/user/createuser`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:UserController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:UserController"],
        beego.ControllerComments{
            Method: "DeleteUserById",
            Router: `/user/deleteuserbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:UserController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:UserController"],
        beego.ControllerComments{
            Method: "GetUserlist",
            Router: `/user/getuserlist`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:UserController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:UserController"],
        beego.ControllerComments{
            Method: "GetUsers",
            Router: `/user/getusers`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/permission:UserController"] = append(beego.GlobalControllerRouter["ycyj/controllers/permission:UserController"],
        beego.ControllerComments{
            Method: "UpdateUser",
            Router: `/user/updateuser`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
