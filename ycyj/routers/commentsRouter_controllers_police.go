package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["ycyj/controllers/police:PoliceCountController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:PoliceCountController"],
		beego.ControllerComments{
			Method: "GetNumByType",
			Router: `/policeaffairs/getnumbytype`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/police:PoliceHistoryController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:PoliceHistoryController"],
		beego.ControllerComments{
			Method: "GetAllCount",
			Router: `/policeaffairs/getallcount`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/police:PoliceHistoryController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:PoliceHistoryController"],
		beego.ControllerComments{
			Method: "GetAllCountYear",
			Router: `/policeaffairs/getallcountyear`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/police:PoliceHistoryController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:PoliceHistoryController"],
		beego.ControllerComments{
			Method: "GetDetailCount",
			Router: `/policeaffairs/getdetailcount`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/police:PoliceRankController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:PoliceRankController"],
		beego.ControllerComments{
			Method: "GetAllCountRank",
			Router: `/policeaffairs/getallcountrank`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/police:PoliceRankController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:PoliceRankController"],
		beego.ControllerComments{
			Method: "GetAllCountTimeRank",
			Router: `/policeaffairs/getallcounttimerank`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/police:PoliceaffairsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:PoliceaffairsController"],
		beego.ControllerComments{
			Method: "CreatePoliceaffairs",
			Router: `/policeaffairs/createpoliceaffairs`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/police:PoliceaffairsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:PoliceaffairsController"],
		beego.ControllerComments{
			Method: "DeletePoliceaffairsById",
			Router: `/policeaffairs/deletepoliceaffairsbyid/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/police:PoliceaffairsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:PoliceaffairsController"],
		beego.ControllerComments{
			Method: "GetPoliceaffairsById",
			Router: `/policeaffairs/getpoliceaffairsbyid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/police:PoliceaffairsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:PoliceaffairsController"],
		beego.ControllerComments{
			Method: "GetPoliceaffairsByPage",
			Router: `/policeaffairs/getpoliceaffairsbypage`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/police:PoliceaffairsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:PoliceaffairsController"],
		beego.ControllerComments{
			Method: "GetPoliceaffairsBySearch",
			Router: `/policeaffairs/getpoliceaffairsbysearch`,
			AllowHTTPMethods: []string{"patch"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/police:PoliceaffairsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:PoliceaffairsController"],
		beego.ControllerComments{
			Method: "UpdatePoliceaffairs",
			Router: `/policeaffairs/updatepoliceaffairs`,
			AllowHTTPMethods: []string{"patch"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/police:ReplyController"] = append(beego.GlobalControllerRouter["ycyj/controllers/police:ReplyController"],
		beego.ControllerComments{
			Method: "CreateReply",
			Router: `/reply/createreply`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

}
