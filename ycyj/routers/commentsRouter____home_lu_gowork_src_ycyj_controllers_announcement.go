package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"] = append(beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"],
		beego.ControllerComments{
			Method: "CreateAnnouncement",
			Router: `/announcement/createannouncement`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"] = append(beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"],
		beego.ControllerComments{
			Method: "DeleteAnnouncementById",
			Router: `/announcement/deleteannouncementbyid/:id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"] = append(beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"],
		beego.ControllerComments{
			Method: "GetAnnouncementById",
			Router: `/announcement/getannouncementbyid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"] = append(beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"],
		beego.ControllerComments{
			Method: "GetAnnouncementByPage",
			Router: `/announcement/getannouncementbypage`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"] = append(beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"],
		beego.ControllerComments{
			Method: "GetAnnouncementBySearch",
			Router: `/announcement/getannouncementbysearch`,
			AllowHTTPMethods: []string{"patch"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"] = append(beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"],
		beego.ControllerComments{
			Method: "UpdateAnnouncement",
			Router: `/announcement/updateannouncement`,
			AllowHTTPMethods: []string{"patch"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"] = append(beego.GlobalControllerRouter["ycyj/controllers/announcement:AnnouncementController"],
		beego.ControllerComments{
			Method: "UpdateReply",
			Router: `/announcement/updatereply`,
			AllowHTTPMethods: []string{"patch"},
			MethodParams: param.Make(),
			Params: nil})

}
