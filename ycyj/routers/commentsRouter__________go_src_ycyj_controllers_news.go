package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"],
        beego.ControllerComments{
            Method: "CreateNews",
            Router: `/news/createnews`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"],
        beego.ControllerComments{
            Method: "DeleteNewsById",
            Router: `/news/deletenewsbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"],
        beego.ControllerComments{
            Method: "GetNewsById",
            Router: `/news/getnewsbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"],
        beego.ControllerComments{
            Method: "GetNewsByPage",
            Router: `/news/getnewsbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"],
        beego.ControllerComments{
            Method: "GetNewsBySearch",
            Router: `/news/getnewsbysearch`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"],
        beego.ControllerComments{
            Method: "GetNewsBySort",
            Router: `/news/getnewsbysort`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"],
        beego.ControllerComments{
            Method: "GetNewsByTop",
            Router: `/news/getnewsbytop`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/news:NewsController"],
        beego.ControllerComments{
            Method: "UpdateNews",
            Router: `/news/updatenews`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
