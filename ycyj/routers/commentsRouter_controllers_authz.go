package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["ycyj/controllers/authz:Policy"] = append(beego.GlobalControllerRouter["ycyj/controllers/authz:Policy"],
		beego.ControllerComments{
			Method: "GetAllSubjects",
			Router: `/policy/getallsubjects`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/authz:Policy"] = append(beego.GlobalControllerRouter["ycyj/controllers/authz:Policy"],
		beego.ControllerComments{
			Method: "GetRoles",
			Router: `/policy/getallsubjects`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

}
