package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"github.com/astaxie/beego/plugins/cors"
	"github.com/casbin/casbin"
	"ycyj/controllers/announcement"
	"ycyj/controllers/authz"
	"ycyj/controllers/basic"
	"ycyj/controllers/basic/Graphics"
	"ycyj/controllers/news"
	"ycyj/controllers/permission"
	"ycyj/controllers/police"
	"ycyj/controllers/util"
)

//登录验证/api路由权限验证
var FilterUser = func(ctx *context.Context) {

	if ctx.Input.Method() == "OPTIONS" {
		ctx.Output.SetStatus(200)
		ctx.Output.Header("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PATCH,DELETE")
		return
	}
}

func init() {
	beego.InsertFilter("*", beego.BeforeRouter, authz.NewAuthorizer(casbin.NewEnforcer("conf/authz_model.conf", "conf/authz_policy.csv")))
	beego.InsertFilter("/*", beego.BeforeRouter, FilterUser)
	//允许跨域,测试用
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowOrigins:     []string{"http://192.168.31.173:3000"},
		AllowMethods:     []string{"get", "post", "options", "patch", "delete", "*"},
		AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "X-Requested-With", "Access-Control-Allow-Credentials"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))
	beego.Include(&basic.UnitController{})
	beego.Include(&basic.PoliceController{})
	beego.Include(&basic.VillageController{})
	beego.Include(&basic.TownshipController{})
	beego.Include(&police.PoliceaffairsController{})
	beego.Include(&police.ReplyController{})
	beego.Include(&announcement.AnnouncementController{})
	beego.Include(&news.NewsController{})
	beego.Include(&basic.FlowpersonController{})
	beego.Include(&basic.NativepersonController{})
	beego.Include(&basic.EmphasispersonController{})
	beego.Include(&permission.MenuController{})
	beego.Include(&permission.RoleController{})
	beego.Include(&basic.EmphasiscontrolController{})
	beego.Include(&police.PoliceCountController{})
	beego.Include(&police.PoliceHistoryController{})
	beego.Include(&police.PoliceRankController{})
	beego.Include(&Graphics.GraphicsController{})
	beego.Include(&util.FileController{})
	beego.Include(&permission.UserController{})
	beego.Include(&authz.Policy{})
}
