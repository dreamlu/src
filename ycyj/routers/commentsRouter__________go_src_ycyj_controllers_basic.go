package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["ycyj/controllers/basic:EmphasiscontrolController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:EmphasiscontrolController"],
        beego.ControllerComments{
            Method: "CreateEmphasiscontrol",
            Router: `/emphasiscontrol/createemphasiscontrol`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:EmphasiscontrolController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:EmphasiscontrolController"],
        beego.ControllerComments{
            Method: "DeleteEmphasiscontrolById",
            Router: `/emphasiscontrol/deleteemphasiscontrolbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:EmphasiscontrolController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:EmphasiscontrolController"],
        beego.ControllerComments{
            Method: "GetEmphasiscontrolByNativepersonId",
            Router: `/emphasiscontrol/getemphasiscontrolbynativepersonid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:EmphasispersonController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:EmphasispersonController"],
        beego.ControllerComments{
            Method: "CreateEmphasisperson",
            Router: `/emphasisperson/createemphasisperson`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:EmphasispersonController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:EmphasispersonController"],
        beego.ControllerComments{
            Method: "DeleteEmphasispersonById",
            Router: `/emphasisperson/deleteemphasispersonbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:EmphasispersonController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:EmphasispersonController"],
        beego.ControllerComments{
            Method: "UpdateEmphasisperson",
            Router: `/emphasisperson/updateemphasisperson`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:FlowpersonController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:FlowpersonController"],
        beego.ControllerComments{
            Method: "CreateFlowperson",
            Router: `/flowperson/createflowperson`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:FlowpersonController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:FlowpersonController"],
        beego.ControllerComments{
            Method: "DeleteFlowpersonById",
            Router: `/flowperson/deleteflowpersonbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:FlowpersonController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:FlowpersonController"],
        beego.ControllerComments{
            Method: "UpdateFlowperson",
            Router: `/flowperson/updateflowperson`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:NativepersonController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:NativepersonController"],
        beego.ControllerComments{
            Method: "CreateNativeperson",
            Router: `/nativeperson/createnativeperson`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:NativepersonController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:NativepersonController"],
        beego.ControllerComments{
            Method: "DeleteNativepersonById",
            Router: `/nativeperson/deletenativepersonbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:NativepersonController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:NativepersonController"],
        beego.ControllerComments{
            Method: "UpdateNativeperson",
            Router: `/nativeperson/updatenativeperson`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetEmphasispersonById",
            Router: `/emphasisperson/getemphasispersonbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetEmphasispersonByPage",
            Router: `/emphasisperson/getemphasispersonbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetEmphasispersonBySearch",
            Router: `/emphasisperson/getemphasispersonbysearch`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetFlowpersonById",
            Router: `/flowperson/getflowpersonbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetFlowpersonByPage",
            Router: `/flowperson/getflowpersonbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetFlowpersonBySearch",
            Router: `/flowperson/getflowpersonbysearch`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetCareer",
            Router: `/nativeperson/getcareer`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetEducation",
            Router: `/nativeperson/geteducation`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetNativepersonByHousehold",
            Router: `/nativeperson/getnativepersonbyhousehold`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetNativepersonByIdcard",
            Router: `/nativeperson/getnativepersonbyidcard`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetNativepersonByIdP",
            Router: `/nativeperson/getnativepersonbyidp`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetNativepersonByPage",
            Router: `/nativeperson/getnativepersonbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetNativepersonBySearch",
            Router: `/nativeperson/getnativepersonbysearch`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetPolitical",
            Router: `/nativeperson/getpolitical`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "CreatePolice",
            Router: `/police/createpolice`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "DeletePoliceById",
            Router: `/police/deletepolicebyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetIdByNumber",
            Router: `/police/getidbynumber`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetPoliceById",
            Router: `/police/getpolicebyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetPoliceByPage",
            Router: `/police/getpolicebypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "SearchPolice",
            Router: `/police/searchpolice`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "UpdatePolice",
            Router: `/police/updatepolice`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetTownshipById",
            Router: `/township/gettownshipbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetTownshipByPage",
            Router: `/township/gettownshipbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetTownshipList",
            Router: `/township/gettownshiplist`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:PoliceController"],
        beego.ControllerComments{
            Method: "GetTownshipListN",
            Router: `/township/gettownshiplistn`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:TownshipController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:TownshipController"],
        beego.ControllerComments{
            Method: "CreateTownship",
            Router: `/township/createtownship`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:TownshipController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:TownshipController"],
        beego.ControllerComments{
            Method: "DeleteTownshipById",
            Router: `/township/deletetownshipbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:TownshipController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:TownshipController"],
        beego.ControllerComments{
            Method: "UpdateTownship",
            Router: `/township/updatetownship`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"],
        beego.ControllerComments{
            Method: "CreateUnit",
            Router: `/unit/createunit`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"],
        beego.ControllerComments{
            Method: "DeleteUnitById",
            Router: `/unit/deleteunitbyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"],
        beego.ControllerComments{
            Method: "GetUnitById",
            Router: `/unit/getunitbyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"],
        beego.ControllerComments{
            Method: "GetUnitByPage",
            Router: `/unit/getunitbypage`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"],
        beego.ControllerComments{
            Method: "GetUnitList",
            Router: `/unit/getunitlist`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"],
        beego.ControllerComments{
            Method: "GetUnitPoliceById",
            Router: `/unit/getunitpolicebyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:UnitController"],
        beego.ControllerComments{
            Method: "UpdateUnit",
            Router: `/unit/updateunit`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:VillageController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:VillageController"],
        beego.ControllerComments{
            Method: "CreateVillage",
            Router: `/village/createvillage`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:VillageController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:VillageController"],
        beego.ControllerComments{
            Method: "DeleteVillageById",
            Router: `/village/deletevillagebyid/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:VillageController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:VillageController"],
        beego.ControllerComments{
            Method: "GetNativePersonByTownshipId",
            Router: `/village/getnativepersonbytownshipid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:VillageController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:VillageController"],
        beego.ControllerComments{
            Method: "GetVillageById",
            Router: `/village/getvillagebyid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:VillageController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:VillageController"],
        beego.ControllerComments{
            Method: "GetVillageByTownshipId",
            Router: `/village/getvillagebytownshipid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["ycyj/controllers/basic:VillageController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic:VillageController"],
        beego.ControllerComments{
            Method: "UpdateVillage",
            Router: `/village/updatevillage`,
            AllowHTTPMethods: []string{"patch"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
