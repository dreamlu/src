package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["ycyj/controllers/util:FileController"] = append(beego.GlobalControllerRouter["ycyj/controllers/util:FileController"],
		beego.ControllerComments{
			Method: "CreateSomething",
			Router: `/file/createsomething`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/util:FileController"] = append(beego.GlobalControllerRouter["ycyj/controllers/util:FileController"],
		beego.ControllerComments{
			Method: "GetFilePath",
			Router: `/file/getfilepath`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

}
