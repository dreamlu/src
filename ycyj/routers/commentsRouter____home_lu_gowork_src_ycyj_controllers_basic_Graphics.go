package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["ycyj/controllers/basic/Graphics:GraphicsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic/Graphics:GraphicsController"],
		beego.ControllerComments{
			Method: "GetCompareFNGraphics",
			Router: `/graphics/getcomparefngraphics`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/basic/Graphics:GraphicsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic/Graphics:GraphicsController"],
		beego.ControllerComments{
			Method: "GetFlowChange",
			Router: `/graphics/getflowchange`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/basic/Graphics:GraphicsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic/Graphics:GraphicsController"],
		beego.ControllerComments{
			Method: "GetFlowLeave",
			Router: `/graphics/getflowleave`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/basic/Graphics:GraphicsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic/Graphics:GraphicsController"],
		beego.ControllerComments{
			Method: "GetNativeAgeGraphics",
			Router: `/graphics/getnativeagegraphics`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/basic/Graphics:GraphicsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic/Graphics:GraphicsController"],
		beego.ControllerComments{
			Method: "GetNativeLeave",
			Router: `/graphics/getnativeleave`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["ycyj/controllers/basic/Graphics:GraphicsController"] = append(beego.GlobalControllerRouter["ycyj/controllers/basic/Graphics:GraphicsController"],
		beego.ControllerComments{
			Method: "GetNativeSexGraphics",
			Router: `/graphics/getnativesexgraphics`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

}
