package announcement

import (
	"github.com/astaxie/beego/orm"
	"strconv"
	"time"
	"ycyj/models/lib"
)

type HasRead struct {
	Readnum    int //已读数量
	Noreadnum  int //未读数量
	Readstatus []*Readstatus
}

/*阅读状态内容*/
type Readstatus struct {
	Id        int
	Unit_id   int    //单位id
	Unit_name string //单位名称
	Isread    int    //0/1,0未阅读
	Read_date string //阅读时间
	//Announcement_id	int		//阅读的公告id
}

//根据公告id获得对应的阅读状态
func GetHasReadById(id int) HasRead {
	var has HasRead
	var readstatus []*Readstatus
	o := orm.NewOrm()
	o.Raw("select *from `readstatus` where announcement_id=?", id).QueryRows(&readstatus)
	for i := 0; i < len(readstatus); i++ {
		has.Readstatus = append(has.Readstatus, readstatus[i])
		//unit-name
		var maps []orm.Params
		num, _ := o.Raw("select unit_name from `unit` where id=?",readstatus[i].Unit_id).Values(&maps)
		if num > 0{
			readstatus[i].Unit_name = maps[0]["unit_name"].(string)
		}
	}

	var maps []orm.Params
	num, err := o.Raw("SELECT count(id) as num FROM `readstatus` where isread=1 and announcement_id=?", id).Values(&maps)
	if err == nil && num > 0 {
		has.Readnum, _ = strconv.Atoi(maps[0]["num"].(string))
	}
	num2, err := o.Raw("SELECT count(id) as num FROM `readstatus` where isread=0 and announcement_id=?", id).Values(&maps)
	if err == nil && num2 > 0 {
		has.Noreadnum, _ = strconv.Atoi(maps[0]["num"].(string))
	}

	return has
}

//修改公告对应部门的阅读状态
func UpdateReply(user_id, announcement_id string) interface{} {

	o := orm.NewOrm()
	var maps2 []orm.Params
	num2,_ := o.Raw("select unit_id from `user` where id=?", user_id).Values(&maps2)
	if num2 == 0{
		return lib.GetMapDataError(222,"用户单位不存在")
	}

	var info interface{}
	var num int64 //返回影响的行数
	var date = time.Now().Format("2006-01-02 15:04:05")

	sql := "update `readstatus` set isread=1,read_date=? where unit_id=? and announcement_id=?"

	res, err := o.Raw(sql,date,maps2[0]["unit_id"],announcement_id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 && err != nil {
		info = lib.GetMapDataError(222,err.Error())
	} else if err == nil{
		info = lib.GetMapDataError(222,"该单位不在该公告对应阅读列表中")
	} else {
		info = lib.MapUpdate
	}
	return info
}
