package announcement

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"ycyj/models/lib"
)

/*公告内容*/
type Announcement struct {
	Id           int
	Title        string //title
	Content      string //内容
	File         string //附件,&分割
	Recieve_unit string //接收单位们的id
	Publish_name string //发布人
	Publish_date string //发布日期
	HasRead      []HasRead
}

//根据搜索结果获得公告,分页
func GetAnnouncementBySearch(user_id string, args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	//页码,每页数量
	clientPageStr := beego.AppConfig.String("clientPage") //默认第1页
	everyPageStr := beego.AppConfig.String("everyPage")   //默认10页
	time := "publish_date"

	sql := "select distinct a.* FROM `announcement` a inner join readstatus b on a.id=b.announcement_id where 1=1 and "
	for k, v := range args {
		if k == "time1" { //时间范围
			sql += "timestampdiff(day,'" + v[0] + "'," + time + ") >= 0 and "
			continue
		}
		if k == "time2" {
			sql += "timestampdiff(day,'" + v[0] + "'," + time + ") <= 0 and "
			continue
		}
		if k == "clientPage" { //页码
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" { //每页数量
			everyPageStr = v[0]
			continue
		}
		if v[0] == "" { //条件为空,舍弃
			continue
		}
		sql += k + " like '%" + v[0] + "%' and "
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	c := []byte(sql)
	sql = string(c[:len(c)-4]) //去and
	sqlnolimit := sql

	sql += " limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr

	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)
	//角色id判断
	var admin_id string //单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" { //非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}
	}

	if admin_id != "1" {
		sqlnolimit = strings.Replace(sqlnolimit,"where 1=1","where 1=1 and unit_id="+maps2[0]["unit_id"].(string),-1)
		sql = strings.Replace(sql,"where 1=1","where 1=1 and unit_id="+maps2[0]["unit_id"].(string),-1)
	}

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var announcement []*Announcement
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&announcement)
		//统计页码等状态
		for i := 0; i < len(announcement); i++ {
			ac := announcement[i]
			ac.HasRead = append(ac.HasRead, GetHasReadById(ac.Id))
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = announcement //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoData
	} else {
		info = lib.MapError
	}
	return info
}

//根据id获得公告
func GetAnnouncementById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var announcement []*Announcement
	var getinfo lib.GetInfo
	num2, err := o.Raw("select *from `announcement` where id=?", id).QueryRows(&announcement)

	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		for i := 0; i < len(announcement); i++ {
			ac := announcement[i]
			ac.HasRead = append(ac.HasRead, GetHasReadById(ac.Id))
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = announcement //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得公告,分页
func GetAnnouncementByPage(user_id string, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)
	//角色id判断
	var admin_id string //单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" { //非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}
	}

	sqlnolimit := "SELECT distinct a.id as num FROM `announcement` a inner join readstatus b on a.id=b.announcement_id "
	sql := "SELECT distinct a.* FROM `announcement` a inner join readstatus b on a.id=b.announcement_id order by a.id desc limit ?,?"

	if admin_id != "1" {
		sqlnolimit = strings.Replace(sqlnolimit,"a.id=b.announcement_id","a.id=b.announcement_id where unit_id="+maps2[0]["unit_id"].(string),-1)
		sql = strings.Replace(sql,"a.id=b.announcement_id","a.id=b.announcement_id where unit_id="+maps2[0]["unit_id"].(string),-1)
	}

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var announcement []*Announcement
		var getinfo lib.GetInfo
		o.Raw(sql, (clientPage-1)*everyPage, everyPage).QueryRows(&announcement)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)
		for i := 0; i < len(announcement); i++ {
			ac := announcement[i]
			ac.HasRead = append(ac.HasRead, GetHasReadById(ac.Id))
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = announcement //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else if err == nil && num2 == 0 {
		info = lib.MapNoData
	} else {
		info = lib.MapError
	}
	return info
}

//删除公告,根据id
func DeleteAnnouncementByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `announcement` where id=?", id).Exec()
	//res,err := o.Raw("delete from `news` where id=?",id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		//删除阅读状态
		o.Raw("delete from `readstatus` where id=?", id).Exec()
		info = lib.MapDelete
	}
	return info
}

//修改公告
func UpdateAnnouncement(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("announcement", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.GetMapDataError(222,err.Error())
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建公告
func CreateAnnouncement(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数
	//var announcement_id  string

	sql := lib.GetInsertSqlById("announcement", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	//获取刚刚插入的id
	var maps []orm.Params
	o.Raw("SELECT max(id) as id from `announcement`").Values(&maps)
	announcement_id := maps[0]["id"].(string)

	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.GetMapDataError(222,err.Error())
	} else {
		info = lib.MapCreate
		//创建公告成功时,同时创建对应的阅读状态表
		var units []string
		for k, v := range args {
			if k == "recieve_unit" {
				units = strings.Split(v[0], "&")
				break
			}
		}
		for i := 0; i < len(units); i++ {
			o.Raw("insert `readstatus`(unit_id,announcement_id) value(?,?)", units[i], announcement_id).Exec()
		}
	}

	return info
}
