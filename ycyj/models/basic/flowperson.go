package basic

import (
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"time"
	"ycyj/models/lib"
	"ycyj/util"
)

/*流动人口管理*/
type Flowperson struct {
	Id               int
	Name             string //姓名
	Sex              string //性别
	Born_date        string //出生日期
	Age              int    //年龄
	Idcard           string //身份证号码
	Phone            string //电话号码
	Village_id       string //乡村id
	Village_name     string //乡村名
	Township_id      string //乡镇id
	Township_name    string //乡镇名称
	Birthplace       string //籍贯,"-"分割
	Flow_type        string //流动原因类型
	Flow_reason      string //流动原因
	Flow_tothis_time string //到此地日期
	Flow_time        string //停留时间(自动计算)
	Remark           string //到此地类型
}

//根据条件查询搜索结果
func GetFlowpersonBySearch(user_id string,args map[string][]string) interface{} {

	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := lib.SearchTableSqlInclueDate("flow_tothis_time", "flowperson", args)

	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)

	//角色id判断
	var admin_id string//单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" {//非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}

		if admin_id != "1" {//非根管理员
			var role_id string
			//判断是否拥有查看全部的角色权限,角色15固定
			for _,v := range maps2{
				if v["role_id"] == "16"{
					role_id = "16"
					break
				}
			}
			if role_id != "16"{//查看各自账号所在流动人口信息
				sql = strings.Replace(sql,"`flowperson`","`flowperson` a inner join `village` bb " +
					"on a.village_id=bb.id " +
					"inner join `township` cc on bb.township_id=cc.id " +
					"and cc.unit_id="+maps2[0]["unit_id"].(string),-1)
				sqlnolimit = strings.Replace(sqlnolimit,"`flowperson`","`flowperson` a inner join `village` bb " +
					"on a.village_id=bb.id " +
					"inner join `township` cc on bb.township_id=cc.id " +
					"and cc.unit_id="+maps2[0]["unit_id"].(string),-1)
			}
		}
	}


	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var flowperson []*Flowperson
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&flowperson)
		//统计页码等状态
		for i := 0; i < len(flowperson); i++ {
			fp := flowperson[i]
			num, _ := o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", flowperson[i].Village_id).Values(&maps)
			if num > 0 {
				fp.Village_name = maps[0]["village_name"].(string)
				fp.Township_id = maps[0]["township_id"].(string)
				fp.Township_name = maps[0]["township_name"].(string)
			}
			//计算流动人口的停留时间
			tm, _ := time.Parse("2006-01-02", fp.Flow_tothis_time)
			fp.Flow_time = util.SubDate(time.Now(), tm)
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = flowperson //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//根据id获得流动人口信息
func GetFlowpersonById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	var flowperson []*Flowperson
	var getinfo lib.GetInfo
	num2, err := o.Raw("select *from `flowperson` where id=?", id).QueryRows(&flowperson)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		for i := 0; i < len(flowperson); i++ {
			fp := flowperson[i]
			num, _ := o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", flowperson[i].Village_id).Values(&maps)
			if num > 0 {
				fp.Village_name = maps[0]["village_name"].(string)
				fp.Township_id = maps[0]["township_id"].(string)
				fp.Township_name = maps[0]["township_name"].(string)
			}
			//计算流动人口的停留时间
			tm, _ := time.Parse("2006-01-02", fp.Flow_tothis_time)
			fp.Flow_time = util.SubDate(time.Now(), tm)
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = flowperson //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得流动人口信息,分页
func GetFlowpersonByPage(user_id string, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit := "SELECT a.id num FROM `flowperson` a"
	sql := "select *from `flowperson` a order by a.id desc limit ?,?"

	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)

	//角色id判断
	var admin_id string//单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" {//非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}

		if admin_id != "1" {//非根管理员
			var role_id string
			//判断是否拥有查看全部的角色权限,角色15固定
			for _,v := range maps2{
				if v["role_id"] == "16"{
					role_id = "16"
					break
				}
			}
			if role_id != "16"{//查看各自账号所在流动人口信息
				sql = strings.Replace(sql,"`flowperson` a","`flowperson` a inner join `village` bb " +
					"on a.village_id=bb.id " +
					"inner join `township` cc on bb.township_id=cc.id " +
					"and cc.unit_id="+maps2[0]["unit_id"].(string),-1)
				sqlnolimit = strings.Replace(sqlnolimit,"`flowperson` a","`flowperson` a inner join `village` bb " +
					"on a.village_id=bb.id " +
					"inner join `township` cc on bb.township_id=cc.id " +
					"and cc.unit_id="+maps2[0]["unit_id"].(string),-1)
			}
		}
	}


	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var flowperson []*Flowperson
		var getinfo lib.GetInfo
		o.Raw(sql, (clientPage-1)*everyPage, everyPage).QueryRows(&flowperson)
		//统计页码等状态
		for i := 0; i < len(flowperson); i++ {
			fp := flowperson[i]
			num, _ := o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", flowperson[i].Village_id).Values(&maps)
			if num > 0 {
				fp.Village_name = maps[0]["village_name"].(string)
				fp.Township_id = maps[0]["township_id"].(string)
				fp.Township_name = maps[0]["township_name"].(string)
			}
			//计算流动人口的停留时间
			tm, _ := time.Parse("2006-01-02", fp.Flow_tothis_time)
			fp.Flow_time = util.SubDate(time.Now(), tm)

			if fp.Born_date != ""{
				tm, _ = time.Parse("2006-01-02", fp.Born_date)
				//fmt.Println(tm)
				fp.Age = time.Now().Year() - tm.Year()
			}
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = flowperson //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//删除流动人口信息,根据id
func DeleteFlowpersonByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `flowperson` where id=?", id).Exec()
	//res,err := o.Raw("delete from `flowperson` where id=?",id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改流动人口信息
func UpdateFlowperson(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("flowperson", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建流动人口信息
func CreateFlowperson(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("flowperson", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
