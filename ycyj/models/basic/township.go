package basic

import (
	"github.com/astaxie/beego/orm"
	"strings"
	"ycyj/models/lib"
)

/*乡镇信息*/
type Township struct {
	Id                       int
	Number                   string     //编号
	Township_name            string     //乡镇名称
	Township_area_num        string     //国土面积
	Township_altitude        string     //平均海拔
	Township_person_num      int        //户籍人口
	Township_p_person_num    int        //重点人口
	Township_flow_person_num int        //流动人口
	Township_dwsj_name       string     //党委书记
	Township_dwsj_phone      string     //党委书记联系方式
	Township_leader          string     //乡镇长
	Township_leader_phone    string     //乡镇长联系方式
	Unit_id                  int        //关联派出所id
	Unit_name                string     //关联派出所
	Nativeperson_num         string     //本地人口数量,以下人口数量动态计算
	Emphasisperson_num       string     //本地人口中的重点人口数量
	Flowperson_num           string     //流动人口数量
	Village                  []*Village //乡镇下面的多个村子信息>=0
}

/*乡镇乡村下拉框*/
type TownshipList struct {
	Id            int
	Township_name string //乡镇名称
	VillageList   []*VillageList
}

type VillageList struct {
	Id           int
	Village_name string //乡村名
}

/*乡镇乡村下拉框*/
type TownshipListN struct {
	Id            int
	Township_name string //乡镇名称
}

//获得所有乡镇以及下面的乡村,下拉框
func GetTownshipListN() interface{} {
	var info interface{}
	var getinfo lib.GetInfoN
	o := orm.NewOrm()
	o.Using("default")
	var township []*TownshipListN
	num, _ := o.Raw("select id,township_name from `township`").QueryRows(&township)
	if num > 0 {
		getinfo.Status = 200
		getinfo.Data = township
		getinfo.Msg = "请求成功"
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得所有乡镇以及下面的乡村,下拉框
func GetTownshipList() interface{} {
	var info interface{}
	var getinfo lib.GetInfoN
	o := orm.NewOrm()
	o.Using("default")
	var township []*TownshipList
	num, _ := o.Raw("select id,township_name from `township`").QueryRows(&township)
	if num > 0 {
		var villagelist []*VillageList
		for i := 0; i < len(township); i++ {
			num, _ = o.Raw("select id,village_name from `village` where township_id=?", township[i].Id).QueryRows(&villagelist)
			for j := 0; j < len(villagelist); j++ {
				township[i].VillageList = append(township[i].VillageList, villagelist[j])
			}
		}
		getinfo.Status = 200
		getinfo.Data = township
		getinfo.Msg = "请求成功"
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//根据id查询相应的乡镇
func GetTownshipById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var township []*Township
	var getinfo lib.GetInfo
	num2, err := o.Raw("select *from `township` where id=?", id).QueryRows(&township)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//每个乡镇中对应的村子信息
		var mapsP []orm.Params
		//多少个乡镇
		for i := 0; i < len(township); i++ {
			ts := township[i]
			//乡镇中对应的多个村子
			num, err := o.Raw("SELECT unit_name FROM `unit` WHERE id = ?", ts.Unit_id).Values(&mapsP)
			if err == nil && num > 0 {
				ts.Unit_name = mapsP[0]["unit_name"].(string)
				villages := GetVillage(ts.Id)
				for j := 0; j < len(villages); j++ {
					ts.Village = append(ts.Village, villages[j])
				}
			}
			//动态计算几种人口数量,根据乡镇id
			num, err = o.Raw("select count(c.id) as num1 from township a inner join village b inner join nativeperson c on a.id=b.township_id and b.id=c.village_id where a.id=?", ts.Id).Values(&mapsP)
			if err == nil && num > 0 {
				ts.Nativeperson_num = mapsP[0]["num1"].(string)
			}
			num, err = o.Raw("select count(c.id) as num1 from township a inner join village b inner join nativeperson c inner join emphasisperson d on a.id=b.township_id and b.id=c.village_id and c.id=d.nativeperson_id where a.id=?", ts.Id).Values(&mapsP)
			if err == nil && num > 0 {
				ts.Emphasisperson_num = mapsP[0]["num1"].(string)
			}
			num, err = o.Raw("select count(c.id) as num1 from township a inner join village b inner join flowperson c on a.id=b.township_id and b.id=c.village_id where a.id=?", ts.Id).Values(&mapsP)
			if err == nil && num > 0 {
				ts.Flowperson_num = mapsP[0]["num1"].(string)
			}
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = township //数据
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得乡镇,分页
func GetTownshipByPage(user_id string, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)

	//角色id判断
	var admin_id string //单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" { //非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}
	}

	sqlnolimit := "SELECT count(id) as num FROM `township`"
	sql := "select *from `township` limit ?,?"

	if admin_id != "1" { //非根管理员
		var role_id string
		//判断是否拥有查看全部的角色权限,角色13固定
		for _, v := range maps2 {
			if v["role_id"] == "13" {
				role_id = "13"
				break
			}
		}
		if role_id != "13" { //查看各自账号所在单位信息
			sql = strings.Replace(sql, "`township`", "`township` where unit_id="+maps2[0]["unit_id"].(string), -1)
			sqlnolimit = strings.Replace(sqlnolimit, "`township`", "`township` where unit_id="+maps2[0]["unit_id"].(string), -1)
		}
	}

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var township []*Township
		var getinfo lib.GetInfo
		o.Raw(sql, (clientPage-1)*everyPage, everyPage).QueryRows(&township)
		//o.QueryTable("township").Limit(clientPage, everyPage).All(&township)
		//每个乡镇中对应的村子信息
		var mapsP []orm.Params
		//多少个乡镇
		for i := 0; i < len(township); i++ {
			ts := township[i]
			//乡镇中对应的多个村子
			num, err := o.Raw("SELECT unit_name FROM `unit` WHERE id = ?", ts.Unit_id).Values(&mapsP)
			if err == nil && num > 0 {
				ts.Unit_name = mapsP[0]["unit_name"].(string)
				villages := GetVillage(ts.Id)
				for j := 0; j < len(villages); j++ {
					ts.Village = append(ts.Village, villages[j])
				}
			}
			//动态计算几种人口数量,根据乡镇id
			num, err = o.Raw("select count(c.id) as num1 from township a inner join village b inner join nativeperson c on a.id=b.township_id and b.id=c.village_id where a.id=?", ts.Id).Values(&mapsP)
			if err == nil && num > 0 {
				ts.Nativeperson_num = mapsP[0]["num1"].(string)
			}
			num, err = o.Raw("select count(c.id) as num1 from township a inner join village b inner join nativeperson c inner join emphasisperson d on a.id=b.township_id and b.id=c.village_id and c.id=d.nativeperson_id where a.id=?", ts.Id).Values(&mapsP)
			if err == nil && num > 0 {
				ts.Emphasisperson_num = mapsP[0]["num1"].(string)
			}
			num, err = o.Raw("select count(c.id) as num1 from township a inner join village b inner join flowperson c on a.id=b.township_id and b.id=c.village_id where a.id=?", ts.Id).Values(&mapsP)
			if err == nil && num > 0 {
				ts.Flowperson_num = mapsP[0]["num1"].(string)
			}
		}
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = township //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//删除乡镇,根据id
func DeleteTownshipByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `township` where id=?", id).Exec()
	//res,err := o.Raw("delete from `news` where id=?",id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改乡镇
func UpdateTownship(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("township", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建乡镇,返回创建时的id,增加乡村用
func CreateTownship(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("township", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		var maps []orm.Params
		var id string
		num, _ := o.Raw("select LAST_INSERT_ID() as id").Values(&maps)
		if num > 0 {
			id = maps[0]["id"].(string)
		}
		info = map[string]string{"Status": "201", "Msg": "创建成功", "id": id}
	}
	return info
}
