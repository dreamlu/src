package basic

import (
	"github.com/astaxie/beego/orm"
	"ycyj/models/lib"
	"strconv"
	"time"
)

/*村子*/
type Village struct {
	Id                  int
	Township_id         int
	Village_name        string //乡村名称
	Village_area		string //国土面积
	Person_num          int    //户籍人口
	P_person_num        string //重点人口
	F_person_num        string //流动人口数量
	Village_distance    string //距离乡镇距离
	Village_dzber       string //党支部
	Village_dzber_phone string //党支部联系方式
	Village_cwher       string //村委会主任
	Village_cwher_phone string //村委会主任联系方式
	Village_gzder       string //驻村工作队队长
	Village_gzder_phone string //驻村工作队队长联系方式
	Police_id           int    //警员id
	Police_name         string //关联警员
}

//根据乡镇id返回对应乡村信息,分页
func GetNativePersonByTownshipId(id, clientPage, everyPage int) interface{} {
	var info interface{}
	var village []*Village
	var npAll []*Nativeperson
	var getinfo lib.GetInfo

	o := orm.NewOrm()
	var maps []orm.Params
	num2, err := o.Raw("SELECT a.id FROM `village` a inner join `nativeperson` b on a.id=b.village_id where township_id=?", id).Values(&maps)
	if err == nil && num2 > 0 {
		num, _ := o.Raw("SELECT a.id FROM `village` a inner join `nativeperson` b on a.id=b.village_id where township_id=? order by id desc limit ?,?", id, (clientPage-1)*everyPage, everyPage).QueryRows(&village)
		if num > 0 {
			for _,v := range village{
				var nativeperson []*Nativeperson
				num,_ := o.Raw("select * from `nativeperson` where village_id=?",v.Id).QueryRows(&nativeperson)
				if num > 0{
					for _,v := range nativeperson{
						fp := v

						o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", v.Village_id).Values(&maps)
						fp.Village_name = maps[0]["village_name"].(string)
						fp.Township_id = maps[0]["township_id"].(string)
						fp.Township_name = maps[0]["township_name"].(string)
						//年龄计算
						tm, _ := time.Parse("2006-01-02", fp.Born_date)
						//fmt.Println(tm)
						fp.Age = time.Now().Year() - tm.Year()

						//重点人口判断
						num, _ := o.Raw("select id from `emphasisperson` where nativeperson_id=?", fp.Id).Values(&maps)
						if num > 0 {
							fp.Isz = "是"
						}

						npAll = append(npAll,v)
					}
				}
			}


			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = npAll //数据
			getinfo.Pager.SumPage = strconv.FormatInt(num2,10)
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	} else {
		info = lib.MapError
	}
	return info
}

//根据乡镇id返回对应乡村信息,分页
func GetVillageByTownshipId(id, clientPage, everyPage int) interface{} {
	var info interface{}
	var village []*Village
	var getinfo lib.GetInfo

	o := orm.NewOrm()
	var maps []orm.Params
	num2, err := o.Raw("SELECT id FROM `village` where township_id=?", id).Values(&maps)
	if err == nil && num2 > 0 {
		num, _ := o.Raw("select *from `village` where township_id=? order by id desc limit ?,?", id, (clientPage-1)*everyPage, everyPage).QueryRows(&village)
		if num > 0 {

			for i := 0; i < len(village); i++ {
				vg := village[i]
				num, _ := o.Raw("select police_name from `police` where id=?", vg.Police_id).Values(&maps)
				if num > 0 {
					vg.Police_name = maps[0]["police_name"].(string)
				}
			}

			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = village //数据
			getinfo.Pager.SumPage = strconv.FormatInt(num2,10)
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		}
	} else {
		info = lib.MapError
	}
	return info
}

//根据id返回对应乡村信息
func GetVillageById(id int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo

	var village []*Village
	o := orm.NewOrm()
	num, _ := o.Raw("select *from `village` where id=?", id).QueryRows(&village)
	if num > 0 {
		var maps []orm.Params
		for i := 0; i < len(village); i++ {
			vg := village[i]
			num2, _ := o.Raw("select police_name from `police` where id=?", vg.Police_id).Values(&maps)
			if num2 > 0 {
				vg.Police_name = maps[0]["police_name"].(string)
			}
		}
		getinfo.Data = village
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//删除村子,根据id
func DeleteVillageByid(id int) interface{} {
	var info interface{}

	o := orm.NewOrm()
	qs := o.QueryTable("village")
	num, _ := qs.Filter("id", id).Delete()
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改村子
func UpdateVillage(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("village", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建村子
func CreateVillage(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("village", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.GetMapDataError(500,err.Error())
	} else {
		info = lib.MapCreate
	}
	return info
}

//根据乡镇id返回对应乡村
func GetVillage(township_id int) []*Village {
	var village []*Village
	o := orm.NewOrm()
	num, _ := o.Raw("select *from `village` where township_id=?", township_id).QueryRows(&village)
	if num > 0 {
		var maps []orm.Params
		for i := 0; i < len(village); i++ {
			vg := village[i]
			num2, _ := o.Raw("select police_name from `police` where id=?", vg.Police_id).Values(&maps)
			if num2 > 0 {
				vg.Police_name = maps[0]["police_name"].(string)
			}
		}
	}
	return village
}

func init() {
	// 需要在init中注册定义的model
	orm.RegisterModel(new(Village))
}
