package basic

import (
	"github.com/astaxie/beego/orm"
	"ycyj/models/lib"
)

/*本地重点人口管控记录管理*/
type Emphasiscontrol struct {
	Id              int
	Related_remark  string //相关记录
	Remark_name     string //记录人,不一定是登录的(警员)账号
	Info_source     string //消息来源
	Record_date     string //记录日期
	Nativeperson_id string //本地人口id
}

//根据本地人口id获取对应的重点人口管控信息
func GetEmphasiscontrolByNativepersonId(nativeperson_id int) interface{} {
	var info interface{}
	var getinfo lib.GetInfoN
	o := orm.NewOrm()
	o.Using("default")

	var emphasiscontrol []*Emphasiscontrol
	num2, err := o.Raw("select *from `emphasiscontrol` where nativeperson_id=?", nativeperson_id).QueryRows(&emphasiscontrol)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = emphasiscontrol
		info = getinfo
	}else{
		info = lib.MapError
	}
	return info
}

//删除本地重点人口管控记录信息,根据id
func DeleteEmphasiscontrolByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `emphasiscontrol` where id=?", id).Exec()
	//res,err := o.Raw("delete from `emphasiscontrol` where id=?",id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//创建本地重点人口管控记录信息
func CreateEmphasiscontrol(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("emphasiscontrol", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
