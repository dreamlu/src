package basic

import (
	"github.com/astaxie/beego/orm"
	"strings"
	"ycyj/models/lib"
)

//单位(派出所)信息维护
type Unit struct {
	Id                  int
	Number              string //编号
	Unit_name           string //单位名称
	Police_num          int    //警员人数
	Police_leader       string //负责人
	Police_leader_phone string //负责人联系方式
}

/*单位下拉框*/
type UnitList struct {
	Id        int
	Unit_name string
}

//获得所有单位id以及名称
func GetUnitList() interface{} {
	var info interface{}
	var getinfo lib.GetInfoN
	o := orm.NewOrm()
	o.Using("default")
	var unit []*UnitList
	num, _ := o.Raw("select id,unit_name from `unit`").QueryRows(&unit)
	if num > 0 {
		getinfo.Status = 200
		getinfo.Data = unit
		getinfo.Msg = "请求成功"
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//根据id获得相应的单位
func GetUnitById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var unit []Unit
	var getinfo lib.GetInfo
	num2, err := o.Raw("select *from `unit` where id=?", id).QueryRows(&unit)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = unit //数据
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//根据单位id查看对应的警员信息
func GetUnitPoliceById(id, clientPage, everyPage int) interface{} {
	var info interface{}
	info = GetPoliceByUnit_id(id, clientPage, everyPage)
	return info
}

//获得单位,分页
func GetUnitByPage(user_id string, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)

	//角色id判断
	var admin_id string//单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" {//非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}
	}

	sqlnolimit := "SELECT count(id) as num FROM `unit`"
	sql := "select *from `unit` order by id desc limit ?,?"

	if admin_id != "1" {//非根管理员
		var role_id string
		//判断是否拥有查看全部的角色权限,角色12固定
		for _,v := range maps2{
			if v["role_id"] == "12"{
				role_id = "12"
				break
			}
		}
		if role_id != "12"{//查看各自账号所在单位信息
			sql = strings.Replace(sql,"`unit`","`unit` where id="+maps2[0]["unit_id"].(string),-1)
			sqlnolimit = strings.Replace(sqlnolimit,"`unit`","`unit` where id="+maps2[0]["unit_id"].(string),-1)
		}
	}

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var unit []Unit
		var getinfo lib.GetInfo
		o.Raw(sql, (clientPage-1)*everyPage, everyPage).QueryRows(&unit)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = unit //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//删除单位,根据id
func DeleteUnitByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw("delete from `unit` where id=?", id).Exec()
	//res,err := o.Raw("delete from `news` where id=?",id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改单位
func UpdateUnit(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("unit", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建单位
func CreateUnit(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("unit", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
