package basic

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"time"
	"ycyj/models/lib"
)

/*本地人口管理*/
type Nativeperson struct {
	Id                   int
	Name                 string //姓名
	Sex                  string //性别
	Born_date            string //出生日期
	Village_id           string //乡村id
	Village_name         string //乡村名
	Township_id          string //乡镇id
	Township_name        string //乡镇名称
	Age                  int    //年龄
	Idcard               string //身份证号码
	Now_location         string //现居地
	Education            string //文化程度
	Political_status     string //政治面貌
	Ishouseholder        string //是否是户主
	Household_num        string //户号
	Householder_relation string //与户主关系
	Phone                string //电话号码
	Career               string //职业
	Ismarry              string //是否已婚
	Ismove               string //是否外地迁入
	Move_address         string //迁入地
	Move_time            string //迁入时间
	Isz                  string //重点人口标记
}

/*学历*/
type Education struct {
	Education	string
}

/*政治面貌*/
type Political struct {
	Political_status	string
}

/*职业*/
type Career struct {
	Career	string
}

//查找职业下拉框
func GetCareer() interface{}{
	var info interface{}
	var getinfo lib.GetInfoN

	var Career []*Career
	o := orm.NewOrm()
	num,_ := o.Raw("select distinct career from `nativeperson`").QueryRows(&Career)
	if num > 0 {
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = Career
		info = getinfo
	}else{
		info = lib.MapError
	}
	return info
}

//查找政治面貌下拉框
func GetPolitical() interface{}{
	var info interface{}
	var getinfo lib.GetInfoN

	var political []*Political
	o := orm.NewOrm()
	num,_ := o.Raw("select distinct political_status from `nativeperson`").QueryRows(&political)
	if num > 0 {
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = political
		info = getinfo
	}else{
		info = lib.MapError
	}
	return info
}

//查找教育学历下拉框
func GetEducation() interface{}{
	var info interface{}
	var getinfo lib.GetInfoN

	var education []*Education
	o := orm.NewOrm()
	num,_ := o.Raw("select distinct education from `nativeperson`").QueryRows(&education)
	if num > 0 {
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = education
		info = getinfo
	}else{
		info = lib.MapError
	}
	return info
}

//本地人口信息搜索,分页
func GetNativepersonBySearch(user_id string, args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	//页码,每页数量
	clientPageStr := beego.AppConfig.String("clientPage") //默认第1页
	everyPageStr := beego.AppConfig.String("everyPage")   //默认10页
	//var township_id string

	sql := "select distinct a.id,name,sex,born_date,village_id,b.village_name,b.township_id,c.township_name,idcard,now_location,education,political_status,ishouseholder,household_num,householder_relation,phone,career,ismarry,ismove,move_address,move_time " +
		"from `nativeperson` a " +
		"left join `village` b on a.village_id=b.id " +
		"left join `township` c " +
		"on b.township_id=c.id where 1=1 and "


	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)

	//角色id判断
	var admin_id string//单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" {//非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}

		if admin_id != "1" {//非根管理员
			var role_id string
			//判断是否拥有查看全部的角色权限,角色15固定
			for _,v := range maps2{
				if v["role_id"] == "15"{
					role_id = "15"
					break
				}
			}
			if role_id != "15"{//查看各自账号所在本地人口信息
				sql = strings.Replace(sql,"`nativeperson` a","`nativeperson` a inner join `village` bb " +
					"on a.village_id=bb.id " +
					"inner join `township` cc on bb.township_id=cc.id " +
					"and cc.unit_id="+maps2[0]["unit_id"].(string),-1)
			}
		}
	}


	for k, v := range args {
		if k == "clientPage" {
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" {
			everyPageStr = v[0]
			continue
		}
		if v[0] == "" { //条件为空,舍弃
			continue
		}
		sql += k + " = '" + v[0] + "' and "
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	c := []byte(sql)
	sql = string(c[:len(c)-4]) //去and
	sqlnolimit := sql
	sql += " limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var nativeperson []*Nativeperson
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&nativeperson)
		//统计页码等状态
		for i := 0; i < len(nativeperson); i++ {
			//o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", nativeperson[i].Village_id).Values(&maps)
			fp := nativeperson[i]
			fp.Isz = "否" //重点人口,默认否

			//fp.Village_name = maps[0]["village_name"].(string)
			//fp.Township_id = maps[0]["township_id"].(string)
			//fp.Township_name = maps[0]["township_name"].(string)
			//年龄计算
			tm, _ := time.Parse("2006-01-02", fp.Born_date)
			//fmt.Println(tm)
			fp.Age = time.Now().Year() - tm.Year()

			//重点人口判断
			num, _ := o.Raw("select id from `emphasisperson` where nativeperson_id=?", fp.Id).Values(&maps)
			if num > 0 {
				fp.Isz = "是"
			}
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = nativeperson //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//根据户号获取对应的本地人口信息(一家人)
func GetNativepersonByHousehold(household_num string) interface{} {
	var info interface{}
	var getinfo lib.GetInfoN
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	var nativeperson []*Nativeperson
	num2, err := o.Raw("select *from `nativeperson` where household_num=?", household_num).QueryRows(&nativeperson)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		for i := 0; i < len(nativeperson); i++ {
			fp := nativeperson[i]

			num,_ := o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", nativeperson[i].Village_id).Values(&maps)
			if num > 0 {

				fp.Village_name = maps[0]["village_name"].(string)
				fp.Township_id = maps[0]["township_id"].(string)
				fp.Township_name = maps[0]["township_name"].(string)
				//年龄计算
				tm, _ := time.Parse("2006-01-02", fp.Born_date)
				//fmt.Println(tm)
				fp.Age = time.Now().Year() - tm.Year()
			}

			//重点人口判断
			num, _ = o.Raw("select id from `emphasisperson` where nativeperson_id=?", fp.Id).Values(&maps)
			if num > 0 {
				fp.Isz = "是"
			}
		}
		getinfo.Status = 200
		getinfo.Data = nativeperson
		getinfo.Msg = "请求成功"
		info = getinfo
	}else {
		info = lib.MapError
	}
	return info
}

//根据id获取对应的本地人口信息
func GetNativepersonByIdP(id string) interface{} {
	var info interface{}
	var getinfo lib.GetInfoN

	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	var nativeperson []*Nativeperson
	num2, err := o.Raw("select *from `nativeperson` where id=?", id).QueryRows(&nativeperson)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		for i := 0; i < len(nativeperson); i++ {
			fp := nativeperson[i]

			num,_ := o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", nativeperson[i].Village_id).Values(&maps)
			if num > 0 {

				fp.Village_name = maps[0]["village_name"].(string)
				fp.Township_id = maps[0]["township_id"].(string)
				fp.Township_name = maps[0]["township_name"].(string)
				//年龄计算
				tm, _ := time.Parse("2006-01-02", fp.Born_date)
				//fmt.Println(tm)
				fp.Age = time.Now().Year() - tm.Year()
			}

			//重点人口判断
			num, _ = o.Raw("select id from `emphasisperson` where nativeperson_id=?", fp.Id).Values(&maps)
			if num > 0 {
				fp.Isz = "是"
			}
		}
		getinfo.Msg = "请求成功"
		getinfo.Status = 200
		getinfo.Data = nativeperson
		info = getinfo
	}else {
		info = lib.MapError
	}
	return info
}


//根据id获取对应的本地人口信息
func GetNativepersonById(id string) []*Nativeperson {

	if id == ""{
		return nil
	}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	var nativeperson []*Nativeperson
	num2, err := o.Raw("select *from `nativeperson` where id=?", id).QueryRows(&nativeperson)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		for i := 0; i < len(nativeperson); i++ {
			fp := nativeperson[i]

			num,_ := o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", nativeperson[i].Village_id).Values(&maps)
			if num > 0 {

				fp.Village_name = maps[0]["village_name"].(string)
				fp.Township_id = maps[0]["township_id"].(string)
				fp.Township_name = maps[0]["township_name"].(string)
				//年龄计算
				tm, _ := time.Parse("2006-01-02", fp.Born_date)
				//fmt.Println(tm)
				fp.Age = time.Now().Year() - tm.Year()
			}

			//重点人口判断
			num, _ = o.Raw("select id from `emphasisperson` where nativeperson_id=?", fp.Id).Values(&maps)
			if num > 0 {
				fp.Isz = "是"
			}
		}
	}
	return nativeperson
}

//根据身份证号码获取对应的本地人口信息
func GetNativepersonByIdcard(idcard string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	var nativeperson []*Nativeperson
	var getinfo lib.GetInfo
	num2, err := o.Raw("select *from `nativeperson` where idcard='" + idcard + "'").QueryRows(&nativeperson)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		for i := 0; i < len(nativeperson); i++ {
			fp := nativeperson[i]
			num,_ := o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", nativeperson[i].Village_id).Values(&maps)
			if num > 0 {

				fp.Village_name = maps[0]["village_name"].(string)
				fp.Township_id = maps[0]["township_id"].(string)
				fp.Township_name = maps[0]["township_name"].(string)
				//年龄计算
				tm, _ := time.Parse("2006-01-02", fp.Born_date)
				//fmt.Println(tm)
				fp.Age = time.Now().Year() - tm.Year()
			}
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = nativeperson //数据
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得本地人口信息,分页
func GetNativepersonByPage(user_id string, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit := "SELECT count(a.id) as num FROM `nativeperson` a "
	sql := "select *from `nativeperson` a order by a.id desc limit ?,?"

	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)

	//角色id判断
	var admin_id string//单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" {//非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}

		if admin_id != "1" {//非根管理员
			var role_id string
			//判断是否拥有查看全部的角色权限,角色15固定
			for _,v := range maps2{
				if v["role_id"] == "15"{
					role_id = "15"
					break
				}
			}
			if role_id != "15"{//查看各自账号所在本地人口信息
				sql = strings.Replace(sql,"`nativeperson` a","`nativeperson` a inner join `village` b " +
					"on a.village_id=b.id " +
					"inner join `township` c on b.township_id=c.id " +
					"where c.unit_id="+maps2[0]["unit_id"].(string),-1)
				sqlnolimit = strings.Replace(sqlnolimit,"`nativeperson` a","`nativeperson` a inner join `village` b " +
					"on a.village_id=b.id " +
					"inner join `township` c on b.township_id=c.id " +
					"where c.unit_id="+maps2[0]["unit_id"].(string),-1)
			}
		}
	}

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var nativeperson []*Nativeperson
		var getinfo lib.GetInfo
		o.Raw(sql, (clientPage-1)*everyPage, everyPage).QueryRows(&nativeperson)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)
		for i := 0; i < len(nativeperson); i++ {
			fp := nativeperson[i]
			fp.Isz = "否" //重点人口,默认否

			num,_ := o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", nativeperson[i].Village_id).Values(&maps)
			if num > 0 {

				fp.Village_name = maps[0]["village_name"].(string)
				fp.Township_id = maps[0]["township_id"].(string)
				fp.Township_name = maps[0]["township_name"].(string)
				//年龄计算
				tm, _ := time.Parse("2006-01-02", fp.Born_date)
				//fmt.Println(tm)
				fp.Age = time.Now().Year() - tm.Year()
			}
			//重点人口判断
			num, _ = o.Raw("select id from `emphasisperson` where nativeperson_id=?", fp.Id).Values(&maps)
			if num > 0 {
				fp.Isz = "是"
			}
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = nativeperson //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//删除本地人口信息,根据id
func DeleteNativepersonByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `nativeperson` where id=?", id).Exec()
	//res,err := o.Raw("delete from `nativeperson` where id=?",id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		//删除对应的重点人口数据
		o.Raw("delete from `emphasisperson` where nativeperson_id=?",id)
		info = lib.MapDelete
	}
	return info
}

//修改本地人口信息
func UpdateNativeperson(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("nativeperson", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建本地人口信息
func CreateNativeperson(args map[string][]string) interface{} {

	o := orm.NewOrm()
	//查询对应户号是否已经存在户主
	if _,ok := args["household_num"];ok{
		var nap Nativeperson
		o.Raw("select id,ishouseholder from `nativeperson` where household_num = ?",args["household_num"][0]).QueryRow(&nap)
		if nap.Id != 0 && nap.Ishouseholder == "是" && args["ishouseholder"][0] == "是"{
			return lib.GetMapDataError(222,"该户号已存在户主")
		}
	}


	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("nativeperson", args)

	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 && err != nil {
		info = lib.GetMapDataError(222,err.Error())
	} else if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
