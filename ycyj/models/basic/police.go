package basic

import (
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"time"
	"ycyj/models/lib"
)

//警员信息管理
type Police struct {
	Id              int
	Number          string    //编号
	Police_name     string //警员姓名
	Police_sex      string //性别
	Age             int    //年龄
	Born_date       string //出生日期
	Idcard          string //身份证号码
	Birthplace      string //籍贯
	Unit_id         string //单位id,更新时用
	Unit_name       string //所属单位  表中unit_id
	Township_id     string
	Township_name   string //关联乡村 表中village_id==>township_id
	Police_position string //职务
	Village_id      string //村子id,更新用
	Village_name    string //村子名称
	Police_phone    string //联系方式
}

//警员信息搜索,分页
func SearchPolice(user_id string, args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := lib.SearchTableSql("police", args)

	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)

	//角色id判断
	var admin_id string//单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" {//非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}

		if admin_id != "1" {//非根管理员
			var role_id string
			//判断是否拥有查看全部的角色权限,角色12固定
			for _,v := range maps2{
				if v["role_id"] == "14"{
					role_id = "14"
					break
				}
			}
			if role_id != "14"{//查看各自账号所在警员信息
				sql = strings.Replace(sql,"1=1","1=1 unit_id="+maps2[0]["unit_id"].(string),-1)
				sqlnolimit = strings.Replace(sqlnolimit,"1=1","1=1 unit_id="+maps2[0]["unit_id"].(string),-1)
			}
		}
	}

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var police []*Police
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&police)
		//统计页码等状态
		for i := 0; i < len(police); i++ {
			fp := police[i]
			//乡镇数据
			num, _ := o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", fp.Village_id).Values(&maps)
			if num > 0 {
				fp.Village_name = maps[0]["village_name"].(string)
				fp.Township_id = maps[0]["township_id"].(string)
				fp.Township_name = maps[0]["township_name"].(string)
			}
			//单位信息
			num, _ = o.Raw("select unit_name from `unit` where id=?", fp.Unit_id).Values(&maps)
			if num > 0 {
				fp.Unit_name = maps[0]["unit_name"].(string)
			}
			//年龄计算
			tm, _ := time.Parse("2006-01-02", fp.Born_date)
			//fmt.Println(tm)
			fp.Age = time.Now().Year() - tm.Year()
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = police //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//根据单位unit_id,获得对应的警员信息,分页
func GetPoliceByUnit_id(unit_id, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `police` where unit_id=?", unit_id).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var police []*Police
		var getinfo lib.GetInfo
		o.Raw("select *from `police` where unit_id=? order by id desc limit ?,?", unit_id, (clientPage-1)*everyPage, everyPage).QueryRows(&police)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)
		for i := 0; i < len(police); i++ {
			fp := police[i]
			//乡镇数据
			num, _ := o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", fp.Village_id).Values(&maps)
			if num > 0 {
				fp.Village_name = maps[0]["village_name"].(string)
				fp.Township_id = maps[0]["township_id"].(string)
				fp.Township_name = maps[0]["township_name"].(string)
			}
			//单位信息
			num, _ = o.Raw("select unit_name from `unit` where id=?", fp.Unit_id).Values(&maps)
			if num > 0 {
				fp.Unit_name = maps[0]["unit_name"].(string)
			}

			//年龄计算
			tm, _ := time.Parse("2006-01-02", fp.Born_date)
			//fmt.Println(tm)
			fp.Age = time.Now().Year() - tm.Year()
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = police //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//根据id或的警员相关信息
func GetPoliceById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	var police []*Police
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `police` where id=?", id).QueryRows(&police)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		for i := 0; i < len(police); i++ {
			fp := police[i]
			//乡镇数据
			num, _ := o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", fp.Village_id).Values(&maps)
			if num > 0 {
				fp.Village_name = maps[0]["village_name"].(string)
				fp.Township_id = maps[0]["township_id"].(string)
				fp.Township_name = maps[0]["township_name"].(string)
			}
			//单位信息
			num, _ = o.Raw("select unit_name from `unit` where id=?", fp.Unit_id).Values(&maps)
			if num > 0 {
				fp.Unit_name = maps[0]["unit_name"].(string)
			}

			//年龄计算
			tm, _ := time.Parse("2006-01-02", fp.Born_date)
			//fmt.Println(tm)
			fp.Age = time.Now().Year() - tm.Year()
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = police //数据
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得警员,分页
func GetPoliceByPage(user_id string, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit := "SELECT count(id) as num FROM `police`"
	sql := "select *from `police` order by id desc limit ?,?"

	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)

	//角色id判断
	var admin_id string//单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" {//非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}

		if admin_id != "1" {//非根管理员
			var role_id string
			//判断是否拥有查看全部的角色权限,角色12固定
			for _,v := range maps2{
				if v["role_id"] == "14"{
					role_id = "14"
					break
				}
			}
			if role_id != "14"{//查看各自账号所在单位信息
				sql = strings.Replace(sql,"`police`","`police` where unit_id="+maps2[0]["unit_id"].(string),-1)
				sqlnolimit = strings.Replace(sqlnolimit,"`police`","`police` where unit_id="+maps2[0]["unit_id"].(string),-1)
			}
		}
	}

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var police []*Police
		var getinfo lib.GetInfo
		o.Raw(sql, (clientPage-1)*everyPage, everyPage).QueryRows(&police)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)
		for i := 0; i < len(police); i++ {
			fp := police[i]
			//乡镇数据
			num, _ := o.Raw("select village_name,township_id,township_name from `village` a INNER JOIN `township` b on a.township_id=b.id where a.id=?", fp.Village_id).Values(&maps)
			if num > 0 {
				fp.Village_name = maps[0]["village_name"].(string)
				fp.Township_id = maps[0]["township_id"].(string)
				fp.Township_name = maps[0]["township_name"].(string)
			}
			//单位信息
			num, _ = o.Raw("select unit_name from `unit` where id=?", fp.Unit_id).Values(&maps)
			if num > 0 {
				fp.Unit_name = maps[0]["unit_name"].(string)
			}

			//年龄计算
			tm, _ := time.Parse("2006-01-02", fp.Born_date)
			//fmt.Println(tm)
			fp.Age = time.Now().Year() - tm.Year()
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = police //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//删除警员,根据id
func DeletePoliceByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw("delete from `police` where id=?", id).Exec()
	//res,err := o.Raw("delete from `news` where id=?",id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改警员
func UpdatePolice(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("police", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建警员,默认密码
func CreatePolice(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("police", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}

//根据警员编号number返回id
func GetIdByNumber(number int) interface{} {
	var info interface{}
	var id string
	var name string

	o := orm.NewOrm()
	var maps []orm.Params
	num, _ := o.Raw("select id,police_name from `police` where number=?", number).Values(&maps)
	if num > 0 {
		id = maps[0]["id"].(string)
		name = maps[0]["police_name"].(string)
		info = map[string]string{"Status": "200", "id": id,"police_name":name}
	}else{
		info = lib.MapError
	}
	return info
}

func init() {
	// 需要在init中注册定义的model
	orm.RegisterModel(new(Police))
}
