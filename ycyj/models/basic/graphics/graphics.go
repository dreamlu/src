package graphics

import (
	"github.com/astaxie/beego/orm"
	"ycyj/models/lib"
	"strconv"
)

/*本地人口性别比例图数据*/
type NativeSexGraphics struct {
	Sex string `json:"item"`  //性别
	Num int    `json:"count"` //对应数量
}

/*本地人口年龄比例图数据*/
type NativeAgeGraphics struct {
	Age string `json:"item"`  //年龄
	Num int    `json:"count"` //对应数量
}

/*流动人口与本地户籍人口比例*/
type CompareFlowAndNative struct {
	Type string `json:"item"`  //流动/本地
	Num  int    `json:"count"` //对应数量
}

/*本地户籍人户分离、流动人口户籍数据*/
type NativeLeave struct {
	Province string `json:"item"`  //省份
	Num      int    `json:"count"` //对应数量
}

/*流动人口动态变化,月为单位*/
type FlowChange struct {
	Time string `json:"item"`
	Num  int    `json:"count"`
}

/*流动人口动态变化数据*/
func GetFlowChange(township_id, time1, time2 string) interface{} {
	var info interface{}
	var getinfo lib.GetInfoN

	var nl []*FlowChange
	var num int64
	o := orm.NewOrm()

	if township_id != "" && time1 != "" {
		num, _ = o.Raw("select count(*) as num,date_format(flow_tothis_time, '%Y-%m') as time from `flowperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			"where township_id=? and timestampdiff(day,'"+time1+"',flow_tothis_time) >= 0 and timestampdiff(day,'"+time2+"',flow_tothis_time) <= 0 group by time ", township_id).QueryRows(&nl)
	} else if township_id != "" {
		num, _ = o.Raw("select count(*) as num,date_format(flow_tothis_time, '%Y-%m') as time from `flowperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			"where township_id=?  group by time ", township_id).QueryRows(&nl)
	} else {
		num, _ = o.Raw("select count(*) as num,date_format(flow_tothis_time, '%Y-%m') as time from `flowperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id " +
			"group by time ").QueryRows(&nl)
	}
	if num > 0 {

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = nl //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

/*流动人口户籍分布数据*/
func GetFlowLeave(township_id, time1, time2 string) interface{} {
	var info interface{}
	var getinfo lib.GetInfoN

	var nl []*NativeLeave
	var num int64
	o := orm.NewOrm()

	if township_id != "" && time1 != "" {
		num, _ = o.Raw("select count(*) as num,substring_index(birthplace,'-', 1) as province from `flowperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			"where township_id=? and timestampdiff(day,'"+time1+"',flow_tothis_time) >= 0 and timestampdiff(day,'"+time2+"',flow_tothis_time) <= 0 group by province ", township_id).QueryRows(&nl)

	} else if township_id != "" {
		num, _ = o.Raw("select count(*) as num,substring_index(birthplace,'-', 1) as province from `flowperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			"where township_id=?  group by province ", township_id).QueryRows(&nl)
	} else {
		num, _ = o.Raw("select count(*) as num,substring_index(birthplace,'-', 1) as province from `flowperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id " +
			"group by province ").QueryRows(&nl)
	}
	if num > 0 {

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = nl //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得本地户籍人户分离数据
func GetNativeLeave(township_id, time1, time2 string) interface{} {
	var info interface{}
	var getinfo lib.GetInfoN

	var nl []*NativeLeave
	var num int64
	o := orm.NewOrm()

	if township_id != "" && time1 != "" {
		num, _ = o.Raw("select count(*) as num,substring_index(now_location,'-', 1) as province from `nativeperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			//"where substring_index(now_location,'-', 1) != substring_index(move_address,'-', 1) "+
			" where township_id=? and timestampdiff(day,'"+time1+"',move_time) >= 0 and timestampdiff(day,'"+time2+"',move_time) <= 0 group by province ", township_id).QueryRows(&nl)

	} else if township_id != "" {
		num, _ = o.Raw("select count(*) as num,substring_index(now_location,'-', 1) as province from `nativeperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			//"where substring_index(now_location,'-', 1) != substring_index(move_address,'-', 1) "+
			" where township_id=?  group by province ", township_id).QueryRows(&nl)
	} else {
		num, _ = o.Raw("select count(*) as num,substring_index(now_location,'-', 1) as province from `nativeperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id " +
			" " +
			" group by province ").QueryRows(&nl)
	}
	if num > 0 {

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = nl //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得本地人口和流动人口比例数据
func GetCompareFNGraphics(township_id, time1, time2 string) interface{} {
	var info interface{}
	var getinfo lib.GetInfoN

	var cfn [2]CompareFlowAndNative
	cfn[0].Type = "本地人口"
	cfn[1].Type = "流动人口"
	var num int64
	var num2 int64
	o := orm.NewOrm()
	var maps []orm.Params

	if township_id != "" && time1 != "" {
		num, _ = o.Raw("select count(*) as num from `nativeperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			" where township_id=? and timestampdiff(day,'"+time1+"',move_time) >= 0 and timestampdiff(day,'"+time2+"',move_time) <= 0", township_id).Values(&maps)
		if num > 0 {
			cfn[0].Num,_ = strconv.Atoi(maps[0]["num"].(string))
		}
		num2, _ = o.Raw("select count(*) as num2 from `flowperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			" where township_id=? and timestampdiff(day,'"+time1+"',flow_tothis_time) >= 0 and timestampdiff(day,'"+time2+"',flow_tothis_time) <= 0", township_id).Values(&maps)
		if num2 > 0 {
			cfn[1].Num,_ = strconv.Atoi(maps[0]["num2"].(string))
		}
	} else if township_id != "" {
		num, _ = o.Raw("select count(*) as num from `nativeperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			" where township_id=? ", township_id).Values(&maps)
		if num > 0 {
			cfn[0].Num,_ = strconv.Atoi(maps[0]["num"].(string))
		}
		num2, _ = o.Raw("select count(*) as num2 from `flowperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			" where township_id=? ", township_id).Values(&maps)

		if num2 > 0 {
			cfn[1].Num,_ = strconv.Atoi(maps[0]["num2"].(string))
		}
	} else {
		num, _ = o.Raw("select count(*) as num from `nativeperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id " +
			" ").Values(&maps)
		if num > 0 {
			cfn[0].Num,_ = strconv.Atoi(maps[0]["num"].(string))
		}
		num2, _ = o.Raw("select count(*) as num2 from `flowperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id " +
			" ").Values(&maps)

		if num2 > 0 {
			cfn[1].Num,_ = strconv.Atoi(maps[0]["num2"].(string))
		}
	}
	if num > 0 || num2 > 0 {

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = cfn //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得本地人口性别比例数据
func GetNativeAgeGraphics(township_id, time1, time2 string) interface{} {
	var info interface{}
	var getinfo lib.GetInfoN

	var nsg []*NativeAgeGraphics
	var num int64
	o := orm.NewOrm()
	//无时间范围时默认所有
	if township_id != "" && time1 == "" {
		num, _ = o.Raw("SELECT "+
			" nld AS age, "+
			" 	count(*) AS num "+
			" FROM "+
			" ( "+
			" 	SELECT "+
			" CASE "+

			" WHEN timestampdiff(year,born_date,now()) >= 0 "+
			" AND timestampdiff(year,born_date,now()) <= 20 THEN "+
			" '1-20' "+
			"  WHEN timestampdiff(year,born_date,now()) >= 21 "+
			" AND timestampdiff(year,born_date,now()) <= 40 THEN "+
			" '21-40' "+
			" 		WHEN timestampdiff(year,born_date,now()) >= 41 "+
			" AND timestampdiff(year,born_date,now()) <= 60 THEN "+
			" '41-60' "+
			" 		WHEN timestampdiff(year,born_date,now()) >= 61 "+
			" THEN "+
			" '60+' "+

			" END AS nld "+
			" 	FROM "+
			"  `nativeperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			" where township_id=?) a "+
			" GROUP BY "+
			" nld", township_id).QueryRows(&nsg)
	} else if township_id != "" {
		num, _ = o.Raw("SELECT "+
			" nld AS age, "+
			" 	count(*) AS num "+
			" FROM "+
			" ( "+
			" 	SELECT "+
			" CASE "+

			" WHEN timestampdiff(year,born_date,now()) >= 0 "+
			" AND timestampdiff(year,born_date,now()) <= 20 THEN "+
			" '1-20' "+
			"  WHEN timestampdiff(year,born_date,now()) >= 21 "+
			" AND timestampdiff(year,born_date,now()) <= 40 THEN "+
			" '21-40' "+
			" 		WHEN timestampdiff(year,born_date,now()) >= 41 "+
			" AND timestampdiff(year,born_date,now()) <= 60 THEN "+
			" '41-60' "+
			" 		WHEN timestampdiff(year,born_date,now()) >= 61 "+
			" THEN "+
			" '60+' "+

			" END AS nld "+
			" 	FROM "+
			"  `nativeperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			" where township_id=? and timestampdiff(day,'"+time1+"',move_time) >= 0 and timestampdiff(day,'"+time2+"',move_time) <= 0) a "+
			" GROUP BY "+
			" nld", township_id).QueryRows(&nsg)
	} else {
		num, _ = o.Raw("SELECT " +
			" nld AS age, " +
			" 	count(*) AS num " +
			" FROM " +
			" ( " +
			" 	SELECT " +
			" CASE " +

			" WHEN timestampdiff(year,born_date,now()) >= 0 " +
			" AND timestampdiff(year,born_date,now()) <= 20 THEN " +
			" '1-20' " +
			"  WHEN timestampdiff(year,born_date,now()) >= 21 " +
			" AND timestampdiff(year,born_date,now()) <= 40 THEN " +
			" '21-40' " +
			" 		WHEN timestampdiff(year,born_date,now()) >= 41 " +
			" AND timestampdiff(year,born_date,now()) <= 60 THEN " +
			" '41-60' " +
			" 		WHEN timestampdiff(year,born_date,now()) >= 61 " +
			" THEN " +
			" '60+' " +

			" END AS nld " +
			" 	FROM " +
			"  `nativeperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id ) a" +
			" GROUP BY " +
			" nld").QueryRows(&nsg)
	}
	if num > 0 {

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = nsg //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得本地人口性别比例数据
func GetNativeSexGraphics(township_id, time1, time2 string) interface{} {
	var info interface{}
	var getinfo lib.GetInfoN

	var nsg []*NativeSexGraphics
	var num int64
	o := orm.NewOrm()
	//无时间,无乡镇范围时默认所有
	if township_id != "" && time1 == "" {
		num, _ = o.Raw("select count(sex) as num,sex from `nativeperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			"where township_id=? group by sex", township_id).QueryRows(&nsg)
	} else if township_id != "" {
		num, _ = o.Raw("select count(sex) as num,sex from `nativeperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id "+
			"where township_id=? and timestampdiff(day,'"+time1+"',move_time) >= 0 and timestampdiff(day,'"+time2+"',move_time) <= 0 "+
			"group by sex", township_id).QueryRows(&nsg)
	} else {
		num, _ = o.Raw("select count(sex) as num,sex from `nativeperson` c left join  `township` a left join `village` b on a.id=b.township_id on b.id=c.village_id group by sex").QueryRows(&nsg)
	}
	if num > 0 {

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = nsg //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}
