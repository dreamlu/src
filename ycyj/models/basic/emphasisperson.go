package basic

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"ycyj/models/lib"
)

/*本地重点人口管理*/
type Emphasisperson struct {
	Id             int
	Type           string //人员类别
	Police_number  string //警员编号
	Police_name    string //警员姓名
	Related_remark string //相关记录
	//Remark_name		string	//记录人,不一定是登录的(警员)账号
	//Info_source		string	//消息来源
	//Record_date		string	//记录日期
	Nativeperson_id string //本地人口id
	Nativeperson    []*Nativeperson
}

//根据id查询重点人口信息
func GetEmphasispersonById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var emphasisperson []*Emphasisperson
	var getinfo lib.GetInfoN
	var maps []orm.Params
	num2, err := o.Raw("select *from `emphasisperson` where id=?", id).QueryRows(&emphasisperson)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {

		for i := 0; i < len(emphasisperson); i++ {
			es := emphasisperson[i]
			num, _ := o.Raw("select police_name from `police` where number=?", es.Police_number).Values(&maps)
			if num > 0 {
				es.Police_name = maps[0]["police_name"].(string)
			}
			if GetNativepersonById(es.Nativeperson_id) != nil {
				es.Nativeperson = append(es.Nativeperson, GetNativepersonById(es.Nativeperson_id)[0])
			}
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = emphasisperson //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//根据查询条件,分页
func GetEmphasispersonBySearch(user_id string, args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	//页码,每页数量
	clientPageStr := beego.AppConfig.String("clientPage") //默认第1页
	everyPageStr := beego.AppConfig.String("everyPage")   //默认10页

	sql := "select distinct nativeperson_id,type,police_number,police_name,related_remark from `emphasisperson` a inner join `nativeperson` b on a.nativeperson_id=b.id left join `police` c on a.police_number=c.number and a.nativeperson_id=b.id where 1=1 and "
	for k, v := range args {
		if k == "clientPage" {
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" {
			everyPageStr = v[0]
			continue
		}
		if v[0] == "" { //条件为空,舍弃
			continue
		}
		sql += "`" + k + "` like '%" + v[0] + "%' and "
	}

	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)

	//角色id判断
	var admin_id string //单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" { //非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}

		if admin_id != "1" { //非根管理员
			var role_id string
			//判断是否拥有查看全部的角色权限,角色15固定
			for _, v := range maps2 {
				if v["role_id"] == "17" {
					role_id = "17"
					break
				}
			}
			if role_id != "17" { //查看各自账号所在重点人口信息
				sql = strings.Replace(sql, "where 1=1",
					"inner join `village` bb "+
						"on b.village_id=bb.id "+
						"inner join `township` cc on bb.township_id=cc.id where 1=1 "+
						"and cc.unit_id="+maps2[0]["unit_id"].(string), -1)
			}
		}
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	c := []byte(sql)
	sql = string(c[:len(c)-4]) //去and
	sqlnolimit := sql
	sql += " limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var emphasisperson []*Emphasisperson
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&emphasisperson)
		for i := 0; i < len(emphasisperson); i++ {
			es := emphasisperson[i]
			num, _ := o.Raw("select police_name from `police` where number=?", es.Police_number).Values(&maps)
			if num > 0 {
				es.Police_name = maps[0]["police_name"].(string)
			}
			if GetNativepersonById(es.Nativeperson_id) != nil {
				es.Nativeperson = append(es.Nativeperson, GetNativepersonById(es.Nativeperson_id)[0])
			}
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = emphasisperson //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得本地重点人口信息,分页
func GetEmphasispersonByPage(user_id string, clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit := "SELECT count(a.id) as num FROM `emphasisperson` a"
	sql := "select *from `emphasisperson` a order by a.id desc limit ?,?"

	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)

	//角色id判断
	var admin_id string //单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" { //非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}

		if admin_id != "1" { //非根管理员
			var role_id string
			//判断是否拥有查看全部的角色权限,角色15固定
			for _, v := range maps2 {
				if v["role_id"] == "17" {
					role_id = "17"
					break
				}
			}
			if role_id != "17" { //查看各自账号所在重点人口信息
				sql = strings.Replace(sql, "`emphasisperson` a", "`emphasisperson` a "+
					"inner join `nativeperson` aa "+
					"on aa.id=a.nativeperson_id "+
					"inner join `village` bb "+
					"on aa.village_id=bb.id "+
					"inner join `township` cc on bb.township_id=cc.id "+
					"and cc.unit_id="+maps2[0]["unit_id"].(string), -1)
				sqlnolimit = strings.Replace(sqlnolimit, "`emphasisperson` a", "`emphasisperson` a "+
					"inner join `nativeperson` aa "+
					"on aa.id=a.nativeperson_id "+
					"inner join `village` bb "+
					"on aa.village_id=bb.id "+
					"inner join `township` cc on bb.township_id=cc.id "+
					"and cc.unit_id="+maps2[0]["unit_id"].(string), -1)
			}
		}
	}

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var emphasisperson []*Emphasisperson
		var getinfo lib.GetInfo
		o.Raw(sql, (clientPage-1)*everyPage, everyPage).QueryRows(&emphasisperson)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)
		for i := 0; i < len(emphasisperson); i++ {
			es := emphasisperson[i]
			num, _ := o.Raw("select police_name from `police` where number=?", es.Police_number).Values(&maps)
			if num > 0 {
				es.Police_name = maps[0]["police_name"].(string)
			}
			if GetNativepersonById(es.Nativeperson_id) != nil {
				es.Nativeperson = append(es.Nativeperson, GetNativepersonById(es.Nativeperson_id)[0])
			}
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = emphasisperson //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//删除本地重点人口信息,根据id
func DeleteEmphasispersonByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `emphasisperson` where id=?", id).Exec()
	//res,err := o.Raw("delete from `emphasisperson` where id=?",id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改本地重点人口信息
func UpdateEmphasisperson(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("emphasisperson", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建本地重点人口信息
func CreateEmphasisperson(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("emphasisperson", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
