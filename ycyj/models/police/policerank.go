package police

import (
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"ycyj/models/lib"
)

/*增加排名机制*/
type PoliceAllSort struct {
	PoliceAll
	Ranking            int    //排名
}

type UnitAllSort struct {
	UnitAll
	Ranking            int    //排名
}

type VillageAllSort struct {
	VillageAll
	Ranking            int    //排名
}

//获得所有警员/行政村/单位的报送数据,排名数据统计,按时间范围
func GetAllCountTimeRank(category, time1, time2 string,clientPage, everyPage int) interface{} {

	var info interface{}
	var getinfo lib.GetInfo
	var maps []orm.Params

	o := orm.NewOrm()

	switch category {
	case "police":
		var policecount []*PoliceAllSort
		num, _ := o.Raw(`select distinct a.id as num

		from police a
		inner join policeaffairs b
		on a.number = b.police_number

		left join reply c
		on b.id=c.policeaffairs_id
		where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0`).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(`select distinct
				police_number as number,
				police_name,
				count(distinct b.id) as pa_num,
				count(distinct c.policeaffairs_id) as pa_reply_num

				from police a
				inner join policeaffairs b
				on a.number = b.police_number

				left join reply c
				on b.id=c.policeaffairs_id
				where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0
				group by number,police_name`).QueryRows(&policecount)

			i := (clientPage - 1) * everyPage
			for k, v := range policecount {
				v.Ranking = i + k + 1
			}

			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = policecount
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	case "unit":
		var unitall []*UnitAllSort
		num, _ := o.Raw(`select distinct a.id as num

		from unit a
		inner join policeaffairs b
		on a.id = b.unit_id

		left join reply c
		on b.id=c.policeaffairs_id
		where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0`).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(`select distinct
				    unit_name,
					(select count(distinct aa.id) from village aa inner join township bb on aa.township_id=bb.id where bb.unit_id=a.id) as village_num,
					count(distinct b.id) as pa_num,
					count(distinct c.policeaffairs_id) as pa_reply_num

					from unit a
					inner join policeaffairs b
					on a.id = b.unit_id

					left join reply c
					on b.id=c.policeaffairs_id
					where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0
					group by unit_name,village_num`).QueryRows(&unitall)

			i := (clientPage - 1) * everyPage
			for k, v := range unitall {
				v.Ranking = i + k + 1
			}

			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = unitall
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	case "village":
		var villageAll []*VillageAllSort
		num, _ := o.Raw(`select distinct a.id as num

		from village a
		inner join township d
		on a.township_id=d.id

		inner join policeaffairs b
		on a.id = b.village_id

		left join reply c
		on b.id=c.policeaffairs_id
		where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0`).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(`select distinct
					township_name,
					village_name,
					(select police_name from police where id=a.id) as police_name,
					count(distinct b.id) as pa_num,
					count(distinct c.policeaffairs_id) as pa_reply_num

					from village a
					inner join township d
					on a.township_id=d.id

					inner join policeaffairs b
					on a.id = b.village_id

				left join reply c
				on b.id=c.policeaffairs_id
				where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0
				group by village_name,township_name,police_name`).QueryRows(&villageAll)

			i := (clientPage - 1) * everyPage
			for k, v := range villageAll {
				v.Ranking = i + k + 1
			}

			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = villageAll
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	}
	return info
}

//获得所有警员/行政村/单位的报送数据,排名数据统计
func GetAllCountRank(category, year, month string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var maps []orm.Params

	o := orm.NewOrm()

	switch category {
	case "police":
		var policecount []*PoliceAllSort

		sqlnolimit := `select distinct a.id as num

		from police a
		inner join policeaffairs b
		on a.number = b.police_number

		left join reply c
		on b.id=c.policeaffairs_id
		where year(publish_date) = ?`

		sql := `select distinct
				police_number as number,
				police_name,
				count(distinct b.id) as pa_num,
				count(distinct c.policeaffairs_id) as pa_reply_num

				from police a
				inner join policeaffairs b
				on a.number = b.police_number

				left join reply c
				on b.id=c.policeaffairs_id
				where year(publish_date) = ?
				group by number,police_name`

		if month != ""{
			sqlnolimit = strings.Replace(sqlnolimit,"?","? and month(publish_date)="+month,-1)
			sql = strings.Replace(sql,"?","? and month(publish_date)="+month,-1)
		}

		num, _ := o.Raw(sqlnolimit,year).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(sql,year).QueryRows(&policecount)

			i := (clientPage - 1) * everyPage
			for k, v := range policecount {
				v.Ranking = i + k + 1
			}

			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = policecount
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	case "unit":
		var unitall []*UnitAllSort

		sqlnolimit := `select distinct a.id as num

		from unit a
		inner join policeaffairs b
		on a.id = b.unit_id

		left join reply c
		on b.id=c.policeaffairs_id
		where year(publish_date) = ?`

		sql := `select distinct
				    unit_name,
					(select count(distinct aa.id) from village aa inner join township bb on aa.township_id=bb.id where bb.unit_id=a.id) as village_num,
					count(distinct b.id) as pa_num,
					count(distinct c.policeaffairs_id) as pa_reply_num

					from unit a
					inner join policeaffairs b
					on a.id = b.unit_id

					left join reply c
					on b.id=c.policeaffairs_id
					where year(publish_date) = ?
					group by unit_name,village_num`

		num, _ := o.Raw(sqlnolimit,year).Values(&maps)

		if month != ""{
			sqlnolimit = strings.Replace(sqlnolimit,"?","? and month(publish_date)="+month,-1)
			sql = strings.Replace(sql,"?","? and month(publish_date)="+month,-1)
		}

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(sql,year).QueryRows(&unitall)

			i := (clientPage - 1) * everyPage
			for k, v := range unitall {
				v.Ranking = i + k + 1
			}

			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = unitall
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	case "village":
		var villageAll []*VillageAllSort

		sqlnolimit := `select distinct a.id as num

		from village a
		inner join township d
		on a.township_id=d.id

		inner join policeaffairs b
		on a.id = b.village_id

		left join reply c
		on b.id=c.policeaffairs_id
		where year(publish_date) = ?`

		sql := `select distinct
					township_name,
					village_name,
					(select police_name from police where id=a.id) as police_name,
					count(distinct b.id) as pa_num,
					count(distinct c.policeaffairs_id) as pa_reply_num

					from village a
					inner join township d
					on a.township_id=d.id

					inner join policeaffairs b
					on a.id = b.village_id

				left join reply c
				on b.id=c.policeaffairs_id
				where year(publish_date) = ?
				group by village_name,township_name,police_name`

		if month != ""{
			sqlnolimit = strings.Replace(sqlnolimit,"?","? and month(publish_date)="+month,-1)
			sql = strings.Replace(sql,"?","? and month(publish_date)="+month,-1)
		}
		num, _ := o.Raw(sqlnolimit,year).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(sql,year).QueryRows(&villageAll)

			i := (clientPage - 1) * everyPage
			for k, v := range villageAll {
				v.Ranking = i + k + 1
			}

			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = villageAll
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	}
	return info
}