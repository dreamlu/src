package police

import (
	"github.com/astaxie/beego/orm"
	"ycyj/models/lib"
)

/*警务信息量数据统计*/
type Policecount struct {
	Police_number string //警员编号
	Police_name   string //警员姓名
	Unit_name     string //所属乡镇
	Countall
	Count
	Year []*Year //每年报送数据
}

/*单位信息数据统计*/
type Unitcount struct {
	Unit_id     int    //单位id
	Unit_name   string //单位名称
	Village_num string //辖区行政村数量
	Countall
	Count
	Year []*Year //每年报送数据
}

/*行政村数据统计*/
type Villagecount struct {
	Village_id    int
	Township_name string //乡镇名称
	Village_name  string //行政村名称
	Police_name   string //关联警员
	Countall
	Count
	Year []*Year //每年报送数据
}

//月周数据统计通用
type Count struct {
	Pa_month_num       string //本月警务信息量
	Pa_week_num        string //本周警务信息量
	Pa_reply_month_num string //本月批示信息量
	Pa_reply_week_num  string //本周批示信息量
	Ranking            int    //排名
}

/*all数据统计通用*/
type Countall struct {
	Pa_num       string //警务信息量
	Pa_reply_num string //信息批示量
}

/*重构代码v2*/

/*警务信息量数据统计V2*/
type PolicecountV2 struct {
	Police_name        string //警员姓名
	Unit_name          string //所属乡镇
	Pa_month_num       int    //本月警务信息量
	Pa_week_num        int    //本周警务信息量
	Pa_reply_month_num int    //本月信息批示量
	Pa_reply_week_num  int    //本周信息批示量
	Ranking            int    //排名
}

/*单位信息数据统计V2*/
type UnitcountV2 struct {
	Unit_name          string //单位名称
	Village_num        string //辖区行政村数量
	Pa_month_num       int    //本月警务信息量
	Pa_week_num        int    //本周警务信息量
	Pa_reply_month_num int    //本月信息批示量
	Pa_reply_week_num  int    //本周信息批示量
	Ranking            int    //排名
}

/*行政村数据统计*/
type VillagecountV2 struct {
	Township_name      string //乡镇名称
	Village_name       string //行政村名称
	Police_name        string //关联警员
	Pa_month_num       int    //本月警务信息量
	Pa_week_num        int    //本周警务信息量
	Pa_reply_month_num int    //本月信息批示量
	Pa_reply_week_num  int    //本周信息批示量
	Ranking            int    //排名
}

//按行政村统计
func GetNumByVillage(clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo

	var maps []orm.Params
	var villagecount []*VillagecountV2

	o := orm.NewOrm()
	num, _ := o.Raw(`select count(distinct b.id) as num

			from policeaffairs a
			inner join village b
			inner join township c
			on a.village_id=b.id
			and b.township_id=c.id`).Values(&maps)

	if num > 0 {
		//页码
		getinfo.Pager.SumPage = maps[0]["num"].(string)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		o.Raw(`select distinct
				township_name,
				village_name,
				(select police_name from police where id=b.police_id) as police_name,
				(select count(id) from policeaffairs aa where month(aa.publish_date)=month(now())) as pa_month_num,
				(select count(id) from policeaffairs aa where week(aa.publish_date)=week(now())) as pa_week_num,
				(select count(distinct policeaffairs_id) from reply a inner join policeaffairs b on a.policeaffairs_id=b.id where month(publish_date)=month(now()) and policeaffairs_id=b.id) as pa_reply_month_num,

				(select count(distinct policeaffairs_id) from reply a inner join policeaffairs b on a.policeaffairs_id=b.id where week(publish_date)=week(now()) and policeaffairs_id=b.id) as pa_reply_week_num

				from policeaffairs a
				inner join village b
				inner join township c
				on a.village_id=b.id
				and b.township_id=c.id
				order by pa_month_num desc,pa_week_num desc,pa_reply_month_num desc,pa_reply_week_num desc

				limit ?,?`, (clientPage-1)*everyPage, everyPage).QueryRows(&villagecount)

		//sort.Sort(pl(policecount)) //排序
		i := (clientPage - 1) * everyPage
		for k, v := range villagecount {
			v.Ranking = i + k + 1
		}

		//排名统计
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = villagecount
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//按报送单位统计
func GetNumByUnit(clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo

	var maps []orm.Params
	var unitcount []*UnitcountV2

	o := orm.NewOrm()
	num, _ := o.Raw(`select count(distinct b.id) as num

			from unit a
			inner join policeaffairs b
			inner join police c
			on a.id=c.unit_id
			and b.police_number=c.number`).Values(&maps)

	if num > 0 {
		//页码
		getinfo.Pager.SumPage = maps[0]["num"].(string)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		o.Raw(`select distinct
			unit_name,
				(select count(distinct c.id) from unit aa inner join township b inner join village c on aa.id=b.unit_id and b.id=c.township_id where aa.id=a.id) as village_num,
				(select count(id) from policeaffairs aa where month(aa.publish_date)=month(now())) as pa_month_num,
				(select count(id) from policeaffairs aa where week(aa.publish_date)=week(now())) as pa_week_num,
				(select count(distinct policeaffairs_id) from reply a inner join policeaffairs b on a.policeaffairs_id=b.id where month(publish_date)=month(now()) and policeaffairs_id=b.id) as pa_reply_month_num,

				(select count(distinct policeaffairs_id) from reply a inner join policeaffairs b on a.policeaffairs_id=b.id where week(publish_date)=week(now()) and policeaffairs_id=b.id) as pa_reply_week_num

				from unit a
				inner join policeaffairs b
				inner join police c
				on a.id=c.unit_id
				and b.police_number=c.number
				order by pa_month_num desc,pa_week_num desc,pa_reply_month_num desc,pa_reply_week_num desc
				limit ?,?`, (clientPage-1)*everyPage, everyPage).QueryRows(&unitcount)

		//sort.Sort(pl(policecount)) //排序
		i := (clientPage - 1) * everyPage
		for k, v := range unitcount {
			v.Ranking = i + k + 1
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = unitcount
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//按警员姓名统计
func GetNumByPolice(clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo

	var maps []orm.Params
	var policecount []*PolicecountV2

	o := orm.NewOrm()
	num, _ := o.Raw(`select count(distinct b.id) as num

			from unit a
			inner join policeaffairs b
			inner join police c
			on a.id=c.unit_id
			and b.police_number=c.number`).Values(&maps)

	if num > 0 {
		//页码
		getinfo.Pager.SumPage = maps[0]["num"].(string)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		o.Raw(`select distinct
				police_name,
				unit_name,
				(select count(id) from policeaffairs aa where month(aa.publish_date)=month(now()) and police_number=c.number) as pa_month_num,
				(select count(id) from policeaffairs aa where week(aa.publish_date)=week(now()) and police_number=c.number) as pa_week_num,
				(select count(distinct policeaffairs_id) from reply a inner join policeaffairs b on a.policeaffairs_id=b.id where month(publish_date)=month(now()) and policeaffairs_id=b.id) as pa_reply_month_num,

				(select count(distinct policeaffairs_id) from reply a inner join policeaffairs b on a.policeaffairs_id=b.id where week(publish_date)=week(now()) and policeaffairs_id=b.id) as pa_reply_week_num

				from unit a
				inner join policeaffairs b
				inner join police c
				on a.id=c.unit_id
				and b.police_number=c.number
				order by pa_month_num desc,pa_week_num desc,pa_reply_month_num desc,pa_reply_week_num desc
				limit ?,?`, (clientPage-1)*everyPage, everyPage).QueryRows(&policecount)

		//sort.Sort(pl(policecount)) //排序
		i := (clientPage - 1) * everyPage
		for k, v := range policecount {
			v.Ranking = i + k + 1
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = policecount
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

///*结构体排序实现方式*/
//type pl []*Policecount
//
//func (I pl) Len() int {
//	return len(I)
//}
//func (I pl) Less(i, j int) bool {
//	return I[i].Pa_month_num > I[j].Pa_month_num
//}
//func (I pl) Swap(i, j int) {
//	I[i], I[j] = I[j], I[i]
//}
//
///*结构体排序实现方式*/
//type uc []*Unitcount
//
//func (I uc) Len() int {
//	return len(I)
//}
//func (I uc) Less(i, j int) bool {
//	return I[i].Pa_month_num > I[j].Pa_month_num
//}
//func (I uc) Swap(i, j int) {
//	I[i], I[j] = I[j], I[i]
//}
//
///*结构体排序实现方式*/
//type vc []*Villagecount
//
//func (I vc) Len() int {
//	return len(I)
//}
//func (I vc) Less(i, j int) bool {
//	return I[i].Pa_month_num > I[j].Pa_month_num
//}
//func (I vc) Swap(i, j int) {
//	I[i], I[j] = I[j], I[i]
//}
