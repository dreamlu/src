package police

import (
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"ycyj/models/lib"
)

/*年度数据统计*/
type Year struct {
	Year           string
	Month          []*Month
	Year_num       int //年度总报送量
	Year_reply_num int //年度总批示量
}

type Month struct {
	Month string
	Countall
}

/*所有、具体警员模型数据*/
type PoliceAll struct {
	Number      string
	Police_name string
	Countall
}

/*所有、具体单位模型数据*/
type UnitAll struct {
	Unit_name   string
	Village_num string
	Countall
}

/*所有、具体行政村模型数据*/
type VillageAll struct {
	Township_name string
	Village_name  string
	Police_name   string //
	Countall
}

//============================================
//============================================

/*年度警员模型数据*/
type PoliceYear struct {
	Number      string
	Police_name string
	YearData
}

/*年度单位模型数据*/
type UnitYear struct {
	Unit_name   string
	Village_num string //
	YearData
}

/*年度行政村模型数据*/
type VillageYear struct {
	Township_name string
	Village_name  string
	Police_name   string //
	YearData
}

/*年度数据*/
type YearData struct {
	Year       int
	Jan        int
	Jan_p      int
	Feb        int
	Feb_p      int
	Mar        int
	Mar_p      int
	April      int
	April_p    int
	May        int
	May_p      int
	June       int
	June_p     int
	July       int
	July_p     int
	August     int
	August_p   int
	Sep        int
	Sep_p      int
	Oct        int
	Oct_p      int
	Nov        int
	Nov_p      int
	Dece       int
	Dece_p     int
	Year_num   int
	Year_num_p int
}

//获得所有警员/行政村/单位的报送数据,每年每月数据统计
func GetAllCountYear(category string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var maps []orm.Params

	o := orm.NewOrm()

	switch category {
	case "police":
		var policecount []*PoliceYear
		num, _ := o.Raw(`select distinct a.id as num

		from police a
		inner join policeaffairs b
		on a.number = b.police_number

		left join reply c
		on b.id=c.policeaffairs_id
		`).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(`select distinct
				number,
					police_name,
					year(publish_date) year,
					count(distinct b.id,if(month(publish_date)=1, true, null)) as jan,
					count(distinct c.policeaffairs_id , if(month(publish_date)=1, true, null)) as jan_p,
					count(distinct b.id,if(month(publish_date)=2, true, null)) as feb,
					count(distinct c.policeaffairs_id , if(month(publish_date)=2, true, null)) as feb_p,
					count(distinct b.id,if(month(publish_date)=3, true, null)) as mar,
					count(distinct c.policeaffairs_id , if(month(publish_date)=3, true, null)) as mar_p,
					count(distinct b.id,if(month(publish_date)=4, true, null)) as april,
					count(distinct c.policeaffairs_id , if(month(publish_date)=4, true, null)) as april_p,
					count(distinct b.id,if(month(publish_date)=5, true, null)) as may,
					count(distinct c.policeaffairs_id , if(month(publish_date)=5, true, null)) as may_p,
					count(distinct b.id,if(month(publish_date)=6, true, null)) as june,
					count(distinct c.policeaffairs_id , if(month(publish_date)=6, true, null)) as june_p,
					count(distinct b.id,if(month(publish_date)=7, true, null)) as july,
					count(distinct c.policeaffairs_id , if(month(publish_date)=7, true, null)) as july_p,
					count(distinct b.id,if(month(publish_date)=8, true, null)) as august,
					count(distinct c.policeaffairs_id , if(month(publish_date)=8, true, null)) as august_p,

					count(distinct b.id,if(month(publish_date)=9, true, null)) as sep,
					count(distinct c.policeaffairs_id,month(publish_date)=9) as sep_p,

					count(distinct b.id,if(month(publish_date)=10, true, null)) as oct,
					count(distinct c.policeaffairs_id , if(month(publish_date)=10, true, null)) as oct_p,
					count(distinct b.id,if(month(publish_date)=11, true, null)) as nov,
					count(distinct c.policeaffairs_id , if(month(publish_date)=11, true, null)) as nov_p,
					count(distinct b.id,if(month(publish_date)=12, true, null)) as dece,
					count(distinct c.policeaffairs_id , if(month(publish_date)=12, true, null)) as dece_p,
					count(distinct b.id) as year_num,
					count(distinct c.policeaffairs_id) as year_num_p

				from police a
				inner join policeaffairs b
				on a.number=b.police_number

				left join reply c
				on b.id=c.policeaffairs_id
				group by number,police_name,year`).QueryRows(&policecount)
			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = policecount
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	case "unit":
		var unitall []*UnitYear
		num, _ := o.Raw(`select distinct a.id as num

		from unit a
		inner join policeaffairs b
		on a.id = b.unit_id

		left join reply c
		on b.id=c.policeaffairs_id
		`).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(`select distinct
				    unit_name,
					(select count(distinct aa.id) from village aa inner join township bb on aa.township_id=bb.id where bb.unit_id=a.id) as village_num,
					year(publish_date) year,
					count(distinct b.id,if(month(publish_date)=1, true, null)) as jan,
					count(distinct c.policeaffairs_id , if(month(publish_date)=1, true, null)) as jan_p,
					count(distinct b.id,if(month(publish_date)=2, true, null)) as feb,
					count(distinct c.policeaffairs_id , if(month(publish_date)=2, true, null)) as feb_p,
					count(distinct b.id,if(month(publish_date)=3, true, null)) as mar,
					count(distinct c.policeaffairs_id , if(month(publish_date)=3, true, null)) as mar_p,
					count(distinct b.id,if(month(publish_date)=4, true, null)) as april,
					count(distinct c.policeaffairs_id , if(month(publish_date)=4, true, null)) as april_p,
					count(distinct b.id,if(month(publish_date)=5, true, null)) as may,
					count(distinct c.policeaffairs_id , if(month(publish_date)=5, true, null)) as may_p,
					count(distinct b.id,if(month(publish_date)=6, true, null)) as june,
					count(distinct c.policeaffairs_id , if(month(publish_date)=6, true, null)) as june_p,
					count(distinct b.id,if(month(publish_date)=7, true, null)) as july,
					count(distinct c.policeaffairs_id , if(month(publish_date)=7, true, null)) as july_p,
					count(distinct b.id,if(month(publish_date)=8, true, null)) as august,
					count(distinct c.policeaffairs_id , if(month(publish_date)=8, true, null)) as august_p,

					count(distinct b.id,if(month(publish_date)=9, true, null)) as sep,
					count(distinct c.policeaffairs_id,month(publish_date)=9) as sep_p,

					count(distinct b.id,if(month(publish_date)=10, true, null)) as oct,
					count(distinct c.policeaffairs_id , if(month(publish_date)=10, true, null)) as oct_p,
					count(distinct b.id,if(month(publish_date)=11, true, null)) as nov,
					count(distinct c.policeaffairs_id , if(month(publish_date)=11, true, null)) as nov_p,
					count(distinct b.id,if(month(publish_date)=12, true, null)) as dece,
					count(distinct c.policeaffairs_id , if(month(publish_date)=12, true, null)) as dece_p,
					count(distinct b.id) as year_num,
					count(distinct c.policeaffairs_id) as year_num_p

					from unit a
					inner join policeaffairs b
					on a.id = b.unit_id

					left join reply c
					on b.id=c.policeaffairs_id
					group by unit_name,village_num,year`).QueryRows(&unitall)
			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = unitall
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	case "village":
		var villageAll []*VillageYear
		num, _ := o.Raw(`select distinct a.id as num

		from village a
		inner join township d
		on a.township_id=d.id

		inner join policeaffairs b
		on a.id = b.village_id

		left join reply c
		on b.id=c.policeaffairs_id
		`).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(`select distinct
					township_name,
					village_name,
					(select police_name from police where id=a.id) as police_name,
					year(publish_date) year,
					count(distinct b.id,if(month(publish_date)=1, true, null)) as jan,
					count(distinct c.policeaffairs_id , if(month(publish_date)=1, true, null)) as jan_p,
					count(distinct b.id,if(month(publish_date)=2, true, null)) as feb,
					count(distinct c.policeaffairs_id , if(month(publish_date)=2, true, null)) as feb_p,
					count(distinct b.id,if(month(publish_date)=3, true, null)) as mar,
					count(distinct c.policeaffairs_id , if(month(publish_date)=3, true, null)) as mar_p,
					count(distinct b.id,if(month(publish_date)=4, true, null)) as april,
					count(distinct c.policeaffairs_id , if(month(publish_date)=4, true, null)) as april_p,
					count(distinct b.id,if(month(publish_date)=5, true, null)) as may,
					count(distinct c.policeaffairs_id , if(month(publish_date)=5, true, null)) as may_p,
					count(distinct b.id,if(month(publish_date)=6, true, null)) as june,
					count(distinct c.policeaffairs_id , if(month(publish_date)=6, true, null)) as june_p,
					count(distinct b.id,if(month(publish_date)=7, true, null)) as july,
					count(distinct c.policeaffairs_id , if(month(publish_date)=7, true, null)) as july_p,
					count(distinct b.id,if(month(publish_date)=8, true, null)) as august,
					count(distinct c.policeaffairs_id , if(month(publish_date)=8, true, null)) as august_p,

					count(distinct b.id,if(month(publish_date)=9, true, null)) as sep,
					count(distinct c.policeaffairs_id,month(publish_date)=9) as sep_p,

					count(distinct b.id,if(month(publish_date)=10, true, null)) as oct,
					count(distinct c.policeaffairs_id , if(month(publish_date)=10, true, null)) as oct_p,
					count(distinct b.id,if(month(publish_date)=11, true, null)) as nov,
					count(distinct c.policeaffairs_id , if(month(publish_date)=11, true, null)) as nov_p,
					count(distinct b.id,if(month(publish_date)=12, true, null)) as dece,
					count(distinct c.policeaffairs_id , if(month(publish_date)=12, true, null)) as dece_p,
					count(distinct b.id) as year_num,
					count(distinct c.policeaffairs_id) as year_num_p

					from village a
					inner join township d
					on a.township_id=d.id

					inner join policeaffairs b
					on a.id = b.village_id

				left join reply c
				on b.id=c.policeaffairs_id
				group by village_name,township_name,police_name,year`).QueryRows(&villageAll)
			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = villageAll
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	}
	return info
}

//根据具体id或编号获得警员/行政村/单位的报送数据
func GetDetailCount(category, data, time1, time2 string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var maps []orm.Params

	o := orm.NewOrm()

	switch category {
	case "police":
		numbers := strings.Split(data, "-")
		sql := ""
		for _, v := range numbers {
			sql += "a.number=" + v + " or "
		}
		if len(numbers) > 0 {
			sql = string([]byte(sql)[:len(sql)-3])
		}
		var policecount []*PoliceAll
		num, _ := o.Raw(`select distinct a.id

		from police a
		inner join policeaffairs b
		on a.number = b.police_number

		left join reply c
		on b.id=c.policeaffairs_id
		where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0
		and ` + sql).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(`select distinct
				police_number as number,
				police_name,
				count(distinct b.id) as pa_num,
				count(distinct c.policeaffairs_id) as pa_reply_num

				from police a
				inner join policeaffairs b
				on a.number = b.police_number

				left join reply c
				on b.id=c.policeaffairs_id
				where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0
				and ` + sql + `
				group by number,police_name`).QueryRows(&policecount)
			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = policecount
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	case "unit":
		var unitall []*UnitAll
		num, _ := o.Raw(`select distinct a.id as num

		from unit a
		inner join policeaffairs b
		on a.id = b.unit_id

		left join reply c
		on b.id=c.policeaffairs_id
		where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0
		and a.id=?`).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(`select distinct
				    unit_name,
					(select count(distinct aa.id) from village aa inner join township bb on aa.township_id=bb.id where bb.unit_id=a.id) as village_num,
					count(distinct b.id) as pa_num,
					count(distinct c.policeaffairs_id) as pa_reply_num

					from unit a
					inner join policeaffairs b
					on a.id = b.unit_id

					left join reply c
					on b.id=c.policeaffairs_id
					where timestampdiff(day,'`+time1+`',publish_date) >= 0 and timestampdiff(day,'`+time2+` ',publish_date) <= 0
					and a.id=?
					group by unit_name,village_num`, data).QueryRows(&unitall)
			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = unitall
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	case "village":
		var villageAll []*VillageAll
		num, _ := o.Raw(`select distinct a.id as num

		from village a
		inner join township d
		on a.township_id=d.id

		inner join policeaffairs b
		on a.id = b.village_id

		left join reply c
		on b.id=c.policeaffairs_id
		where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0
		and a.id=?`).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(`select distinct
					township_name,
					village_name,
					(select police_name from police where id=a.id) as police_name,
					count(distinct b.id) as pa_num,
					count(distinct c.policeaffairs_id) as pa_reply_num

					from village a
					inner join township d
					on a.township_id=d.id

					inner join policeaffairs b
					on a.id = b.village_id

				left join reply c
				on b.id=c.policeaffairs_id
				where timestampdiff(day,'`+time1+`',publish_date) >= 0 and timestampdiff(day,'`+time2+` ',publish_date) <= 0
				and a.id=?
				group by village_name,township_name,police_name`, data).QueryRows(&villageAll)
			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = villageAll
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	}
	return info
}

//获得所有警员/行政村/单位的报送数据
func GetAllCount(category, time1, time2 string, clientPage, everyPage int) interface{} {
	var info interface{}
	var getinfo lib.GetInfo
	var maps []orm.Params

	o := orm.NewOrm()

	switch category {
	case "police":
		var policecount []*PoliceAll
		num, _ := o.Raw(`select distinct a.id as num

		from police a
		inner join policeaffairs b
		on a.number = b.police_number

		left join reply c
		on b.id=c.policeaffairs_id
		where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0`).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(`select distinct
				police_number as number,
				police_name,
				count(distinct b.id) as pa_num,
				count(distinct c.policeaffairs_id) as pa_reply_num

				from police a
				inner join policeaffairs b
				on a.number = b.police_number

				left join reply c
				on b.id=c.policeaffairs_id
				where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0
				group by number,police_name`).QueryRows(&policecount)
			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = policecount
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	case "unit":
		var unitall []*UnitAll
		num, _ := o.Raw(`select distinct a.id as num

		from unit a
		inner join policeaffairs b
		on a.id = b.unit_id

		left join reply c
		on b.id=c.policeaffairs_id
		where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0`).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(`select distinct
				    unit_name,
					(select count(distinct aa.id) from village aa inner join township bb on aa.township_id=bb.id where bb.unit_id=a.id) as village_num,
					count(distinct b.id) as pa_num,
					count(distinct c.policeaffairs_id) as pa_reply_num

					from unit a
					inner join policeaffairs b
					on a.id = b.unit_id

					left join reply c
					on b.id=c.policeaffairs_id
					where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0
					group by unit_name,village_num`).QueryRows(&unitall)
			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = unitall
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	case "village":
		var villageAll []*VillageAll
		num, _ := o.Raw(`select distinct a.id as num

		from village a
		inner join township d
		on a.township_id=d.id

		inner join policeaffairs b
		on a.id = b.village_id

		left join reply c
		on b.id=c.policeaffairs_id
		where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0`).Values(&maps)

		if num > 0 {
			//总数量
			getinfo.Pager.SumPage = strconv.FormatInt(num, 10)
			//分页
			o.Raw(`select distinct
					township_name,
					village_name,
					(select police_name from police where id=a.id) as police_name,
					count(distinct b.id) as pa_num,
					count(distinct c.policeaffairs_id) as pa_reply_num

					from village a
					inner join township d
					on a.township_id=d.id

					inner join policeaffairs b
					on a.id = b.village_id

				left join reply c
				on b.id=c.policeaffairs_id
				where timestampdiff(day,'` + time1 + `',publish_date) >= 0 and timestampdiff(day,'` + time2 + ` ',publish_date) <= 0
				group by village_name,township_name,police_name`).QueryRows(&villageAll)
			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = villageAll
			getinfo.Pager.EveryPage = everyPage
			getinfo.Pager.ClientPage = clientPage
			info = getinfo
		} else {
			info = lib.MapNoData
		}
	}
	return info
}
