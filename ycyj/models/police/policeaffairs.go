package police

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"ycyj/models/lib"
)

/*警务内容*/
type Policeaffairs struct {
	Id                  int
	Title               string //title
	Content             string //内容
	Unit_id             int
	Unit_name           string   //接收单位名称
	Township_id         int      //乡镇id
	Village_id          int      //
	Village_name        string   //村名
	Related_police_id   string   //关联警员id
	Related_police_name string   //关联警员
	Police_number       string   //报送人警员编号
	Police_name         string   //报送人警员姓名
	Police_unit_name    string   //报送警员编号
	Issend              int      //默认0,存为草稿,1提交
	File                string   //下载的附件地址,多个用&符号分割
	Isread              string   //已读/未,默认未读
	Publish_date        string   //发布时间
	Reply               []*Reply //回复或批示
}

//首页基础警务数据
type BasicData struct {
	TodaySendNum      int    //今日报送量
	MonthSendNum      int    //本月报送量
	MaxPolice         string //最大报送量警员
	MaxPoliceSendNum  int    //最大报送量警员对应数量
	MaxTownship      string //最大信息量乡镇
	MaxVillage        string //最大信息量行政村
	MaxVillageSendNum int    //最大信息量行政村对应数量
}

//首页基础数据显示
func GetBasicData() interface{} {
	var info interface{}
	var getinfo lib.GetInfoN
	o := orm.NewOrm()

	sql := `select 
		(select count(*) from policeaffairs where issend=1 and date_format(publish_date,'%Y-%m-%d')= date_format(now(),'%Y-%m-%d')) as today_send_num,
		(select count(*) from policeaffairs where issend=1 and date_format(publish_date,'%Y-%m')= date_format(now(),'%Y-%m')) as month_send_num,
		# 统计本月最大报送量对应警员
		(select police_name from (select police_name,count(*) as num from policeaffairs a inner join police b on a.police_number=b.number where issend=1 and date_format(publish_date,'%Y-%m')=date_format(now(),'%Y-%m') group by b.number,police_name) as a group by police_name limit 0,1) as max_police,
		# 统计本月最大报送量对应警员的报送数量
		(select max(num) from (select count(*) as num from policeaffairs a inner join police b on a.police_number=b.number where issend=1 and date_format(publish_date,'%Y-%m')=date_format(now(),'%Y-%m') group by b.number) as a) as max_police_send_num,
		# 统计本月最大报送量对应行政村
		(select township_name from (select township_name,count(*) as num from policeaffairs a inner join police b on a.police_number=b.number inner join village c on b.village_id=c.id inner join township d on c.township_id=d.id where issend=1 and date_format(publish_date,'%Y-%m')=date_format(now(),'%Y-%m') group by c.id,township_name) as a group by township_name limit 0,1) as max_township,
		(select village_name from (select township_name,village_name,count(*) as num from policeaffairs a inner join police b on a.police_number=b.number inner join village c on b.village_id=c.id inner join township d on c.township_id=d.id where issend=1 and date_format(publish_date,'%Y-%m')=date_format(now(),'%Y-%m') group by c.id,village_name) as a group by village_name limit 0,1) as max_village,
		# 统计本月最大报送量对应行政村的报送数量
		(select max(num) from (select count(*) as num from policeaffairs a inner join police b on a.police_number=b.number inner join village c on b.village_id=c.id where issend=1 and date_format(publish_date,'%Y-%m')=date_format(now(),'%Y-%m') group by c.id) as a) as max_village_sendNum
		`

	var bd BasicData
	err := o.Raw(sql).QueryRow(&bd)
	if err == nil {
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = bd //数据
		info = getinfo
	}else {
		info = lib.GetMapDataError(222,err.Error())
	}

	return info
}

//根据查询条件获得警务,分页,类型(草稿/已发送)
func GetPoliceaffairsBySearch(user_id string, args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps2 []orm.Params
	num3, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)

	sql := "select a.*,b.police_name,c.unit_name,d.village_name,d.police_id as related_police_id,e.police_name as related_police_name,f.unit_name as police_unit_name " +
		"from `policeaffairs` a inner join `police` b on a.police_number=b.number " +
		"left join `unit` c on a.unit_id=c.id " +
		"left join `village` d on a.village_id=d.id " +
		"left join `police` e on d.police_id=e.id " +
		"left join `unit` f on b.unit_id=f.id " +
		"where 1=1 and "

	//角色id判断
	var admin_id string //单独定义,防止多角色问题
	if err == nil && num3 > 0 {

		for _, v := range maps2 {
			if v["role_id"] == "1" { //非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}

	}

	//页码,每页数量
	clientPageStr := beego.AppConfig.String("clientPage") //默认第1页
	everyPageStr := beego.AppConfig.String("everyPage")   //默认10页
	var flag int

	for k, v := range args {
		if k == "time1" { //时间范围
			sql += "timestampdiff(day,'" + v[0] + "',publish_date) >= 0 and "
			continue
		}
		if k == "time2" {
			sql += "timestampdiff(day,'" + v[0] + "',publish_date) <= 0 and "
			continue
		}
		if k == "clientPage" { //页码
			clientPageStr = v[0]
			continue
		}
		if k == "everyPage" { //每页数量
			everyPageStr = v[0]
			continue
		}
		if k == "publish_num" { //轮播/新闻识别,轮播>0
			sql += "publish_num > 0 and "
			continue
		}
		if k == "flag" {
			flag = 1
			continue
		}
		if v[0] == "" { //条件为空,舍弃
			continue
		}
		sql += k + " like '%" + v[0] + "%' and "
	}

	if admin_id != "1" {
		switch flag {
		case 0: //默认作为报送单位
			sql = strings.Replace(sql, "1=1", "1=1 and b.unit_id="+maps2[0]["unit_id"].(string), -1)
		case 1: //作为接收单位
			sql = strings.Replace(sql, "1=1", "1=1 and a.unit_id="+maps2[0]["unit_id"].(string), -1)
		}
	}

	clientPage, _ := strconv.Atoi(clientPageStr)
	everyPage, _ := strconv.Atoi(everyPageStr)

	c := []byte(sql)
	sql = string(c[:len(c)-4]) //去and
	sqlnolimit := sql

	sql += " limit " + strconv.Itoa((clientPage-1)*everyPage) + "," + everyPageStr
	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var policeaffairs []*Policeaffairs
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&policeaffairs)
		//统计页码等状态
		for i := 0; i < len(policeaffairs); i++ {
			pa := policeaffairs[i]
			//对应的批示
			ry := GetReplyByPoliceaffairsid(pa.Id)
			for j := 0; j < len(ry); j++ {
				pa.Reply = append(pa.Reply, ry[j])
			}
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = policeaffairs //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//根据id获得警务
func GetPoliceaffairsById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	var policeaffairs []*Policeaffairs
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `policeaffairs` where id=?", id).QueryRows(&policeaffairs)

	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		for i := 0; i < len(policeaffairs); i++ {
			pa := policeaffairs[i]
			//单位名或村名
			num, _ := o.Raw("SELECT unit_name FROM `unit` where id=?", pa.Unit_id).Values(&maps)
			if num > 0 {
				pa.Unit_name = maps[0]["unit_name"].(string)
			}
			num, _ = o.Raw("SELECT village_name FROM `village` a inner join `policeaffairs` b on a.id=b.village_id where a.id=?", pa.Village_id).Values(&maps)
			if num > 0 {
				pa.Village_name = maps[0]["village_name"].(string)
			}

			num, _ = o.Raw("SELECT police_id,police_name FROM `village` a inner join `police` b on a.police_id=b.id where a.id=?", pa.Village_id).Values(&maps)
			if num > 0 {
				pa.Related_police_id = maps[0]["police_id"].(string)
				pa.Related_police_name = maps[0]["police_name"].(string)
			}
			num, _ = o.Raw("select a.id from `township` a inner join `village` b on a.id=b.township_id where b.id=?", pa.Village_id).Values(&maps)
			if num > 0 {
				pa.Township_id, _ = strconv.Atoi(maps[0]["id"].(string))
			}
			num, _ = o.Raw("SELECT police_name FROM `police` where number=?", pa.Police_number).Values(&maps)
			if num > 0 {
				pa.Police_name = maps[0]["police_name"].(string)
				num, _ := o.Raw("SELECT unit_name FROM `police` a inner join `unit` b on a.unit_id=b.id where a.number=?", pa.Police_number).Values(&maps)
				if num > 0 {
					pa.Police_unit_name = maps[0]["unit_name"].(string)
				}
			}

			//对应的批示
			ry := GetReplyByPoliceaffairsid(pa.Id)
			for j := 0; j < len(ry); j++ {
				pa.Reply = append(pa.Reply, ry[j])
			}
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = policeaffairs //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得警务,分页,类型(草稿/已发送),默认只看自己单位的警务信息,这么多if else..不优化～开心～你能na我怎么办~
func GetPoliceaffairsByPage(user_id string, issend, clientPage, everyPage, flag int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit := "SELECT count(a.id) as num FROM `policeaffairs` a inner join `police` b on a.police_number=b.number where issend=? "
	sql := "select a.*,b.police_name,c.unit_name,d.village_name,d.police_id as related_police_id,e.police_name as related_police_name,f.unit_name as police_unit_name " +
		"from `policeaffairs` a inner join `police` b on a.police_number=b.number " +
		"left join `unit` c on a.unit_id=c.id " +
		"left join `village` d on a.village_id=d.id " +
		"left join `police` e on d.police_id=e.id " +
		"left join `unit` f on b.unit_id=f.id " +
		"where issend=? order by id desc limit ?,?"

	//先查询对应信息
	var maps2 []orm.Params
	num2, err := o.Raw("select role_id,unit_id from userlist,`user` b where user_id=? and b.id=?", user_id, user_id).Values(&maps2)
	if err == nil && num2 > 0 {

		//角色id判断
		var admin_id string //单独定义,防止多角色问题
		for _, v := range maps2 {
			if v["role_id"] == "1" { //非管理员仅查看各自账号单位的数据,简单点
				admin_id = "1"
				break
			}
		}

		if admin_id != "1" {
			switch flag {
			case 0: //默认作为报送单位
				sql = strings.Replace(sql, "issend=?", "issend=? and b.unit_id="+maps2[0]["unit_id"].(string), -1)
				sqlnolimit = strings.Replace(sqlnolimit, "issend=?", "issend=? and b.unit_id="+maps2[0]["unit_id"].(string), -1)
			case 1: //作为接收单位
				sql = strings.Replace(sql, "issend=?", "issend=? and a.unit_id="+maps2[0]["unit_id"].(string), -1)
				sqlnolimit = strings.Replace(sqlnolimit, "issend=?", "issend=? and a.unit_id="+maps2[0]["unit_id"].(string), -1)
			}
		}

		var maps []orm.Params
		//var maps []orm.Params
		num2, err = o.Raw(sqlnolimit, issend).Values(&maps)
		//有数据是返回相应信息
		if err == nil && num2 > 0 {
			var policeaffairs []*Policeaffairs
			var getinfo lib.GetInfo
			o.Raw(sql, issend, (clientPage-1)*everyPage, everyPage).QueryRows(&policeaffairs)
			//统计页码等状态
			var SumPage = "0"
			SumPage = maps[0]["num"].(string)
			for i := 0; i < len(policeaffairs); i++ {
				pa := policeaffairs[i]
				//对应的批示
				ry := GetReplyByPoliceaffairsid(pa.Id)
				for j := 0; j < len(ry); j++ {
					pa.Reply = append(pa.Reply, ry[j])
				}
			}

			getinfo.Status = 200
			getinfo.Msg = "请求成功"
			getinfo.Data = policeaffairs //数据
			getinfo.Pager.SumPage = SumPage
			getinfo.Pager.ClientPage = clientPage
			getinfo.Pager.EveryPage = everyPage

			info = getinfo
		} else {
			info = lib.MapError
		}
	} else {
		info = lib.MapError
	}

	return info
}

//删除警务,根据id
func DeletePoliceaffairsByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `policeaffairs` where id=?", id).Exec()
	//res,err := o.Raw("delete from `news` where id=?",id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改警务
func UpdatePoliceaffairs(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("policeaffairs", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建警务
func CreatePoliceaffairs(args map[string][]string) interface{} {

	//if _,ok := args["publish_date"]

	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("policeaffairs", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
