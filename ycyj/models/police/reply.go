package police

import (
	"github.com/astaxie/beego/orm"
	"ycyj/models/lib"
)

/*回复/批示*/
type Reply struct {
	Id      int
	Name    string //名字
	Content string //批示或回复内容
	//Policeaffairs_id	string	//批示或回复i
}

//根据警务信息id获得对应的回复或批示
func GetReplyByPoliceaffairsid(policeaffairs_id int) []*Reply {
	o := orm.NewOrm()
	var reply []*Reply
	o.Raw("select *from `reply` where policeaffairs_id=? order by id desc", policeaffairs_id).QueryRows(&reply)

	return reply
}

//创建回复或批示
func CreateReply(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("reply", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
