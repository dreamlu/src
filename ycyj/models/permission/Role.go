package permission

import (
	"github.com/astaxie/beego/orm"
	"github.com/casbin/casbin"
	"github.com/casbin/casbin/persist/file-adapter"
	"strings"
	"ycyj/models/lib"
)

//角色
type Role struct {
	Id        int
	Role_name string
}

//角色、相关权限以及角色成员
type RoleInfo struct {
	Role
	Menu     Menu     //角色相关权限
	Username []string //角色成员
}

//获得角色
func GetRoles() interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var role []Role
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `role`").QueryRows(&role)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = role //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//删除角色,根据id
func DeleteRoleByid(id int) interface{} {

	var roleids = []int{1, 9, 12, 13, 14, 15, 16, 17}
	for _, v := range roleids {
		if v == id {
			return lib.GetMapDataError(222, "禁止删除基本权限")
		}
	}
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	e := o.Begin()
	res, err := o.Raw("delete from `role` where id=?", id).Exec()
	//res,err := o.Raw("delete from `news` where id=?",id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.GetMapDataError(222, err.Error())
	} else {
		//删除角色对应的权限
		o.Raw("delete from `rolelist` where role_id=?", id).Exec()
		//删除用户对应的角色权限
		o.Raw("delete from `userlist` where role_id=?", id).Exec()
		info = lib.MapDelete
	}
	if e != nil {
		err = o.Rollback()
	} else {
		err = o.Commit()
	}
	return info
}

//修改角色
func UpdateRole(id, role_name, menu_ids string) interface{} {

	switch {
	case menu_ids == "":
		return lib.GetMapDataError(271, "权限不能为空")
	case id == "1":
		return lib.GetMapDataError(271, "超级管理员禁止修改权限")
	}

	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	o.Using("default")
	err2 := o.Begin()

	o.Raw("update `role` set role_name=? where id=?", role_name, id).Exec()
	//删除对应菜单权限
	o.Raw("delete from `rolelist` where role_id=?", id).Exec()

	//创建对应策略
	a := fileadapter.NewAdapter("conf/authz_policy.csv")
	e := casbin.NewEnforcer("conf/authz_model.conf", a)
	//删除对应角色的策略
	e.DeleteRolesForUser("role_" + id)
	e.SavePolicy()

	rds := strings.Split(menu_ids, "&")
	for _, v := range rds {
		res, err := o.Raw("insert `rolelist`(role_id,menu_id) value(?,?)", id, v).Exec()
		if err == nil {
			num, _ = res.RowsAffected()
		}

		//删除后重新添加策略
		e.AddGroupingPolicy("role_"+id, v)
		e.SavePolicy()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	if err2 != nil {
		err2 = o.Rollback()
	} else {
		err2 = o.Commit()
	}
	return info
}

//创建角色以及相关菜单权限
func CreateRole(role_name, menu_ids string) interface{} {

	if menu_ids == "" {
		return lib.GetMapDataError(271, "权限不能为空")
	}

	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	o.Using("default")
	err2 := o.Begin()
	res, err := o.Raw("insert `role`(role_name) value(?)", role_name).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		//创建对应策略
		a := fileadapter.NewAdapter("conf/authz_policy.csv")
		e := casbin.NewEnforcer("conf/authz_model.conf", a)

		var maps []orm.Params
		num, err := o.Raw("select max(id) as id from `role`").Values(&maps)
		if num > 0 && err == nil {
			id := maps[0]["id"].(string)
			rds := strings.Split(menu_ids, "&")
			for _, v := range rds {
				o.Raw("insert `rolelist`(role_id,menu_id) value(?,?)", id, v).Exec()

				//添加策略
				e.AddGroupingPolicy("role_"+id, v)
				e.SavePolicy()
			}
		}
		info = lib.MapCreate
	}
	if err2 != nil {
		err2 = o.Rollback()
	} else {
		err2 = o.Commit()
	}
	return info
}
