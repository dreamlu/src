package permission

import (
	"github.com/astaxie/beego/orm"
	"strconv"
	"ycyj/models/lib"
)

/*角色对应菜单权限*/
type Rolelist struct {
	Id        int
	Role_id   int
	Menu_id   int `json:"-"`
	Menu      []*Menu //一个角色对应多个菜单
}

//根据id获得相应的角色对应菜单id,&连接
func GetMenuByRoleId(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var menu_ids []string
	var rolelist []Rolelist
	var getinfo lib.GetInfoN
	num2 ,err := o.Raw("select *from `rolelist` where role_id=?", id).QueryRows(&rolelist)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		for _,v := range rolelist{
			menu_ids = append(menu_ids,strconv.Itoa(v.Menu_id))
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = menu_ids //数据
		info = getinfo
	}else if err == nil && num2 == 0{
		info = lib.MapNoData
	}else {
		info = lib.MapError
	}
	return info
}