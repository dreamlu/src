package permission

import (
	"github.com/astaxie/beego/orm"
	"ycyj/models/lib"
)

/*功能菜单*/
type Menu struct {
	Id        int    `json:"key"`
	Parent_id int 	 `json:"parent_id"`
	Menu_name string `json:"title"`    //菜单
	Children  []Menu `json:"children"` //下一级菜单
}

//菜单递归查询
func GetChildrenMenuByParentId(parent_id int) []Menu {
	o := orm.NewOrm()
	var menus []Menu
	num, _ := o.Raw("select *from `menu` where parent_id=?", parent_id).QueryRows(&menus)
	if num > 0 {
		for k, v := range menus {
			menusv := GetChildrenMenuByParentId(v.Id)
			for _, v2 := range menusv {
				menus[k].Children = append(menus[k].Children, v2)
			}
		}
	}
	return menus
}

//菜单递归查询，权限
func GetChildrenMenuByParentIdP(user_id, parent_id int) []Menu {
	o := orm.NewOrm()
	var menus []Menu
	num, _ := o.Raw(`select a.* from menu a
			inner join rolelist b
			on a.id=b.menu_id
			inner join userlist c
			on b.role_id=c.role_id
			where user_id = ? and parent_id = ?`, user_id, parent_id).QueryRows(&menus)
	if num > 0 {
		for k, v := range menus {
			menusv := GetChildrenMenuByParentIdP(user_id, v.Id)
			for _, v2 := range menusv {
				menus[k].Children = append(menus[k].Children, v2)
			}
		}
	}
	return menus
}

//根据id获得相应的菜单按钮
func GetMenuById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var menus []Menu
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `menu` where id=?", id).QueryRows(&menus)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {

		for k, v := range menus {
			menusv := GetChildrenMenuByParentId(v.Id)
			for _, v2 := range menusv {
				menus[k].Children = append(menus[k].Children, v2)
			}
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = menus //数据
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得所有菜单按钮,分页
func GetAllMenu() interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var menus []Menu
	var getinfo lib.GetInfoN
	num2, err := o.Raw("select *from `menu` where parent_id is null").QueryRows(&menus)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {

		for k, v := range menus {
			menusv := GetChildrenMenuByParentId(v.Id)
			for _, v2 := range menusv {
				menus[k].Children = append(menus[k].Children, v2)
			}
			//v.Children = append(v.Children,GetChildrenMenuByParentId(v.Id))
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = menus //数据
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得菜单按钮,分页,权限
func GetMenuByPage(user_id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")
	var getinfo lib.GetInfoN
	var menus []Menu

	num2, err := o.Raw(`select a.* from menu a
			inner join rolelist b
			on a.id=b.menu_id
			inner join userlist c
			on b.role_id=c.role_id
			where user_id = ? and parent_id is null`, user_id).QueryRows(&menus)

	//有数据是返回相应信息
	if err == nil && num2 > 0 {

		for k, v := range menus {
			menusv := GetChildrenMenuByParentIdP(user_id, v.Id)
			for _, v2 := range menusv {
				menus[k].Children = append(menus[k].Children, v2)
			}
			//v.Children = append(v.Children,GetChildrenMenuByParentId(v.Id))
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = menus //数据
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//删除菜单按钮,根据id
func DeleteMenuByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw("delete from `menu` where id=?", id).Exec()
	//res,err := o.Raw("delete from `news` where id=?",id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改菜单按钮
func UpdateMenu(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql, _ := lib.GetUpdateSqlById("menu", args)

	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建菜单按钮
func CreateMenu(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("menu", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
