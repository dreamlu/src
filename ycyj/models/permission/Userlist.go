package permission

import (
	"github.com/astaxie/beego/orm"
)

/*用户角色权限*/
type Userlist struct {
	Id        int
	User_id   int
	Role_id   int
	Role_name string
}

//根据用户id获取所有用户对应角色权限
func GetRoleListByUserId(user_id int) []*Userlist {

	o := orm.NewOrm()
	o.Using("default")
	var userlist []*Userlist
	o.Raw("select a.id as id,user_id,role_id,role_name from `userlist` a inner join `role` b on a.role_id=b.id where user_id=?",user_id).QueryRows(&userlist)

	return userlist
}