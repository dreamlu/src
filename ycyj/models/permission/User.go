package permission

import (
	"github.com/astaxie/beego/orm"
	"strings"
	"ycyj/models/lib"
	"ycyj/util"
)

/*用户表*/
type User struct {
	Id            int
	User_name     string
	User_password string
	Unit_id		  int
	Unit_name	  string
	Userlist      []*Userlist
}

//根据用户id获得对应的所有角色id
func GetRoleIds(id int) string{
	o := orm.NewOrm()
	var maps []orm.Params
	o.Raw("select role_id from `userlist` where user_id=?",id).Values(&maps)
	roleids := ""
	for _,v := range maps{
		roleids += v["role_id"].(string) + "&"
	}
	return string([]byte(roleids)[:len(roleids)-1])
}

//根据用户id获得下面不重复权限菜单id和对应路由
// 格式id&api,id&api(舍弃),新格式,分割
func GetMenuRouters(id int) string{
	o := orm.NewOrm()
	var maps []orm.Params
	o.Raw("select distinct a.id,api from `menu` a inner join `rolelist` b inner join `userlist` c on a.id=b.menu_id and b.role_id=c.role_id where c.user_id=?",id).Values(&maps)
	per := ""
	for _,v := range maps{
		per += v["api"].(string) + ","//v["id"].(string) + "&" + v["api"].(string) + ","
	}
	return string([]byte(per)[:len(per)-1])
}

//所有用户对应角色权限
func GetUsers() interface{} {
	var info interface{}
	var getinfo lib.GetInfoN

	o := orm.NewOrm()
	o.Using("default")
	var users []*User
	_,err := o.Raw("select *from `user` ").QueryRows(&users)
	//res,err := o.Raw("delete from `news` where id=?",id).Exec()
	if err == nil {
		for _,v := range users{
			ulist := GetRoleListByUserId(v.Id)
			for _, v2 := range ulist {
				v.Userlist = append(v.Userlist, v2)
			}
			o.Raw("select unit_name from `unit` where id=?",v.Unit_id).QueryRow(&v.Unit_name)
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = users //数据
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//根据用户id获取所有用户对应角色权限
func GetUserlist(id int) interface{} {
	var info interface{}
	var getinfo lib.GetInfoN

	o := orm.NewOrm()
	o.Using("default")
	var user User
	err := o.Raw("select *from `user` where id=?", id).QueryRow(&user)
	//res,err := o.Raw("delete from `news` where id=?",id).Exec()
	if err == nil {
		ulist := GetRoleListByUserId(user.Id)
		for _, v2 := range ulist {
			user.Userlist = append(user.Userlist, v2)
		}
		o.Raw("select unit_name from `unit` where id=?",user.Unit_id).QueryRow(&user.Unit_name)
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = user //数据
		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//删除用户,根据id
func DeleteUserByid(id int) interface{} {

	if id == 1 {
		return lib.GetMapDataError(222,"禁止删除根管理员")
	}
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	o.Using("default")
	err2 := o.Begin()
	//删除对应权限
	o.Raw("delete from `userlist` where user_id=?", id).Exec()
	//删除用户
	res, err := o.Raw("delete from `user` where id=?", id).Exec()
	//res,err := o.Raw("delete from `news` where id=?",id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.GetMapDataError(222,err.Error())
	} else {
		info = lib.MapDelete
	}
	if err2 != nil {
		err2 = o.Rollback()
	} else {
		err2 = o.Commit()
	}
	return info
}

//修改用户
func UpdateUser(id, user_name, user_password,role_ids,unit_id string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	o.Using("default")
	err2 := o.Begin()

	//密码为空,保持原密码不变
	switch user_password {
	case "":
		o.Raw("update `user` set user_name=?,unit_id=? where id=?", user_name, unit_id, id).Exec()
	default:
		user_password = util.AesEn(user_password)
		o.Raw("update `user` set user_name=?,user_password=?,unit_id=? where id=?", user_name, user_password, unit_id, id).Exec()
	}
	//删除对应角色
	o.Raw("delete from `userlist` where user_id=?", id).Exec()

	rds := strings.Split(role_ids, "&")
	for _, v := range rds {
		res, err := o.Raw("insert `userlist`(user_id,role_id) value(?,?)", id, v).Exec()
		if err == nil {
			num, _ = res.RowsAffected()
		}
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	if err2 != nil {
		err2 = o.Rollback()
	} else {
		err2 = o.Commit()
	}
	return info
}

//创建用户
func CreateUser(user_name, user_password, role_ids, unit_id string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	user_password = util.AesEn(user_password)

	o := orm.NewOrm()
	o.Using("default")
	err2 := o.Begin()
	res, err := o.Raw("insert `user`(user_name,user_password, unit_id) value(?,?,?)", user_name, user_password,unit_id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		var maps []orm.Params
		num, err := o.Raw("select max(id) as id from `user`").Values(&maps)
		if num > 0 && err == nil {
			id := maps[0]["id"].(string)
			rds := strings.Split(role_ids, "&")
			for _, v := range rds {
				o.Raw("insert `userlist`(user_id,role_id) value(?,?)", id, v).Exec()
			}
		}
		info = lib.MapCreate
	}
	if err2 != nil {
		err2 = o.Rollback()
	} else {
		err2 = o.Commit()
	}
	return info
}
