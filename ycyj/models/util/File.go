package util

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	"github.com/tealeg/xlsx"
	"strings"
	"ycyj/models/lib"
)

/*文件上传管理
type File struct {
	Id           int
	File_address string
	Describe     string
}*/

//获得刚上传的文件路径,富文本
func GetFilePath(oldname,path string) interface{} {
	var info interface{}
	//上传成功
	if strings.Index(path, ".") > 0 {
		info = map[string]string{"Status": "201", "Msg": "创建成功", "default": "/" + path,"oldname":oldname}
	} else {
		info = lib.MapError
	}
	return info
}

/*通用批量新增*/
func CreateSomething(table, path string) interface{} {
	var info interface{}
	o := orm.NewOrm()

	sql := "insert `" + table + "`("

	excelFileName := path
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		fmt.Printf("通用批量新增文件打开失败: %s\n", err)
	}
	for _, sheet := range xlFile.Sheets { //一个文件中的多张表
		//新增的字段数据中可能包含多张表的外健
		var indexs []int    //id字段所在excel表索引数组
		var values []string //id字段相应值(表名)数组
		for i := 1; i < len(sheet.Rows); i++ {
			row := sheet.Rows[i]
			//字段名
			if i == 1 {
				for k, cell := range row.Cells {
					text := cell.String()
					IntId := strings.Index(text, "_id")
					if IntId > 0 {
						indexs = append(indexs, k)
						values = append(values, strings.Split(text, "_")[0])
					}
					sql += text + ","
				}
				c := []byte(sql)
				sql = string(c[:len(c)-1]) + ") value(" //去掉点
				continue
			}
			//插入值
			for k, cell := range row.Cells {
				text := cell.String()
				//对应字段中id字段将相应的值替换成对应id
				//判断该值是否是对应的id字段列
				for j := 0; j < len(indexs); j++ {
					if k == indexs[j] {
						var maps []orm.Params
						var num int64
						//这里乡村涉及到了多张表级联,暂时没有做那么复杂
						//针对两张表的级联完全可以通用
						if values[j] == "village" {
							num, _ = o.Raw("select a.id from `village` a inner join `township` b on a.township_id=b.id where concat(township_name,'-',village_name) =?", text).Values(&maps)
						} else {
							num, _ = o.Raw("select id from `"+values[j]+"` where "+values[j]+"_name=?", text).Values(&maps)
						}
						if num > 0 {
							text = maps[0]["id"].(string)
						} else {
							text = "0"
						}
					}
				}
				sql += "'" + text + "',"
			}
			c := []byte(sql)
			sql = string(c[:len(c)-1]) + "),(" //去掉点
		}
		c := []byte(sql)
		sql = string(c[:len(c)-2]) //去掉点(
	}
	//fmt.Println(sql)
	var num int64
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.GetMapDataError(500,err.Error())
	} else {
		info = lib.MapCreate
	}
	return info
}
