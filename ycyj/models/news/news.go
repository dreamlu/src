package news

import (
	"github.com/astaxie/beego/orm"
	"strconv"
	"ycyj/models/lib"
)

/*新闻或轮播信息*/
type News struct {
	Id            int
	Title         string //标题
	Content       string //内容
	Imageurl      string //轮播封面
	Publish_name  string //发布人
	Publish_num   int    //发布轮播展示序号
	Township_id	  string	 //乡镇id
	Township_name string //乡镇名称
	Village_id    int    //发布来源乡村id
	Village_name  string //发布来源乡村
	Istop         string //默认"不置顶"
	Ispublish     string //默认"已发布"
	Read_num      int    //阅读数量
	Publish_date  string //发布时间
}

//获得置顶的几个新闻
func GetNewsByTop(everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `news`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var news []*News
		var getinfo lib.GetInfo
		//排序规则：按轮播序号升序,发布日期降序(最先发布)
		o.Raw("select *from `news` where istop='置顶' order by id desc limit ?", everyPage).QueryRows(&news)
		//统计页码等状态
		for i := 0; i < len(news); i++ {
			num, _ := o.Raw("select village_name,township_name,b.id as township_id from `village` a inner join `township` b on a.township_id=b.id where a.id=?", news[i].Village_id).Values(&maps)
			if num > 0 {
				news[i].Village_name = maps[0]["village_name"].(string)
				news[i].Township_name = maps[0]["township_name"].(string)
				news[i].Township_id = maps[0]["township_id"].(string)
			}
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = news //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//根据搜索条件获得轮播图展示,分页
func GetNewsBySearch(args map[string][]string) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	sqlnolimit, sql, clientPage, everyPage := lib.SearchTableSqlInclueDate("publish_date", "news", args)

	var maps []orm.Params
	num2, err := o.Raw(sqlnolimit).Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var news []*News
		var getinfo lib.GetInfo
		o.Raw(sql).QueryRows(&news)
		//统计页码等状态
		for i := 0; i < len(news); i++ {
			num, _ := o.Raw("select village_name,township_name,b.id as township_id from `village` a inner join `township` b on a.township_id=b.id where a.id=?", news[i].Village_id).Values(&maps)
			if num > 0 {
				news[i].Village_name = maps[0]["village_name"].(string)
				news[i].Township_name = maps[0]["township_name"].(string)
				news[i].Township_id = maps[0]["township_id"].(string)
			}
		}
		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = news //数据
		getinfo.Pager.SumPage = strconv.FormatInt(num2, 10)
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//根据id获得新闻,分页,类型(草稿/已发送)
func GetNewsById(id int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	var news []*News
	var getinfo lib.GetInfo
	num2, err := o.Raw("select *from `news` where id=?", id).QueryRows(&news)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		//统计页码等状态
		for i := 0; i < len(news); i++ {
			num, _ := o.Raw("select village_name,township_name,b.id as township_id from `village` a inner join `township` b on a.township_id=b.id where a.id=?", news[i].Village_id).Values(&maps)
			if num > 0 {
				news[i].Village_name = maps[0]["village_name"].(string)
				news[i].Township_name = maps[0]["township_name"].(string)
				news[i].Township_id = maps[0]["township_id"].(string)
			}
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = news //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得轮播图展示,排序,展示数量
func GetNewsBySort(everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `news` where publish_num > 0").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var news []*News
		var getinfo lib.GetInfo
		//排序规则：按轮播序号升序,发布日期降序(最先发布)
		o.Raw("select *from `news` where publish_num > 0 order by publish_num,publish_date desc limit ?", everyPage).QueryRows(&news)
		//统计页码等状态
		for i := 0; i < len(news); i++ {
			num, _ := o.Raw("select village_name,township_name,b.id as township_id from `village` a inner join `township` b on a.township_id=b.id where a.id=?", news[i].Village_id).Values(&maps)
			if num > 0 {
				news[i].Village_name = maps[0]["village_name"].(string)
				news[i].Township_name = maps[0]["township_name"].(string)
				news[i].Township_id = maps[0]["township_id"].(string)
			}
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = news //数据

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//获得新闻,分页,类型(草稿/已发送)
func GetNewsByPage(clientPage, everyPage int) interface{} {
	var info interface{}
	o := orm.NewOrm()
	o.Using("default")

	var maps []orm.Params
	num2, err := o.Raw("SELECT count(id) as num FROM `news`").Values(&maps)
	//有数据是返回相应信息
	if err == nil && num2 > 0 {
		var news []*News
		var getinfo lib.GetInfo
		o.Raw("select *from `news` order by id desc limit ?,?", (clientPage-1)*everyPage, everyPage).QueryRows(&news)
		//统计页码等状态
		var SumPage = "0"
		SumPage = maps[0]["num"].(string)
		for i := 0; i < len(news); i++ {
			num, _ := o.Raw("select village_name,township_name,b.id as township_id from `village` a inner join `township` b on a.township_id=b.id where a.id=?", news[i].Village_id).Values(&maps)
			if num > 0 {
				news[i].Village_name = maps[0]["village_name"].(string)
				news[i].Township_name = maps[0]["township_name"].(string)
				news[i].Township_id = maps[0]["township_id"].(string)
			}
		}

		getinfo.Status = 200
		getinfo.Msg = "请求成功"
		getinfo.Data = news //数据
		getinfo.Pager.SumPage = SumPage
		getinfo.Pager.ClientPage = clientPage
		getinfo.Pager.EveryPage = everyPage

		info = getinfo
	} else {
		info = lib.MapError
	}
	return info
}

//删除新闻,根据id
func DeleteNewsByid(id int) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	o := orm.NewOrm()
	res, err := o.Raw("delete from `news` where id=?", id).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapDelete
	}
	return info
}

//修改新闻
func UpdateNews(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	var id string
	sql := "update `news` set "
	for k, v := range args {
		if k == "id" {
			id = v[0]
			continue
		}
		if k == "read_num"{
			sql += k + "=read_num+1,"
			continue
		}
		sql += k + "='" + v[0] + "',"
	}
	c := []byte(sql)
	sql = string(c[:len(c)-1]) //去掉点
	sql += " where id=" + id


	o := orm.NewOrm()
	o.Using("default")

	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapUpdate
	}
	return info
}

//创建新闻
func CreateNews(args map[string][]string) interface{} {
	var info interface{}
	var num int64 //返回影响的行数

	sql := lib.GetInsertSqlById("news", args)
	o := orm.NewOrm()
	o.Using("default")
	res, err := o.Raw(sql).Exec()
	if err == nil {
		num, _ = res.RowsAffected()
	}
	if num == 0 {
		info = lib.MapError
	} else {
		info = lib.MapCreate
	}
	return info
}
